
% Add all required folders to the path.
% We need absolute paths, so changing directories won't break things.

if ~exist('FLAG_LIBFACEMARKUP', 'var') || ~FLAG_LIBFACEMARKUP
  basepath = fileparts(mfilename('fullpath'));
  folders = {
    'classes';
    'demos';
    'external';
    'external/assignmentoptimal';
    'external/export_fig';
    'external/spams-matlab/build';
    'util';
  };
  for i = 1:numel(folders)
    % disp(fullfile(basepath, folders{i}));
    fldi = fullfile(basepath, folders{i});
    if exist(fldi, 'dir')
      addpath(fldi);
      % Silently skip non-existing folders. We may be in a binary.
    end
  end

  FLAG_LIBFACEMARKUP    = true;
  FLD_LIBFACEMARKUP_AUX = fullfile(basepath, 'auxi');
  
  % In mains functions we get the FLD_LIBFACEMARKUP_AUX from env.
  setenv('FLD_LIBFACEMARKUP_AUX', FLD_LIBFACEMARKUP_AUX);
  
  clear basepath folders i;
end
