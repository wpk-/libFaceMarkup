libFaceMarkup
=============

A package of Matlab classes and code to work with surface meshes efficiently.


Install
-------

**1.** Clone the libFaceMarkup folder to a nice location:

```
git clone git@gitlab.com/facer2vm/libFaceMarkup/
```

**2.** Inside Matlab, run `init_libfacemarkup`:

```matlab
cd libFaceMarkup
init_libfacemarkup;
```

**3.** Test everything is working:

```matlab
m = Mesh.load(fullfile(FLD_LIBFACEMARKUP_AUX, 'SymRef.wrl'));
trisurf(m);
```

That should show you a 3D rendered mesh, which you can rotate with the
`rotate3d` command or via the appropriate toolbar buttons.


Classes
-------

Please see the `demos` folder for demonstrations and use cases for all the
classes in this library.


Folders
-------

* `auxi` contains auxiliary files: two example meshes with annotation, a set of
  default landmark definitions, and the great Landmark Encyclopedia.
* `classes` contains all class definitions. The most important ones are
  `@Mesh`, `@LandmarkDefinition`, `@Annotation`. There is also `@AnnotationSet`
  to capture annotations for many files.
* `demos` has some demo code to illustrate the use of the classes. Although the
  classes should be fairly well documented, the demo code enables you to play
  around with them easily.
* `external` holds a collection of Matlab packages developed by others. We
  include them here for ease of access, and because some have bugfixes that
  their respective authors have not yet pulled in.
* `util` contains a set of utility functions used by the class files. They can
  also be useful directly to you.


Licence
-------

Copyright (c) 2016 Paul Koppen, University of Surrey -- Centre for Vision,
Speech and Signal Processing

The code is available under the [MIT licence][mit]. For scientific publications
we ask that you cite the following paper:

> Koppen, WP, Chan, CH, Christmas, WJ and Kittler, J (2012) *An intrinsic
  coordinate system for 3D face registration* In: Proceedings - International
  Conference on Pattern Recognition, 2012-11-11.


[mit]: https://opensource.org/licenses/MIT
