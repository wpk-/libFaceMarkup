function [propertyArray,valueArray] = format_param_list(paramList,nObjects)
%FORMAT_PARAM_LIST   Checks and formats a property/value input list.
%   Private function for use by boundary object methods.

% Author: Ken Eaton
% Last modified: 11/13/08
%--------------------------------------------------------------------------

  % Initializations:

  propertyArray = {};
  valueArray = {};
  if (nargin == 0),
    return;
  elseif (nargin == 1),
    nObjects = 1;
  end
  nList = numel(paramList);

  % Check and format property/value input list:

  switch nList,
    case 0,  % No inputs
      return;
    case 1,  % 1 input
      single_input;
    otherwise,  % 2 or more inputs
      iInput = 0;
      while (iInput < nList),
        iInput = iInput+1;
        switch class(paramList{iInput}),
          case 'char',  % Property/value string pair input
            char_input;
          case 'cell',  % Property/value cell pair input
            cell_input;
          case 'struct',  % Structure input
            struct_input;
          otherwise,  % Invalid input
            error('format_param_list:invalidArguments',...
                  'Invalid property/value pair arguments.');
        end
      end

  end

%~~~Begin nested functions~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  %------------------------------------------------------------------------
  function single_input
  %
  %   Function that handles the case of a single input.
  %
  %------------------------------------------------------------------------

    switch class(paramList{1}),
      case 'char',
        propertyArray = {paramList{1}(:).'};
      case 'struct',
        propertyArray = fieldnames(paramList{1}).';
        if (~isempty(paramList{1})),
          valueArray = struct2cell(paramList{1}(end)).';
        end
      otherwise,
        error('format_param_list:invalidArguments',...
              'Invalid property/value pair arguments.');
    end

  end

  %------------------------------------------------------------------------
  function char_input
  %
  %   Function that handles the case of a property/value string pair input.
  %
  %------------------------------------------------------------------------

    if (iInput == nList),
      error('format_param_list:invalidArguments',...
            'Invalid property/value pair arguments.');
    end
    property = paramList{iInput}(:).';
    iInput = iInput+1;
    value = paramList{iInput};
    index = strcmp(propertyArray,property);
    if any(index),
      valueArray(:,index) = {value};
    else
      propertyArray = [propertyArray {property}];
      valueArray(:,numel(index)+1) = {value};
    end

  end

  %------------------------------------------------------------------------
  function cell_input
  %
  %   Function that handles the case of a property/value cell pair input.
  %
  %------------------------------------------------------------------------

    if (iInput == nList),
      error('format_param_list:invalidArguments',...
            'Invalid property/value pair arguments.');
    end
    propertyCell = row_format(paramList{iInput}(:).');
    iInput = iInput+1;
    valueCell = paramList{iInput};
    if (~iscell(valueCell)),
      error('format_param_list:invalidArguments',...
            'Invalid property/value pair arguments.');
    end
    if (size(valueCell,2) ~= numel(propertyCell)),
      error('format_param_list:badArgumentSize',...
            ['Number of columns in value cell should equal length of ' ...
             'property cell.']);
    end
    [propertyCell,index] = unique(propertyCell);
    valueCell = valueCell(:,index);
    [junk,cellIndex,arrayIndex] = intersect(propertyCell,propertyArray);
    [propertyCell,newIndex] = setdiff(propertyCell,propertyArray);
    arrayIndex = [arrayIndex numel(propertyArray)+(1:numel(propertyCell))];
    propertyArray = [propertyArray propertyCell];
    valueCell = valueCell(:,[cellIndex newIndex]);
    nValues = size(valueArray,1);
    switch size(valueCell,1),
      case 1,
        if (nValues < 2),
          valueArray(arrayIndex) = valueCell;
        else
          valueArray(:,arrayIndex) = repmat(valueCell,nValues,1);
        end
      case nObjects,
        if (nValues == 1),
          valueArray = repmat(valueArray,nObjects,1);
        end
        valueArray(:,arrayIndex) = valueCell;
      otherwise,
        error('format_param_list:badArgumentSize',...
              ['Number of rows in value cell should equal 1 or the ' ...
               'number of objects.']);
    end

  end

  %------------------------------------------------------------------------
  function struct_input
  %
  %   Function that handles the case of a structure input.
  %
  %------------------------------------------------------------------------

    propertyCell = fieldnames(paramList{iInput}).';
    valueCell = struct2cell(paramList{iInput}(end)).';
    if (~(isempty(propertyCell) || isempty(valueCell))),
      [junk,cellIndex,arrayIndex] = intersect(propertyCell,propertyArray);
      [propertyCell,newIndex] = setdiff(propertyCell,propertyArray);
      arrayIndex = [arrayIndex numel(propertyArray)+...
                               (1:numel(propertyCell))];
      propertyArray = [propertyArray propertyCell];
      valueCell = valueCell(:,[cellIndex newIndex]);
      nValues = size(valueArray,1);
      if (nValues < 2),
        valueArray(arrayIndex) = valueCell;
      else
        valueArray(:,arrayIndex) = repmat(valueCell,nValues,1);
      end
    end

  end

%~~~End nested functions~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

end