function cellArray = row_format(cellArray)
%ROW_FORMAT   Reshapes entries within a cell array into row vectors.
%   Private function for use by boundary object methods.

% Author: Ken Eaton
% Last modified: 7/6/08
%--------------------------------------------------------------------------

  if (~iscell(cellArray)),
    error('row_format:badArgumentType',...
          'Input argument should be of type cell.');
  end
  if isempty(cellArray),
    return;
  end
  r = num2cell(ones(size(cellArray)));
  c = num2cell(cellfun('prodofsize',cellArray));
  cellArray = cellfun(@reshape,cellArray,r,c,'UniformOutput',false);

end