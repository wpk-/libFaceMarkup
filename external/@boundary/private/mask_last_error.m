function errorInput = mask_last_error(displayFcn)
%MASK_LAST_ERROR   Masks the function name of the last error message.
%   Private function for use by boundary object methods.

% Author: Ken Eaton
% Last modified: 7/6/07
%--------------------------------------------------------------------------

  errorInput = lasterror;
  if (~isempty(errorInput.message)),
    message = errorInput.message;
    index = regexp(message,{'==>',char(10)});
    if (~isempty(index{1})),
      switch nargin,
        case 0,
          errorInput.message = message((index{2}(1)):end);
        case 1,
          if (~ischar(displayFcn)),
            error('mask_last_error:badArgumentType',...
                  'Input argument should be of type char.');
          end
          errorInput.message = [message(1:(index{1}(1)+3)) ...
                                displayFcn(:).' ...
                                message((index{2}(1)):end)];
      end
    end
  end

end