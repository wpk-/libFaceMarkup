function delete(boundaryObject)
%DELETE   Overloaded delete function for boundary objects.
%   DELETE(OBJ) deletes the graphics for boundary object OBJ. OBJ can be a
%   vector or matrix of boundary objects. If any object in OBJ is not valid
%   (i.e. its graphic object has already been deleted), then DELETE issues
%   an error.
%
%   See also boundary, boundary/ishandle.

% Author: Ken Eaton
% Last modified: 7/8/08
%--------------------------------------------------------------------------

  if (~all(ishandle(boundaryObject(:)))),
    error('boundary:delete:invalidObject','Invalid boundary object.');
  end
  nObjects = numel(boundaryObject);
  switch nObjects,
    case 0,
      return;
    case 1,
      boundaryObject.handler('delete');
    otherwise,
      for iObject = 1:nObjects,
        boundaryObject(iObject).handler('delete');
      end
  end

end