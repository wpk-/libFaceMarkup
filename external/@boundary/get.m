function varargout = get(boundaryObject,propertyArray)
%GET   Overloaded get command for boundary objects.
%   V = GET(OBJ,'PropertyName') returns the value of the specified property
%   for the boundary object OBJ. If OBJ is a vector, then GET will return
%   an M-by-1 cell array of values where M is equal to length(OBJ). If
%   'PropertyName' is replaced by a 1-by-N or N-by-1 cell array of strings
%   containing property names, then GET will return an M-by-N cell array of
%   values. If any object in OBJ is not valid (i.e. its graphic object has
%   been deleted), then GET issues an error.
%
%   GET(OBJ) displays all property names and their current values for the
%   boundary object OBJ.
%
%   V = GET(OBJ) where OBJ is a scalar boundary object, returns a structure
%   where each field name is the name of a property of OBJ and each field
%   contains the value of that property.
%
%   See also boundary, boundary/set.

% Author: Ken Eaton
% Last modified: 7/8/08
%--------------------------------------------------------------------------

  % Initializations:

  parameters = struct('propertyArray',{{}});
  returnOutput = (nargout > 0);
  cellOutput = true;
  nObjects = numel(boundaryObject);

  % Check inputs:

  if (~all(ishandle(boundaryObject(:)))),
    error('boundary:get:invalidObject','Invalid boundary object.');
  end
  if (nargin < 2),
    propertyArray = {};
  else
    propertyArray = propertyArray(:);
    switch class(propertyArray),
      case 'char',
        propertyArray = {propertyArray.'};
        cellOutput = false;
      case 'cell',
        if (~all(cellfun('isclass',propertyArray,'char'))),
          error('boundary:get:badArgumentType',...
                ['Entries of property name cell array argument should ' ...
                 'be of type char.']);
        end
        propertyArray = row_format(propertyArray);
      otherwise,
        error('boundary:get:badArgumentType',...
              'Property name argument should be of type char or cell.');
    end
  end
  nProperties = numel(propertyArray);
  if ((nProperties == 0) && (~returnOutput) && (nObjects > 1)),
    error('boundary:get:badArgumentSize',...
          ['Boundary object argument should be a scalar when not ' ...
           'returning information.']);
  end

  % Get boundary object properties:

  try
    parameters.propertyArray = propertyArray;
    if (nProperties > 0),
      if ((nObjects*nProperties) > 1),
        varargout = cell(nObjects,nProperties);
        for iObject = 1:nObjects,
          varargout(iObject,:) = ...
            boundaryObject(iObject).handler('get',parameters);
        end
        varargout = {varargout};
      elseif cellOutput,
        varargout = {boundaryObject.handler('get',parameters)};
      else
        varargout = boundaryObject.handler('get',parameters);
      end
    elseif returnOutput,
      varargout = cell(nObjects,1);
      for iObject = 1:nObjects,
        varargout{iObject} = ...
          boundaryObject(iObject).handler('get',parameters);
      end
      varargout = {[varargout{:}].'};
    else
      boundaryObject.handler('get',parameters);
      varargout = {};
    end
  catch
    rethrow(mask_last_error('boundary.get'));
  end

end