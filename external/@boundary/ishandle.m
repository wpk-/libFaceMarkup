function validArray = ishandle(boundaryObject)
%ISHANDLE   Determine if a boundary object has a valid handle.
%   ISHANDLE(OBJ) returns an array that contains 1's where the elements
%   of H are boundary objects with valid graphics handles (i.e. they
%   haven't been deleted) and 0's where they are not.
%
%   See also boundary, boundary/delete.

% Author: Ken Eaton
% Last modified: 7/8/08
%--------------------------------------------------------------------------

  objectSize = size(boundaryObject);
  nObjects = prod(objectSize);
  switch nObjects,
    case 0,
      validArray = [];
    case 1,
      validArray = boundaryObject.handler('ishandle');
    otherwise,
      validArray = false(objectSize);
      for iObject = 1:nObjects,
        validArray(iObject) = boundaryObject(iObject).handler('ishandle');
      end
  end

end