function varargout = set(boundaryObject,varargin)
%SET   Overloaded set command for boundary objects.
%   SET(OBJ,'PropertyName',PropertyValue) sets the value of the specified
%   property for the boundary object OBJ. OBJ can be a vector of boundary
%   objects, in which case SET sets the properties' values for all the
%   objects. If any object in OBJ is not valid (i.e. its graphic object has
%   been deleted), then SET issues an error.
%
%   SET(OBJ,a) where a is a structure whose field names are object property
%   names, sets the properties named in each field name with the values
%   contained in the structure.
%
%   SET(OBJ,pn,pv) sets the named properties specified in the cell array of
%   strings pn to the corresponding values in the cell array pv for all
%   objects specified in OBJ. The cell array pn must be 1-by-N, but the
%   cell array pv can be M-by-N where M is equal to length(OBJ) so that
%   each object will be updated with a different set of values for the list
%   of property names contained in pn.
%
%   SET(OBJ,'PropertyName',PropertyValue,'PropertyName',PropertyValue,...)
%   sets multiple property values with a single statement. Note that it is
%   permissible to use property/value string pairs, structures, and
%   property/value cell array pairs in the same call to SET.
%
%   A = SET(OBJ,'PropertyName')
%   SET(OBJ,'PropertyName')
%   returns or displays the possible values for the specified property of
%   the scalar boundary object OBJ. The returned array is a cell array of
%   possible value strings or an empty cell array if the property does not
%   have a finite set of possible string values.
%
%   A = SET(OBJ)
%   SET(OBJ)
%   returns or displays all property names and their possible values for
%   the scalar boundary object OBJ. The return value is a structure whose
%   field names are the property names of OBJ, and whose values are cell
%   arrays of possible property values or empty cell arrays.
%
%   See also boundary, boundary/get.

% Author: Ken Eaton
% Last modified: 7/8/08
%--------------------------------------------------------------------------

  % Initializations:

  parameters = struct('propertyArray',{{}},'valueArray',{{}});
  propertyArray = {};
  valueArray = {};
  returnOutput = false;
  nObjects = numel(boundaryObject);

  % Parse argument list:

  if (~all(ishandle(boundaryObject(:)))),
    error('boundary:set:invalidObject','Invalid boundary object.');
  end
  if (nargin == 1),
    if (nObjects > 1),
      error('boundary:set:badArgumentSize',...
            ['Boundary object argument should be a scalar when it is ' ...
             'the only argument.']);
    end
    returnOutput = (nargout > 0);
  else
    try
      [propertyArray,valueArray] = format_param_list(varargin,nObjects);
    catch
      rethrow(mask_last_error('boundary.set'));
    end
    if (nargin == 2),
      if ischar(varargin{1}),
        if (nObjects > 1),
          error('boundary:set:badArgumentSize',...
                ['Boundary object argument should be a scalar when ' ...
                 'returning information.']);
        end
        returnOutput = (nargout > 0);
      elseif isstruct(varargin{1}),
        if (isempty(propertyArray) || isempty(valueArray)),
          return;
        end
      end
    end
  end

  % Set boundary object properties:

  try
    parameters.propertyArray = propertyArray;
    if returnOutput,
      varargout = {boundaryObject.handler('set',parameters)};
    else
      nValues = size(valueArray,1);
      if (nValues > 0),
        parameters.valueArray = valueArray(1,:);
      end
      for iObject = 1:nObjects,
        if (nValues > 1),
          parameters.valueArray = valueArray(iObject,:);
        end
        boundaryObject(iObject).handler('set',parameters);
      end
      varargout = {};
    end
  catch
    rethrow(mask_last_error('boundary.set'));
  end

end