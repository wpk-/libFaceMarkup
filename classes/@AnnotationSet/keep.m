function ix = keep(as, ix)
% Keep annotations of the selected files and discard the rest.
%
% Input arguments:
%  AS  AnnotationSet instance.
%  IX  Nx1 logical array or Kx1 index array marking the rows in
%      `AS.coordinates` (which is NxMxD) to be kept.
%      Alternatively, IX can be a Nx1 cell array of strings, whose values
%      should match `AS.names`. In this case the order of AS is changed to
%      match IX. Be careful though, because names that do not match (either in
%      `AS.names` or in IX), are ignored/removed. Also note that IX cannot
%      contain repetitions.
%
% Output arguments:
%  IX  Kx1 index array of the exact order of items kept. This serves both as a
%      confirmation, in case you were uncertain if the order of IX input
%      argument was maintained (which it is), and as information in case the
%      input argument IX was a cell array of strings.
%
% Considerations:
%  Note that AS is changed by reference.
%
  if iscellstr(ix)
    [ism,ii] = ismember(ix, as.names);
    if ~all(ism)
      warning('Some items to keep (IX) are not present in the AnnotationSet.');
    end
    ix = ii(ism);
  end

  as.numfiles = nnz(ix);
  as.coordinates = as.coordinates(ix,:,:);
  as.names = as.names(ix);
end
