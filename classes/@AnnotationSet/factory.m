function ann = factory(lnddef, lidx, pts, names)
% Construct an annotation set from partial data.
%
% Input arguments:
%  LNDDEF  LandmarkDefinition instance, with `numel(LNDDEF.lxmap) == M`.
%  LIDX    Kx1 array with landmark indices, with `K <= M` and `max(K) <= M`.
%  PTS     NxKxD matrix defining K points in D dimensions.
%  NAMES   Nx1 cell array of strings providing unique names for the
%          annotations.
%
% Output arguments:
%  AS  AnnotationSet instance.
%
% Considerations:
%  - The key difference to using the AnnotationSet constructor is that here PTS
%    is NxKxD instead of NxMxD. So, use the factory if you only have a matrix
%    with the defined points (partial data), rather than a complete matrix.
%
  [N,K,D] = size(pts);
  maxindex = numel(lnddef.lxmap);
  maxlidx = max([lidx(:); 0]);  % allow lidx to be empty.

  assert(K == numel(lidx), 'Number of indices does not match number of points.');
  assert(maxindex >= maxlidx, 'Indices out of range for landmark definition.');

  coords = nan(N, maxindex, D);
  coords(:,lidx,:) = pts;

  ann = AnnotationSet(lnddef, coords, names);
end
