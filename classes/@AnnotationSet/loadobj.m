function as = loadobj(s)
% Restore AnnotationSet instance from saved struct.
%
% Input arguments:
%  S  Struct with fields containing all important data to reconstruct AS
%     (see `saveobj`).
%
% Output arguments:
%  AS  AnnotationSet instance.
%
  if isstruct(s)
    if isempty(s.lnddef) && isempty(s.pts) && isempty(s.names)
      as = AnnotationSet;
    else
      as = AnnotationSet.factory(s.lnddef, s.indices, s.pts, s.names);
    end
  else
    as = s;
  end
end
