function err = mle(ann, truth)
% Compute the mean landmark error against a ground truth annotation set.
%
% Input arguments:
%  AS     AnnotationSet instance.
%  TRUTH  AnnotationSet instance.
%
% Output arguments:
%  ERR  Array providing the mean landmark annotation error per file in AS.
%       Where `AS.names{i}` is not found in TRUTH, `ERR(i)` is `nan`.
%
% Considerations:
%  - You are responsible for checking that all necessary landmark points are
%    defined in AS, because NaN values are ignored.
%
  err = nan(ann.numfiles, 1);

  truthy = AnnotationSet.copy(truth);
  truthy.convertto(ann.lnddef);

  [~,ix] = ismember(ann.names, truthy.names);
  ix1 = ix > 0;

  delta = ann.coordinates(ix1,:,:) - truthy.coordinates(ix(ix1),:,:);
  err(ix1) = nanmean(sqrt(sum(delta .^ 2, 3)), 2);
end
