function s = load_mat(filename, lnddef, format)
% Load multiple annotations from a .mat file.
%
% Input arguments:
%  FILENAME  String specifying the .mat file name.
%  LNDDEF    Default landmark definition, in case none is defined in the file.
%  FORMAT    String specifying the field name to read coordinate data from. In
%            the output this will be stored under `S.pts`.
%            For example, ASM stores the best landmark estimate in `'bst'`. By
%            passing `'bst'` as argument to FORMAT the returned `S.pts` is a
%            copy of the `bst` data.
%            Default value is `'pts'`.
%
% Output arguments:
%  S  Struct with the following fields:
%   .names    Nx1 cell array of strings with unique file base names.
%   .pts      NxKxD matrix of annotation data. K<=M.
%   .lnddef   LandmarkDefinition instance defining the indices 1..M.
%   and optionally
%   .indices  Kx1 array marking the original columns (1 <= indices <= M)
%             for the K columns in `.pts`. If not specified, K == M.
%
  if nargin<3 || isempty(format), format='pts'; end

  % The .mat file stores a struct,
  % with fields `.names` and `.pts` and optionally `.lnddef` and/or `.indices`.

  data = load(filename);

  s.names = data.names;
  s.pts = data.(format);

  if isfield(data, 'lnddef')
    s.lnddef = data.lnddef;
  else
    s.lnddef = lnddef;
  end

  if isfield(data, 'indices')
    s.indices = data.indices;
  end
end
