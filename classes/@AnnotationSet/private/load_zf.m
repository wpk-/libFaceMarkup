function s = load_zf(filename, lnddef, var_name, pts_field, ndim)
% Load multiple annotations from a .mat file.
%
% Input arguments:
%  FILENAME   String specifying the .mat file name.
%  LNDDEF     Default landmark definition, in case none is defined in the file.
%  VAR_NAME   Variable name in the .mat file that stores the data struct.
%  PTS_FIELD  Name of the struct field name that stores the coordinates.
%             Default value is 'pts'.
%  NDIM       Dimensionality of the annotations (2 for 2D, 3 for 3D
%             annotations). Default value is 2, but reshaping is only done if
%             `size(data.(pts_field),2) == 1`.
%
% Output arguments:
%  S  Struct with the following fields:
%   .names    Nx1 cell array of strings with unique file base names.
%   .pts      NxKxD matrix of annotation data. K<=M.
%   .lnddef   LandmarkDefinition instance defining the indices 1..M.
%   and optionally
%   .indices  Kx1 array marking the original columns (1 <= indices <= M)
%             for the K columns in `.pts`. If not specified, K == M.
%
  if nargin<3 || isempty(pts_field), pts_field='pts'; end
  if nargin<4 || isempty(ndim), ndim=2; end

  % The .mat file stores a struct,
  % with fields `.names` and `.pts` and optionally `.lnddef` and/or `.indices`.

  data = load(filename);
  data = data.(var_name);

  [~,s.names] = cellfun(@fileparts, {data.name}', 'UniformOutput',0);
  
  s.pts = cat(3, data.(pts_field));
  [~,D,N] = size(s.pts);
  
  if D == 1
    s.pts = reshape(s.pts, [], ndim, N);
  end
  
  % make N x M x D.
  s.pts = permute(s.pts, [3 1 2]);

  if isfield(data, 'lnddef')
    s.lnddef = data.lnddef;
  else
    s.lnddef = lnddef;
  end

  if isfield(data, 'indices')
    s.indices = data.indices;
  end
end
