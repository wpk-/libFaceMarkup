function s = load_files(filename, lnddef, format)
% Load all annotation files from a given folder.
%
% Input arguments:
%  FILENAME  String holding the file pattern to be matched. (e.g. `path/*.lnd`)
%  LNDDEF    Default landmark definition, in case none is defined in the file.
%  FORMAT    File format, if it cannot/shouldn't be derived from file extension.
%
% Output arguments:
%  S  Struct with the following fields:
%   .names    Nx1 cell array of strings with unique file base names.
%   .pts      NxMxD matrix of annotation data.
%   .lnddef   LandmarkDefinition instance defining the indices 1..M.
%
  if nargin<3, format=[]; end
  if nargin<2, lnddef=[]; end

  if isempty(format)
    files = listfiles(filename, '*.lnd');
  else
    files = listfiles(filename, ['*.' format]);
  end
  [~,names] = cellfun(@fileparts, files, 'UniformOutput',0);

  nfiles = numel(files);
  if nfiles == 0
    s.names = names;
    s.lnddef = lnddef;
    s.pts = nan(numel(names), numel(lnddef.lxmap), 3);
    return;
  end

  annos(nfiles,1) = Annotation;

  parfor i = 1:nfiles
    %if ~mod(i,100), fprintf('%7d\n',i); end
    annos(i) = Annotation.load(files{i}, lnddef, format);
    % Overwriting `names{i}` with `annos(i).name` may give undesired results
    % when saving the whole set at once (file names may not match any longer).
  end

  ldef = annos(1).lnddef;
  assert(all(cellfun(@(ld)isequal(ldef,ld), {annos.lnddef})), ...
        'Annotations use different landmark definitions.');

  %npts = numel(ldef.lxmap);
  ndim = annos(1).dim;
  assert(all(ndim == [annos.dim]), 'Annotations span different spaces.');

  pts = cat(3, annos.coordinates);
  pts = permute(pts, [3 1 2]);

  s.names = names;
  s.lnddef = ldef;
  s.pts = pts;
end
