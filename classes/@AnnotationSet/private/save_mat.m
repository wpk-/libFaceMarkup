function save_mat(filename, s, ~) %#ok<INUSD>
% Save each annotation to its own file.
%
% Input arguments:
%  FILENAME  String with a pattern for the output files. For example:
%            `'path/to/*.ext'` to save files in the folder `'path/to'` and
%            using `[names{i} '.ext']` for file name. If FILENAME is just a
%            folder, without the pattern, the default file extension .lnd is
%            used.
%  S         Struct as generated by `saveobj`. It has the following fields:
%   .names    Nx1 cell array of strings with unique file base names.
%   .pts      NxKxD matrix of annotation data. K<=M.
%   .indices  Kx1 array marking the original columns (1 <= indices <= M) for
%             the K columns in `.pts`.
%   .lnddef   LandmarkDefinition instance defining the indices 1..M.
%  FORMAT    Not used.
%
  save(filename, '-struct','s');
end
