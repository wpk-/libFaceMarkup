classdef AnnotationSet < handle
%ANNOTATIONSET Summary of this class goes here
%   Detailed explanation goes here

  properties
    coordinates;  % NxMxD array of landmark positions.
    names;        % Nx1 cell array of strings.
  end

  properties (SetAccess = protected)
    lnddef;       % LandmarkDefinition instance with numel(lnddef.lxmap) == M.
    dim;          % D = Dimensionality of the coordinates.
    maxindex;     % M = Number of points defined in the landmark definition.
    numfiles;     % N = Number of files in the set.
  end

  properties (Dependent)
    indices;      % Indices of the coordinates that have proper values.
    iscomplete;   % Have all landmark positions been defined?
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Class constructor.

  methods
    function as = AnnotationSet(lnddef, coordinates, names)
      % Class constructor for landmark data on a set of files.
      %
      % Input arguments:
      %  LNDDEF       LandmarkDefinition instance.
      %  COORDINATES  NxMx3 dense matrix.
      %  NAMES        Nx1 cell array of strings with unique names for all
      %               anntoations.
      %
      % Output arguments:
      %  AS  AnnotationSet instance.
      %
      if nargin<1, return; end          % useful for parfor loops and the like.
      if nargin<2, coordinates=[]; end
      if nargin<3, names=[]; end

      maxidx = numel(lnddef.lxmap);
      [N,M,D] = size(coordinates);

      if M == 0
        M = maxidx;
      end
      if D < 2
        % Edge case where no data is available during contruction.
        % Default to 3 dimensions. If it's a bad guess we're screwed I'm afraid.
        D = 3;
      end

      assert(M == maxidx, ...
            'Coordinates matrix does not match landmark definition.');

      if isempty(coordinates)
        coordinates = nan(N, M, D);
      end
      if isempty(names)
        names = cell(N, 1);
      else
        assert(numel(names) == N, 'Names do not match data.');
      end

      as.lnddef      = lnddef;
      as.dim         = D;
      as.maxindex    = M;
      as.numfiles    = N;
      as.coordinates = coordinates;
      as.names       = names;
    end
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ IO & object copying.

  methods (Static)
    % Return a copy of the annotation set.
    as = copy(orig);
    % Construct an annotation set from partial data.
    as = factory(lnddef, lidx, pts, names);
    % Load annotation data from a .mat file, or annotation files in a folder.
    as = load(filename, lnddef, format, varargin);
    % Restore Annotation instance from saved struct.
    as = loadobj(s);
  end

  methods
    % Write landmark positions to file.
    saveas(ann, filename, format);
    % Save data to struct.
    s = saveobj(as);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Visualisation methods.

% % % % %   methods
% % % % %     % Scatter plot the annotation points.
% % % % %     h = scatter3(ann, varargin)
% % % % %   end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Methods that alter data.

  methods
    % Convert all annotations (by reference) to a new landmark numbering system.
    convertto(as, lnddef2);
    % Copy landmark positions from another, aligned, annotation set.
    copyfrom(as, as2);
% % % % %     % Set the positions of one or more specified landmarks.
% % % % %     define(ann, lidx, pts);
    % Keep annotations of the selected files and discard the rest.
    ix = keep(as, ix);
    % Change the order of the files.
    orderby(as, x, mode);
% % % % %     % Undefine all stored landmark coordinates.
% % % % %     reset(ann);
% % % % %     % Move the annotation points to the nearest points on `mesh`.
% % % % %     snaptosurface(ann, mesh);
% % % % %     % Remove one or more points from the annotation.
% % % % %     undefine(ann, lidx);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Methods that return information.

  methods
    % Extract one annotation from the set.
    ann = annotation(as, index);
% % % % %     % Return a copy of the annotation in another LandmarkDefinition format.
% % % % %     ann2 = as(ann, lnddef2);
    % Compute the maximum landmark error against a ground truth annotation set.
    err = maxle(ann, truth);
% % % % %     % Subtract another annotation from this, return as new annotation.
% % % % %     ann3 = minus(ann, ann2);
    % Compute the mean landmark error against a ground truth annotation set.
    err = mle(ann, truth);
% % % % %     % Determine the rigid transformation that best fits the points in Y to X.
% % % % %     [d,Z,rt,lidx] = procrustes(X, Y, varargin);
% % % % %     % Rotate and translate (rigidly transform) the annotation by coordsys.
% % % % %     ann2 = transform(ann, coordsys);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Getters and setters.

  methods
    function set.coordinates(as, coords)
      % Overwrite coordinates.
      % As long as the size doesn't change, I'm happy.
      %---
      % By the way, using [maxindex,dim] means we can change those values and
      % then change coordinates. Had we used size(coordinates) this would have
      % been impossible. Use with the utmost care though!
      should_size = [as.numfiles, as.maxindex, as.dim]; %#ok<MCSUP>
      actual_size = size(coords);
      assert(numel(actual_size) == numel(should_size) && ...
            all(actual_size == should_size));
      as.coordinates = coords;
    end
    function ix = get.indices(as)
      % Return the indices of the coordinates that some have proper values for.
      ix = find(~all(all(isnan(as.coordinates), 1), 3));
    end
    function set.indices(as, ix)
      % Basically undefine (NaN) any of the coordinates /not/ in ix.
      m = true(as.maxindex, 1);
      m(ix) = false;
      as.coordinates(:,m,:) = nan;
    end
    function tf = get.iscomplete(as)
      % Have all landmark positions been defined?
      tf = ~any(isnan(as.coordinates(:)));
    end
  end
end
