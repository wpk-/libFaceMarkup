function copyfrom(as, as2)
% Copy landmark positions from another, aligned, annotation set.
%
% Input arguments:
%  AS    AnnotationSet instance.
%  AS2   AnnotationSet instance providing new coordinates.
%
  assert(isa(as2, 'AnnotationSet'));
  assert(as.dim == as2.dim);
  assert(all(strcmp(as.names, as2.names)));
  
  if ~isequal(as.lnddef, as2.lnddef)
    as2 = AnnotationSet.copy(as2);
    as2.convertto(as.lnddef);
  end
  
  mask = ~isnan(as2.coordinates);       % only copy defined points.
  
  as.coordinates(mask) = as2.coordinates(mask);
end
