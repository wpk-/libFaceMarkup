function ann = annotation(as, index)
% Extract one annotation from the set.
%
% Input arguments:
%  AS     AnnotationSet instance.
%  INDEX  Scalar specifying the annotation to be extracted.
%         `1 <= INDEX <= AS.numfiles`.
%
% Output arguments:
%  ANN  Annotation instance for the requested annotation.
%
  coords = permute(as.coordinates(index,:,:), [2 3 1]);
  npts = as.maxindex;

  ann = Annotation(1:npts, coords, as.lnddef);
  ann.name = as.names{index};
end
