function as = copy(orig)
% Return a copy of the annotation set.
%
% Input arguments:
%  ORIG  AnnotationSet instance.
%
% Output arguments:
%  AS  Another AnnotationSet instance with the same properties as the input.
%
  as = AnnotationSet(orig.lnddef, orig.coordinates, orig.names);
end
