function orderby(as, x, mode)
% Change the order of the files.
%
% Input arguments:
%  AS    AnnotationSet instance with N files.
%  X     Nx1 array assigning a number to each file. The files will be sorted
%        according to their associated value in X, in ascending order.
%  MODE  Optional string to specify the sorting order. `'descend'` indicates
%        descending order.
%        Default is `'ascend'`.
%
  if nargin<3 || isempty(mode), mode='ascend'; end

  assert(numel(x) == as.numfiles, ...
        'Sorting values do not match number of files.');

  [~,ix] = sort(x, mode);

  as.names = as.names(ix);
  as.coordinates = as.coordinates(ix,:,:);
end
