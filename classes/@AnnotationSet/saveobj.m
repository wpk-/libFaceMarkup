function s = saveobj(as)
% Save data to struct.
%
% Input arguments:
%  AS  AnnotationSet instance.
%
% Output arguments:
%  S  Struct with all data necessary to reconstruct AS.
%
  s.lnddef = as.lnddef;
  s.indices = as.indices;
  s.pts = as.coordinates(:,s.indices,:);
  s.names = as.names;
end
