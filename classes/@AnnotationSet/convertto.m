function convertto(as, lnddef2)
% Convert all annotations (by reference) to a new landmark numbering system.
%
% Input arguments:
%  AS       AnnotationSet instance.
%  LNDDEF2  LandmarkDefinition instance of the new landmark numbering system.
%
% Notes:
%  - Points defined in `AS.lnddef` but not in `LNDDEF2` will be lost.
%  - Points defined in `LNDDEF2` but not in `AS.lnddef` will NaN.
%
  lndsrc = as.lnddef;
  if isequal(lndsrc, lnddef2)
    return;
  end

  % Convert indices.
  
  [ixfrom,ixto] = LandmarkDefinition.conversion(lndsrc, lnddef2);
  maxixto = numel(lnddef2.lxmap);

  % Convert annotation.

  newcoords = nan(as.numfiles, maxixto, as.dim);
  newcoords(:,ixto,:) = as.coordinates(:,ixfrom,:);

  as.lnddef      = lnddef2;
  as.maxindex    = maxixto;
  as.coordinates = newcoords;
  % as.indices     = ixto; % not needed. newcoords has nans already.
end
