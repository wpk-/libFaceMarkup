function as = load(filename, lnddef, format, varargin)
% Load annotation data from a .mat file, or annotation files in a folder.
%
% Input arguments:
%  FILENAME  String with the .mat file name, or path/pattern to match
%            annotation files. By default, if a folder name is specified, files
%            with the .lnd extension will be loaded. Add `'*.ext'` to the path
%            to load files with the .ext extionsion.
%  LNDDEF    LandmarkDefinition instance providing interpretation of the
%            read landmark indices. If the annotation is stored under a
%            different landmark definition, it is converted to LNDDEF
%            before being returned.
%  FORMAT    Optional string specifying the annotation file format.
%            Default value is the right side of FILENAME starting after the
%            first period (".") in lower case.
%
% Output arguments:
%  AS  AnnotationSet instance. Always uses landmark definition LNDDEF.
%
  if nargin<2, lnddef=[]; end
  if nargin<3, format=[]; end
  
  if ischar(lnddef)
    lnddef = LandmarkDefinition.load(lnddef);
  end
    
  if iscellstr(filename)
    loadfcn = @load_files;
  else
    [~,~,ext] = fileparts(filename);
    
    if isempty(format)
      format = ext;
    end

    switch lower(format)
      case {'.mat', 'mat'}
        loadfcn = @load_mat;
      case {'.zf', 'zf'}
        loadfcn = @load_zf;
      otherwise
        loadfcn = @load_files;
    end
  end

  s = loadfcn(filename, lnddef, varargin{:});
  
  if ~isfield(s, 'lnddef') || ~isa(s.lnddef, 'LandmarkDefinition')
    error('Annotation:NoLandmarkDefinition', ['The file does not specify ' ...
          'a LandmarkDefinition, so it must be supplied.']);
  end

  if isfield(s, 'indices')
    as = AnnotationSet.factory(s.lnddef, s.indices, s.pts, s.names);
  else
    as = AnnotationSet(s.lnddef, s.pts, s.names);
  end

  if isa(lnddef, 'LandmarkDefinition') && ~isequal(as.lnddef, lnddef)
    % Always return with the requested landmark definition.
    as.convertto(lnddef);
  end
end
