function saveas(as, filename, format)
% Write landmark positions to file.
%
% Input arguments:
%  ANN       Annotation instance.
%  FILENAME  String with the file name of the annotation data.
%  FORMAT    Optional string specifying the annotation file format.
%            Default value is the right side of FILENAME starting after the
%            first period (".") in lower case.
%
  if nargin<3, format=[]; end

  [~,~,ext] = fileparts(filename);
  switch lower(ext)
    case '.mat'
      savefcn = @save_mat;
    otherwise
      savefcn = @save_files;
  end

  savefcn(filename, saveobj(as), format);
end
