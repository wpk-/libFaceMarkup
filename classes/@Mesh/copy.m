function tri = copy(tri0)
% Copy a Mesh or struct.
%
% Input arguments:
%  TRI0  Mesh instance or a struct with the appropriate fields.
%
% Output arguments:
%  TRI  Mesh instance with identical values to TRI0.
%

  % 1. Copy all necessary properties.

  tri = Mesh.loadobj(saveobj(tri0));

  % 2. Copy all computed properties.

  if tri0.has_barycenters_defined
    tri.A = tri0.A;
    tri.V0 = tri0.V0;
    tri.V1 = tri0.V1;
    tri.has_barycenters_defined = tri0.has_barycenters_defined;
  end

  if tri0.has_halfedges_defined
    tri.f2he = tri0.f2he;
    tri.halfedges = tri0.halfedges;
    tri.v2he = tri0.v2he;
    tri.has_halfedges_defined = tri0.has_halfedges_defined;
  end

  if ~isempty(tri0.nb1_)
    tri.nb1_ = tri0.nb1_;
  end

  if ~isempty(tri0.nb10_)
    tri.nb10_ = tri0.nb10_;
  end

  if tri0.has_normals_defined
    tri.facenormals = tri0.facenormals;
    tri.vertexnormals = tri0.vertexnormals;
    tri.has_normals_defined = tri0.has_normals_defined;
  end

  if tri0.has_parameters_defined
    % `default_neighbourhood_order` was already defined in `loadobj`.
    tri.parameters = tri0.parameters;
    tri.has_parameters_defined = tri0.has_parameters_defined;
  end

  % 3. Compatibility with old, non-class, mesh objects.

  if isfield(tri0, 'wrlfile')
    tri.sourcefile = tri0.wrlfile;
  end
end
