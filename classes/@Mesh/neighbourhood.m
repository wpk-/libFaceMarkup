function nb = neighbourhood(tri, nth_order)
% Construct a sparse vertex neighbourhood matrix.
%
% Input arguments:
%  TRI        A Mesh instance.
%  NTH_ORDER  Scalar specifying the neighbourhood order. For example a value
%             of 1 finds all direct neighbours, while 2 finds all direct
%             neighbours and their neighbours.
%
% Output arguments:
%  NB  NxN logical sparse matrix. Element NB(i,j) is true if vertex i is in the
%      NTH_ORDER neighbourhood of vertex j. The matrix is symmetric, and
%      NB(i,i) is always true (a vertex is always in its own neighbourhood).
%
  if isempty(tri.nb1_)
    tri.nb1_ = neighbourhood(tri.halfedges, 1);
  end
  switch nth_order
    case 1
      nb = tri.nb1_;
    case 10
      if isempty(tri.nb10_)
        tri.nb10_ = logical(tri.nb1_ ^ 10);
      end
      nb = tri.nb10_;
    otherwise
      nb = logical(tri.nb1_ ^ nth_order);
  end
end
