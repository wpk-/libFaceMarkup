function tri = loadobj(s)
% Restore the Mesh from struct.
%
% Input arguments:
%  S  Struct with the necessary fields to reconstruct TRI, excluding any
%     dependent properties.
%
% Output arguments:
%  TRI  A Mesh instance.
%
  if isstruct(s)
    tri = Mesh;

    flds = fieldnames(s);
    for i = 1:numel(flds)
      f = flds{i};
      tri.(f) = s.(f);
    end
  else
    tri = s;
  end
end
