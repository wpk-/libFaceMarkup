function A = areas(tri)
% Calculate surface areas of all faces.
%
% Input arguments:
%  TRI  A Mesh instance.
%
% Output arguments:
%  A  Mx1 array of triangle areas of each of the M faces in TRI.
%
% Considerations:
%  - http://en.wikipedia.org/wiki/Heron%27s_formula
%  - http://http.cs.berkeley.edu/~wkahan/Triangle.pdf
%  - and comment from Andres Toennesmann on:
%    http://www.mathworks.com/matlabcentral/fileexchange/16448
%
% XXX FIXME this function only works on triangles.
% XXX TODO also, is there good use for this function?
%
	vertices = tri.vertices;
	faces    = tri.faces;

	% Reshape the vertices to DxMxK, where
	% D=3 is the dimension of a vertex,
	% M   is the number of triangles in TRI,
	% K=3 is the number of points in a triangle.

	D     = size(vertices, 2);
	[M,K] = size(faces);

	if K ~= 3
		error('Mesh:InvalidArgument', ...
					'Triangles have 3 points. You didn''t pass triangles.');
	end

	P = reshape(vertices(faces,:)', D, M, K);

	% Calculate all edge lengths at once.

	L = [
		sqrt(sum((P(:,:,1)-P(:,:,2)) .^ 2));
		sqrt(sum((P(:,:,2)-P(:,:,3)) .^ 2));
		sqrt(sum((P(:,:,3)-P(:,:,1)) .^ 2))
	];
	L = sort(L);

	% Stabilized Heron's formula to calculate the triangle areas.

	A = sqrt( ...
		(L(3,:) + (L(2,:)+L(1,:)))   .*   (L(1,:) - (L(3,:)-L(2,:)))   .* ...
		(L(1,:) + (L(3,:)-L(2,:)))   .*   (L(3,:) + (L(2,:)-L(1,:)))      ...
	) / 4;

	A = A(:);
end
