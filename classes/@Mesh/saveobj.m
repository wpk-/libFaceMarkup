function s = saveobj(tri)
% Provide a minimal object to be stored, ignoring all derived properties.
%
% Input arguments:
%  TRI  A Mesh instance.
%
% Output arguments:
%  S  Struct with the necessary fields to reconstruct TRI, excluding any
%     derived properties.
%
  copy_properties = {...
    'name', ...
    'sourcefile', 'texturefile', ...
    'vertices', 'faces', ...
    'vertexcolor', 'facecolor', ...
    'texturecoords', 'textureindices', ...
    'default_neighbourhood_order'
  };

  for i = 1:numel(copy_properties)
    prop = copy_properties{i};
    s.(prop) = tri.(prop);
  end
end
