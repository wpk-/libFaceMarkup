function save_tri(tri, filename)
% Save the mesh in MorphModel (*.tri) file format.
%
% Input arguments:
%  TRI       Mesh instance.
%  FILENAME  String of the file name under which to save the mesh.
%
% Considerations:
%  - This file format does not support texture mapping. Only vertices and
%    faces are stored.
%

  % Read the file.

  fid = fopen(filename, 'w');
  
  fprintf(fid, '%d %d\n', tri.numvertices, tri.numfaces);
  fprintf(fid, '%.6g %.6g %.6g\n', tri.vertices.');
  fprintf(fid, '3 %d %d %d\n', tri.faces.' - 1);

  fclose(fid);
end
