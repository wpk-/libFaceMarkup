function pairs = indexpairs(faces)
% Returns all valid pairs of positions in the input argument.
%
% Let's consider two rows i and j in the input variable `faces`. They
% describe two triangles, ABC and DEF. Suppose they share an edge via
% AB = ED. Then `faces(i,1) == faces(j,2)` and `faces(i,2) == faces(j,1)`.
% In other words, the two positions `i,1` and `j,2` hold the same value
% (and the same for `i,2` and `j,1`). We term this an "index pair".
%
% This function finds all index pairs and returns them as a Nx2 matrix,
% where the subscripts `i,k` are stored as linear index `sub(sz, i, k)`,
% and so the first column describes one position in `faces` and the second
% column the paired position.
%
% Note that not all positions with the same value form a pair. Only those
% sharing an edge. Moreover, only edges are considered that link exactly
% two triangles.
%
% Input arguments:
%  FACES     Fx3 matrix of vertex index triplets.
%
% Output arguments:
%  PAIRS     Kx2 matrix of pairs of linear indices into FACES.
%            `max(PAIRS(:)) == N` is the number of elements in the faces
%            matrix (= 3 * numfaces).
%
%tic;
  [numfaces, fdim] = size(faces);
  numedges = numfaces * fdim;

  [ei1, ei2, c] = goodedges(faces);

  cnt = accumarray(c, 1);
  mask = false(size(ei1));
  mask(c(cnt(c)==2)) = true;

  % NB. Assumes all triangles have the same orientation (e.g. CCW).
  pos_a = [ei1(mask);  ei2(mask)];
  pos_b = [ei2(mask);  ei1(mask)];
  pos_b = mod(pos_b + numfaces - 1, numedges) + 1;
  pairs = [pos_a pos_b];
%toc;
end

function [ei1, ei2, c] = goodedges(faces)
% Good edges link exactly two faces together.
%
% Input arguments:
%  FACES     Fx3 matrix of vertex index triplets.
%
% Output arguments:
%  EI1, EI2  Vectors of linear indices into FACES.
%            The topology of faces can essentially defined by the pairing
%            of elements of FACES. EI states that for all i
%            `FACES(EI1(i)) == FACES(EI2(i))` must hold.
%  C         The inverse mapping of EI as a vector of length 3F. For all i
%            `C(EI1(i)) == C(EI2(i)) == i`. See also `unique`.
%
  step = max(faces(:)) + 1;
  f121 = faces(:,[1 2 1]);  % NB. Code relies on this being 121, not 112.
  f233 = faces(:,[2 3 3]);
  edges = min(f121, f233) * step + max(f121, f233);
  
  [~, ei1, c] = unique(edges, 'first');
  [~, ei2] = unique(edges, 'last');
end
