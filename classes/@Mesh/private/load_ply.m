function [P,T,texfile,texP,texT] = load_ply(sourcefile)
% Load a mesh from a file in the PLY format (.ply).
%
% Input arguments:
%  SOURCEFILE  String of the file name.
%
% Output arguments:
%  P        Nx3 matrix of vertices.
%  T        Mx3 matrix of vertex indices. Each row indexes three vertices, and
%           thus describes a triangle.
%  TEXFILE  String of the texture image file name.
%  TEXP     Px2 matrix of point coordinates in the texture image. Coordinates
%           range between 0 and 1 inclusive.
%  TEXT     Mx3 matrix of indices of texture points. The i-th texture triangle
%           matches the i-th shape triangle.
%
  sourcefile = char(sourcefile);
  path = fileparts(sourcefile);

% ply
% format ascii 1.0
% element vertex 46990
% property float x
% property float y
% property float z
% property uchar red
% property uchar green
% property uchar blue
% element face 93323
% property list uchar int vertex_indices
% end_header
  
  V = struct();  % vertex/face, count, properties.
  e = '';
  
  % ~~~~~ Read the file.
  fid = fopen(sourcefile);
  l = '';
  while ~strcmp(l, 'end_header')
    l = strtrim(fgetl(fid));
    t = strsplit(l);
    if strcmp(t{1}, 'element')
      e = t{2};
      V.(e).count = str2num(t{3});
      V.(e).properties = {};
      V.(e).data = [];
    elseif strcmp(t{1}, 'property')
      V.(e).properties{end+1} = t{end};
    end
  end
  elements = fieldnames(V);
  for i = 1:numel(elements)
    e = elements{i};
    if strcmp(e, 'vertex')
      fstr = repmat('%f ', 1, numel(V.vertex.properties));
    elseif strcmp(e, 'face')
      fstr = '%d %d %d %d'; % include leading "3".
    else
      error('Unsupported element: %s', e);
    end
    data = textscan(fid, fstr, V.(e).count, 'CollectOutput',true);
    V.(e).data = data{1};
  end
  fclose(fid);

  % ~~~~~ Construct vertices, faces,
  %       and texture mapping.
  ixx = find(strcmp('x', V.vertex.properties), 1);
  ixy = find(strcmp('y', V.vertex.properties), 1);
  ixz = find(strcmp('z', V.vertex.properties), 1);
  P = V.vertex.data(:,[ixx ixy ixz]);
%   ixu = find(strcmp('u', V.vertex.properties), 1);
%   ixv = find(strcmp('v', V.vertex.properties), 1);
  ixr = find(strcmp('red', V.vertex.properties), 1);
  ixg = find(strcmp('green', V.vertex.properties), 1);
  ixb = find(strcmp('blue', V.vertex.properties), 1);
%   if ixu && ixv
%     texP = V.vertex.data(:,[ixu ixv]);
%   elseif ixr && ixg && ixb
    texP = V.vertex.data(:,[ixr ixg ixb]);
    if max(texP(:)) > 1
      texP = texP / 255;
    end
%   end
  
  T = V.face.data(:,2:end) + 1;
  texT = [];

  texfile = '';
end
