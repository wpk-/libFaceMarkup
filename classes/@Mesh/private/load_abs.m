function [P,T,texfile,texP,texT] = load_abs(sourcefile)
% Load a Mesh from a file in UND database format (.abs), such as FRGC v2.0.
%
% Input arguments:
%  SOURCEFILE  String of the file name.
%
% Output arguments:
%  P        Nx3 matrix of vertices.
%  T        Mx3 matrix of vertex indices. Each row indexes three vertices, and
%           thus describes a triangle.
%  TEXFILE  String of the texture image file name.
%  TEXP     Px2 matrix of point coordinates in the texture image. Coordinates
%           range between 0 and 1 inclusive.
%  TEXT     Mx3 matrix of indices of texture points. The i-th texture triangle
%           matches the i-th shape triangle.
%
  sourcefile     = char(sourcefile);

  % Read the file.

  [fld,name,ext] = fileparts(sourcefile);
  
  if strcmpi(ext, '.gz')
    [~,name]     = fileparts(name);
    unpacked     = gunzip(sourcefile, tempdir);
    unpacked     = unpacked{1};
    fid          = fopen(unpacked);
  else
    unpacked     = false;
    fid          = fopen(sourcefile);
  end
  
  nrows = fgetl(fid);
  nrows = sscanf(nrows, '%d');
  ncols = fgetl(fid);
  ncols = sscanf(ncols, '%d');
  fgetl(fid); % "junk" line
  mask  = fscanf(fid, '%d', [ncols nrows]) == 1;  % x/y & row/col spaghetti is
  x     = fscanf(fid, '%f', [ncols nrows]);       % Matlab/C discrepancy.
  y     = fscanf(fid, '%f', [ncols nrows]);       % Or blame Fortran if you
  z     = fscanf(fid, '%f', [ncols nrows]);       % like.

  fclose(fid);
  
  if unpacked
    delete(unpacked);
  end

  % Parse the bits and pieces.

  P       = [x(:) y(:) z(:)];
  P       = P(mask,:);
  
  map       = zeros(size(mask));
  map(mask) = 1:nnz(mask);
  [xt,yt] = meshgrid(1:nrows, 1:ncols);

  T = triangulation48(nrows, ncols, true);
  % Last step is to filter all triangles that fall outside the mask,
  % and to map to the appropriate smaller vertex numbers.
  T = map(T(all(mask(T),2),:));
  
  texT    = T;
  texP    = [yt(mask)./ncols xt(mask)./nrows];

  % Guess the texture file.
  % Note: although .ppm is more likely, .jpg and .png are preferred and thus
  %       tried first.
  
  altname = regexprep(name, '\d+$', '${num2str(str2double($0)+1)}');
  
  texguesses = {
    [name '.jpg'];
    [name '.png'];
    [name '.ppm'];
    [altname '.jpg']; % In worst case, if regexprep did not match, ...
    [altname '.png']; % ... this is just a repetition of the first three.
    [altname '.ppm'];
  };
  
  for i = 1:numel(texguesses)
    texfile = fullfile(fld, texguesses{i});
    if exist(texfile, 'file')
      return
    end
  end
	  
  texfile = '';
end
