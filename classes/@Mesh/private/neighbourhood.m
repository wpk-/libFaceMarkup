function nb = neighbourhood(hedges, nth_order)
% Construct a sparse vertex neighbourhood matrix.
%
% Input arguments:
%  HEDGES     Px4 matrix whose columns are pointers to:
%             1. the vertex it points to,
%             2. the face it borders (or NaN if the half-edge is the outer half
%                of a border edge),
%             3. the next half-edge around the face,
%             4. the oppositely oriented adjacent half-edge.
%  NTH_ORDER  Scalar specifying the neighbourhood order. For example a value of
%             1 finds all direct neighbours, while 2 finds all direct
%             neighbours and their neighbours.
%
% Output arguments:
%  NB  NxN logical sparse matrix. Element NB(i,j) is true if vertex i is in the
%      NTH_ORDER neighbourhood of vertex j. The matrix is symmetric, and
%      NB(i,i) is always true (a vertex is always in its own neighbourhood).
%

  %tim = @tic;%@noop;%
  %tom = @toc;%@noop;%
  
	% Start with the first order neighbourhood.
	nvtx  = max(hedges(:,1));
	nb    = sparse(hedges(:,1), hedges(hedges(:,4),1), 1, nvtx, nvtx);
	nb    = nb + speye(nvtx);

	% Then expand to requested neigbourhood order.
	if nth_order > 1
		nb  = logical(nb ^ nth_order);
	else
		nb  = logical(nb);
  end
end
