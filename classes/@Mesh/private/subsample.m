function subimg = subsample(im, coords)
% Subsample an image at specified coordinates.
%
% Input arguments:
%  IM      Image as HxWxD matrix.
%  COORDS  Nx2 matrix of coordinates between 0 and 1.
%
% Output arguments:
%  SUBIMG  NxD matrix of interpolated color values
%
  im      = im2double(im);
  [h,w,d] = size(im);
  n       = size(coords, 1);

  cx      = coords(:,1) * (w - 1) + 1;
  cy      = coords(:,2) * (h - 1) + 1;
  %method  = 'linear';  % = default.
  subimg  = zeros(n, d);

  for c = 1:d
    subimg(:,c) = interp2(im(:,:,c), cx, cy);
  end
end
