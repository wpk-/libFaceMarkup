function SI = curvature_SI(params, k2)
% Compute the shape index from quadratic surface parameterization.
%
% Input arguments:
%  PARAMS  Vx6 matrix. The columns a..f represent the coefficients of the
%          polynomial equation: z(x,y) = a*x^2 + b*xy + c*y^2 + d*x + e*y + f.
%
% Alternative input arguments:
%  K1      Vx1 array of the major principal curvature at each vertex.
%  K2      Vx1 array of the minor principal curvature.
%
% Output arguments:
%  C  Vx1 array of the shape index for each polynomial, at x=y=0.
%
  if nargin == 2
    k1 = params;
  else
    [k1,k2] = curvature_k1k2(params);
  end

  SI = .5 - atan2(k1+k2, k1-k2) / pi;
end
