function [components, sizes] = surfaces(hedges, f2he, numsides)
% Find the all connected components of faces.
%
% Input arguments:
%  HEDGES    Px4 matrix whose columns are pointers to:
%            1. the vertex it points to,
%            2. the face it borders (or NaN if the half-edge is the outer half
%               of a border edge),
%            3. the next half-edge around the face,
%            4. the oppositely oriented adjacent half-edge.
%  F2HE      Mx1 array indexing one half-edge per face.
%  NUMSIDES  Number of sides per face.
%
% Output arguments:
%  COMPONENTS  Mx1 array of surface labels. Face i is assigned to surface
%              COMPONENTS(i). The labels are ordered from 1, the largest
%              surface (face count), to K == max(COMPONENTS), the smallest
%              surface.
%  SIZES       Kx1 array counting per component their size in number of faces.
%
	numfaces    = size(f2he,1);

	% Set up face linkage, from face to all adjacent faces = MxD matrix.

	f2f         = zeros(numfaces, numsides+1);
  f2f(:,end)  = (1:numfaces)';
	h           = hedges(f2he,:);
	for i = 1:numsides
		f2f(:,i)  = hedges(h(:,4),2);
		h         = hedges(h(:,3),:);
  end

	% Point outer border edges to their inner border polygon to solve NaN indexes.

	ixnan       = isnan(f2f);
	[fidx, ~]   = find(ixnan);
	f2f(ixnan)  = fidx;
  
	% Now find out which face belongs to which surface.

  colour = (1:numfaces)';
  changed = true(numfaces, 1);
  
  while any(changed)  
    initial = colour;
    changed(f2f(changed,:)) = true;
    colour(changed) = min(colour(f2f(changed,:)), [], 2);
    changed = colour ~= initial;
  end
  
  [~, ~, components] = unique(colour);
  sizes = accumarray(components, 1);

  [sizes, ix] = sort(sizes, 'descend');
  [~, ix] = sort(ix);
  components = ix(components);
end
