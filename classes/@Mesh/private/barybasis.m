function [A, V0, V1] = barybasis(hedges, f2he, vertices)
% For every triangle, construct a Barycentric coordinate system.
%
% Input arguments:
%  TRI  A Mesh instance.
%
% Output arguments:
%  A   Mx3 matrix marking the origin for each triangle.
%  V0  Mx3 matrix defining the first basis vector (along the side B-A).
%  V1  Mx3 matrix defining the second basis vector (along the side C-A).
%
% XXX FIXME this function applies to triangle meshes only.
%
	vertices = tri.vertices;
	faces    = tri.faces;

	A        = vertices(faces(:,1),:);
	V0       = vertices(faces(:,2),:) - A;
	V1       = vertices(faces(:,3),:) - A;
end
