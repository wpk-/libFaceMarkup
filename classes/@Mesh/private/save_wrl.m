function save_wrl(tri, filename)
% Save the mesh in *.wrl (VRML) file format.
%
% Input arguments:
%  TRI       Mesh instance.
%  FILENAME  String of the file name under which to save the mesh.
%
% Considerations:
%  - The texture file (if present) will be renamed to match FILENAME while
%    keeping its extension. It is thought that this is more intuitive behaviour
%    than keeping the original texture file name.
%  - VRML specs: http://tecfa.unige.ch/guides/vrml/vrml97/spec/
%
  [fld,basename] = fileparts(filename);

  f = fopen(filename, 'w');

  fprintf(f, '#VRML V2.0 utf8\n');
  fprintf(f, 'DEF _CVSSP_object Transform {\n');
  fprintf(f, '  children [\n');
  fprintf(f, '    Shape {\n');

  if ~isempty(tri.texturefile)
    [~,~,ext] = fileparts(tri.texturefile);
    
    try
      texfile = fullfile(fld, [basename ext]);
      if ~strcmp(tri.texturefile, texfile)
        copyfile(tri.texturefile, texfile);
      end
    catch %#ok<CTCH>
      warning('TRISAVE:NoTextureFile', 'Could not copy texture file.');
    end
    
    fprintf(f, '      appearance Appearance {\n');
    fprintf(f, '        texture ImageTexture {\n');
    fprintf(f, '          url "%s"\n', [basename ext]);
    fprintf(f, '          repeatS FALSE\n');
    fprintf(f, '          repeatT FALSE\n');
    fprintf(f, '        }\n');
    fprintf(f, '      }\n');
  end

  fprintf(f, '      geometry IndexedFaceSet {\n');
  fprintf(f, '        ccw TRUE\n');
  fprintf(f, '        solid FALSE\n');
  %fprintf(f, '        convex TRUE\n');
  %fprintf(f, '        creaseAngle 1.57\n');
  fprintf(f, '        coord Coordinate {\n');
  fprintf(f, '          point [\n');
  fprintf(f, '%.4f %.4f %.4f,\n', tri.vertices(1:end-1,:)');
  fprintf(f, '%.4f %.4f %.4f\n',  tri.vertices(end,:));
  fprintf(f, '          ]\n');
  fprintf(f, '        }\n');
  fprintf(f, '        coordIndex [\n');
  fprintf(f, '%d %d %d -1,\n', tri.faces(1:end-1,:)'-1);
  fprintf(f, '%d %d %d -1\n',  tri.faces(end,:)-1);
  fprintf(f, '        ]\n');

  if ~isempty(tri.texturecoords)
    texcoords      = tri.texturecoords;
    texcoords(:,2) = 1 - texcoords(:,2);
    
    fprintf(f, '        texCoord TextureCoordinate {\n');
    fprintf(f, '          point [\n');
    fprintf(f, '%.6f %.6f,\n', texcoords(1:end-1,:)');
    fprintf(f, '%.6f %.6f\n',  texcoords(end,:));
    fprintf(f, '          ]\n');
    fprintf(f, '        }\n');

    if ~isempty(tri.textureindices)
      fprintf(f, '        texCoordIndex [\n');
      fprintf(f, '%d %d %d -1,\n', tri.textureindices(1:end-1,:)'-1);
      fprintf(f, '%d %d %d -1\n',  tri.textureindices(end,:)-1);
      fprintf(f, '        ]\n');
    end
  end

  if isempty(tri.texturefile)
    if ~isempty(tri.vertexcolor)
      fprintf(f, '        colorPerVertex TRUE\n');
      fprintf(f, '        color Color {\n');
      fprintf(f, '          color [\n');
      fprintf(f, '%.6f %.6f %.6f,\n', tri.vertexcolor(1:end-1,:)');
      fprintf(f, '%.6f %.6f %.6f\n',  tri.vertexcolor(end,:));
      fprintf(f, '          ]\n');
      fprintf(f, '        }\n');
    elseif ~isempty(tri.facecolor)
      fprintf(f, '        colorPerVertex FALSE\n');
      fprintf(f, '        color Color {\n');
      fprintf(f, '          color [\n');
      fprintf(f, '%.6f %.6f %.6f,\n', tri.facecolor(1:end-1,:)');
      fprintf(f, '%.6f %.6f %.6f\n',  tri.facecolor(end,:));
      fprintf(f, '          ]\n');
      fprintf(f, '        }\n');
    end
  end

  fprintf(f, '      }\n');
  fprintf(f, '    }\n');
  fprintf(f, '  ]\n');
  fprintf(f, '}\n');

  fclose(f);
end
