function K = curvature_K(params)
% Compute the Gaussian curvature from quadratic surface parameterization.
%
% Input arguments:
%  PARAMS  Vx6 matrix. The columns a..f represent the coefficients of the
%          polynomial equation: z(x,y) = a*x^2 + b*xy + c*y^2 + d*x + e*y + f.
%
% Output arguments:
%  K  Vx1 array of the Gaussian curvature for each polynomial, at x=y=0.
%
  x2 = params(:,1);
  xy = params(:,2);
  y2 = params(:,3);
  x  = params(:,4);
  y  = params(:,5);

  K = (4.*x2.*y2 - xy.^2) ./ ((1 + x.^2 + y.^2) .^ 2);
end
