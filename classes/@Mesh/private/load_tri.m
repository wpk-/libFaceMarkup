function [P,T,texfile,texP,texT] = load_tri(sourcefile)
% Load a Mesh from a file in MorphModel format (.tri).
%
% Input arguments:
%  SOURCEFILE  String of the file name.
%
% Output arguments:
%  P        Nx3 matrix of vertices.
%  T        Mx3 matrix of vertex indices. Each row indexes three vertices, and
%           thus describes a triangle.
%  TEXFILE  Empty string.
%  TEXP     Empty 0x2 matrix of texture coordinates.
%  TEXT     Empty 0x3 matrix of texture coordinate indices.
%
  sourcefile = char(sourcefile);

  % Read the file.

  fid    = fopen(sourcefile);
  
  counts = fgetl(fid);
  counts = sscanf(counts, '%d');
  nvtx   = counts(1);
  nfaces = counts(2);
  
  P      = fscanf(fid, '%f', [3 nvtx])';
  T      = fscanf(fid, '%d', [4 nfaces])';
  T      = T(:,2:4) + 1;
  
  fclose(fid);
  
  % Parse the bits and pieces.

  texT    = T;
  texP    = [];  
  texfile = '';
end
