function T = curvature_T(params)
% Compute the curvature tensor from quadratic surface parameterization.
%
% Input arguments:
%  PARAMS  Vx6 matrix. The columns a..f represent the coefficients of the
%          polynomial equation: z(x,y) = a*x^2 + b*xy + c*y^2 + d*x + e*y + f.
%
% Output arguments:
%  T  Vx4 matrix of the curvature tensor for each polynomial, at x=y=0. Whereas
%     the curvature tensor (at any point) is a matrix of the form `[A B; B C]`,
%     we return it as a row vector `[A B B C]` which simplifies most of the
%     multiplication work later on.
%
% Note: To compute the curvature in the direction of `x = [u v]` (we operate
%       in the surface tangent plane, which is the null space of the surface
%       normal), use:
%
%             k = T * (x(:,[1 1 2 2]) .* x(:,[1 2 1 2]))'
%
%       `u` and `v` can be scalars or Vx1 vectors.
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
warning('Implementation not quite right yet.');
% Use the below "for testing code" to get v1
% and compare the k computed as above with
% `mesh.k2` (eig returns v in ascending
% eigenvalue order).
%
% Interestingly, there seems to be an almost
% exact factor 2 discrepancy.
%
% Note, by the way, that the eigenvectors are
% not necessarily orthogonal.
%
% `II` is symmetric. Should it be positive definite?
% I think not, because its eigenvalues, k1 and
% k2, can be < 0.
%
% Perhaps relevant: http://math.stackexchange.com/questions/363400/principal-curvature-and-its-relationship-to-second-fundamental-form
% In particular, II is NOT the shape operator.
%
% Answer might be here:
% https://github.com/alecjacobson/gptoolbox
%
% Not directly relevant, but interesting:
% http://www.mathworks.com/matlabcentral/fileexchange/47134-curvature-estimationl-on-triangle-mesh
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  pp      = params(:,1:3);
  pp(:,[1 3]) = pp(:,[1 3]) * 2; % the second fundamental form has two `xy`s.
  pp      = permute(pp, [3 2 1]);

  % II is the second fundamental form.
  II = reshape(pp(1,[1 2 2 3],:), 2, 2, size(pp,3));
  % T is the curvature tensor.
  T  = zeros(size(II));

  % ~~~~~ For testing:
  %Vk1 = zeros(size(pp,3), 2);
  %D1 = zeros(size(pp,3), 1);
  %D2 = zeros(size(pp,3), 1);
  % ~~~~~~~~~~~~~~~~~~

  for i = 1:size(pp,3)
    [V,D] = eig(II(:,:,i));
    T(:,:,i) = D(1) * V(:,1) * V(:,1)' + D(end) * V(:,2) * V(:,2)';
    % ~~~~~ For testing:
    %Vk1(i,:) = V(:,1);
    %D1(i) = D(1);
    %D2(i) = D(end);
    % ~~~~~~~~~~~~~~~~~~
  end
  
  T = reshape(T, 4, size(pp,3))';
  
  % ~~~~~ For testing:
  %T = Vk1;
  %T = D1;
  %T = D2;
  % ~~~~~~~~~~~~~~~~~~
end
