function faces = fillhole(hei, halfedges, vertices)
% Fills one hole in the mesh, defined by the indices of bounding halfedges.
%
% Explanation:
%   For every vertex A store the two neighbouring vertices around the
%   boundary, B and C.
%
%   One step picks a vertex A and adds an edge BC connecting its two
%   neighbours B and C. B's neighbourhood is changed replacing A with C
%   and similarly for C. Vertex A is closed for further exploration. Then,
%   the lengths of the new proposed edges for B and C are computed and the
%   new state is stored.
%
%   The valuation of the new state is updated from the old by adding length
%   BC to the sum of past lengths (function G), and replacing the expected
%   lengths of vertices B and C (function H).
%
%   The order in which vertices are selected uniquely identify each state.
%   States that have the same selected vertices but in different order are
%   can have different valuations. They are, however, treated as the same
%   node in the graph. The search algorithm thus only maintains the best
%   state for the given set of selected vertices (this was true when we
%   did proper searching).
%
%   A greedy search is performed. And since every action is guaranteed to
%   lead to a valid state, it is a linear path to the goal node without
%   backtracking. (So not really searching, after all.)
%
% Input arguments:
%  HEI        Indices of the halfedges ordered around the hole.
%  HALFEDGES  Nx4 structure of halfedges. Each halfedge point to the vertex
%             with index in column 1 and links to the face with index in
%             column 2. Column 3 references the next halfedge around the
%             face, and column 4 references the oppositely oriented
%             halfedge. See also `halfedges`.
%  VERTICES   VxD matrix of vertex coordinates (V vertices in D
%             dimensions).
%
% Output arguments:
%  FACES      A set of faces (Kx3 array of vertex indices) that cover the
%             hole. They are properly oriented.
%
  e = halfedges(hei,:);

  iv = e(:,1);
  nv = numel(iv);
  
  if nv == 3
    v12 = [halfedges(e(1,4),1) iv(1)];
    ix = ~ismember(iv, v12);
    assert(nnz(ix) == 1);
    faces = [v12 iv(ix)];
    return;
  end
  
  % Precomputed edge lengths.
  L = squareform(pdist(vertices(iv,:)));

  % Halfedge counts. (Every halfedge can occur at most once.)
  vmap = zeros(size(vertices, 1), 1);
  vmap(iv) = 1:nv;
  m = vmap(halfedges(:,1)) > 0; % halfedge target vertex in loop.
  m(m) = m(halfedges(m,4));     % halfedge source vertex in loop too.
  m(hei) = false;               % but exclude the loop halfedges self.
  ec = full(sparse(...
      sub2ind([nv nv], vmap(halfedges(halfedges(m,4),1)), ...
                       vmap(halfedges(m,1))), ...
      1, true, nv^2, 1));

  % Initial state.
  ia = (1:nv)';
  ib = circshift(ia, 1, 1);
  ic = circshift(ia, -1, 1);
  lbc = L(sub2ind([nv nv], ib, ic));
  done = false(nv, 1);

  s0 = [ib ic lbc done];
  
  % Node expansion.
  function s = play(s, a)
    b = s(a,1);
    c = s(a,2);
    s(b,2) = c;             % Link B and C.
    s(c,1) = b;
    s(b,3) = L(s(b,1),c);   % Update lengths.
    s(c,3) = L(b,s(c,2));
    s(a,4) = true;
  end
  actions = @(s) find(~(s(:,4) | ec(sub2ind([nv nv],s(:,1),s(:,2)))));
  explore = @(s) arrayfun(@(a)play(s,a), actions(s), 'UniformOutput',0);

  % Greedy = cost of explored path. No heuristic.
  g = @(s) sum(s(~~s(:,4),3));
  
  % Goal state.
  function tf = goal(s)
    tf = all(s(:,4));
  end

  % Solve.
  
  s1 = s0;
  while ~goal(s1)
    ss = explore(s1);
    [~, ix] = sort(cellfun(g, ss));
    s1 = ss{ix(1)};
  end
  
  % Map back to original vertex indices.
  
  rows = s1(:,1) ~= s1(:,2);  % (There's one ABB and one BBB.)
  faces = [iv(rows) iv(s1(rows,1:2))];
end
