function manifold = makemanifold(faces)
% Rebuilds the mesh topology to form correct manifold surfaces.
%
% Input arguments:
%  FACES     Fx3 matrix of triplets of vertex indices.
%
% Output arguments:
%  MAP       Nx1 array of new vertex indices. N = 3 * numfaces and
%            `reshape(MAP, numfaces, 3)` should replace the mesh faces
%            matrix.
%tic;
  pairs = indexpairs(faces);
  
  n = numel(faces);
  a = pairs(:,1);
  b = pairs(:,2);
  
  fw = makemap(a, b, n, 19);
  bw = makemap(b, a, n, 15);
  map = min(fw, bw(fw));
  
  [~,~,manifold] = unique(map);
%toc; % 0.08 sec.
end

function map = makemap(from, to, n, rounds)
  map = 1:n;
  map(from) = to;
  for i = 1:rounds
    map = min(map, map(map));
  end
end
