function [P,T,texfile,texP,texT] = load_wrl(sourcefile)
% Load a TriMesh from a file in VRML format (.wrl).
%
% Input arguments:
%  SOURCEFILE  String of the file name.
%
% Output arguments:
%  P        Nx3 matrix of vertices.
%  T        Mx3 matrix of vertex indices. Each row indexes three vertices, and
%           thus describes a triangle.
%  TEXFILE  String of the texture image file name.
%  TEXP     Px2 matrix of point coordinates in the texture image. Coordinates
%           range between 0 and 1 inclusive.
%  TEXT     Mx3 matrix of indices of texture points. The i-th texture triangle
%           matches the i-th shape triangle.
%
  sourcefile     = char(sourcefile);
  path           = fileparts(sourcefile);

  flag_vertices  = {'coord Coordinate {', 'Coordinate3 {'};
  flag_faces     = {'coordIndex ['};
  flag_texcoords = {'texCoord TextureCoordinate {', 'TextureCoordinate2 {'};
  flag_texfaces  = {'texCoordIndex ['};
  flag_texfname  = {'texture ImageTexture {', 'Texture2 {'};
  flag_point     = {'point ['};

  flag_matches   = @(s,flgs) max(cellfun(@(flg)max([0 strfind(s,flg)+numel(flg)]), flgs));

  P              = [];
  T              = [];
  texfile        = '';
  texP           = [];
  texT           = [];

  offset         = 1;	% Depends on VRML version. Can later be set to 0.

  % Read the file.

  fid            = fopen(sourcefile);

  while ~feof(fid)
    s = fgets(fid);

    % - Vertices

    if flag_matches(s, flag_vertices)
      if ~isempty(strfind(s, 'Coordinate3 {'))
        offset = 0; % File format version differences.
      end
      while ~flag_matches(s, flag_point)
        s = fgets(fid);
      end
      P = textscan(fid, '%f %f %f', 'WhiteSpace',' \b\t,');
      P = [P{:}];
      P0 = flag_matches(s, flag_point);
      P0 = textscan(s(P0:end), '%f %f %f', 'WhiteSpace',' \b\t,');
      if numel(P0)
        P = cat(1, [P0{:}], P);
      end

    % - Faces

    elseif flag_matches(s, flag_faces)
      T = textscan(fid, '%d %d %d -1', 'WhiteSpace',' \b\t,');
      T = double([T{:}]);
      T0 = flag_matches(s, flag_faces);
      T0 = textscan(s(T0:end), '%d %d %d -1', 'WhiteSpace',' \b\t,');
      if numel(T0)
        T = cat(1, [T0{:}], T);
      end

    % - Texture coordinates

    elseif flag_matches(s, flag_texcoords)
      if ~isempty(strfind(s, 'TextureCoordinate2 {'))
        offset = 0;
      end
      while ~flag_matches(s, flag_point)
        s = fgets(fid);
      end
      texP = textscan(fid, '%f %f', 'WhiteSpace',' \b\t,');
      texP = [texP{:}];
      texP0 = flag_matches(s, flag_point);
      texP0 = textscan(s(texP0:end), '%f %f', 'WhiteSpace',' \b\t,');
      if numel(texP0)
        texP = cat(1, [texP0{:}], texP);
      end

    % - Texture faces

    elseif flag_matches(s, flag_texfaces)
      texT = textscan(fid, '%d %d %d -1', 'WhiteSpace',' \b\t,');
      texT = double([texT{:}]);
      texT0 = flag_matches(s, flag_texfaces);
      texT0 = textscan(s(texT0:end), '%d %d %d -1', 'WhiteSpace',' \b\t,');
      if numel(texT0)
        texT = cat(1, [texT0{:}], texT);
      end

    % - Texture file name

    elseif flag_matches(s, flag_texfname)
      if isempty(strfind(s, 'Texture2 {'))
        pattern = 'url %q';
      else
        pattern = 'filename %q';
        offset  = 0;
      end
      texfile = textscan(fid, pattern, 1);
      if ~isempty(texfile)
        texfile = char(texfile{:});
        if ~exist(texfile, 'file')
          texfile = fullfile(path, texfile);
        end
      else
        texfile = '';
      end
    end
  end

  fclose(fid);

  % Parse the bits and pieces.

  T         = T + offset;
  texT      = texT + offset;
  if ~isempty(texP)
    texP(:,2) = mod(1 - texP(:,2), 1);
  end
end
