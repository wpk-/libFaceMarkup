function [k1, k2] = curvature_k1k2(params, H)
% Compute the principal curvature from quadratic surface parameterization.
%
% Input arguments:
%  PARAMS  Vx6 matrix. The columns a..f represent the coefficients of the
%          polynomial equation: z(x,y) = a*x^2 + b*xy + c*y^2 + d*x + e*y + f.
%
% Alternative input arguments:
%  K       Vx1 array of the Gaussian curvature at each vertex.
%  H       Vx1 array of the mean curvatures.
%
% Output arguments:
%  K1  Vx1 array of the major principal curvature for each polynomial, at x=y=0.
%  K2  Vx1 array of the minor principal curvature.
%
% Considerations:
%  - To prevent computing the Gaussian and mean curvatures again when you have
%    already computed them, you can pass K and H instead of PARAMS.
%
  if nargin == 2
    K = params;
  else
    K = curvature_K(params);
    H = curvature_H(params);
  end

  root = sqrt(H .^ 2 - K);
  k1 = H + root;
  k2 = H - root;
end
