function [values, varargout] = components(hedges)
%[ VALUES, LARGEST, SIZES ] = TRICOMPONENTS( TRI, FLDVERTICES, FLDFACES )
%
% Find all connected components in a mesh.
%
% TRI is a trimesh of N vertices.
% FLDVERTICES and FLDFACES are optional field names for the vertices and
% faces defined on TRI. This defaults to 'vertices' and 'faces', but you
% could e.g. specify 'texturecoords' and 'textureindices' to search for
% components in the texture mesh.
% VALUES is a Nx1 array of component indices, i.e. if VALUES(i) == a then vertex
% i belongs to component a.
% LARGEST contains the index of the largest component. Ties are broken
% arbitrarily.
% SIZES is Cx1 array of component sizes, i.e. the number of vertices per
% component.
%
	if nargin<2 || isempty(fldvertices), fldvertices='vertices'; end
	if nargin<3 || isempty(fldfaces), fldfaces='faces'; end

%
% Note: Computing an extra order neighbourhood takes more time
%       than it saves on search loops
	nvtx     = size(tri.(fldvertices), 1);
	values   = zeros(nvtx, 1);
	nb       = trineighbourhood(tri, 1, fldvertices, fldfaces);
	
%
% Nice visualisation, but takes some seconds of precious time
%
	%hold off;
	%h = trisurf_(tri, values');
	%view(2);
	%drawnow;
	
%
% Continue as long as there is an unassigned vertex
%
	vi = 0;
	i  = find(values == 0, 1);
	while any(i)
		% nbi is the neighbourhood of vertex i
		vi            = vi + 1;
		nbi           = false(nvtx,1);
		nbi(i)        = true;
		nbinc         = nbi;
		
		% expand the neighbourhood (only its marching front)
		% until there is no expansion left, then we have the full component
		while any(nbinc)
			nbj       = any(nb(:,nbinc), 2);
			nbinc     = nbj & ~nbi;
			nbi       = nbi | nbinc;
			%set(h, 'FaceVertexCData', double(nbi));
			%drawnow;
		end
		
		% label all vertices in the found component and
		% find the next unassigned vertex
		values(nbi)   = vi;
		i             = find(values == 0, 1);
	end
	
%
% Upon request we can return some useful pointers
%
	if nargout > 1
		sizes       = histc(values, 1:max(values));
		[~,largest] = max(sizes);
		varargout   = {largest, sizes};
	end
end
