function save_json(filename, tri, anno)
% Save the mesh in *.json (Javascript Object Notation) file format.
%
% Input arguments:
%  TRI       Mesh instance.
%  FILENAME  String of the file name under which to save the mesh.
%  ANNO      Optional Annotation to add to the file.
%
% Considerations:
%  - To keep the data contained in a single file, only the vertex/face colour
%    data is saved (not the texture file). Also, face colour data is only saved
%    if no vertex colour data is available.
%
% TODO: Camera?
  three = ThreeJS();
  three.importMesh(tri, 'matlab-mesh');

  if nargin>2 && ~isempty(anno)
    three.importAnnotation(anno, 'matlab-annotation');
    
    % Group mesh with annotation.
    % XXX: Is there a better way to find the mesh object?
    groupname = sprintf('%s annotated', tri.name);
    group = ThreeJS.Group('name',groupname, ...
        'children',three.tree.object.children(end-1:end));
    three.tree.object.children{end-1} = group;
    three.tree.object.children(end) = [];
  end

  % Write to file.
  saveas(three, filename);
end
