function C = unwrap(Ci)
% Subdivide a loop wherever it visits the same node more than once.
%
% Input arguments:
%  CI  1xN or Nx1 array of indices describing a path along nodes: every node
%      `CI(i)` points to node `CI(i+1)` and node `CI(end)` points back to
%      `CI(1)`. The path may visit the same node (index) more than once,
%      meaning that it includes one or more loops.
%
% Output arguments:
%  C   Kx1 cell array of paths. Every returned path is loop-free.
%
  [c,~,ic] = unique(Ci);

  if numel(c) < numel(ic)
    h = hist(ic, 1:numel(c));
    ix = find(h > 1, 1);
    ix = find(ic == ix);
    C1 = unwrap(Ci(ix(1):(ix(2)-1)));
    C2 = unwrap([Ci(1:(ix(1)-1)) Ci(ix(2):end)]);
    C = [C1; C2];
  else
    C = {Ci};
  end
end
