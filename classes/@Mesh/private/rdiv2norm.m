function B = rdiv2norm(A)
% Divide rows in A by their 2-norm, thus scale rows to unit length. Do it fast.
%
% Input arguments:
%  A  NxM matrix.
%
% Output arguments:
%  B  NxM matrix. Rows of B have unit length (2-norm).
%
  D = size(A, 2);
  B = bsxfun(@ldivide, (A.^2) * ones(D,1), 1);
  B = bsxfun(@times, A, sqrt(B));
end
