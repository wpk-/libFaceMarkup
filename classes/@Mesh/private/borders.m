function [hepaths, len] = borders(hedges)
% Find loops of half-edges around all surfaces.
%
% Input arguments:
%  HEDGES  Px4 matrix whose columns are pointers to:
%          1. the vertex it points to,
%          2. the face it borders (or NaN if the half-edge is the outer half of
%             a border edge),
%          3. the next half-edge around the face,
%          4. the oppositely oriented adjacent half-edge.
%
% Output arguments:
%  HEPATHS  Kx1 cell array of paths along the surface exteriors. Each path
%           is a row vector of half-edge indices, ordered around the
%           surface in the direction of the half-edges(*). All paths are
%           loop-free, except that a path along the surface exterior always
%           ends where it started, thus forming a ring.
%           (*not sure about the direction...)
%  LEN      Kx1 array providing the length of each path.
%

  % ~~~~~ Find all outer (half-)edges, making sure
  %       to point in the triangle direction (CCW).
  numhedges = size(hedges, 1);
	link = zeros(numhedges, 1);
	outside1 = isnan(hedges(:,2));
  outside2 = hedges(outside1,3);
	link(outside2) = find(outside1);

  if isempty(outside2)
    fprintf(2, 'Warning: the surface has no borders (is closed).\n');
    hepaths = {};
    return;
  end

	% ~~~~~ Sort into disjoint loops of linked half-edges.
  %       The paths of half-edges are loop-free.
  hepaths = findloops(link);

  % ~~~~~ Sort from longest to shortest.
	len = cellfun(@numel, hepaths);
	[len,ix] = sort(len, 'descend');
	hepaths = hepaths(ix);
end
