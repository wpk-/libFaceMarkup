function [P,T,texfile,texP,texT] = load_obj(sourcefile)
  sourcefile  = char(sourcefile);
  [path,name] = fileparts(sourcefile);

  % Read the file.

  fid = fopen(sourcefile);
  data = fread(fid);
  fclose(fid);

  data = char(data.');

  % vertices
  x = regexp(data, 'v\s+[Ee\s\d.-]+', 'match');
  P = textscan(cat(2, x{:}), 'v %f %f %f', 'CollectOutput',true);
  P = P{1};

  % texture coordinates
  % Some formats give vt in 3D with z all zeros. Hence %f %f %f.
  x = regexp(data, 'vt\s+[Ee\s\d.-]+', 'match');
  texP = textscan(cat(2, x{:}), 'vt %f %f %f', 'CollectOutput',true);
  texP = mod([texP{1}(:,1) 1-texP{1}(:,2)], 1);

  % normals
  % Mesh class doesn't support defined normals so ignore them.
  %x = regexp(data, 'vn\s+[Ee\s\d.-]+', 'match');
  %N = textscan(cat(2, x{:}), 'vn %f %f %f', 'CollectOutput',true);
  %N = N{1};

  % faces
  % Mesh class doesn't support normal indices so ignore them.
  x = regexp(data, 'f\s+(\d+/?\d*/?\d*\s*)+', 'match');
  x = regexprep(cat(2, x{:}), '(\d+)/?(\d*)/?(\d*)', '$1 0$2'); % '... 0$3'
  F = textscan(x, ['f' repmat(' %d', 1, 20)], 'CollectOutput',true);  % 30
  F = F{1};
  fskip = 2;  % 3

  % Chew on the bits and pieces.

  % Reduce to maximum polygon size.
  d = ceil(find(~any(F, 1), 1) / fskip) * fskip;
  F = F(:,1:d);

  % Break quads and higher order polygons down into triangles.
  A = cell(0, 1);
  i = 0;
  w = size(F, 2);
  while w > 3 * fskip
    i = i + 1;
    ix = F(:,w-fskip+1) > 0;
    A{i} = [F(ix,1:fskip) F(ix,(w-2*fskip+1):end)];
    F = F(:,1:(w-fskip));
    w = size(F, 2);
  end
  F = cat(1, F, A{:});

  if ~isempty(texP)
    % Texture faces
    if fskip > 1
      texT = F(:,2:fskip:end);
    else
      texT = F;
    end
  end

  T = F(:,1:fskip:end);
  
  % Texture file name
  % (no support for material file etc.)
  exts    = {'.png', '.jpg', '.bmp'};
  for ext = exts
    texfile = fullfile(path, [name char(ext)]);
    if exist(texfile, 'file')
      break
    else
      texfile = '';
    end
  end
end

function [P,T,texfile,texP,texT] = load_obj_(sourcefile)
% Load a TriMesh from a file in Wavefront OBJ format (.obj).
%
% Input arguments:
%  SOURCEFILE  String of the file name.
%
% Output arguments:
%  P        Nx3 matrix of vertices.
%  T        Mx3 matrix of vertex indices. Each row indexes three vertices, and
%           thus describes a triangle.
%  TEXFILE  String of the texture image file name.
%  TEXP     Px2 matrix of point coordinates in the texture image. Coordinates
%           range between 0 and 1 inclusive.
%  TEXT     Mx3 matrix of indices of texture points. The i-th texture triangle
%           matches the i-th shape triangle.
%
  sourcefile  = char(sourcefile);
  [path,name] = fileparts(sourcefile);

  % Read the file.

  fid = fopen(sourcefile);
  
  F = [];
  P = [];
  T = [];
  N = [];
  texP = [];
  texT = [];
  fskip = 1;

  p = ftell(fid);
  l = fgetl(fid);
  while ischar(l)
    t = strsplit(l);
    switch lower(t{1})
      case 'v'
        fseek(fid, p, 'bof');
        P = textscan(fid, 'v %f %f %f %*[^\n]', 'CommentStyle','#');
        P = [P{:}];
        fseek(fid, -1, 'cof');
      case 'vn'
        fseek(fid, p, 'bof');
        N = textscan(fid, 'vn %f %f %f %*[^\n]', 'CommentStyle','#');
        N = [N{:}];
        fseek(fid, -1, 'cof');
      case 'vt'
        fseek(fid, p, 'bof');
        texP = textscan(fid, 'vt %f %f %*[^\n]', 'CommentStyle','#');
        texP = [texP{:}];
        texP(:,2) = 1 - texP(:,2);
        fseek(fid, -1, 'cof');
      case 'f'
        fseek(fid, p, 'bof');
        fskip = nnz(t{2} == '/') + 1;
        fpat = [' %d' repmat('%d', 1, fskip-1)];
        fpat = ['f' repmat(fpat, 1, numel(t))];
        F = textscan(fid, fpat, ...
                     'CommentStyle','#', ...
                     'Delimiter',{' ','/'}, ...
                     'CollectOutput',true);
        F = F{1};
        fseek(fid, -1, 'cof');
      otherwise
        % nothing.
    end
    p = ftell(fid);
    l = fgetl(fid);
  end

  fclose(fid);

  % Parse the bits and pieces.

  % Break quads and higher order polygons down into triangles.
  A = cell(0, 1);
  i = 0;
  w = size(F, 2);
  while w > 3 * fskip
    i = i + 1;
    ix = F(:,end) > 0;
    A{i} = [F(ix,1:fskip) F(ix,(w-2*fskip+1):end)];
    F = F(:,1:(w-fskip));
    w = size(F, 2);
  end
  F = cat(1, F, A{:});
  
  if ~isempty(texP)
    % Texture faces
    if fskip > 1
      texT = F(:,2:fskip:end);  % I guess...
    else
      texT = F;
    end
  end
  
  T = F(:,1:fskip:end);
  
  % Texture file name
  % (no support for material file etc.)
  exts    = {'.png', '.jpg', '.bmp'};
  for ext = exts
    texfile = fullfile(path, [name char(ext)]);
    if exist(texfile, 'file')
      break
    else
      texfile = '';
    end
  end
end
