function edges = edges(hedges)
% Find all undirected edges.
%
% Input arguments:
%  HEDGES  Px4 matrix whose columns are pointers to:
%          1. the vertex it points to,
%          2. the face it borders (or NaN if the half-edge is the outer half of
%             a border edge),
%          3. the next half-edge around the face,
%          4. the oppositely oriented adjacent half-edge.
%
% Output arguments:
%  EDGES  Px2 matrix of pairs of vertex indices. Unique undirected edges are
%         returned.
%
% Considerations:
%  - Most edges connect two faces, each in opposite orientation. This function
%    only considers the undirected edges and thus returns each edge once.
%  - To find all *outside* edges see borders().
%
  numhedges = size(hedges, 1);

  % Decide which half-edge to pick (side).

  side = (1:numhedges)';
  opp = hedges(:,4);
  side = unique(min(side, opp)); %#ok<UDIM>

  % Then find its opposite side.

  opp = opp(side);

  % Now get the vertices at both ends = edges.

  edges = [hedges(side,1) hedges(opp,1)];
end
