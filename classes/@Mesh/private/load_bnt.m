function [P,T,texfile,texP,texT] = load_bnt(sourcefile)
% Load a TriMesh from a file in the Bosphorus data set format (.bnt).
%
% Input arguments:
%  SOURCEFILE  String of the file name.
%
% Output arguments:
%  P        Nx3 matrix of vertices.
%  T        Mx3 matrix of vertex indices. Each row indexes three vertices, and
%           thus describes a triangle.
%  TEXFILE  String of the texture image file name.
%  TEXP     Px2 matrix of point coordinates in the texture image. Coordinates
%           range between 0 and 1 inclusive.
%  TEXT     Mx3 matrix of indices of texture points. The i-th texture triangle
%           matches the i-th shape triangle.
%
  sourcefile = char(sourcefile);
  path = fileparts(sourcefile);

  % ~~~~~ Read the file.
  fid = fopen(sourcefile);
  nrows   = fread(fid, 1, 'uint16');
  ncols   = fread(fid, 1, 'uint16');  % This should be 5.
  zmin    = fread(fid, 1, 'float64'); %#ok<NASGU>
  len     = fread(fid, 1, 'uint16');
  texfile = fread(fid, [1 len], 'uint8=>char');
  len     = fread(fid, 1, 'uint32');
  assert(len == nrows*ncols*5, 'TriMesh:load:dimensions', ...
            'The number of vertices must equal 5 * nrows * ncols.');
  data    = fread(fid, [len/5 5], 'float64');
  fclose(fid);

  % ~~~~~ Filter coordinates.
  mask = ~any(data < -1e6, 2);
  data = data(mask,:);

  % ~~~~~ Mapping to the reduced set.
  map = zeros(size(mask));
  map(mask) = 1:nnz(mask);

  % ~~~~~ Construct vertices, faces,
  %       and texture mapping.
  P = data(:,1:3);
  texP = data(:,4:5);
  T = triangulation48(nrows, ncols, true);
  T = T(:,[1 3 2]); % CCW. (`data` reads from bottom to top)
  T = map(T(all(mask(T),2),:));
  texT = T;

  texfile = fullfile(path, texfile);
end
