function [hedges, v2he, f2he] = halfedges(faces)
% Construct the half-edge structure.
%
% Input arguments:
%  FACES  MxD matrix of point indices, providing a connectivity structure over
%         the points (such as vertices). For triangles D=3, for quads D=4.
%
% Output arguments:
%  HEDGES  Px4 matrix whose columns are pointers to:
%          1. the vertex it points to,
%          2. the face it borders (or NaN if the half-edge is the outer half of
%             a border edge),
%          3. the next half-edge around the face,
%          4. the oppositely oriented adjacent half-edge.
%  V2HE    Nx1 array indexing half-edges. For each vertex V2HE points to a
%          half-edge that emanates from it.
%  F2HE    Mx1 array indexing half-edges. For each face F2HE points to a
%          half-edge that borders it.
%
% Considerations:
%  - Column 2 is NaN for half-edges on the outer border of the surface. So, the
%    border of the surface has two half-edges: an inner half-edge connected to
%    a face, and an outer half-edge not connected to a face. This allows for
%    efficient traversal around the surface border.
%  - More information on half-edge structures can be found at:
%    http://www.flipcode.com/archives/The_Half-Edge_Data_Structure.shtml
%    https://www.sidefx.com/docs/houdini13.0/vex/halfedges
%    http://www.openmesh.org/Daily-Builds/Doc/a00016.html
%    http://doc.cgal.org/latest/HalfedgeDS/index.html
%
  [numfaces,fdim]   = size(faces);
  numvertices       = max(faces(:));
  numedges          = numfaces * fdim;

  edges             = reshape(faces(:,[1:fdim 2:fdim 1]), numedges, 2);
  fidx              = (1:numfaces)' * ones(1, fdim);
  hidx              = mod((1:numedges) + numfaces - 1, numedges) + 1;

  % Find all half-edge pairs.
  
  flip              = edges(:,2) < edges(:,1);
  undir             = edges;
  undir(flip,:)     = undir(flip,[2 1]);
  [~,m1]            = unique(undir, 'rows', 'first');
  [~,m2]            = unique(undir, 'rows', 'last');

  % Pair up lonely half-edges around the surface border.

  lonely            = m1 == m2;% works because unique() sorts consistently.
  numlonely         = nnz(lonely);
  mates             = edges(m1(lonely),[2 1]);
  edges             = [edges; mates];
  fidx              = [fidx(:); nan(numlonely,1)];
  hidx              = [hidx(:); nan(numlonely,1)];% tricky. solve below.
  m2(lonely)        = numedges + (1:numlonely);
  numedges          = numedges + numlonely;

  % Build all paired up half-edge structure.

  hedges            = nan(numedges, 4);
  hedges(:,1)       = edges(:,2);
  hedges(:,2)       = fidx;
  hedges(:,3)       = hidx;
  hedges(m1,4)      = m2;
  hedges(m2,4)      = m1;

  % Link the outer border half-edges to each other so we can quickly and easily
  % traverse the surface borders in both directions.

  mateidx           = m2(lonely);
  nextmate          = nextaroundtheborder(hedges, mateidx);
  hedges(mateidx,3) = nextmate;

  % Match a half-edges to each vertex and face.

  v2he              = zeros(numvertices, 1);
  v2he(edges(:,1))  = 1:numedges;

  f2he              = (1:numfaces)';
end

function henext = nextaroundtheborder(hedges, hei0)
% Find the half-edge linkage around the surface borders.
%
% Input arguments:
%  HEDGES  Px4 matrix (see above).
%  HEI0    Kx1 array indexing the half-edges around the surface borders. That
%          is the indices of the newly added `mates` (see above).
%
% Output arguments:
%  HENEXT  Kx1 array indexing the next half-edge around the outside of the
%          surface for each given half-edge in HEI0.
%
% Considerations:
%  - This is a fairly liberal interpretation of "next half-edge around the
%    face". Imagine triangles connecting three consecutive vertices around the
%    border of the surface. We should be careful though, because this really
%    creates triangular structures (albeit withtout the faces) even if the
%    mesh is a quad mesh.
%  - We know that HEI0 is circular in the sense that every next half-edge for
%    HEI0 (which we will compute) is also requested in HEI0. We just don't know
%    its index. That means that instead of finding all *next* half-edges, we
%    can instead (more quickly) find all *previous* half-edges, and then fill
%    them in appropriately.
%
  numhedges     = size(hedges,1);
  h             = false(numhedges, 1);
  h(hei0)       = true;

  % Link half-edges around all surfaces.

  opp           = hedges(h,4);
  next          = hedges(opp,3);
  mask          = ~isnan(next);

  while any(mask)
    opp(mask)   = hedges(next(mask),4);
    next(mask)  = hedges(opp(mask),3);
    mask(mask)  = ~isnan(next(mask));
  end

  % OPP(i) marks the half-edge whose next half-edge is HEI0(i).
  % So we are now solving backwards and all will be fine (see considerations).

  link          = zeros(numhedges,1);
  link(opp)     = hei0;
  henext        = link(hei0);
end
