function [vtx, faces, tc_, ti] = cleandata(vertices, faces, tc, ti, options)
% Clean raw incoming data.
%
% Some data files define a non-manifold structure. This code aims to detect
% such errors and clean the data. It is most likely not complete, though,
% so be sure to contribute code to this function whenever you find a new
% violation.
%
% Input arguments:
%  VERTICES        Nx3 matrix of 3D vertex coordinates.
%  FACES           Mx3 matrix indexing triplets of vertices in P, to define
%                  triangles.
%  TEXUREFILE      String specifying the file name of the texture image.
%  TEXTURECOORDS   Px2 matrix of 2D coordinates on the texture image.
%                  Coordinates range between 0 and 1 inclusive.
%  TEXTUREINDICES  Mx3 matrix indexing triplets of texture points, to define
%                  triangles.
%  OPTIONS         A struct with the following fields:
%                  .min_edge_length: faces with an edge shorter than this
%                                    are discarded. Default value is 1e-6.
%                  .is_3dmd: if true, triangles with texture coordinates
%                            in the top left hand corner are discarded.
%
% Output arguments:
%  VERTICES        Cleaned copies of the input arguments.
%  FACES
%  TEXTURECOORDS
%  TEXTUREINDICES
%
  if nargin<3, tc = []; end
  if nargin<4, ti = []; end
  if nargin<5, options = struct(); end

  if ~isfield(options, 'min_edge_length')
    options.min_edge_length = 1e-6;
  end
  if ~isfield(options, 'is_3dmd')
    options.is_3dmd = false;
  end
  
  has_tc           = ~isempty(tc);
	has_ti           = ~isempty(ti);

  vdim             = size(vertices, 2);
  [nfaces,fdim]    = size(faces);
  fidx             = true(nfaces, 1);
  tdim             = size(tc, 2);
  
  % 3dMD top-left texture corner.
  
  if options.is_3dmd && has_tc && has_ti
    discard        = tc < 0.01;
    discard        = all(discard(ti), 2);
    fidx(discard)  = false;
  end

  % Needle-like triangles / zero-length edges.
  
  faces2           = circshift(faces, -1, 2);
  edge_length      = vertices(faces,:) - vertices(faces2,:);
  discard          = dot(edge_length, edge_length, 2) < ...
                     options.min_edge_length .^ 2;
  if any(discard)
    vidx           = max(faces(discard), faces2(discard));
    discard        = any(ismember(faces, unique(vidx)), 2);
    fidx(discard)  = false;
  end
  
  if has_tc && has_ti
    ti2            = circshift(ti, -1, 2);
    edge_length    = tc(ti,:) - tc(ti2,:);
    discard        = dot(edge_length, edge_length, 2) < ...
                     options.min_edge_length .^ 2;
    if any(discard)
      tcidx          = max(ti(discard), ti2(discard));
      discard        = any(ismember(ti, unique(tcidx)), 2);
      fidx(discard)  = false;
    end
  end

%   % Unstable normal vectors.
% 
%   N             = tri.normals;
%   Nlength       = sum(N .* N, 2);
%   vidx          = (Nlength > 0.9) & (Nlength < 1.1);
%   fidx          = fidx & all(vidx(faces), 2);

  % Throw them all out.
  
  [vertices, faces, tc, ti] = filterfaces(...
          vertices, faces, tc, ti, fidx);
  
  nfaces = size(faces, 1);
  
  % Impose correct manifold structure.
  
  manifold           = makemanifold(faces);
  vtx                = zeros(max(manifold), vdim);
  vtx(manifold,:)    = vertices(faces,:);
  faces              = reshape(manifold, nfaces, fdim);

  if has_tc && has_ti
    manifold         = makemanifold(ti);
    tc_              = zeros(max(manifold), tdim);
    tc_(manifold,:)  = tc(ti,:);
    ti               = reshape(manifold, nfaces, fdim);
  end
end

function [vertices, faces, texturecoords, textureindices] = filterfaces(...
          vertices, faces, texturecoords, textureindices, fidx)

  has_ti       = ~isempty(textureindices);
  has_tc       = ~isempty(texturecoords);
  
	% Determine the vertices that will be kept and their new order.

	vidx         = unique(faces(fidx,:));

	refv         = zeros(vidx(end), 1);% unique() sorts in ascending order
	refv(vidx)   = 1:numel(vidx);

	if has_ti
		tidx       = unique(textureindices(fidx,:));
		reft       = zeros(tidx(end), 1);
		reft(tidx) = 1:numel(tidx);
	end

	% Now update all properties to match the new structure.

	faces            = refv(faces(fidx,:));
	vertices         = vertices(vidx,:);

	if has_ti
		textureindices = reft(textureindices(fidx,:));
	end
	if has_tc
		texturecoords  = texturecoords(tidx,:);
  end
end
