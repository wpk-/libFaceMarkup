function [P, T, texfile, texP, texT] = load_pes(sourcefile)
  [fld, pes_id, ~] = fileparts(sourcefile);
  texfile = fullfile(fld, [pes_id '_face_bsm_alp.png']);

  % Load the shape.
  % Head only and no vertex normals.

  fid = fopen(sourcefile);
  data = char(fread(fid).');
  fclose(fid);

  % - vertices
  x = regexp(data, 'v\s+[Ee\s\d.-]+', 'match');
  P = textscan(cat(2, x{:}), 'v %f %f %f', 'CollectOutput', true);
  P = P{1};

  % - texture coordinates
  % vt is given in 3D with z all zeros. Hence %f %f %f.
  x = regexp(data, 'vt\s+[Ee\s\d.-]+', 'match');
  texP = textscan(cat(2, x{:}), 'vt %f %f %f', 'CollectOutput', true);
  texP = mod([texP{1}(:,1) 1-texP{1}(:,2)], 1);

  % - faces
  r = 'o Object\d+.*?((f\s+(\d+/\d+/\d+\s+)+|s\s+\d+\s+)+)';
  x = regexp(data, r, 'tokens');
  F = textscan(cat(2, x{1}{1}, x{3}{1}), ['f' repmat(' %d', 1, 9)], ...
               'Delimiter', {' ', '/'}, 'CommentStyle', 's ', ...
               'CollectOutput', true);
  T = F{1}(:,1:3:end);
  texT = F{1}(:,2:3:end);

  % Extract consistent indices.
  % Vertex ID's are variable, but the shape faces are ordered.

  [nf, nt] = size(T);

  % - shape
  [ix, ~, T] = unique(T.', 'stable');
  T = reshape(T, [nt nf]).';
  P = P(ix,:);

  % - texture
  [ix, ~, texT] = unique(texT.', 'stable');
  texT = reshape(texT, [nt nf]).';
  texP = texP(ix,:);
end
