function txt = labeldatatips(~, evt, tri)
% Add vertex index to the data cursor labels.
%
% Input arguments:
%  ~    Unused.
%  EVT  Event object.
%  TRI  Mesh instance.
%
% Output arguments:
%  TXT  Data label describing the selected point. Its x, y, and z coordinates
%       as well as its vertex index.
%
% Quick setup:
%    trisurf(tri);
%    hdt = datacursormode(gcf);
%    set(hdt, 'UpdateFcn', {@labeldatatips,tri});
%
  xyz = evt.Position;
  [~,ix] = pdist2(tri.vertices, xyz, 'Euclidean', 'Smallest',1);

  txt = { ...
    ['X: ' num2str(xyz(1),4)], ...
    ['Y: ' num2str(xyz(2),4)], ...
    ['Z: ' num2str(xyz(3),4)], ...
    ['Vertex: ',num2str(ix)] ...
  };
end
