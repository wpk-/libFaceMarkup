function loops = findloops(link)
% Find loops of linked nodes.
%
% Input arguments:
%  LINK  Hx1 array describing one or more paths over nodes. Each entry, `i`,
%        represents a node, and the value `LINK(i)` stores the index of the
%        next node along the path. Nodes not on any path have value zero, so
%        the non-zero indices form a mapping onto themselves. Consequently,
%        every path is a ring (contains no sub-loops but the last node does
%        link back to the first).
%
% Output arguments:
%  LOOPS  Kx1 cell array of disjoint paths. Each path is a row vector of unique
%         node in indices.
%
  nlinks = nnz(link);
  loops = cell(floor(nlinks/3), 1);

  visited = 0;
  i = 0;

  while visited < nlinks
    i = i + 1;
    j = 1;
    he0 = find(link, 1);
    he1 = link(he0);
    loopi = zeros(1, nlinks-visited);
    loopi(j) = he0;

    while he1 ~= he0
      j = j + 1;
      loopi(j) = he1;
      he1 = link(he1);
    end

    loopi = loopi(1:j);
    loops{i} = loopi;
    link(loopi) = 0;
    visited = visited + j;
  end

  loops = loops(1:i);
end
