function [TI,TC] = faces2textureindices(tri, newfaces)
% Convert a set of faces to texture indices that minimise the texture area.
%
% Input arguments:
%  TRI       Mesh instance.
%  NEWFACES  Fx3 matrix of shape vertex indices.
%
% Output arguments:
%  TI  Fx3 matrix of texture coordinate indices.
%  TC  Vx2 matrix of extra texture coordinates to be appended to
%                  `tri.texturecoords`.
%
% Considerations:
%  - Although the faces may cover a continuous area on the shape, the texture
%    indices do not have to be continuous.
%  - TRI must have texture defined, obviously.
%
  nfaces = size(newfaces, 1);

  faces             = tri.faces;
  textureindices    = tri.textureindices;
  texturecoords     = tri.texturecoords;
  texturecomponents = tri.texturecomponents();

  % 1. Candidate texture vertices.

  % `vi2ti` is a cell array mapping from vertices to candidate texture coords.
  % For a given vertex `vi`, the vector `vi2ti{vi}` maps to `tri.texturecoords`.

  vi1  = unique(newfaces);
  fm   = ismember(faces, vi1);
  vi2  = faces(fm);
  ti   = textureindices(fm);
  smap = sparse(ti, vi2, 1);

  vi2ti      = cell(max(vi1), 1);
  vi2ti(vi1) = arrayfun(@(vi)find(smap(:,vi)), vi1, 'UniformOutput',0);

  % 2. Candidate texture triangles.

  % Construct all possible texture triangles.
  % `tt` is a Fx1 cell array. The `i`th cell contains a Nx3 matrix of all
  % possible texture triangles for shape triangle `newfaces(i,:)`.

  tt = arrayfun(...
        @(i) cartprod(vi2ti{newfaces(i,1)}, vi2ti{newfaces(i,2)}, vi2ti{newfaces(i,3)}), ...
        (1:nfaces)', 'UniformOutput',0);
  sz = cellfun(@(tti) size(tti, 1), tt);

  % 3. Select smallest triangle (circumference).

  ct = cat(1, tt{:});
  nt = size(ct, 1);

  E  = reshape(ct(:,[1 2 3 2 3 1]), 3*nt, 2);
  L  = sqrt(sum((texturecoords(E(:,1),:) - texturecoords(E(:,2),:)) .^ 2, 2));
  L  = reshape(L, nt, 3);
  A  = sum(L, 2);
  % Ella :)

  [A,ix] = cellfun(@min, mat2cell(A,sz));
  rows = cumsum(sz) - sz + ix;

  TI = ct(rows,:);
  
  %%%%% trimesh(TI, texturecoords(:,1), texturecoords(:,2));


  % In some cases there are no suitable vertex combinations, which results in
  % large needle-shaped triangles.

  % TODO: detect bad texture triangles and add vertices (texturecoords).
  % probably an efficient way for detection is measure if the shortest length
  % multiplied by 10 is still smaller than the sum of all three lengths.

  if median(A) * 10 < max(A)
    L = L(rows,:);

    K(textureindices) = repmat(texturecomponents, 3, 1);

    [r,c] = find(bsxfun(@and, ...
            any(K(TI(:,1:2)) ~= K(TI(:,2:3)), 2), ...
            bsxfun(@le, L*10, sum(L,2)) ...
            ));

    ia = sub2ind(size(TI), r, c);
    ib = sub2ind(size(TI), r, mod(c,3)+1);
    ic = sub2ind(size(TI), r, mod(c+1,3)+1);
    A  = texturecoords(TI(ia),:);
    B  = texturecoords(TI(ib),:);
    C  = texturecoords(TI(ic),:);  % <- will be updated.
    V1 = B - A;                   % <- length is L(needle) is "L(r,c)" is L(ia).
    V2 = zeros(size(V1));
    for i = 1:size(V1,1)
      nu = null(V1(i,:))';
      V2(i,:) = nu(1,:);
    end
    V2 = bsxfun(@times, V2, sign(dot(C-A, V2, 2)) .* L(ia));
    C = (B + A + V2) ./ 2;        % is (A+B)/2 + V2/2
    
    TI(ic) = size(texturecoords,1) + (1:numel(ic));
    TC = C;
  else
    TC = zeros(0, size(texturecoords,2));
  end
end

function P = cartprod(varargin)
% Compute the cartesian product of a set of vectors.
%
% Input arguments:
%  VARARGIN  Variable number of vectors (at least one).
%
% Output arguments:
%  P  The cartesian product of the input arguments.
%
  [A{1:nargin}] = ndgrid(varargin{:});
  % concatenate
  N = prod(cellfun(@numel, varargin));
  P = reshape(cat(nargin+1, A{:}), N, nargin);
end
