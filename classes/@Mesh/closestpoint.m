function [PPxyz,PPuv,fidx] = closestpoint(tri, Pxyz)
% Find the point on the mesh closest to given point.
%
% Input arguments:
%  TRI   A Mesh instance.
%  PXYZ  1xD point in Cartesian coordinates. Usually D=3.
%
% Output arguments:
%  PPXYZ  1xD point on the surface of TRI, nearest to PXYZ.
%  PPUV   1x2 coordinate of PPXYZ in the local UV coordinate system of triangle
%         FIDX.
%  FIDX   Scalar index of the triangle of TRI that contains PPXYZ.
%
% Considerations:
%  - PPUV is expressed in the FI-th coordinate system thus:
%    PPXYZ = TRI.A(FIDX,:) + PPUV(1)*TRI.V0(FIDX,:) + PPUV(2)*TRI.V1(FIDX,:)
%
	A   = tri.A;
	V0  = tri.V0;
	V1  = tri.V1;

	% Write Pxyz in Barycentric coordinates.

	Vap         = bsxfun(@minus, Pxyz, A);	% We need these later too.
	[UV,inside] = tri.cart2bary(Vap, true);
  
  % Quick way out if we found the triangle already.
  
  if any(inside)
    fidx      = find(inside);
    Pclose    = tri.bary2cart(UV(inside,:), inside);
    ix        = dsearchn(Pclose, Pxyz);
    
    fidx      = fidx(ix);
    PPxyz     = Pclose(ix,:);
    PPuv      = UV(fidx,:);
    
    return;
  end
  
	% Clip UV coordinates to lie within their triangle.
  
  UV = clipuv(UV);
  
% 	far         = sum(UV, 2) > 1;
% 	Vbp         = Vap(far,:) - V0(far,:);
% 	Vbc         = V1(far,:) - V0(far,:);
% 	W           = dot(Vbp, Vbc, 2) ./ dot(Vbc, Vbc, 2);
% 	UV(far,1)   = 1 - W;
% 	UV(far,2)   = W;
% 
% 	UV(UV<0)    = 0;
% 	UV(UV>1)    = 1;

	% Restore points to Euclidean space.

	Pclose      = tri.bary2cart(UV);

	% Find the closest point.

	fidx        = dsearchn(Pclose, Pxyz);
	PPxyz       = Pclose(fidx,:);
	PPuv        = UV(fidx,:);
end
