function [b, len] = borders(tri, fidx)
% Find all loops of vertices around separate surfaces.
%
% Input arguments:
%  TRI   Mesh instance.
%  FIDX  Optional array of face indices to return only borders of the
%        marked surfaces. The marked faces must span one or more surfaces
%        in full. Parts of surfaces are not supported.
%
% Output arguments:
%  B     Kx1 cell array of outer edge loops. Each loop is a row vector of
%        half-edge indices, ordered around the surface. Because the
%        half-edge representation already includes the ones on the outside
%        of the surface, not connected to any face, the returned indexes
%        point precisely to those half-edges.
%  LEN   Kx1 array providing the length of each path.
%
  he = tri.halfedges;

  if nargin>1 && ~isempty(fidx)
    nfac = tri.numfaces;
    fi = false(nfac, 1);
    fi(fidx) = true;    % fidx can be logical or index.

    heout = isnan(he(:,2));
    hein = he(heout,4); % opp. half-edge.
    fin = he(hein,2);   % connected face.
    heout(heout) = fi(fin);

    map = double(heout);
    map(heout) = (1:nnz(heout))';
    he = he(heout,:);
    he(:,3:4) = map(he(:,3:4));
    % `he(:,4)` is all zeros, but
    % that is fine.
    
    map_back = find(heout);
    [b, len] = borders(he);
    b = cellfun(@(x) map_back(x), b, 'UniformOutput',0);
  else
    [b, len] = borders(he);
  end
end
