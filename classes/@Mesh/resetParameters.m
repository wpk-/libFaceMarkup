function resetParameters(tri)
% Reset all estimated parameters for the surface polynomial equations.
%
% A change in the default neighbourhood order, as well as any of the mesh
% vertices, faces, or normals voids the surface parameterization.
%
% TODO: vertices setter function should call resetNormals.
% TODO: faces setter function should call resetNormals.
%
% Input arguments:
%  TRI  Mesh instance.
%
  if tri.has_parameters_defined
    tri.has_parameters_defined = false;
    tri.parameters = zeros(0, 6);
  end
end
