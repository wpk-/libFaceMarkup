function tri = load(sourcefile, varargin)
% Load a Mesh instance from file.
%
% Input arguments:
%  SOURCEFILE  String of the data file name.
%  FORMAT      Optional string specifying the file format.
%              Default value is the source file extension.
%  ISDIRTY     Optional logical scalar (pass as keyword/value pair) to
%              specify that the data loaded from file needs cleaning before
%              returning the mesh.
%              Default value is false.
%
% Output arguments:
%  TRI  A Mesh instance.
%
  p = inputParser;
  p.FunctionName = 'load';
  p.addRequired('sourcefile',      @ischar);
  p.addOptional('format',      '', @ischar);
  p.addParameter('isdirty', false, @islogical);
  
  p.parse(sourcefile, varargin{:});
  sourcefile = p.Results.sourcefile;
  format     = p.Results.format;
  isdirty    = p.Results.isdirty;
  
  if isempty(format)
    [~,name,ext] = fileparts(sourcefile);
    name         = [name ext];
    ix           = strfind([name '.'], '.');
    format       = name((ix+1):end);
  end

  switch lower(format)
    case 'wrl'                  % VRML
      fcn = @load_wrl;
    case 'obj'                  % OBJ
      fcn = @load_obj;
    case 'bnt'                  % Bosphorus 3D Face Database
      fcn = @load_bnt;
    case {'abs','gz','abs.gz'}  % UND Database range image (such as FRGC v2.0)
      fcn = @load_abs;
    case 'tri'                  % MorphModel
      fcn = @load_tri;
    case 'ply'                  % PLY
      fcn = @load_ply;
    case 'pes'                  % Pro Evolution Soccer
      fcn = @load_pes;
    otherwise                   % By default, guess VRML
      fcn = @load_wrl;
  end

  [P,T,texfile,texP,texT] = fcn(sourcefile);

  tri            = Mesh(P, T, texfile, texP, texT, isdirty);
  tri.sourcefile = sourcefile;
  [~,tri.name]   = fileparts(sourcefile);
end
