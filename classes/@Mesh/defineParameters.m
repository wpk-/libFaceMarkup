function defineParameters(tri, params)
% Estimate parameters for polynomial equations modelling the mesh surface.
%
% Input arguments:
%  TRI     Mesh instance.
%  PARAMS  Optional Vx6 matrix of parameter values for the polynomials at all
%          vertices.
%
  if tri.verbose
    fprintf('Define parameters...');
    t0 = tic;
  end

  if nargin == 1
    params = tri.parameterization(tri.default_neighbourhood_order);
  elseif size(params, 2) ~= 6
    error('Incompatible parameterisation. `size(params,2)` must be six.');
  end

  tri.parameters = params;
  tri.has_parameters_defined = true;

  if tri.verbose
    fprintf(' %.02f sec.\n', toc(t0));
  end
end
