function defineNormals(tri, vn, fn)
% Compute vertex and face normals.
%
% Input arguments:
%  TRI  Mesh instance.
%  VERTEXNORMALS  Optional VxD matrix of surface normal vectors at the vertices.
%  FACENORMALS    Optional FxD matrix of surface normal vectors at the faces.
%
% Considerations:
%  - By default the vertex and face normals are computed, but you can specify
%    manually computed values. If so, you must specify values for both.
%
  if tri.verbose
    fprintf('Define normals...');
    t0 = tic;
  end

  if nargin == 1
    [vn,fn] = normals(tri);
  elseif nargin ~= 3
    error('Either specify all args VERTEXNORMALS and FACENORMALS, or none.');
  end

  tri.vertexnormals = vn;
  tri.facenormals = fn;

  tri.has_normals_defined = true;

  if tri.verbose
    fprintf(' %.02f sec.\n', toc(t0));
  end

  % ~~~~~ Reset properties that depend on the normals.
  resetParameters(tri);
end
