function filterfaces(tri, fidx)
% Reduce the mesh to include only the selected faces.
%
% Input arguments:
%  TRI   A Mesh instance.
%  FIDX  Nx1 logical array or Kx1 index array marking the faces to be kept.
%
	faces          = tri.faces;
	textureindices = tri.textureindices;

	updatetexture  = ~isempty(textureindices);

	% Determine the vertices that will be kept and their new order.

	vidx         = unique(faces(fidx,:));

	refv         = zeros(vidx(end), 1);% unique() sorts in ascending order
	refv(vidx)   = 1:numel(vidx);

	if updatetexture
		tidx       = unique(textureindices(fidx,:));
		reft       = zeros(tidx(end), 1);
		reft(tidx) = 1:numel(tidx);
	end

	% Now update all properties to match the new structure.

	tri.faces            = refv(faces(fidx,:));
	tri.vertices         = tri.vertices(vidx,:);

	if updatetexture
		tri.textureindices = reft(textureindices(fidx,:));
	end
	if ~isempty(tri.texturecoords)
		tri.texturecoords  = tri.texturecoords(tidx,:);
	end
	if ~isempty(tri.vertexcolor)
		tri.vertexcolor    = tri.vertexcolor(vidx,:);
	end
	if ~isempty(tri.facecolor)
		tri.facecolor      = tri.facecolor(fidx,:);
  end
  
  % Lastly clear the caches, but where possible just update them.
  
  % For certain properties it is debatable whether they should be reset:
  %  -> we keep `parameters` and `vertexnormals`
  
  if tri.has_barycenters_defined
    tri.A = tri.A(fidx,:);
    tri.V0 = tri.V0(fidx,:);
    tri.V1 = tri.V1(fidx,:);
  end
  if tri.has_halfedges_defined
    tri.f2he = [];
    tri.halfedges = [];
    tri.v2he = [];
    tri.has_halfedges_defined = false;
%     warning('FIXME: filter the half edges.');
%     tri.f2he = tri.f2he(fidx,:);
%     tri.halfedges;    % Hx4 matrix. A half-edge rep. See private/halfedges.m.
%     tri.v2he = unique...;         % Vx1 array, referencing (by index) one half-edge per vertex.
  end
  if ~isempty(tri.nb1_)
    tri.nb1_ = tri.nb1_(vidx,vidx);
  end
  if ~isempty(tri.nb10_)
    tri.nb10_ = tri.nb10_(vidx,vidx);
  end
  if tri.has_normals_defined
    tri.facenormals = tri.facenormals(fidx,:);
    tri.vertexnormals = tri.vertexnormals(vidx,:);
  end
  if tri.has_parameters_defined
    % These values will not be identical to parameters computed on the filtered
    % mesh. But I think they are good enough to save the time needed to compute
    % them again. And besides, the old values are, in a sense, better than
    % newly computed values because edges are always troublesome.
    tri.parameters = tri.parameters(vidx,:);
  end
end
