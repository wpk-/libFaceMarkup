function L = laplacian(tri, func)
% Computes the (sparse symmetric) Laplacian matrix for the mesh.
%
% Input arguments:
%  TRI     Mesh instance of N vertices.
%  FUNC    String specifying the function over the surface of which the
%          Laplacian is computed. Available choices:
%          - 'none'/'graph' (default): compute the Laplacian of the graph
%                   connectivity only, ignoring any geometric information.
%          - 'cotan': sum the cotangents of angles opposing each edge. Using
%                   this function allows to preserve most of the local
%                   geometric structure of the mesh.
%          - 'meanvalue': mean value weights result in a non-symmetric
%                   Laplacian matrix. The directed edge (vi,vj) is weighted by
%                   the tangents of the two angles measured at vertex vi.
%          - 'area': edge weights are the sum of the areas of the connected
%                   triangles. this matrix aims to preserve triangle area,
%                   rather than angles.
%          - 'mix': a silly mix between preservation of angles and area, though
%                   the results are not too bad.
%
% Output arguments:
%  L    NxN sparse matrix. It is the Laplacian matrix of the selected function
%       over the mesh.
%
% See also:
%  - Slides (esp. 45-48) of the following presentation
%    http://graphics.stanford.edu/courses/cs468-10-fall/LectureSlides/12_Parameterization1.pdf
%
% @TODO: investigate mean-value weights. the result is not as expected.
%        would have expected result to be similar to cotan.
%
% @TODO: implement minimal stretch / equiareal (slides above).
%        but maybe it can't work? See e.g. the follow-up slides
%        http://graphics.stanford.edu/courses/cs468-10-fall/LectureSlides/13_Parameterization2.pdf
%
  if nargin<2 || isempty(func), func = 'none'; end

% Note, the halfedge structure is a matrix of four columns, specifying for each
% halfedge:
%    1. the vertex it points to,
%    2. the face it borders (or NaN if the half-edge is the outer half
%       of a border edge),
%    3. the next half-edge around the face,
%    4. the oppositely oriented adjacent half-edge.
  
  switch func
    case {'cotan', 'harmonic'}
      W = cotan_weights(tri.halfedges, tri.vertices);
    case 'meanvalue'
      W = meanvalue_weights(tri.halfedges, tri.vertices);
    case 'area'
      W = area_weights(tri.halfedges, tri.areas);
    case 'mix'
      W = area_weights(tri.halfedges, tri.areas) * 0.2 + ...
          cotan_weights(tri.halfedges, tri.vertices);
    case 'pdf'
      model = createns(tri.vertices);
      [xi, d] = rangesearch(model, tri.vertices, 10);
      xj = arrayfun(@(i) i*ones(size(xi{i})), 1:numel(xi), 'UniformOutput',0);
      W = sparse(cat(2,xi{:}), cat(2,xj{:}), normpdf(cat(2,d{:}), 0, 10), ...
                 tri.numvertices, tri.numvertices);
      W = spdiags(zeros(tri.numvertices, 1), 0, W);
    case 'uniform'
      W = uniform_weights(tri.halfedges, tri.vertices);
    otherwise
      if ~strcmp(func, 'none')
        warning('libFaceMarkup:Mesh:unsupportedLaplacianWeights', ...
                ['Unknown weights for the mesh laplacian, "%s". ' ...
                 'Using "none".'], func);
      end
      W = no_weights(tri.halfedges);
  end
  
  L = spdiags(full(sum(W,2)), 0, -W);
end


function W = no_weights(he)
  
  [vtx, ~, opp] = internal_halfedges(he);
  
  nvtx = max(vtx);
  
  vi = vtx;
  vj = he(opp,1);
  
  W  = sparse(vi, vj, 1, nvtx, nvtx);
  W  = W + W';
end

function W = cotan_weights(he, vertices)
% Edge weights taken from cotangent of angles opposing the edge.
% Note:
%   For a non-right angled triangle with inner angles A, B and C, and
%   opposing edges a, b and c (vectors), tan(A) = || b x c || / (b . c),
%   where x is the cross product and . the dot product. The cotangent is
%   simply its reciprocal.
%
  nvtx = size(vertices, 1);
  
  [vtx, next, opp] = internal_halfedges(he);
  
  v0 = vertices(vtx,:);
  v1 = v0(next,:);
  e1 = v1 - v0;           % edge 1
  e2 = e1(next,:);        % edge 2
  e3 = cross(e1, e2, 2);
  den = max(1e-6, sqrt(dot(e3, e3, 2)));
  w  = dot(e1, e2, 2) ./ den;  % = cot(angle edge 1 edge 2)
  
  vi = vtx;
  vj = he(opp,1);         % NB. `opp` indexes `he`.

  W  = sparse(vi, vj, -w, nvtx, nvtx);
  W  = W + W';
end

function W = meanvalue_weights(he, vertices)
% Edge weigts taken from tangents of angles on the vertex.
% Note:
%   See note above.
%   Also tan(theta/2)  = sin(theta) / (1+cos(theta))
%                      = sqrt( (1-cos(theta)) / (1+cos(theta)) )
%       where |.| denotes the absolute value.
%
  nvtx = size(vertices, 1);
  [vtx, next] = internal_halfedges(he);
  
  vnxt = vtx(next);
  
  v0 = vertices(vtx,:);
  v1 = vertices(vnxt,:);
  e1 = v1 - v0;
  e2 = e1(next,:);
  l1 = sqrt(dot(e1, e1, 2));
  l2 = l1(next);
  s  = l1 .* l2;
  c  = dot(e1, e2, 2);
  t  = sqrt((s-c) ./ (s+c));
  w  = [t./l1 t./l2];

  vi = [vnxt vnxt];
  vj = [vtx vnxt(next)];
  
  W = sparse(vi, vj, w, nvtx, nvtx);
  %W = W + W';
end

function W = area_weights(he, areas)
% Edge weights are the sum of areas of connected triangles.
% Input arg areas are precomputed triangle areas.
%
  ix = ~isnan(he(:,2));
  
  vtx0 = he(ix,1);
  vtx1 = he(he(ix,3),1);
  af = areas(he(ix,2));
  
  nvtx = max(vtx0);
  
  W = sparse([vtx0;vtx1], [vtx1;vtx0], [af;af], nvtx, nvtx);
  W = W + W';
end

function W = uniform_weights(he, vertices)
% Edge weights are the edge lengths relative to the shortest edge.
%
  nvtx = size(vertices, 1);

  [vi, ~, opp] = internal_halfedges(he);
  vj = he(opp,1);
  w = sqrt(sum((vertices(vi,:) - vertices(vj,:)) .^ 2, 2));
  mn = max(min(w), 1e-6);
  w = w ./ mn;

  W = sparse(vi, vj, -w, nvtx, nvtx);
  W = W + W';
end

function [vtx, next, opp, ix] = internal_halfedges(he)
  ix = ~isnan(he(:,2));
  he = he(ix,:);
  
  ix = accumarray(find(ix), 1:nnz(ix), [numel(ix) 1]);
  
  vtx = he(:,1);
  next = ix(he(:,3));
  %opp = ix(he(:,4));
  opp = he(:,4);      % NB. cannot map ix, because some opp are external.
end
