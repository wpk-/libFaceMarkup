function fillholes(tri)
% For each surface, close all but the biggest outer edge loops.
%
% Tricky situations:
%
% - a triangle points "inwards" with both edges part of the loop. we are
%   not allowed to connect these edges together because that triangle
%   already exists.
% - a triangle shares an edge with the loop and its third vertex pops up
%   anywhere later in the loop. we cannot connect the edge to that point in
%   the loop.
% - a triangle shares no edge with the loop, but all its vertices. we are
%   not allowed to connect these vertices because they form a triangle that
%   exists.
%
% The smaller part of the trick is solving it. The main trick is
% identifying the trick.
%
% Input arguments:
%  TRI  Mesh instance.
%

  % Find all surfaces.

  components = tri.surfaces();

  % For each surface find all loops, and close all but the largest.

  vtx = tri.vertices;
  he = tri.halfedges;

  loops = tri.borders();
  
  % Remove the first (= largest) loop from every surface.
  lc = components(he(he(cellfun(@(loop)loop(1), loops),4),2));
  [~,ix_first] = unique(lc);
  loops(ix_first) = [];
  
  newfaces = cell2mat(cellfun(@(loop) fillhole(loop, he, vtx), loops, ...
                              'UniformOutput',0));
  
  updatetexture = false;
  if ~isempty(newfaces) && ...
      ~isempty(tri.texturecoords) && ~isempty(tri.textureindices)
    % S...L...O...W...
    [newtextureindices,newtexturecoords] = faces2textureindices(tri, newfaces);
    updatetexture = true;
  end

  % Apply the update, and
  % mark all caches invalid.

  tri.faces = cat(1, tri.faces, newfaces);

  resetAllDependents(tri);
  tri.facecolor = [];
  
  if updatetexture
    tri.textureindices = cat(1, tri.textureindices, newtextureindices);
    tri.texturecoords = cat(1, tri.texturecoords, newtexturecoords);
    tri.addtexture(tri.texturefile, tri.texturecoords, tri.textureindices);
  end
end
