function smoothborders(tri)
% Insert faces to reduce the vertex count on the outer border.
%
% Input arguments:
%  TRI  The Mesh instance.
%

  % Find all surfaces.

  components = tri.surfaces();

  % For each surface find all loops, and smooth the largest.

  v2 = tri.vertices(:,[1 2]);
  he = tri.halfedges;

  loops = tri.borders();

  % The first (= largest) loop from every surface is the outer border.
  lc = components(he(he(cellfun(@(loop)loop(1), loops),4),2));
  [~,ix_first] = unique(lc);
  loops = loops(ix_first);
  loops = loops(cellfun(@numel, loops) > 4);

  newfaces = cell(numel(loops), 1);

  for i = 1:numel(loops)
    loop = loops{i};
    k = convhull(v2(he(loop,1),:));
    k = unique(k);  % Sorts at the same time.
    k = cat(1, k, numel(loop)+k(1));
    loop = cat(2, loop, loop(1:k(1)));

    d = k(2:end) - k(1:end-1);
    nf = arrayfun(@(j) loop(k(j):k(j+1)), find(d > 2), 'UniformOutput',0);

    for j = 1:numel(nf)
      jj = nf{j};
      t = jj(~ismember(he(jj,3), jj));
      s = jj(~ismember(jj, he(jj,3)));
      hej = he;
      hej(t,3) = s;
      hej(hej(s,4),1) = hej(t,1);
      nf{j} = fillhole(jj, hej, v2);
    end

    newfaces{i} = cat(1, nf{:});
  end
  newfaces = cat(1, newfaces{:});

  % Extend to texture where required.

  updatetexture = false;
  if ~isempty(newfaces) && ...
      ~isempty(tri.texturecoords) && ~isempty(tri.textureindices)
    % S...L...O...W...
    [newtextureindices,newtexturecoords] = faces2textureindices(tri, newfaces);
    updatetexture = true;
  end

  % Apply the update, and
  % mark all caches invalid.

  tri.faces = cat(1, tri.faces, newfaces);

  resetAllDependents(tri);
  tri.facecolor = [];
  
  if updatetexture
    tri.textureindices = cat(1, tri.textureindices, newtextureindices);
    tri.texturecoords = cat(1, tri.texturecoords, newtexturecoords);
    tri.addtexture(tri.texturefile, tri.texturecoords, tri.textureindices);
  end
end
