function tri = hemisphere(varargin)
% Create half a unit ball, approximately uniformly sampled.
%
% Input arguments:
%  MINPTS    See `Mesh.unitball`.
%  MAXPTS    See `Mesh.unitball`.
%  MINANGLE  See `Mesh.unitball`.
%  MAXANGLE  See `Mesh.unitball`.
%
% Output arguments:
%  TRI  A Mesh of the hemisphere, with the vertices (approximately) evenly
%       spaced over the surface.
%
% Considerations:
%  - This function uses `Mesh.unitball` internally and then chops off one half.
%    For all arguments and considerations, therefore, see that function's
%    documentation.
%
  tri  = Mesh.unitball(varargin{:});
  half = tri.vertices * [1 .001 .001]' > 0;
  
  assert(2*nnz(half) == numel(half), 'Oops, unequal halves!');
  
  vidx = tri.filtervertices(half);
  
  assert(nnz(vidx) == nnz(half), 'Oops, unequal halves after all!');
end
