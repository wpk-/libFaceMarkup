function [shp, tex] = quaddata(tri, sz_shape, sz_texture, method, t_uv, texture_image)
% Regular (grid) sampling over the mesh surface.
%
% Input arguments:
%  TRI         Mesh instance.
%  SZ_SHAPE    Scalar or 1x2 array of the horizontal and vertical grid size for
%              shape sampling.
%  SZ_TEXTURE  Scalar or 1x2 array of the horizontal and vertical grid size for
%              texture sampling. Usually you want to set this higher than the
%              shape resolution.
%  METHOD      Interpolation method. Can take any interpolation method accepted
%              by `griddedInterpolant`. Default is 'linear'.
%  T_UV        Optional triangulation of the new texture map (which will be
%              regularly sampled). By default this is constructed from the
%              square UV parameterisation with cotan weights, but when e.g. the
%              mouth is open this results in errors. Also when you want to quad
%              multiple versions of the mesh there is little point in
%              recomputing the UV map each time. Pass a precomputed UV map to
%              use that instead.
%  TEXTURE_IMAGE  Optional texture image as an array if the file
%              TRI.texturefile does not exist.
%
% Output arguments:
%  SHP    Matrix of size SZ_SHAPE x 3. Three layers are (x,y,z).
%  TEX    Matrix of size SZ_TEXTURE x 3. Three layers are (r,g,b).
%
% Considerations:
%  - The mesh must have an associated texture image and texture coordinates.
%  - The mesh cannot contain holes (we can only interpolate within given
%    triangles.
%
  if nargin<2 || isempty(sz_shape), sz_shape=ceil(sqrt(tri.numvertices)); end
  if nargin<3 || isempty(sz_texture), sz_texture=sz_shape; end
  if nargin<4 || isempty(method), method='linear'; end
  
  if numel(sz_shape) == 1
    sz_shape = [sz_shape sz_shape];
  end
  if numel(sz_texture) == 1
    sz_texture = [sz_texture sz_texture];
  end
  
  tri = Mesh.copy(tri);
  if nargin<5 || isempty(t_uv)
    t_uv = tri.uv('square', 'cotan');
  end
  if ~isa(t_uv, 'triangulation')
    t_uv = triangulation(tri.faces, t_uv);
  end
  
  %tic;
  t_shp = triangulation(tri.faces, tri.vertices);
  shp = resample(t_shp, t_uv, sz_shape);
  %t = toc;
  %fprintf('Shape: %.2f sec.\n', t);
  
  %tic;
  if nargin<6 || isempty(texture_image)
    im = im2double(imread(tri.texturefile));
  else
    im = texture_image;
  end
  [h,w,d] = size(im);
  t_tex = triangulation(tri.textureindices, tri.texturecoords * diag([w h]));
  tex_uv = resample(t_tex, t_uv, sz_texture);
  tex = zeros([sz_texture d]);
  for i = 1:d
    interp = griddedInterpolant(im(:,:,i), method);
    tex(:,:,i) = interp(tex_uv(:,:,2), tex_uv(:,:,1));
  end
  %t = toc;
  %fprintf('Texture: %.2f sec.\n', t);
end

function val = resample(t_data, t_map, sz)
% Resample a triangulation over a regular grid in UV space.
%
% Input arguments:
%  T_DATA     Triangulation of the data, with vertices of size NxD.
%  T_MAP      Triangulation of the UV space.
%  SZ         1x2 Array of the sampling grid size (W, H).
%
% Output arguments:
%  VAL        Matrix of size (H, W, D) with the values of T_DATA regularly
%             sampled over an HxW grid on the [0..1] interval.
%
  u = linspace(eps, 1-eps, sz(1));
  v = linspace(eps, 1-eps, sz(2));
  [v,u] = ndgrid(v, u);
  
  [ti,B] = pointLocation(t_map, u(:), v(:));
  
  dim = size(t_data.Points, 2);
  val = nan(numel(ti), dim);
  m = ~isnan(ti);
  
  val(m,:) = barycentricToCartesian(t_data, ti(m), B(m,:));
  val = reshape(val, [size(u), dim]);
end
