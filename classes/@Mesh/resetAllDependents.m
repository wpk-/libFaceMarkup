function resetAllDependents(tri)
% Reset all cached properties and flag them as not available.
%
  resetBarycenters(tri);
  resetHalfEdges(tri);
  resetNormals(tri);
  resetParameters(tri);

  % Cached properties in `tri.neighbourhood()`.
  tri.nb1_ = [];
  tri.nb10_ = [];
end
