function varargout = quadsurf(tri, sz_shape, sz_texture, varargin)
% 3D surface plot of the mesh using a rectangular grid.
%
% The grid computed first, which takes some time.
%
% Input arguments:
%  TRI         Mesh instance.
%  SZ_SHAPE    Scalar or 1x2 array of the horizontal and vertical grid size for
%              shape sampling.
%  SZ_TEXTURE  Scalar or 1x2 array of the horizontal and vertical grid size for
%              texture sampling. Usually you want to set this higher than the
%              shape resolution.
%  VARARGIN    Optional extra keyword arguments passed to `surf()`.
%
% Output arguments:
%  H    Handle of the drawn surface.
%
% Considerations:
%  - The mesh must have an associated texture image and texture coordinates,
%    and it cannot contain holes. (See also `Mesh.quaddata()`).
%
  vargs = varargin;
  
  if numel(vargs) > 0 && ~ischar(vargs{1})
    t_uv = vargs{1};
    vargs = vargs(2:end);
  else
    t_uv = [];
  end
  
  if numel(vargs) > 0 && ~ischar(vargs{1})
    texture_image = vargs{1};
    vargs = vargs(2:end);
  else
    texture_image = [];
  end
  
  [xyz, rgb] = tri.quaddata(sz_shape, sz_texture, [], t_uv, texture_image);
  
  [varargout{1:nargout}] = surf(...
        xyz(:,:,1), xyz(:,:,2), xyz(:,:,3), 'CData',rgb, ...
        'FaceColor','texturemap', vargs{:});
end
