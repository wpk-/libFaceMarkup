function [A, V0, V1] = barycenters(tri)
% For every triangle, construct an origin and two basis vectors.
%
% Input arguments:
%  TRI  A Mesh instance.
%
% Output arguments:
%  A   Mx3 matrix marking the origin for each triangle.
%  V0  Mx3 matrix defining the first basis vector (along the side B-A).
%  V1  Mx3 matrix defining the second basis vector (along the side C-A).
%
	vertices = tri.vertices;
	faces    = tri.faces;

	A        = vertices(faces(:,1),:);
	V0       = vertices(faces(:,2),:) - A;
	V1       = vertices(faces(:,3),:) - A;
end
