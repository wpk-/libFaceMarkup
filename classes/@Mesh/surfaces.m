function [components, sizes] = surfaces(tri)
% Find the disjoint surfaces.
%
% Input arguments:
%  TRI  A Mesh instance.
%
% Output arguments:
%  COMPONENTS  Mx1 array of surface labels. Face i is assigned to surface
%              COMPONENTS(i). The labels are ordered from 1, the largest
%              surface (face count), to K == max(COMPONENTS), the smallest
%              surface.
%  SIZES       Kx1 array counting per component their size in number of faces.
%
  [components,sizes] = surfaces(tri.halfedges, tri.f2he, tri.facedim);
end
