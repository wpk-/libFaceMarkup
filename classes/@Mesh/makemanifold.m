function makemanifold(tri, include_texture)
% Tear and restore the mesh manifold structure.
%
% The method cuts all triangles loose, then stitches all the good ones
% back together. Effectively this removes all invalidly shared points and
% edges such as an edge shared by four triangles, or a triangle attached
% with its vertex but neither of the two edges.
%
% After this operation, the mesh is in good state for geometric operations
% such as computing half-edges, boundaries, segments, uv mapping, hole
% filling, etc.
%
% Basically, it's a good idea to always run this function very soon after
% loading a mesh.
%
% Input arguments:
%  TRI    The Mesh.
%
  if nargin<2 || isempty(include_texture), include_texture = false; end
  
  faces                 = tri.faces;
  manifold              = makemanifold(faces);
  
  vertices              = zeros(max(manifold), size(tri.vertices,2));
  vertices(manifold,:)  = tri.vertices(faces,:);
  
  tri.faces             = reshape(manifold, size(faces));
  tri.vertices          = vertices;
  
  if ~isempty(tri.vertexcolor)
    vcolor              = zeros(max(manifold), size(tri.vertexcolor,2));
    vcolor(manifold,:)  = tri.vertexcolor(faces,:);
    tri.vertexcolor     = vcolor;
  end
  
  if include_texture
    ti                  = tri.textureindices;
    manifold            = makemanifold(ti);
    tc                  = zeros(max(manifold), size(tri.texturecoords,2));
    tc(manifold,:)      = tri.texturecoords(ti,:);
    tri.textureindices  = reshape(manifold, size(ti));
    tri.texturecoords   = tc;
  end
  
  tri.resetAllDependents();
end
