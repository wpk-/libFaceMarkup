function addtexture(tri, texturefile, texturecoords, textureindices)
% Add colour data from a texture file.
%
% Input arguments:
%  TRI             Mesh instance.
%  TEXTUREFILE     Image file name from which to read the colour data.
%                  Alternatively it can be a HxWxD matrix of image data.
%  TEXTURECOORDS   Nx2 matrix of points at which to sample the image.
%  TEXTUREINDICES  Optional Mx3 matrix referencing triplets of points. The i-th
%                  row (the i-th triangle) matches the i-th shape triangle in
%                  TRI.faces.
%                  Default value is TRI.faces, in which case the i-th row in
%                  TEXTURECOORDS must match the i-th row in TRI.vertices.
%
% Output arguments:
%  none.
%
  if nargin<4 || isempty(textureindices)
    textureindices = tri.faces;
  end

  im = [];
  
  try
    im = imread(texturefile);
    im = im2double(im);
  catch %#ok<CTCH>
    if ~isempty(texturefile) && isnumeric(texturefile)
      im = texturefile; % texture image passed as matrix.
      texturefile = '';
    else
      warning('Mesh:load:texture', ...
          'Failed to load texture file (%s).', texturefile);
      %return;
    end
  end
  
  tri.texturefile    = texturefile;
  tri.texturecoords  = texturecoords;
  tri.textureindices = textureindices;
  
  if ~isempty(im)
    tii = all(textureindices, 2);
    ti = textureindices(tii,:);

    texPx    = texturecoords(:,1);
    texPy    = texturecoords(:,2);
    texTx    = mean(texPx(ti), 2);
    texTy    = mean(texPy(ti), 2);
    vtxcolor = subsample(im, texturecoords);
    
    tri.facecolor        = zeros(tri.numfaces, size(vtxcolor, 2));
    tri.facecolor(tii,:) = subsample(im, [texTx texTy]);
    tri.vertexcolor      = zeros(tri.numvertices, size(vtxcolor,2));
    tri.vertexcolor(tri.faces(tii,:),:) = vtxcolor(ti,:);
  end
end
