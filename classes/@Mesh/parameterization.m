function params = parameterization(tri, nth_order)
% Parameterize the mesh surface using second order polynomial equations.
%
% The parameterization is of the form:
%     z(x,y) = a*x^2 + b*xy + c*y^2 + d*x + e*y + f
% where <x,y> spans the surface tangent plane and z (thus) measures along
% the surface normal. Estimation is based on the local neighbourhood of
% points around each vertex.
%
% Input arguments:
%  TRI        A Mesh instance.
%  NTH_ORDER  Scalar of the vertex neighbourhood order, considered in the
%             parameter estimation. Larger values give smoother estimates.
%
% Output arguments:
%  PARAMS  Nx6 array of the coefficients of the 2nd order polynomial(s).
%
% Considerations:
%  - Around each vertex we extract the NTH_ORDER neighbourhood patch to
%    estimate curvature. This patch *ex*cludes the vertex itself.
%
  numvtx = tri.numvertices;

  vertices = tri.vertices';
  vnorm = tri.vertexnormals';
  nbhd = tri.neighbourhood(nth_order);

  if tri.verbose
    fprintf('Parameterize (%d-neighbourhood)...', nth_order);
    t0 = tic;
  end

	% Remove each vertex from its own neighbourhood.

	nbhd(speye(numvtx)==1) = false;

	% We can't solve underdetermined systems (which we get when a vertex has
  % fewer than six neighbours). We also can't define a basis reliably when
  % the vertex normal is unreliable (its length should be 1).

	nbsz     = full(sum(nbhd));
  p        = (nbsz >= 6) & (dot(vnorm, vnorm) > 0.5);
  ix       = find(p);

	% Construct a local basis for every vertex.
  % The upvector will be the surface normal vector. Then the x and y axes are
  % chosen orthogonal to it (the plane tangent to the surface at that point).
  % Seen from a different perspective, the x and y axis span the null space of
  % the normal vector. We can compute it that way and that would be easy and
  % legible (see commented out code below), but the for-loop is quite slow. So
  % we compute it slightly differently:
  % We start with a guess for the new x-axis (being [0 1 0]) from which we
  % subtract multiples of the normal vector to make them orthogonal. The new
  % y-axis is simply computed as the cross product.
  % Also for speed, we build the bases as XYZ x VERTICES x BASIS VECTORS. Then,
  % when done, we permute the axes so it will be XYZ x BASIS VECTORS x VERTICES.

  bases        = zeros(3, numvtx, 3);
  z            = abs(vnorm(2,:)) > .9;% start from z if vertex normal is y,
  y            = ~z;                  % start from y otherwise.
  z            = z & p;
  y            = y & p;
  vnorm        = vnorm(:,p);
	bases(2,y,1) = 1;
  bases(3,z,1) = 1;
	bases(:,p,1) = bases(:,p,1) - bsxfun(@times, dot(bases(:,p,1), vnorm), vnorm);
	bases(:,p,1) = cdiv2norm(bases(:,p,1));
	bases(:,p,2) = cdiv2norm(cross(vnorm, bases(:,p,1)));
	bases(:,p,1) = cdiv2norm(cross(bases(:,p,2), vnorm));
	bases(:,p,3) = vnorm;
	bases        = permute(bases, [1 3 2]);
  % Cleaner, but way slower:
  %bases = zeros(3,3,nvtx);
  %for i = 1:ix
  % basis = [null(vnorm(:,i)') vnorm(:,i)];
  % bases(:,:,i) = basis(:,1:3);
  %end

	% Surprisingly, most time is spent projecting the neighbouring vertices into
	% the local coordinate system, and setting up the linear systems of
  % equations. NOT on constructing the bases, NOR on solving the linear
  % systems. Too bad. We have to do it anyway.

	fnx  = zeros(6, 6, numvtx);
	fnf  = zeros(6, numvtx);
  for i = ix
		[fnx(:,:,i),fnf(:,i)] = localsystem(...
            vertices(:,i), bases(:,:,i), vertices(:,nbhd(:,i)));
	end

	% Solve NV systems of linear equations.
	%  f(x,y) = ax^2 + bxy + cy^2 + dx + ey + f

  coef        = zeros(6, numvtx);
  for i = ix
		coef(:,i) = fnx(:,:,i) \ fnf(:,i);
	end
	params      = coef';

  % Done.

  if tri.verbose
    fprintf(' %.02f sec.\n', toc(t0));
  end
end


function [A, B] = localsystem(origin, basis, vertices)
% Set up a linear system for a polynomial of the neighbourhood around one vertex.
%
% Input arguments:
%  ORIGIN    3x1 point chosen as origin for the neighbourhood.
%  BASIS     3x3 matrix of basis vectors (column vectors).
%  VERTICES  3xN matrix of points around the origin.
%
% Output arguments:
%  A  6x6 matrix of equation variables. (x^2, xy, y^2, x, y, 1)
%  B  6x1 matrix of answers. (z)
%
% Considerations:
%  - The returned linear system is of the form Ax = B.
%
	delta = bsxfun(@minus, vertices, origin);
	pts   = delta' * basis;
	FM    = [pts(:,[1 2 2]).*pts(:,[1 1 2]) pts(:,[1 2]) ones(size(pts,1),1)];
	% Premultiplication makes the system easier to solve.
	A     = FM' * FM;
	B     = FM' * pts(:,3);
end
