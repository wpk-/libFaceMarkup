function h = trisurf(tri, varargin)
% Equivalent to Matlab's built-in trisurf(), but takes the Mesh as one argument.
%
% Input arguments:
%  TRI  Mesh instance.
%  C    Optional Nx3 or Nx1 matrix with colour values. N can be equal to either
%       the number of vertices or the number of faces.
%       Default is TRI.facecolor.
%       Pass an empty array to disable texture rendering.
%
% Output arguments:
%  H  Handle to the graphics object returned by Matlab's built-in trisurf().
%

	% Build argument list for trisurf().
	args = {tri.faces, tri.vertices(:,1), tri.vertices(:,2), tri.vertices(:,3)};

	% There seems to be a bug in trisurf() and trimesh() which causes an
	% Mx3 matrix C to be vectorised to 3M length, e.g.
	% ---
	% Warning: Patch FaceVertexCData length (176289) must equal 1, Vertices
	% length (29587), or Faces length (58763) for flat FaceColor.
	% ---
	% As a rather blunt solution we pass C through FaceVertexCData instead
	% (as long as you haven't passed another value for that argument).
	C = [];
	if mod(nargin, 2) == 0	% C passed
		C         = varargin{1};
		varargin  = varargin(2:end);
	else
		C         = tri.facecolor;
	end
	if ~isempty(C)
		if ~any(strcmpi('FaceVertexCData', varargin(1:2:end)))
			args{end+1} = 'FaceVertexCData';
		end
		args{end+1} = C;
	end

	% Add edge colour.
	E = strcmpi('EdgeColor', varargin(1:2:end));
	if ~any(E)
		args = [args {'EdgeColor','none'}];
	end

	% Add any remaining arguments, then render.
	args = [args varargin];
	tmp  = trisurf(args{:});

	if nargout == 1
		h = tmp;
	end

	axis equal;
	%set(gca, 'CameraViewAngleMode', 'manual');
	axis vis3d;
end
