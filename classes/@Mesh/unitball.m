function ball = unitball(minpts, maxpts, minangle, maxangle)
% Create a Mesh of the unit ball, approximately uniformly sampled.
%
% Input arguments:
%  MINPTS    Scalar of the minimum number of vertices on the ball.
%            Iteration stops when the number of vertices >= MINPTS.
%  MAXPTS    Scalar of the maximum number of vertices on the ball.
%            Iteration stops when further subdivision would lead to more
%            vertices than MAXPTS.
%  MINANGLE  Scalar of the minimum angle between any two neighbouring vertices
%            (in radians).
%            Iteration stops when further subdivision would lead to angles
%            smaller than MINANGLE.
%  MAXANGLE  Scalar of the maximum angle between any two neighbouring vertices.
%            Iteration stops when the largest angle <= MAXANGLE.
%
% Output arguments:
%  BALL  A Mesh of the unit ball, centred on the origin, with the vertices
%        (approximately) evenly spaced over the surface.
%
% Considerations:
%  - The algorithm starts with an isocahedron (12 vertices, 20 faces, 30 edges)
%    and iteratively divides all edges in two (faces in four).
%  - The algorithm stops whenever any one of the criteria in the input
%    arguments is met.
%  - Empty arguments are ignored.
%  - At least one input argument must be non-empty.
%  - We naively assume that all edges have equal length, which is only true for
%    subdivided edges (not for newly created edges).
%
  if nargin<2, maxpts=[]; end
  if nargin<3, minangle=[]; end
  if nargin<4, maxangle=[]; end

  % Start from an isocahedron with the vertex coordinates taken from the golden
  % ratio. So the vertices lie on the unit sphere.

  Phi = 2 * cos(pi/5);
  P   = [ ...
          0  Phi    1; ...
          0 -Phi    1; ...
          0  Phi   -1; ...
          0 -Phi   -1; ...
          1    0  Phi; ...
         -1    0  Phi; ...
          1    0 -Phi; ...
         -1    0 -Phi; ...
         Phi   1    0; ...
        -Phi   1    0; ...
         Phi  -1    0; ...
        -Phi  -1    0 ...
        ] ./ norm([1 Phi]);
  T   = [...
        1  3 10; ...
        1  5  9; ...
        1  6  5; ...
        1  9  3; ...
        1 10  6; ...
        2  4 11; ...
        2  5  6; ...
        2  6 12; ...
        2 11  5; ...
        2 12  4; ...
        3  7  8; ...
        3  9  7; ...
        3  8 10; ...
        4  7 11; ...
        4  8  7; ...
        4 12  8; ...
        5 11  9; ...
        6 10 12; ...
        7  9 11; ...
        8 12 10 ...
        ];

  % Determine the minimal number of iterations before any one of the conditions
  % in the input arguments is met.

  % #f(t) = #f(t-1) * 4        = 20 * (4^t)
  % #e(t) = #f(t) * 3 / 2      = 30 * (4^t)
  % #v(t) = #f(t) * 3 / 6 + 2  = 10 * (4^t) + 2
  %
  % +2 because five triangles join in each vertex in an isocahedron, but our
  % subdivision algorithm joins six triangles in each new vertex.
  %
  % a(t)  = a(t-1) / 2  = 2*atan2(1,phi) * 0.5^t  = atan2(1,phi) * 0.5^(t-1)

  numiter = Inf;

  if ~isempty(minpts)
    % 10*(4^t)+2 >= minpts
    t       = log((minpts-2)/10) / log(4);
    numiter = min(numiter, ceil(t));
  end
  if ~isempty(maxpts)
    % 10*(4^t)+2 <= maxpts
    t       = log((maxpts-2)/10) / log(4);
    numiter = min(numiter, floor(t));
  end
  if ~isempty(minangle)
    % atan2(1,phi) * 0.5^(t-1) >= minangle
    t       = 1 - log(minangle/atan2(1,Phi)) / log(2);
    numiter = min(numiter, floor(t));
  end
  if ~isempty(maxangle)
    % atan2(1,phi) * 0.5^(t-1) <= maxangle
    t       = 1 - log(minangle/atan2(1,Phi)) / log(2);
    numiter = min(numiter, ceil(t));
  end

  if numiter > 7
    warning('Mesh:unitball:ManyVertices', ...
            ['The number of vertices will exceed uint32 indexing, and' ...
            ' rendering will be slow. Just so you know.']);
  end

  % Iteratively divide all edges in two, and so the faces in four.

  for i = 1:numiter
    [P,T] = refine(P, T);
  end

  % Return a Mesh instance.

  ball = Mesh(P, T);
end


function [vertices, faces] = refine(vertices, faces)
% Divide all edges of the faces in two, placing new midpoints on the sphere.
%
% Input arguments:
%  VERTICES  Nx3 matrix of points on the unit sphere.
%  FACES     Mx3 matrix indexing triplets of vertices. The triangles describe
%            the surface of the sphere.
%
% Output arguments:
%  VERTICES  Px3 matrix of points. It includes the original N points. P>N.
%  FACES     Rx3 matrix of triangles. R=4*M. All original triangles have been
%            subdivided.
%
  numverts     = size(vertices, 1);

  % An edge connects two vertices: AB. Edge AB equals edge BA. To find the unique
  % edges, sort them such that A < B.
  edges        = reshape(faces(:,[1 2 3 2 3 1]), [], 2);
  edges        = unique(sort(edges, 2), 'rows');
  numedges     = size(edges, 1);

  % Each edge AB is split to form AC and CB. There will be exactly numedges
  % such splits. Therefore we also know their future vertex indices (appended
  % to the list of existing vertices). The mapping from AB -> C is stored in
  % vidxmap. For efficiency the map also includes AA -> A.
  vtxdiag      = 1:numverts;
  vidxmap      = sparse([edges(:,1); vtxdiag'], [edges(:,2); vtxdiag'], ...
                      [numverts+(1:numedges) vtxdiag], numverts, numverts, ...
                      numedges+numverts);

  % Each triangle is subdivided in four smaller triangles. If the big triangle
  % has vertices ABC and vidxmap maps AB -> D; BC -> E; CA -> F then the
  % smaller triangles will be ADF, DBE, FEC, and DEF after the mapping,
  % originating from the midpoints of edges:
  %    AA AB AC, BA BB BC, CA CB CC, CA AB BC, respectively.
  % In matrix form we write this as
  % from (=vidxa): A A A   to (=vidxb): A B C
  %                B B B                A B C
  %                C C C                A B C
  %                C A B                A B C
  vidxa        = faces(:,[1 2 3 3 1 2 3 1 1 2 3 2]);
  vidxb        = faces(:,[1 1 1 1 2 2 2 2 3 3 3 3]);
  % Need min and max because vidxmap is triangular (not symmetric).
  lin          = sub2ind(size(vidxmap), min(vidxa, vidxb), max(vidxa, vidxb));
  newedges     = full(vidxmap(lin));

  % Add midpoints of all edges to the existing vertices.
  % Replace old small set of big triangles with new big set of small triangles.
  newverts     = (vertices(edges(:,1),:) + vertices(edges(:,2),:)) ./ 2;
  vertices     = [vertices; rdiv2norm(newverts)];
  faces        = reshape(newedges, [], 3);
end
