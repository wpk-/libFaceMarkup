function resetBarycenters(tri)
% Reset any stored barycentric coordinates.
%
% When mesh properties change that affect the barycentric coordinates, we
% reset them to be recomputed when needed. These properties include:
%  - vertices   % TODO: implement setter function that calls resetBarycenters.
%  - faces      % TODO: implement setter function that calls resetBarycenters.
%
% Input arguments:
%  TRI  Mesh instance.
%
  D = tri.vertexdim;

  tri.has_barycenters_defined = false;
  tri.A = zeros(0, D);
  tri.V0 = zeros(0, D);
  tri.V1 = zeros(0, D);
end
