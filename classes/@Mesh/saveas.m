function saveas(tri, filename, format)
% Export the mesh to any known format.
%
% Input arguments:
%  TRI       Mesh instance.
%  FILENAME  String of the file name under which the mesh will be saved.
%  FORMAT    Optional string specifying the output file format.
%            Default value is the file extension of FILENAME.
%
  if nargin<3 || isempty(format)
    [~,~,ext] = fileparts(filename);
    format    = ext(2:end); % strip the period.
  end
  
  switch format
    case 'wrl'
      savefcn = @save_wrl;
    case 'obj'
      savefcn = @save_obj;
    case 'json'
      savefcn = @save_json;
    case 'tri'
      savefcn = @save_tri;
    otherwise
      error('Mesh file format not supported: %s', format);
  end
  
  savefcn(tri, filename);
end

