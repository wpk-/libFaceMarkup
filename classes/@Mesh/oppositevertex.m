function oppidx = oppositevertex(tri, vtxidx, ax, offset)
% Find vertex indices opposite to the given vertex indices,
% mirrored on specified axis.
%
% Input arguments:
%  TRI     Mesh instance.
%  VTXIDX  Mx1 array of vertex indices.
%  AX      Optional 3x1 unit vector specifying a direction for mirroring.
%          Default value is `[1; 0; 0]`, i.e., the x-axis.
%  OFFSET  Optional scalar to mark the point on AX that should reflect to
%          itself -- basically, where the mirror would be.
%          Default value is 0.
%
  if nargin<3 || isempty(ax), ax=[1 0 0]; end
  if nargin<4 || isempty(offset), offset=0; end

  ax = ax(:); % column vector.

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~ Reflect coordinates in the direction of `ax`.

  vtx = tri.vertices(vtxidx,:);
  vtx = vtx + 2 * (offset - (vtx * ax)) * ax';

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Find the nearest vertices.

  [~,oppidx] = pdist2(tri.vertices, vtx, 'Euclidean', 'Smallest',1);
end
