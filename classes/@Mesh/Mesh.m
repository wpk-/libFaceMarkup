classdef Mesh < handle
% Fast 3D Mesh representation based on half-edges.
%
% The class supports many functions for display and computation of mesh data.
%
% See:
% - private/halfedges.m  for details on the half-edge structure.
% - TODO: tests/runtest.m  for examples of all supported functions.
%
%
%
% TODO Look into:
%  - trimarkpoint
%  - precompute half-edge rep for texture.
%  - improve fillholes to keep texture mapping working.
%  -
%  - plotting functions for things as borders, peaks, contours, etc.
%

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Core properties.
  
  % Core properties are included when Matlab serialises the object. For example
  % when using Matlab's built-in `save(...)`, or when using a Mesh instance in
  % a parfor loop when it is loaded outside of the loop. All other properties
  % can be derived, these can't.
  %
  properties
    name;

    % FIXME: make setter functions to reset all stored private properties.
    %        when the size of vertices does not change we can keep nb and edges.
    vertices;       % VxD matrix of V cartesian coodinates.
    faces;          % Fx3 matrix of vertex indices.

    vertexcolor;    % Vx3 matrix of RGB colour values at each vertex.
    facecolor;      % Fx3 matrix of RGB colour values of the faces.
    texturecoords;  % Tx2 matrix of UV texture coordinates of the vertices.
    textureindices; % Fx3 matrix of triplets of texture coordinates.

    sourcefile;     % String holding the mesh file name.
    texturefile;    % String holding the texture file name.
  end

  properties
    % Vertex neighbourhood order for (ao.) mesh parameterisation.
    % `set.default_neighbourhood_order` resets the `parameters`.
    default_neighbourhood_order = 10;
  end

  % Not stored with mesh, but useful for debugging and testing.
  %
  properties
    verbose;
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Computed every time.

  % Dimensions.
  %
  properties (Dependent)
    facedim;      % Scalar of the number of vertices per face. Currently 3.
    numedges;     % Scalar of the number of edges in the mesh. = E.
    numfaces;     % Scalar of the number of faces in the mesh. = F.
    numvertices;  % Scalar of the number of vertices in the mesh. = V.
    vertexdim;    % Scalar of the vertex dimensionality. Probably always 3. = D.
  end

  % Curvature.
  %
  properties
    curvedness;   % Vx1 array of surface curvedness at the vertices.
    H;            % Vx1 array of mean surface curvature at the vertices.
    K;            % Vx1 array of Gaussian surface curvature at the vertices.
    k1;           % Vx1 array of the principal curvature at the vertices.
    k2;           % Vx1 array of the second principal curvature at the vertices.
    ktensor;      % Vx4 matrix of the curvature tensor at the vertices.
    shapeindex;   % Vx1 array of surface shape index at the vertices.
  end

  % Other.
  %
  properties (Dependent)
    areas;        % Fx1 array of face surface areas.
    edges;        % Ex2 matrix. Edge `i` connects vertices `edges(i,:)`.
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Computed on first use, then stored.

  % - defineBarycenters()
  %
  properties (SetAccess = protected)
    A;            % FxD matrix. The origins of Barycentric coordinates.
    V0;           % FxD matrix. The first Barycentric basis vector per face.
    V1;           % FxD matrix. The second Barycentric basis vector per face.
    has_barycenters_defined = false;  % true after `defineBarycenters()`.
  end

  % - defineHalfEdges()
  %
  properties (SetAccess = protected)
    f2he;         % Fx1 array, referencing (by index) one half-edge per face.
    halfedges;    % Hx4 matrix. A half-edge rep. See private/halfedges.m.
    v2he;         % Vx1 array, referencing (by index) one half-edge per vertex.
    has_halfedges_defined = false;  % true after `defineHalfEdges()`.
  end

  % - defineNeighbourhood()
  %
  % Neighbourhood properties are slightly different from the others. To get a
  % vertex neighbourhood use `tri.neighbourhood(nth_order)`. If `nth_order` is
  % of a value that gets cached, you are served a cached value from one of the
  % below private variables. Other neighbourhood orders are always computed.
  properties (Access = protected)
    nb1_;         % VxV sparse logical matrix of the vertex 1-neighbourhood.
    nb10_;        % VxV sparse logical matrix of the vertex 10-neighbourhood.
  end

  % - defineNormals()
  %
  % Surface normals are vectors pointing orthogonal to the surface tangent
  % plane. They are well defined on the faces (the triangles are planar), but
  % involve some estimation on the vertices, where essentially the surface is
  % discontinuous.
  properties %(SetAccess = protected)
    facenormals;  % FxD matrix of surface normal vectors at the faces.
    vertexnormals;% VxD matrix of surface normal vectors at the vertices.
    has_normals_defined = false;  % true after `defineNormals()`.
  end

  % - defineParameters()
  %
  % At each vertex we parameterize the surface using the following polynomial:
  %     z(x,y) = a*x^2 + b*xy + c*y^2 + d*x + e*y + f
  % where <x,y> spans the surface tangent plane at the vertex. Parameter
  % estimation for this equation is based on the local neighbourhood of points
  % around each vertex. Caution is advised changing the neighbourhood order,
  % as it requires the parameters to be re-estimated, which is costly.
  % The parameterisation is used e.g. for the surface curvatures.
  properties (SetAccess = protected)
    parameters;   % Vx6 matrix of degree 2 polynomial equations at the vertices.
    has_parameters_defined = false; % true after `defineParameters()`.
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %   METHODS                                                             %%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Class constructor.

  methods
    function tri = Mesh(vertices, faces, varargin)
      % Construct a mesh with given vertices and faces.
      %
      % Input arguments:
      %  VERTICES        VxD matrix of D-dimensional vertex coordinates.
      %  FACES           Fx3 matrix indexing triplets of vertices. Each row
      %                  defines a polygon and all polygons together define one
      %                  or more surfaces.
      %  TEXUREFILE      String specifying the file name of the texture image.
      %  TEXTURECOORDS   Tx2 matrix of (2D) UV coordinates on the texture image.
      %                  Coordinates range between 0 and 1 inclusive.
      %  TEXTUREINDICES  Fx3 matrix indexing triplets of texture points. Rows of
      %                  TEXTUREINDICES match those of FACES.
      %
      % Output arguments:
      %  TRI  A Mesh instance.
      %
      if nargin == 0
        % Something to do with `saveobj` and `loadobj` and also with declaring
        % arrays of objects like `m(100) = Mesh` for 100 meshes. Please, see
        % the Matlab documentation for more help.
        return;
      end
      
      p = inputParser;
      p.FunctionName = 'Mesh';
      p.addRequired('vertices',           @ismatrix);
      p.addRequired('faces',              @ismatrix);
      p.addOptional('texturefile',    '', @(x)ischar(x)||isnumeric(x));
      p.addOptional('texturecoords',  [], @ismatrix);
      p.addOptional('textureindices', [], @ismatrix);
      p.addOptional('isdirty',     false, @isscalar);
      p.parse(vertices, faces, varargin{:});
      
      if p.Results.isdirty
        [v,f,tc,ti] = cleandata(...
                      p.Results.vertices, ...
                      p.Results.faces, ...
                      p.Results.texturecoords, ...
                      p.Results.textureindices, ...
                      struct('min_edge_length', 1e-6, ...
                             'is_3dmd', true) ...
                      );
      else
        v  = p.Results.vertices;
        f  = double(p.Results.faces);
        tc = p.Results.texturecoords;
        ti = double(p.Results.textureindices);
      end
  
      tri.vertices       = v;
      tri.faces          = f;
      tri.texturecoords  = tc;
      tri.textureindices = ti;
      
      % XXX compute halfedges here, so it can throw an error.

      if ~isempty(p.Results.texturefile) && ~isempty(tc)
        tri.addtexture(p.Results.texturefile, tc, ti)
      elseif isempty(p.Results.texturefile) && ~isempty(tc) && isempty(ti)
        tri.vertexcolor = tc;
        tri.texturecoords = [];
      end
    end
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ IO.

  methods (Static)
    % Copy a Mesh or struct.
    tri = copy(tri0);
    % Load a Mesh instance from file.
    tri = load(filename, varargin);
    % Restore the Mesh from struct.
    tri = loadobj(obj);
  end
  
  methods
    % Export the mesh to JSON for THREE.js, including the annotation.
    export_threejs(tri, filename, anno);
    % Export the mesh to any known format.
    saveas(tri, filename, format);
    % Provide a minimal object to be stored, ignoring all dependent properties.
    obj = saveobj(tri);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Static methods return a Mesh.

  methods (Static)
    % Create half a unit ball, approximately uniformly sampled.
    tri = hemisphere(varargin);
    % Create a Mesh of the unit ball, approximately uniformly sampled.
    tri = unitball(minpts, maxpts, minangle, maxangle);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Plotting.

  methods
    % 3D surface plot of the mesh using a rectangular grid.
    varargout = quadsurf(tri, sz_shape, sz_texture, varargin);
    % Equivalent to Matlab's built-in trimesh(), but takes the Mesh as one argument.
    h = trimesh(tri, varargin);
    % Render the mesh as a white dull surface as if it was made of stone.
    [hmesh, hlight] = trisculpt(tri, varargin);
    % Equivalent to Matlab's built-in trisurf(), but takes the Mesh as one argument.
    h = trisurf(tri, varargin);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Return information about the mesh.

  methods
    % Convert points from Barycentric to Cartesian coordinates.
    P = bary2cart(tri, UV, fidx);
    % Find all loops of vertices around separate surfaces.
    [b, len] = borders(tri, fidx);
    % Express a point or vector in Barycentric coordinates U,V.
    [UV, inside] = cart2bary(tri, point, vector);
    % Find the point on the mesh closest to given point.
    [PPxyz,PPuv,fidx] = closestpoint(tri, Pxyz);
    % Find the stationary points of a given signal measured on the mesh.
    [ismin, ismax] = findpeaks(tri, signal, mask);
    % Computes the Laplacian matrix for the mesh.
    L = laplacian(tri, func);
    % Construct a sparse vertex neighbourhood matrix of the vertices.
    nb = neighbourhood(tri, nth_order);
    % Find vertex indices opposite to the given vertex indices.
    oppidx = oppositevertex(tri, vtxidx, ax, offset);
    % Find a mapping from each vertex to its mirror vertex.
    map = oppositevertex_topo(tri, seed);
    % Parameterize of the mesh surface using second order polynomial equations.
    params = parameterization(tri, nth_order);
    % Regular (grid) sampling over the mesh surface.
    [shp, tex] = quaddata(tri, sz_shape, sz_texture, method, t_uv, texture_image);
    % Smooth a signal defined over the mesh vertices.
    M = smoothmatrix(tri, order, kernel, mask);
    % Find the disjoint surfaces.
    [components, sizes] = surfaces(tri);
    % Find the disjoint surfaces in the texture map.
    [components, sizes] = texturecomponents(tri);
    % Compute a UV (2D) parameterisation for the vertices in the surface mesh.
    UV = uv(tri, shape, weights);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Modify the mesh.

  methods
    % Add colour data from a texture file.
    addtexture(tri, texturefile, texturecoords, textureindices);
    % For each surface, close all but the biggest outer edge loops.
    fillholes(tri);
    % Reduce the mesh to include only the selected faces.
    filterfaces(tri, fidx);
    % Reduce the mesh to include roughly the selected vertices.
    [vi, fi] = filtervertices(tri, vidx, fn_faces)
    % Tear and restore the mesh manifold structure.
    makemanifold(tri, include_texture);
    % Rotate the mesh by given quaternion.
    qrot(tri, Q);
    % Insert faces to reduce the vertex count on the outer border.
    smoothborders(tri);
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %   GETTERS AND SETTERS                                                 %%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Computed every time.

  methods
    function a = get.areas(tri)
      % Surface areas of each face.
      a = areas(tri);
    end
    function c = get.curvedness(tri)
      % Curvature: curvedness.
      params = tri.parameters;
      [k1_,k2_] = curvature_k1k2(params);
      c = curvature_C(k1_, k2_);
    end
    function e = get.edges(tri)
      % All undirected edges.
      e = edges(tri.halfedges);
    end
    function fd = get.facedim(tri)
      fd = size(tri.faces, 2);
    end
    function h = get.H(tri)
      % Curvature: mean curvature.
      params = tri.parameters;
      h = curvature_H(params);
    end
    function k = get.K(tri)
      % Curvature: Gaussian curvature.
      params = tri.parameters;
      k = curvature_K(params);
    end
    function k = get.k1(tri)
      % Curvature: major principal curvature.
      params = tri.parameters;
      k = curvature_k1k2(params);
    end
    function k = get.k2(tri)
      % Curvature: minor principal curvature.
      params = tri.parameters;
      [~,k] = curvature_k1k2(params);
    end
    function T = get.ktensor(tri)
      % Curvature: tensor.
      params = tri.parameters;
      T = curvature_T(params);
    end
    function ne = get.numedges(tri)
      ne = size(tri.edges, 1);
    end
    function nf = get.numfaces(tri)
      nf = size(tri.faces, 1);
    end
    function nv = get.numvertices(tri)
      nv = size(tri.vertices, 1);
    end
    function si = get.shapeindex(tri)
      % Curvature: shape index.
      params = tri.parameters;
      [k1_,k2_] = curvature_k1k2(params);
      si = curvature_SI(k1_, k2_);
    end
    function vd = get.vertexdim(tri)
      vd = size(tri.vertices, 2);
    end
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Computed on first use, then stored.

  % ~~~~~ defineBarycenters()
  methods
    function a = get.A(tri)
      if ~tri.has_barycenters_defined
        defineBarycenters(tri);
      end
      a = tri.A;
    end
    function v0 = get.V0(tri)
      if ~tri.has_barycenters_defined
        defineBarycenters(tri);
      end
      v0 = tri.V0;
    end
    function v1 = get.V1(tri)
      if ~tri.has_barycenters_defined
        defineBarycenters(tri);
      end
      v1 = tri.V1;
    end
  end

  % ~~~~~ defineHalfEdges()
  methods
    function fh = get.f2he(tri)
      if ~tri.has_halfedges_defined
        defineHalfEdges(tri);
      end
      fh = tri.f2he;
    end
    function he = get.halfedges(tri)
      if ~tri.has_halfedges_defined
        defineHalfEdges(tri);
      end
      he = tri.halfedges;
    end
    function vh = get.v2he(tri)
      if ~tri.has_halfedges_defined
        defineHalfEdges(tri);
      end
      vh = tri.v2he;
    end
  end

  % ~~~~~ defineNormals()
  methods
    function fn = get.facenormals(tri)
      if ~tri.has_normals_defined
        defineNormals(tri);
      end
      fn = tri.facenormals;
    end
    function vn = get.vertexnormals(tri)
      if ~tri.has_normals_defined
        defineNormals(tri);
      end
      vn = tri.vertexnormals;
    end
  end

  % ~~~~~ defineParameters()
  methods
    function params = get.parameters(tri)
      % Polynomial (quadratic) mesh parameterisation.
      if ~tri.has_parameters_defined
        defineParameters(tri);
      end
      params = tri.parameters;
    end
    function set.default_neighbourhood_order(tri, nth_order)
      % Detect changes to neighbourhood order.
      if nth_order ~= tri.default_neighbourhood_order
        resetParameters(tri);
        tri.default_neighbourhood_order = nth_order;
      end
    end
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %   DEPRECATIONS                                                        %%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  properties (Dependent)
    nb1;
    nb10;
  end
  methods
    function nb = get.nb1(tri)
      warning('`tri.nb1` is deprecated. Please use `tri.neighbourhood(1)` instead.');
      nb = tri.neighbourhood(1);
    end
    function nb = get.nb10(tri)
      warning('`tri.nb10` is deprecated. Please use `tri.neighbourhood(10)` instead.');
      nb = tri.neighbourhood(10);
    end
  end
end
