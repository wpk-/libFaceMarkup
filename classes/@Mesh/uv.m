function UV = uv(tri, shape, weights)
% Compute a UV (2D) parameterisation for the vertices in the surface mesh.
%
% Input arguments:
%  TRI      Mesh instance of a single surface.
%  SHAPE    Either 'square' (default) or 'circle'.
%  WEIGHTS  A choice of edge weights in the mesh Laplacian. Choices are all
%           values accepted by TRI.laplacian, e.g. 'cotan' (default), 'none',
%           'meanvalue' or 'area'. Cotangent weights depend on the geometry and
%           result in a conformal mapping (preserving angles).
%           Alternatively, the Laplacian matrix can be provided directly as an
%           NxN sparse matrix.
%
% Output arguments:
%  UV   Nx2 matrix of 2D vertex coordinates covering the square (or circle)
%       such that no triangle overlaps any other triangle.
%
% Considerations:
%  - The term UV is used in more places of the Mesh class, i.e. when dealing
%    with barycentric coordinates and for the texture mapping. The barycentric
%    coordinates define a separate UV system for each triangle. The texture
%    mapping is defined in the 3D file, and does not have to be a continuous
%    surface (the texture photo can e.g. be composed of multiple separate parts
%    even though the 3D surface is continuous).
%    By contrast, this function computes a single UV parameterisation for the
%    whole surface.
%  - Be aware that these different UV systems are *not* interchangeable.
%
% Relevant links:
%  - O. Sorkine, Laplacian Mesh Processing, EUROGRAPHICS - STAR, 2005.
%  - Chris Tralie: http://www.ctralie.com/Teaching/LapMesh/
%
  if nargin<2 || isempty(shape), shape='square'; end
  if nargin<3 || isempty(weights), weights='cotan'; end

  nvtx = tri.numvertices;
  vertices = tri.vertices;

  % ~~~~~ Vertex indices along the surface contour.
  %       The longest loop (at index 1) should be the outer loop.
  B = tri.borders();
  C = tri.halfedges(B{1},1);
  
  % ~~~~~ Symmetric Laplacian matrix.
  %       With cotangent edge weights to preserve angles and areas.
  if ischar(weights)
    L = tri.laplacian(weights);
  else
    L = weights;
  end

  % ~~~~~ UV (2D) coordinates for the contour points.
  switch lower(shape)
    case 'square'
      Cuv = uvsquare(vertices(C,:));
    case 'uniformsquare'
      Cuv = uniformsquare(vertices(C,:));
    case 'circle'
      Cuv = uvcircle(vertices(C,:));
    case {'free', 'none'}
      % Free boundary.
      % All UV are obtained from eigendecomposition directly.
      [UV,D] = eigs(L, 3, 'sm');
      [~,ix] = sort(abs(diag(D)));
      UV = UV(:,ix(2:3));
      return;
    case 'canonical'
      [C, Cuv] = uvcanonical(vertices, tri.normals, C);
    otherwise
      error('Unknown UV shape.');
  end
  
  % ~~~~~ Solve with boundary constraints.
  %       This gives the UV parameterisation.
  L(C,:) = 0;
  L(C,C) = speye(numel(C));
  R = zeros(nvtx, 2);
  R(C,:) = Cuv;
  UV = (L \ R);
end

function uv = uvcircle(pts)
% Evenly spaced points on the unit circle.
%
% Input arguments:
%  PTS  Nx3 matrix of points forming a path along the surface exterior (border).
%
% Output arguments:
%  UV  Nx2 matrix of (u,v) coordinates on the unit circle. The point in PTS
%      that aligns most closely with the x-axis is assigned coordinate (1,0),
%      the point aligning with the y-axis is assigned coordinate (0,1), and
%      (-1,0) and (0,-1) equally for the negatively aligned points. Between
%      these four "anchors" the remainder of points are spaced linearly with
%      their distances in 3D.
%
  npts = size(pts, 1);
  pts = bsxfun(@minus, pts, mean(pts,1));

  % ~~~~~ Find the best right, top,
  %       left, and bottom anchor points.
  fixed = [1 0; 0 1; -1 0; 0 -1];
  [~,ix] = pdist2(pts(:,1:2), fixed, 'Cosine', 'Smallest',1);

  % ~~~~~ Map each quadrant,
  %       spacing the points on the circle
  %       linearly with their 3D distances.
  %       `D` stores angles on the unit circle.
  ix = [ix ix(1)];
  D = zeros(npts, 1);
  for i = 1:4
    a = ix(i);
    b = ix(i+1);
    if b > a
      ab = a:b;
    else
      ab = [a:npts 1:b];
    end
    ab0 = ab(1:end-1);
    ab1 = ab(2:end);
    d = sqrt(sum((pts(ab1,:) - pts(ab0,:)) .^ 2, 2));
    D(ab0) = (pi/2) * [0; cumsum(d(1:end-1))] ./ sum(d) + (i-1)*pi/2;
  end

  % ~~~~~ Convert to Cartesian coordinates.
  [u,v] = pol2cart(D, 1);
  uv = [u,v];
end

function uv = uniformsquare(pts)
% Evenly spaced points on the contour of a square between (0,0) and (1,1).
%
% Input arguments:
%  PTS  Cartesian coordinates of points to be mapped onto the unit square.
%
% Output arguments:
%  UV  Nx2 matrix of (u,v) coordinates on the unit square. The points run
%      counter-clockwise along the square.
%
  npts = size(pts, 1);

  uv = zeros(npts, 2);
  ccw = [0 1; -1 0];
  b = 0;
  for i = 1:4
    n = floor((npts - b) ./ (5 - i));
    a = b + 1;
    b = b + n;
    ui = (1:n)' ./ n;
    rot = ccw ^ i;
    uvi = [ui zeros(size(ui))] * rot + double([-1 -1] * rot > 0);
    uv(a:b,:) = uvi;
  end
  
  tp = cart2pol(pts(:,1)-mean(pts(:,1)), pts(:,2)-mean(pts(:,2)));
  tu = cart2pol(uv(:,1)-0.5, uv(:,2)-0.5);
  tu = tu * ones(1, npts);
  for i = 2:npts
    tu(:,i) = circshift(tu(:,1), i, 1);
  end
  [~,k] = max(tp' * tu);
  uv = circshift(uv, k, 1);
end

function uv = uvsquare(pts)
% Points on the contour of a square between (0,0) and (1,1).
%
% Input arguments:
%  PTS  Cartesian coordinates of points to be mapped onto the unit square.
%
% Output arguments:
%  UV  Nx2 matrix of (u,v) coordinates on the unit square. The points run
%      counter-clockwise along the square.
%
  np = size(pts, 1);

  % Establish indices for the top right and top left hand vertices.
  k = top_right(pts, [pi/4; pi*3/4]);

  % Mark the vertices between the top right and top left hand corner.
  % Those will be forced to the UV range (1,1) - (0,1).
  ii = false(np, 1);
  if k(1) < k(2)
    ii(k(1)+1:k(2)) = true;
  else
    ii(k(1)+1:end) = true;
    ii(1:k(2)) = true;
  end
  
  % Distances between neighbouring points along the path.
  d = pts - circshift(pts, 1, 1);
  d = sqrt(dot(d, d, 2));
  % d = min(..., max(..., d));

  d(ii) = d(ii) ./ sum(d(ii));
  d(~ii) = 3 * d(~ii) ./ sum(d(~ii));

  d = circshift(d, -k(1), 1);
  uv = unit_square(d);
  uv = circshift(uv, k(1), 1);
end

function ix = top_right(pts, theta)
% Find the point that should map to the top-right of the UV space.
%
% The function reduces the set of points to those spanning the convex hull
% before matching theta. This gives a more robust selection.
%
% Input arguments:
%  PTS      NxD matrix of N points in D dimensions. This method only
%           considers the first two dimensions, through.
%  THETA    Optionally specify a different anchor point than the top right
%           hand corner by giving the desired angle. Defaults to pi/4
%           (=top right hand corner).
%           For advanced use, one may give a vector of desired angles and
%           this function will return a vector of indices.
%
% Output arguments:
%  IX       Index (1<=IX<=N) of the point that should map to the
%           top-right of the UV space.
%
  if nargin<2 || isempty(theta), theta = pi/4; end

  [tx, ty] = pol2cart(theta, ones(size(theta)));
  vt = [tx(:) ty(:)];

  k = convhull(pts(:,[1 2]));
  k = unique(k);
  vp = pts(k,[1 2]) - mean(pts(k,[1 2]), 1);

  [~, ix] = pdist2(vp, vt, 'cosine', 'Smallest',1);
  ix = k(ix);
end

function uv = unit_square(d)
% Places points on the [0,1] square, with point spacing proportional to D.
%
% Input arguments:
%  D        Nx1 array of point distances.
%
% Output arguments:
%  UV       Nx2 array of point coordinates on the [0,1] square. The
%           distance between consecutive points is proportional to the
%           distance provided in D. The points start at (1,1) and run
%           counter-clockwise.
%
  d = 4 * cumsum(d) ./ sum(d);

  ix = arrayfun(@(n) find(d <= n, 1, 'last'), (1:4)');
  assert(ix(end) == numel(d));

  ix0 = [0; ix(1:3)];
  sz = ix - ix0;

  d = mod(d, 1);
  d(ix) = 1;

  uv = [    1-d(1:ix(1))   ones(sz(1),1);
          zeros(sz(2),1)   1-d(ix(1)+1:ix(2));
        d(ix(2)+1:ix(3))   zeros(sz(3),1);
           ones(sz(4),1)   d(ix(3)+1:ix(4))];
end
