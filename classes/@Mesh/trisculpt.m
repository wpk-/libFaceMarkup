function [hmesh, hlight] = trisculpt(tri, varargin)
% Render the mesh as a white dull surface as if it was made of stone.
%
% Input arguments:
%  TRI  A Mesh instance.
%  C    Optional custom colour.
%       Default value is `[.9 .9 .9]`.
%
% Output arguments:
%  HMESH   Handle of the rendered mesh as returned by trisurf.
%  HLIGHT  Handle of the added light object.
%
 
  % Build argument list for trisurf().
  
  % - faces and vertices
  %
  args = {tri.faces, tri.vertices(:,1), tri.vertices(:,2), tri.vertices(:,3)};

  % - colour
  %
  if mod(nargin, 2) == 0	% C passed
    C         = varargin{1};
    varargin  = varargin(2:end);
  else
    C         = 0.9 * [1 1 1];
  end
  if ~isempty(C)
    % By default we set colour using FaceVertexCData,
    % unless that keyword is in use. In that case we let Matlab work it out.
    if ~any(strcmpi('FaceVertexCData', varargin(1:2:end)))
      args{end+1} = 'FaceVertexCData';
    end
    args{end+1} = C;
  end

  % - edge colour
  %
  E = strcmpi('EdgeColor', varargin(1:2:end));
  if ~any(E)
    args = [args {'EdgeColor','none'}];
  end

  % - other arguments
  %
  ax = find(strcmpi('Parent', varargin(1:2:end)));
  if ~isempty(ax)
    ax = varargin{ax+1};
  else
    ax = gca;
  end
  
  args = [args varargin];
  
  
  % Now draw the mesh.
  
  hmesh = trisurf(args{:});
  axis(ax, 'equal', 'vis3d');
  
  if size(C, 1) == tri.numvertices
    shading interp;
  end
  
  % - `material dull`
  set(hmesh, 'AmbientStrength',0.3, 'DiffuseStrength',0.8, ...
              'SpecularStrength',0.0, 'SpecularExponent',10, ...
              'SpecularColorReflectance',1.0);
  
  % - `lighting phong`
  set(hmesh, 'EdgeLighting','phong', 'FaceLighting','phong');
  
  % - add light
	hlight = light('Position',[0 0 1], 'Style','infinite', 'Parent',ax);
end
