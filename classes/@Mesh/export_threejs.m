function export_threejs(tri, filename, anno)
% Export the mesh to JSON for THREE.js, including the annotation.
%
% Input arguments:
%  TRI       Mesh instance.
%  FILENAME  String specifying the JSON file to be written.
%  ANNO      Annotation instance.
%
  if nargin<3 || isempty(anno), anno=[]; end
  save_json(filename, tri, anno);
end
