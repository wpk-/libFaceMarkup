function [vnormals, fnormals] = normals(tri)
% Compute vertex and face normals.
%
% Input arguments:
%  TRI  A Mesh instance.
%
% Output arguments:
%  VNORMALS  Nx3 matrix of normal vectors at the vertices.
%  FNORMALS  Mx3 matrix of normal vectors of the faces.
%
  vertices = tri.vertices;
  faces    = double(tri.faces);

  F    = faces(:);
  nvtx = max(F);

  % Each triangle has three corner points...
  X1   = vertices(faces(:,1),:);
  X2   = vertices(faces(:,2),:);
  X3   = vertices(faces(:,3),:);

  % ... making up three edges...
  E12  = X2 - X1;
  E23  = X3 - X2;
  E31  = X1 - X3;

  % ... with the following lengths.
  L12  = (E12 .^ 2) * [1; 1; 1];
  L23  = (E23 .^ 2) * [1; 1; 1];
  L31  = (E31 .^ 2) * [1; 1; 1];

  % Each vertex joins two edges of a face. The cross product of the edges is
  % a vector pointing in the face's normal direction. Dividing its length by
  % the product of the two squared edge lengths provides a good scale for
  % estimating vertex normals that is exact on a ball.
  % Consequently we have three estimates of the face normals. They all point in
  % the same direction, but their lengths may differ.
  N1   = bsxfun(@rdivide, cross(E12, -E31), (L12 .* L31));
  N2   = bsxfun(@rdivide, cross(E23, -E12), (L23 .* L12));
  N3   = bsxfun(@rdivide, cross(E31, -E23), (L31 .* L23));
  N    = [N1; N2; N3];

  % Compute vertex normals using sparse() to efficiently sum the adjoining face
  % normals.
  Nx   = full(sparse(F, 1, N(:,1), nvtx, 1));
  Ny   = full(sparse(F, 1, N(:,2), nvtx, 1));
  Nz   = full(sparse(F, 1, N(:,3), nvtx, 1));

  % Scale all normal vectors to unit length.
  vnormals                  = rdiv2norm([Nx Ny Nz]);
  vnormals(isnan(vnormals)) = 0;
  N1                        = rdiv2norm(N1);
  N2                        = rdiv2norm(N2);
  N3                        = rdiv2norm(N3);
  fnormals                  = nanmedian(cat(3,N1,N2,N3), 3);
  fnormals                  = rdiv2norm(fnormals);
  fnormals(isnan(fnormals)) = 0;
end
