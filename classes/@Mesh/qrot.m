function qrot(tri, Q)
% Rotate the mesh by given quaternion.
%
% Input arguments:
%  TRI  Mesh instance.
%  Q    Quaternion in 4x1 vector form.
%

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Set up functions.

  % Cross product of row vectors.
  crossrows = @(A,B) [ ...
    bsxfun(@times,A(:,2),B(:,3)) - bsxfun(@times,A(:,3),B(:,2)), ...
    bsxfun(@times,A(:,3),B(:,1)) - bsxfun(@times,A(:,1),B(:,3)), ...
    bsxfun(@times,A(:,1),B(:,2)) - bsxfun(@times,A(:,2),B(:,1))  ...
  ];

  % Quaternion multiplication (row vectors).
  qmul = @(Si,Vi,Sj,Vj) deal( ...
    bsxfun(@times,Si,Sj) - sum(bsxfun(@times,Vi,Vj),2), ...
    bsxfun(@times,Si,Vj) + bsxfun(@times,Sj,Vi) + crossrows(Vi,Vj) ...
  );

  % Quaternion norm (length) squared.
  qnorm_sq = @(S,V) dot([S V],[S V],2);

  % Quaternion reciprocal: `q*qi == 1`.
  qinv_ = @(S,V,L) deal(S./L, bsxfun(@rdivide,-V,L));
  qinv  = @(S,V) qinv_(S, V, qnorm_sq(S,V));

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Perform the rotation.

  % ~~~~~ Prepare the quaternion.
  Q         = reshape(Q, 1, 4);
  Qs        = Q(1);
  Qv        = Q(2:4);
  [Qis,Qiv] = qinv(Qs, Qv);

  % ~~~~~ Prepare the data.
  Ts        = zeros(tri.numvertices, 1);
  Tv        = tri.vertices;

  % ~~~~~ Rotate.
  [S,V]     = qmul(Qs, Qv, Ts, Tv);
  [~,V]     = qmul(S, V, Qis, Qiv);

  tri.vertices = V;

  % TODO: rotate normals and barycentric coordinates, if they are defined.
  resetBarycenters(tri);
  resetNormals(tri);
end
