function [UV, inside] = cart2bary(tri, coord, asvector)
% Express a point or vector in Barycentric coordinates U,V.
%
% Input arguments:
%  TRI       A Mesh instance.
%  COORD     1xD point in Cartesian coordinates.
%  ASVECTOR  Optional boolean flag to indicate COORD should be interpreted as
%            vector, emanating from each triangle's own local origin (TRI.A).
%            Default is false.
%
% Output arguments:
%  UV      Mx2 matrix expressing POINT (or VECTOR) in each of the M Barycentric
%          coordinate systems.
%  INSIDE  Mx1 logical array marking for each triangle whether it encloses the
%          POINT (or VECTOR).
%
% Considerations:
%  - Algorithm taken from:
%    http://www.blackpawn.com/texts/pointinpoly/default.html
%
	if nargin<3 || isempty(asvector), asvector=false; end

	V0         = tri.V0;
	V1         = tri.V1;
	V2         = coord;
	if ~asvector
		% If coord is not a vector yet, we have to convert it to one.
		V2       = bsxfun(@minus, coord, tri.A);
  end

  % TODO: precompute and cache dot00, dot01, dot11 and invDenom.
	dot00      = dot(V0, V0, 2);
	dot01      = dot(V0, V1, 2);
	dot11      = dot(V1, V1, 2);
	invDenom   = 1 ./ (dot00 .* dot11 - dot01 .* dot01);

	dot02      = dot(V0, V2, 2);
	dot12      = dot(V1, V2, 2);

	U          = (dot11.*dot02 - dot01.*dot12) .* invDenom;
	V          = (dot00.*dot12 - dot01.*dot02) .* invDenom;
	UV         = [U V];

	% Upon request we can find out if the point is inside any triangle.

	if nargout > 1
		% Candidate triangles that may contain our point.
		inside   = (U >= 0) & (V >= 0) & (U + V <= 1);
		if ~any(inside)
			t      = -1e-6;
			inside = (U >= t) & (V >= t) & (U + V <= 1+t);
		end

		% Reconstruct vector from Barycentric coordinates.
		vec      = bsxfun(@times, U(inside), V0(inside,:)) + ...
							bsxfun(@times, V(inside), V1(inside,:));

		% Measure error to filter out bad candidates.
		err      = V2(inside,:) - vec;
		outside  = sum(err.^2, 2) > 1e-10;
		ii       = find(inside);
		inside(ii(outside)) = false;
	end
end
