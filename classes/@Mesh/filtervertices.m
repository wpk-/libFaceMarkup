function [vi, fi] = filtervertices(tri, vidx, fn_faces)
% Reduce the mesh to include roughly the selected vertices.
%
% Input arguments:
%  TRI       A Mesh instance.
%  VIDX      Nx1 locical array or Kx1 index array marking the vertices to be
%            kept.
%  FN_FACES  Optional function handle to select which faces are kept based on
%            how many of its vertices are kept. In principle there are two
%            choices:
%            @all  keeps a face only when all its vertices are kept. Some
%                  vertices may become orphaned and will be removed despite
%                  VIDX marking them for inclusion.
%            @any  keeps a face when any of its vertices are kept. To keep
%                  those faces, some vertices that were marked to be discarded,
%                  according to VIDX, may still be kept.
%            Default is @all.
%
% Output arguments:
%  VI  Nx1 logical array marking which of the original vertices have been kept.
%  FI  Mx1 logical array marking which of the original faces have been kept.
%
	if nargin<3 || isempty(fn_faces), fn_faces=@all; end

	faces      = tri.faces;
	nvtx       = size(tri.vertices, 1);

	% Find out which faces to keep and, from that, which vertices to keep.

	vi         = false(nvtx, 1);
	vi(vidx)   = true;
	fi         = fn_faces(vi(faces), 2);

	vidx       = unique(faces(fi,:));
	vi(:)      = false;
	vi(vidx)   = true;

	% The rest is identical to filterfaces().

	tri.filterfaces(fi);
end
