function defineBarycenters(tri, a, v0, v1)
% For every triangle, construct an origin and two basis vectors.
%
% Input arguments:
%  TRI  Mesh instance.
%  A    Optional FxD matrix. The origins of Barycentric coordinates.
%  V0   Optional FxD matrix. The first Barycentric basis vector per face.
%  V1   Optional FxD matrix. The second Barycentric basis vector per face.
%
% Considerations:
%  - By default the barycentric coordinates are computed, but you can specify
%    manually computed data. If so, you must specify values for all three
%    variables A, V0, and V1.
%
  if tri.verbose
    fprintf('Define barycenters...');
    t0 = tic;
  end

  if nargin == 1
    [a,v0,v1] = barycenters(tri);
  elseif nargin ~= 4
    error('Either specify all A, V0 and V1, or none.');
  end

  tri.A = a;
  tri.V0 = v0;
  tri.V1 = v1;

  tri.has_barycenters_defined = true;

  if tri.verbose
    fprintf(' %.02f sec.\n', toc(t0));
  end
end
