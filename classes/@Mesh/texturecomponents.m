function [components, sizes] = texturecomponents(tri)
% Find the disjoint surfaces in the texture map.
%
% Input arguments:
%  TRI  A Mesh instance.
%
% Output arguments:
%  COMPONENTS  Mx1 array of surface labels. Face i is assigned to surface
%              COMPONENTS(i). The labels are ordered from 1, the largest
%              surface (face count), to K == max(COMPONENTS), the smallest
%              surface.
%  SIZES       Kx1 array counting per component their size in number of faces.
%
  [the,~,ti2the] = halfedges(tri.textureindices);
  [components,sizes] = surfaces(the, ti2the, tri.facedim);
end
