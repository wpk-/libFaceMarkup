function resetHalfEdges(tri)
% Reset the half-edge rep.
%
% When mesh properties change that affect the half-edge representation, we
% reset them to be recomputed when needed. These properties include:
%  - faces      % TODO: implement setter function that calls resetHalfEdges.
%
% Input arguments:
%  TRI  Mesh instance.
%
  tri.has_halfedges_defined = false;
  tri.f2he = zeros(0, 1);
  tri.halfedges = zeros(0, 4);
  tri.v2he = zeros(0, 1);
end
