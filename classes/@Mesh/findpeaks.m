function [ismin, ismax] = findpeaks(tri, signal, mask)
% Find the stationary points of a given signal measured on the mesh.
%
% Input arguments:
%  TRI      A Mesh instance.
%  SIGNAL   Nx1 array evaluating some underlying function at every vertex in
%           TRI.
%  MASK     Optional Nx1 logical array to ignore specific vertices in the
%           computation of peaks. Where `MASK(i) == false` then `SIGNAL(i)` is
%           simply ignored.
%           Default is `true(size(SIGNAL))`.
%
% Output arguments:
%  ISMIN  Nx1 logical vector marking the local minima of SIGNAL. Every vertex
%         TRI has a direct neighbour with a lower SIGNAL value, unless ISMIN(i)
%         is true.
%  ISMAX  Nx1 logical vector marking the local maxima of SIGNAL.
%
% Considerations:
%  - Vertices on the border are never returned as minima or maxima.
%  - Matlab has a built-in function findpeaks() that finds the peaks of a 1D
%    signal, whereas this function finds the peaks of a 2D signal evaluated on
%    a manifold. Despite the great similarity, one should be aware that the
%    arguments (both input and output) are so different that the functions
%    cannot easily be interchanged.
%
  if nargin<3 || isempty(mask), mask=true(size(signal)); end

  hedges      = tri.halfedges;

  outerhe     = isnan(hedges(:,2));
  outerv      = hedges(outerhe,1);
  innerhe     = ~outerhe;

  v1          = hedges(innerhe,1);
  v2          = hedges(hedges(innerhe,4),1);

  less        = signal(v1) < signal(v2);
  more        = signal(v1) > signal(v2);  % Fix for `signal(v1) == signal(v2)`.

  % A local minimum has no neighbours with a lower value.

  % Read the statement `v1(more & mask(v2))` like this:
  % * vertex 1 has higher value than vertex 2. -> thus "more" is true.
  % * that means vertex 1 cannot be a minimum.
  % * however, if vertex 2 has to be ignored (mask is false),
  %   then vertex 1 can still be a minimum.

  ismin                      = true(size(signal));
  ismin(v2(less & mask(v1))) = false;
  ismin(v1(more & mask(v2))) = false;
  ismin(outerv)              = false;

  % Upon request we also compute the local maxima.

  if nargout > 1
    ismax                       = true(size(signal));
    ismax(v1(less & ~mask(v2))) = false;
    ismax(v2(more & ~mask(v1))) = false;
    ismax(outerv)               = false;
  end
end
