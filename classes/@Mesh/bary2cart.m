function P = bary2cart(tri, UV, fidx)
% Convert points from Barycentric to Cartesian coordinates.
%
% Input arguments:
%  TRI   A Mesh instance.
%  UV    1x2 array expressing a point in each of the M Barycentric coordinate
%        systems. Alternatively, UV can be Mx2, to specify a different UV
%        coordinate for each different Barycentric coordinate system.
%  FIDX  Optional scalar index, or array of indices, or Mx1 binary mask to
%        select a subset of the coordinate systems to convert UV. This may save
%        overhead when you need a specific triangle.
%        Default is [], which does not preclude any coordinate system.
%
% Output arguments:
%  P  Mx3 matrix of points in Euclidean space.
%
	if nargin<3, fidx=[]; end

	A  = tri.A;
	V0 = tri.V0;
	V1 = tri.V1;

	if ~isempty(fidx)
		A  = A(fidx,:);
		V0 = V0(fidx,:);
		V1 = V1(fidx,:);
	end

	P = A + bsxfun(@times, UV(:,1), V0) + ...
					bsxfun(@times, UV(:,2), V1);
end
