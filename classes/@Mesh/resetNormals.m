function resetNormals(tri)
% Reset all vertex and face normals.
%
% When mesh properties change that affect the surface normals, we
% reset them to be recomputed when needed. These properties include:
%  - vertices   % TODO: implement setter function that calls resetNormals.
%  - faces      % TODO: implement setter function that calls resetNormals.
%
% Input arguments:
%  TRI  Mesh instance.
%
  D = tri.vertexdim;

  tri.has_normals_defined = false;
  tri.facenormals = zeros(0, D);
  tri.vertexnormals = zeros(0, D);
end
