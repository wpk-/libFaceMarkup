function defineHalfEdges(tri, hedges, v2he, f2he)
% Construct the half-edge structure.
%
% Input arguments:
%  TRI        Mesh instance.
%  HALFEDGES  Optional Hx4 matrix. A half-edge rep. See private/halfedges.m.
%  V2HE       Optional Vx1 array, referencing one half-edge per vertex.
%  F2HE       Optional Fx1 array, referencing (by index) one half-edge per face.
%
% Considerations:
%  - By default HALFEDGES, V2HE and F2HE are computed, but you can specify
%    manually computed data. If so, you must pass values for all three
%    variables.
%
  if tri.verbose
    fprintf('Define half-edges...');
    t0 = tic;
  end

  if nargin == 1
    [hedges,v2he,f2he] = halfedges(tri.faces);
  elseif nargin ~= 4
    error('Either specify all HALFEDGES, V2HE and F2HE, or none.');
  end

  tri.halfedges = hedges;
  tri.v2he = v2he;
  tri.f2he = f2he;

  tri.has_halfedges_defined = true;

  if tri.verbose
    fprintf(' %.02f sec.\n', toc(t0));
  end
end
