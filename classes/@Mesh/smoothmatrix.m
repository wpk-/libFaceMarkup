function W = smoothmatrix(tri, order, kernel, mask)
% Smooth a signal defined over the mesh vertices.
%
% Input arguments:
%  TRI     Mesh instance, with N vertices.
%  ORDER   Optional scalar specifying the neighbourhood order around each
%          vertex.
%          Default value is `TRI.default_neighbourhood_order`.
%  KERNEL  Optional function over a matrix of Euclidean distances, should
%          return a matrix of the same size.
%          Default value is `@(d)normpdf(d,0,10)`.
%  MASK    Optional Nx1 logical vector marking a subset of the vertices over
%          which to perform smoothing.
%          Default is `true(N,1)`.
%
% Output arguments:
%  W  NxN sparse matrix with rows summing up to one. If `X` is a Nx1 vector (a
%     signal over the vertices of the mesh) then `W * X` is a smoothed copy.
%
  if nargin<2 || isempty(order), order=tri.default_neighbourhood_order; end
  if nargin<3 || isempty(kernel), kernel=@(d)normpdf(d,0,10); end
  if nargin<4 || isempty(mask), mask=[]; end
  
  numvtx = tri.numvertices;
  
  tim = @noop;%@tic;
  tom = @noop;%@toc;
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Vertex neighbourhood.
% Timings. Neighbourhood 10, 7, 5, 4, 3
tim();  % 0.00, 1.76, 0.83, 0.57, 0.33
  nb = tri.neighbourhood(order);
tom();
tim();  % 0.00, 0.00, 0.00, 0.00, 0.00
  if ~isempty(mask)
    nb(~mask,~mask) = 0;
  end
tom();
tim();  % 0.28, 0.16, 0.09, 0.06, 0.04
	[r,c] = find(nb);
tom();
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Vertex distances.
tim();  % 2.80, 1.49, 0.81, 0.52, 0.35
  vert_r = tri.vertices(r,:);
	vert_c = tri.vertices(c,:);
	dist = sqrt(sum((vert_r - vert_c) .^ 2, 2));
tom();
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Smoothing matrix.
  
  % Weigh the distances, and
  % make rows sum up to one.
tim();  % 0.49, 0.25, 0.12, 0.12, 0.09
	w = kernel(dist);
tom();
tim();  % 4.03, 2.08, 1.12, 0.76, 0.49
	denom = full(sparse(r, 1, w, numvtx, 1));
	nil = denom < 1e-10;
	denom(nil) = 1;
	w = w ./ denom(r);
tom();
  % Return as NxN sparse matrix.
tim();  % 1.24, 0.66, 0.36, 0.25, 0.15
	W = sparse(r, c, w, numvtx, numvtx);
tom();
% Total timings:
% 9.1, 6.5, 3.4, 2.3, 1.5
end
