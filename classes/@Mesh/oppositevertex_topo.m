function map = oppositevertex_topo(tri, seed)
% Find a mapping from each vertex to its mirror vertex,
% given an initial triangle symmetry.
% (that is, three vertex indices with their mirror indices)
%
% Input arguments:
%  TRI   Mesh instance, with a symmetric topology.
%  SEED  3x2 array of paired vertex indices. The first column describes a
%        triangle on one side, the second comumn describes the same triangle
%        on the opposite side.
%        Default is [10021 10021; 16828 16840; 243 243]; which works for the
%        current symmetric face model.
%
% Output arguments:
%  MAP  Nx1 array of vertex indices. Where `MAP(i) == j` the index of the
%       vertex opposite to vertex `i` is `j`. Consequently, `MAP(j) == i` and
%       `MAP(MAP(i)) == i`.
%
% Considerations:
%  - The mesh topology MUST be symmetric and a (single) surface.
%
  if nargin<2 || isempty(seed)
    seed = [
      10021 10021;
      16828 16840;
        243   243;
    ];
  end

  topology = tri.faces;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Seed the vertex mirror mapping.

  nvtx = max(topology(:));
  map  = zeros(nvtx, 1);

  map(seed(:,1)) = seed(:,2);
  map(seed(:,2)) = seed(:,1);

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Complete the mapping.

  % For any triangle where we know the mapping for two vertices
  % and not the third, find the opposite triangle for which the third
  % vertex opposite is also unknown   -> fill in the blanks.

  f = double(topology');

  while any(map == 0)
    i      = sum(~map(f)) == 1;		% Triangles with 1 unknown opposite vtx.
    j      = reshape(f([1 2 3 1 3 2 2 1 3 2 3 1 3 1 2 3 2 1],i), 3, []);
    s      = sparse(j(1,:), j(2,:), 1);
    err    = s > 1;
    s      = sparse(j(1,:), j(2,:), j(3,:));
    s(err) = 0;

    if ~any(s(:))
      % there are conflicting definitions for the opposite vertex
      % the mesh is not topologically symmetric
      error('The mesh topology must be symmetric.');
    else
      % That test may not be sufficient if the mesh is not symmetric
      % but since this function only makes sense for symmetric
      % topologies in first place, let's not worry about it too much.
  % 		fprintf('%d ', nnz(s));
      % We will just get stuck in an infinite loop.
    end

    fi = f(:,i);
    fm = map(fi);
    %ab = reshape(fi(fm>0), 2, []);  % Vertices with known opposites.
    c  = fi(fm==0);                 % Vertex with unknown opposite.
    xy = reshape(fm(fm>0), 2, []);	% Known opposite vertices.
    z  = diag(s(xy(1,:), xy(2,:)));	% Unknown opposite vertex.

    c  = c(z>0);
    z  = z(z>0);

    map(c) = z;
  end
end
