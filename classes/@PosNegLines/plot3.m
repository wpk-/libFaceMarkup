function h = plot3(pnl, varargin)
% Plot all line segments.
%
% Input arguments:
%  PNL       PosNegLines instance.
%  LSPOS     Optional string providing a LineSpec for the positive lines.
%            Default value is `''`.
%  LSNEG     Optional string providing a LineSpec for the negative lines.
%            Default value is `''`.
%  VARARGIN  Optional keyword arguments to Matlab's built-in `plot3`.
%
% Output arguments:
%  H  Handle to the plotted data as returned by Matlab's built-in `plot3`.
%
% Considerations:
%  - To plot a selection of the lines, first extract that selection using
%    `PNL.subset` (see that function's documentation for details).
%  - To plot to a specific axis use the keyword-value pair arguments:
%    `plot3(pnl, ..., 'Parent',hax)`.
%
  p = inputParser;
  p.FunctionName = 'plot3';
  p.KeepUnmatched = true;
  p.addRequired('pnl',       @(x)isa(x,'PosNegLines'));
  p.addOptional('lspos', '', @islinespec);
  p.addOptional('lsneg', '', @islinespec);
  p.parse(pnl, varargin{:});

  varargs = struct2kwargs(p.Unmatched);

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Prepare `plot3` arguments.

  coords = pnl.coordinates;

  x = coords(:,1);
  y = coords(:,2);
  z = coords(:,3);

  ixpos = pnl.ixpos';
  ixneg = pnl.ixneg';

  % ~~~~~ Pos line segments.

%   
%   ptpos = coords(pnl.ixpos(:),:);
%   xpos = reshape(ptpos(:,1), npos, 2)';
%   ypos = reshape(ptpos(:,2), npos, 2)';
%   zpos = reshape(ptpos(:,3), npos, 2)';
%   lspos = p.Results.lspos;
  npos = pnl.numpos;
  xpos = reshape([x(ixpos); nan(1,npos)], 3*npos, 1);
  ypos = reshape([y(ixpos); nan(1,npos)], 3*npos, 1);
  zpos = reshape([z(ixpos); nan(1,npos)], 3*npos, 1);
  lspos = p.Results.lspos;

  % ~~~~~ Neg line segments.

%   nneg = pnl.numneg;
%   ptneg = coords(pnl.ixneg(:),:);
%   xneg = reshape(ptneg(:,1), nneg, 2)';
%   yneg = reshape(ptneg(:,2), nneg, 2)';
%   zneg = reshape(ptneg(:,3), nneg, 2)';
%   lsneg = p.Results.lsneg;
  nneg = pnl.numneg;
  xneg = reshape([x(ixneg); nan(1,nneg)], 3*nneg, 1);
  yneg = reshape([y(ixneg); nan(1,nneg)], 3*nneg, 1);
  zneg = reshape([z(ixneg); nan(1,nneg)], 3*nneg, 1);
  lsneg = p.Results.lsneg;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Plot everything.

  h_ = plot3(xpos, ypos, zpos, lspos, xneg, yneg, zneg, lsneg, varargs{:});

  % ~~~~~ Only return handle on request,
  %       to prevent ugly output in Matlab.

  if nargout > 0
    h = h_;
  end
end
