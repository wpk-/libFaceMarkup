function s = saveobj(pnl)
% Return a minimal struct representation of the PosNegLines.
%
% Input arguments:
%  PNL  PosNegLines instance.
%
% Output arguments:
%  S  Struct representation of PNL.
%
  props = {
    'coordinates';
    'curvature_points';
    'ixneg';
    'ixpos';
    'has_indices_defined';  % must come after 'ixneg' or 'ixpos'.
    'verbose';
  };

  % ~~~~~ Convert `coordinates` to sparse matrix,
  %       setting all unreferenced points to zero.

  pnl = PosNegLines.copy(pnl);
  pnl.compress();

  % ~~~~~ Create struct.

  for i = 1:numel(props)
    s.(props{i}) = pnl.(props{i});
  end
end
