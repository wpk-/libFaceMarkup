function pnl = loadobj(s)
% Restore the PosNegLines from a saved state (struct).
%
% Input arguments:
%  S  Struct representation of PosNegLines.
%
% Output arguments:
%  PNL  PosNegLines instance.
%
  if isstruct(s)
    pnl = PosNegLines.copy(s);
  else
    pnl = s;
  end
end
