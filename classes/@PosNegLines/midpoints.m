function [midpos, midneg] = midpoints(pnl, ipos, ineg)
% Compute the line segment midpoints.
%
% Input arguments:
%  PNL   PosNegLines instance.
%  IPOS  Optional array indexing rows of `PNL.ixpos` to compute the midpoints
%        of a specific selection of the pos line segments.
%        By default mid points for all pos line segments are returned.
%  INEG  Optional array indexing rows of `PNL.ixneg` to compute the midpoints
%        of a specific selection of the neg line segments.
%        By default mid points for all neg line segments are returned.
%
% Output arguments:
%  MIDPOS  Px3 array of points. The i-th point is the midpoint of the i-th
%          positive line segment.
%  MIDNEG  Qx3 array of points. The i-th point is the midpoint of the i-th
%          negative line segment.
%
% Considerations:
%  - The second output argument is optional, and not computed unless requested.
%
  ends = pnl.coordinates;
  ixpos = pnl.ixpos;
  ixneg = pnl.ixneg;

  if nargin > 1 && ~isempty(ipos)
    ixpos = ixpos(ipos,:);
  end
  if nargin > 2 && ~isempty(ineg)
    ixneg = ixneg(ineg,:);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Find the line segment midpoints.

  midpos = (ends(ixpos(:,1),:) + ends(ixpos(:,2),:)) ./ 2;

  % ~~~~~ For negative line segments
  %       only when requested.

  if nargout > 1
    midneg = (ends(ixneg(:,1),:) + ends(ixneg(:,2),:)) ./ 2;
  end
end
