classdef PosNegLines < handle
% Extract "positive" and "negative" line segments from meshes.
%
% Line segments are formed of point pairs. Since N points can define (N^2)/2
% line segments, it saves a lot of memory to store the line segments as
% references to pairs of points.
%
% "Positive" and "negative" lines are a concept from the Intrinsic Coordinate
% System (see @MeshICS), where the end points of line segments are defined by
% stationary points of the Gaussian surface curvature. Those points can be of
% three types: cups, caps, and saddles. Line segments that connect points of
% the same type are labeled "positive" and those connecting different types are
% labeled "negative".
%
  properties
    curvature_points; % CurvaturePoints instance.
  end

  properties
    verbose;      % Scalar. Set to true to print timings etc.
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~ Properties computed on first use, then stored.

  properties
    % Note: Pairs of points form line segments. Points are caps, cups, or
    % saddles. Pairs of points of the same type form positive line segments.
    ixneg;        % Ix2 matrix of pairs of curvature points of different type.
    ixpos;        % Jx2 matrix of pairs of curvature poitns of the same type.
    has_indices_defined = false;% True after `ixneg` and `ixpos` have been set.
  end

  properties
    % `coordinates` ALWAYS returns `curvaturepoints.coordinates`, UNLESS
    % `isempty(curvaturepoints)`, then it will return its own value. The
    % coordinates are copied over automatically in methods that dereference
    % `curvaturepoints` (such as `compress`).
    % Note that `coordinates` can be sparse (after `compress()` for example).
    coordinates;  % Nx3 matrix of point coordinates (line segment endpoints).
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Properties computed every time.

  properties (Dependent)
    negvec;       % Ix3 matrix of vectors of the negative line segments.
    numneg;       % Scalar, = I.
    numpos;       % Scalar, = J.
    posvec;       % Jx3 matrix of vectors of the positive line segments.
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Constructor.
  
  methods
    function pnl = PosNegLines(cp, varargin)
      % Create a PosNegLines instance.
      %
      % Input arguments:
      %  CP  CurvaturePoints instance.
      %  OPTIONS  Optional keyword value argument pairs / struct.
      %     'Verbose'  Scalar. Set to true to print some timings etc.
      %
      % Output arguments:
      %  PNL  PosNegLines instance.
      %
      if nargin == 0
        % Allow Matlab tricks like `x(100) = PosNegLines`.
        return;
      end

      p = inputParser;
      p.FunctionName = 'PosNegLines';
      p.addRequired('cp', @(x)isa(x,'CurvaturePoints'));
      p.addParameter('Verbose', false, @isscalar);
      p.parse(cp, varargin{:});

      pnl.verbose = p.Results.Verbose;

      if pnl.verbose
        fprintf('[PosNegLines]\n');
        fprintf('Constructor...');
        t0 = tic;
      end

      pnl.curvature_points = cp;

      if pnl.verbose
        fprintf(' time: %.2f sec.\n', toc(t0));
      end
    end
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ I/O.

  methods (Static)
    % Shallow copy a PosNegLines instance.
    pnl = copy(pnl0);
    % Restore the PosNegLines from a saved state (struct).
    pnl = loadobj(s);
  end

  methods
    % Return a minimal struct representation of the PosNegLines.
    s = saveobj(pnl);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Visualisation.

  methods
    % Plot all line segments.
    h = plot3(pnl, varargin);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Modification.

  methods
    % Extract necessary vertices into sparse `coordinates`, dereference `curvature_points`.
    compress(pnl);
    % Flip all line segments at obtuse angles with `direction`.
    orient(pnl, direection);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Information.

  methods
    % Compute the line segment midpoints.
    [midpos, midneg] = midpoints(pnl, ipos, ineg);
    % Return a PosNegLines instance with a subset of the current line segments.
    pnl2 = subset(pnl, ipos, ineg);
    % Find all pos and neg vectors near parallel to VEC.
    pnl2 = supporting(pnl, vec, posangle, negangle);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Getters and setters.

  % ~~~~~ Computed and stored.

  methods
    function pts = get.coordinates(cp)
      if isempty(cp.curvature_points)
        pts = cp.coordinates;
      else
        pts = cp.curvature_points.coordinates;
      end
    end
    function ix = get.ixneg(pnl)
      if ~pnl.has_indices_defined
        defineIndices(pnl);
      end
      ix = pnl.ixneg;
    end
    function ix = get.ixpos(pnl)
      if ~pnl.has_indices_defined
        defineIndices(pnl);
      end
      ix = pnl.ixpos;
    end
  end

  % ~~~~~ Computed every time.

  methods
    function vec = get.negvec(pnl)
      % Get all negative vectors.
      pts = pnl.coordinates;
      ix = pnl.ixneg;
      vec = pts(ix(:,2),:) - pts(ix(:,1),:);
    end
    function num = get.numneg(pnl)
      % Count the number of negative line segments.
      num = size(pnl.ixneg, 1);
    end
    function num = get.numpos(pnl)
      % Count the number of negative line segments.
      num = size(pnl.ixpos, 1);
    end
    function vec = get.posvec(pnl)
      % Get all positive vectors.
      pts = pnl.coordinates;
      ix = pnl.ixpos;
      vec = pts(ix(:,2),:) - pts(ix(:,1),:);
    end
  end
end
