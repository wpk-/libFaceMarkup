function defineIndices(pnl)
% Extract all positive and negative line segments from `curvature_points`.
%
% Input arguments:
%  PNL  PosNegLines instance.
%
  cp = pnl.curvature_points;

  ixcup = cp.ixcup;
  ixcap = cp.ixcap;
  ixsad = cp.ixsaddle;

  ncups = numel(ixcup);
  ncaps = numel(ixcap);
  nsads = numel(ixsad);

  if pnl.verbose
    fprintf('Define pos/neg indices...');
    t0 = tic;
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Connect all pairs of points.

  % ~~~~~ Positive line segments
  %       connect points of the same type.

  % With fewer than 2 items `nchoosek` will balk.
  if ncups>1, posA = nchoosek(ixcup, 2); else posA = []; end
  if ncaps>1, posB = nchoosek(ixcap, 2); else posB = []; end
  if nsads>1, posC = nchoosek(ixsad, 2); else posC = []; end

  % ~~~~~ Negative line segments
  %       connect points of different type.

  [negA1,negB1] = meshgrid(ixcup, ixcap);
  [negA2,negC1] = meshgrid(ixcup, ixsad);
  [negB2,negC2] = meshgrid(ixcap, ixsad);

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Save on `pnl`.

  pnl.ixpos = [posA; posB; posC];
  pnl.ixneg = [negA1(:) negB1(:); negA2(:) negC1(:); negB2(:) negC2(:)];

  pnl.has_indices_defined = true;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Verbosity & visualisation.

  if pnl.verbose
    fprintf(' time: %.2f sec.\n', toc(t0));
  end
end
