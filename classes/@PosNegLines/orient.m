function orient(pnl, direction)
% Flip all line segments at obtuse angles with `direction`.
%
% Input arguments:
%  PNL        PosNegLines instance.
%  DIRECTION  Vector. Any pos/neg vector at an angle > 90 degrees will
%             be flipped.
%
  direction = direction(:)';
  posvec = pnl.posvec;
  negvec = pnl.negvec;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Flip all obtuse angled vectors.

  % ~~~~~ Pos vectors.

  angles = pdist2(posvec, direction, 'cosine');
  flip = angles > 1; % 'cosine' returns 1-cos(alpha)
  pnl.ixpos(flip,:) = pnl.ixpos(flip,[2 1]);

  % ~~~~~ Neg vectors.

  angles = pdist2(negvec, direction, 'cosine');
  flip = angles > 1;
  pnl.ixneg(flip,:) = pnl.ixneg(flip,[2 1]);
end
