function compress(pnl)
% Extract necessary vertices into sparse `coordinates`, dereference `curvature_points`.
%
% Input arguments:
%  PNL  PosNegLines instance.
%
  coords = pnl.coordinates;
  ixpos = pnl.ixpos;
  ixneg = pnl.ixneg;

  if pnl.verbose
    fprintf('Compress pos/neg lines structure...');
    t0 = tic;
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Find and extract the necessary points.

  [npts,ndim] = size(coords);
  allix = unique([ixpos(:); ixneg(:)]);
  numix = numel(allix);

  rows = allix * ones(1, ndim);
  cols = ones(numix, 1) * (1:ndim);
  coords = sparse(rows, cols, coords(allix,:), npts, ndim);

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Save on `pnl`.

  pnl.coordinates = coords;
  pnl.curvature_points = [];

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Verbosity & visualisation.

  if pnl.verbose
    fprintf(' %.2f sec.\n', toc(t0));
  end
end
