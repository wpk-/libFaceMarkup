function pnl = copy(pnl0)
% Shallow copy a PosNegLines instance.
%
% Input arguments:
%  PNL0  PosNegLines instance, or struct with the same fields.
%
% Output arguments:
%  PNL  PosNegLines instance.
%
% Considerations:
%  - Because the copy is a shallow copy, and because CurvaturePoints is a
%    handle class, `CP.curvature_points` will reference the same instance
%    as `PNL0.curvature_points`.
%
  props = {
    'coordinates';
    'curvature_points';
    'ixneg';
    'ixpos';
    'has_indices_defined';  % must come after 'ixneg' or 'ixpos'.
    'verbose';
  };

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Copy all necessary properties.

  pnl = PosNegLines();

  for i = 1:numel(props)
    pnl.(props{i}) = pnl0.(props{i});
  end

  % ~~~~~ `coordinates` will always return `curvature_points.coordinates`,
  %       unless `isempty(curvature_points)`.

  if ~isempty(pnl.curvature_points)
    pnl.coordinates = [];
  end
end
