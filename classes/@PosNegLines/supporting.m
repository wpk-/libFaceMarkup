function pnl2 = supporting(pnl, vec, posangle, negangle)
% Find all pos and neg vectors near parallel to VEC.
%
% Input arguments:
%  PNL       PosNegLines instance.
%  VEC       3x1 or 1x3 vector defining the desired direction.
%  POSANGLE  Optional scalar of the maximum angle, in radians, between VEC and
%            the returned positive vectors.
%            Default is 0, meaning that if you do not specify this value
%            (pass `[]`), no positive line segments will be returned.
%  NEGANGLE  Optional scalar of the maximum angle, in radians, between VEC and
%            the returned negative vectors.
%            Default is 0. See POSANLGE comment.
%
% Output arguments:
%  PNL2  PosNegLines instance. It is a shallow copy of PNL with any line
%        segment removed whose angle to VEC is not within the specified range.
%
% Considerations:
%  - Only acute angles are measured. Vectors at obtuse angles are, in a sense,
%    first flipped. So vectors at an angle greater than `pi - maxangle`
%    qualify.
%
  if nargin<3 || isempty(posangle), posangle=0; end
  if nargin<4 || isempty(negangle), negangle=0; end

  vec = vec(:)';

  if posangle < pi/2
    ipos = abs(1 - pdist2(pnl.posvec, vec, 'cosine')) > cos(posangle);
  else
    ipos = true(pnl.numpos, 1);
  end

  if negangle < pi/2
    ineg = abs(1 - pdist2(pnl.negvec, vec, 'cosine')) > cos(negangle);
  else
    ineg = true(pnl.numneg, 1);
  end

  pnl2 = pnl.subset(ipos, ineg);
end
