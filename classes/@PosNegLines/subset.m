function pnl2 = subset(pnl, ipos, ineg)
% Return a PosNegLines instance with a subset of the current line segments.
%
% Input arguments:
%  PNL   PosNegLines instance.
%  IPOS  Px1 array indexing rows of `PNL.ixpos` to be carried over.
%  INEG  Qx1 array indexing rows of `PNL.ixneg` to be carried over.
%
% Output arguments:
%  PNL2  New PosNegLines instance with the selected subset of lines.
%
% Considerations:
%  - Because `PNL.ixpos` and `PNL.ixneg` are themselves indices (into
%    `PNL.points`), the arguments IPOS and INEG are indices of indices. Beware!
%
  pnl2 = PosNegLines.copy(pnl);

  pnl2.ixpos = pnl.ixpos(ipos,:);
  pnl2.ixneg = pnl.ixneg(ineg,:);
end
