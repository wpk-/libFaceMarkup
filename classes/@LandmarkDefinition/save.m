function save(ld, filename)
% Save the landmark definition to file (with optional comment line).
%
% Input arguments:
%  LD        LandmarkDefinition instance.
%  FILENAME  String specifying the file name to write to. The file extension
%            should be .lnddef. If FILENAME specifies no file extension,
%            ".lnddef" is appended.
%
  [fld,name,ext] = fileparts(filename);
  if isempty(ext)
    ext = '.lnddef';
  end
  filename = fullfile(fld, [name ext]);
  
  comment = ld.comment;
  comment = regexprep(comment, '\r\n|\r|\n', '$1# ');
  comment = strcat('# ', comment);
  
  numbers = 1:numel(ld.lxmap);
  labels = LandmarkEncyclopedia.landmarkid2label(numbers);
  data = [num2cell(numbers); labels(:)'];
  
  fid = fopen(filename, 'w');
  if ~isempty(comment)
    fprintf(fid, comment);
  end
  fprintf(fid, '%2d. %s\n', data{:});
  fclose(fid);
end
