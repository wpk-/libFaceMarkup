function ld = load(filename)
% Load a landmark definition from file (.lnddef).
%
% Input arguments:
%  FILENAME  String.
%
% Output arguments:
%  LD  LandmarkDefinition instance.
%

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Cache.
  
  % It is very wasteful to load a new LandmarkDefinition for each of the 1000s
  % of annotation files in a folder. They will all call LandmarkDefinition.load
  % with the exact same argument, so we can cache the return value and return a
  % reference if we loaded it before.
  
  argin = filename;
  
  persistent ldcache;
  if isempty(ldcache)
    ldcache = cell(0,2);
  end
  
  ixcache = find(strcmp(argin, ldcache(:,1)), 1);
  if ~isempty(ixcache)
    ld = ldcache{ixcache,2};
    return;
  end
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Locate the landmark definition.
  
  if ~exist(filename, 'file')
    [~,name] = fileparts(filename);
    filename2 = [filename '.lnddef'];
    if exist(filename2, 'file')
      filename = filename2;
    else
      ld = LandmarkDefinition.fromname(name);
      if isempty(ld)
        error('File not found: %s', filename);
      end
      return;
    end
  end
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Read all properties.
  
  % 1. Name
  [~,name] = fileparts(filename);
  
  str = fileread(filename);
  
  % 2. Comment
  pathead1 = '^(#\s?.+?\n)+';
  pathead2 = '^#\s?';
  comment = regexp(str, pathead1, 'match');
  if ~isempty(comment)
    comment = regexprep(comment{1}, pathead2, '', 'lineanchors');
  end
  
  % 3. Landmark definition
  patdata = '^\s*(\d+)[ \t,.]+([^#]+?)(?:#.*)?$';
  data = regexp(str, patdata, 'tokens', 'lineanchors');
  data = cat(1, data{:});
  ixlocal = cellfun(@str2double, data(:,1));
  labels = strtrim(data(:,2));
  
  % 3a. Link to The Landmark Encyclopedia
  numpts = max(ixlocal);
  lidx = nan(numpts, 1);
  lidx(ixlocal) = LandmarkEncyclopedia.landmarklabel2id(labels);
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Cache and return the instance.
  
  ld = LandmarkDefinition(name, lidx, comment);
  
  ldcache(end+1,:) = {argin, ld};
end
