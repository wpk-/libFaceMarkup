classdef LandmarkDefinition < handle
% A formal definition of your personal landmarks.
%
% Most people have their own set of numbers assigned to various landmarks. The
% LandmarkDefinition class provides frictionless translation between different
% numbering system, by linking the personal landmarks to a universal landmark
% set, appropriately called "The Landmark Encyclopedia".
% 
% Making this work with your personal landmarks goes as follows:
%  1. Suppose you use four landmarks: the inner corner of the left eye, of the
%     right eye, the nose tip, and the chin tip. In your application these
%     landmarks are numbered 1, 2, 3, and 4 respectively.
%  2. Look up the exact text labels in landmark_encyclopedia.xml, they are:
%     "left eye inner corner", "right eye inner corner", "nose tip", and
%     "chin tip".
%  3. Create a file named something like myset.lnddef (or myset-4.lnddef to be
%     verbose about the number of landmarks you use).
%  4. Write the following lines to that file:
%        # A comment here to say who this definition comes from.
%        1. left eye inner corner
%        2. right eye inner corner
%        3. nose tip
%        4. chin tip
%  5. This file now defines your landmarks exactly. Moreover, this definition,
%     readable by a computer (`LandmarkDefinition.load(filename)`), allows your
%     landmarks to be compared to any other set of landmarks.
%  6. The last step is to reference this landmark definition when you load your
%     landmark data from file. A file format that explicitly adds a field for
%     this is the .lnd file format, taking away the worry of specifying the
%     right landmark definition. Other file formats don't, so they take an
%     extra argument to specify this (or resort to a default value):
%     .lnd - The top line can be a comment field specifying the name of the
%            landmark definition like so:
%            # lnddef: name
%            Now, if a file "name.lnddef" exists in the same folder as the
%            landmark file, the definition is loaded from that and used for
%            reading the landmark file. Alternatively, if name is a well known
%            name, defined in the LandmarkDefinition class, it can be loaded
%            from there without the file being available on the filesystem
%            (although this is not the desired approach because it is less
%            open).
%            Using `Annotation.load(filename)` and `anno.saveas(filename)` does
%            all this automatically.
%     .raw - Load the annotation file using `Annotation.load(filename, lnddef)`
%            where lnddef is either the file name for the .lnddef file, or it
%            is a LandmarkDefinition instance.
%     .lm3 - Same as for .raw.
%
% What if you are using landmarks that are not defined in The Landmark
% Encyclopedia?
% --> PLEASE ADD THEM, preferably by emailing p.koppen@surrey.ac.uk, but you
%     could try to edit the file manually too.
%
  properties (SetAccess=private)
    comment;  % String. Landmark definitions have an optional comment.
    lxmap;    % Nx1 array. Local landmark `i` is universal landmark `LXMAP(i)`.
    name;     % String. Name of this definition, which is the bare file name.
  end
  
  % Class constructor and Matlab internals.
  
  methods
    function ld = LandmarkDefinition(name, lxmap, comment)
      % Class constructor.
      %
      % Input arguments:
      %  NAME     Unique name for "Your" definition.
      %  LXMAP    Nx1 (sparse) array mapping "Your" landmark number `i` to
      %           Universal landmark number `LXMAP(i)`
      %           See @LandmarkDefinition/universe for the universal numbers.
      %  COMMENT  Optional string to add a comment line.
      %
      % Output arguments:
      %  LD  LandmarkDefinition instance.
      %
      if nargin<3 || isempty(comment), comment=''; end
      
      ld = ld@handle();
      
      ld.name = name;
      ld.lxmap = lxmap;
      ld.comment = comment;
    end
  end

  % Static methods of all known LandmarkDefinitions.
  
  methods (Static)
    function ld = feng7()
      % 7 landmarks used for the IEEE FG 2018 Dense 3D Reconstruction Competition.
      [nm,lidx] = lx_feng7;
      ld = LandmarkDefinition(nm, lidx);
    end
    function ld = ibug68()
      % iBug group (Imperial) landmark definition.
      [nm,lidx] = lx_ibug68;
      ld = LandmarkDefinition(nm, lidx);
    end
    function ld = koppen36()
      % Paul Koppen (Surrey) landmark definition (extension of Tena-26).
      [nm,lidx] = lx_koppen36;
      ld = LandmarkDefinition(nm, lidx);
    end
    function ld = perakis8()
      % Facial Landmarks Annotation [Perakis et al.] landmark definition.
      [nm,lidx] = lx_perakis8;
      ld = LandmarkDefinition(nm, lidx);
    end
    function ld = ruiz14()
      % Conny's [Ruiz] landmark definition.
      [nm,lidx] = lx_ruiz14;
      ld = LandmarkDefinition(nm, lidx);
    end
    function ld = savran26()
      % Bosphorus 3D Face Database [Savran et al.] landmark definition.
      [nm,lidx] = lx_savran26;
      ld = LandmarkDefinition(nm, lidx);
    end
    function ld = tena26()
      % Morphable Model [Tena et al.] landmark definition (pts 1..26).
      [nm,lidx] = lx_tena26;
      ld = LandmarkDefinition(nm, lidx);
    end
    function ld = universe()
      % The exhaustive list of all landmarks in the universe.
      ency = LandmarkEncyclopedia.instance();
      lidx = [ency.landmarks{:,2}];
      ld = LandmarkDefinition('Universe', lidx);
    end
    function ld = xm2vts68()
      % XM2VTS landmark definition.
      [nm,lidx] = lx_xm2vts68;
      ld = LandmarkDefinition(nm, lidx);
    end
    function ld = yin8()
      % BU-3DFE [Yin et al.] landmark definition (8 points).
      [nm,lidx] = lx_yin8;
      ld = LandmarkDefinition(nm, lidx);
    end
    function ld = yin83()
      % BU-3DFE [Yin et al.] extended landmark definition (83 points).
      [nm,lidx] = lx_yin83;
      ld = LandmarkDefinition(nm, lidx);
    end
    function ld = yin85()
      % BU-3DFE [Yin et al.] complete landmark definition (85 points).
      [nm,lidx] = lx_yin85;
      ld = LandmarkDefinition(nm, lidx);
    end
  end
  
  % Other static methods.
  
  methods (Static)
    % Return the matching landmark numbers to convert between definitions.
    [lidxfrom, lidxto] = conversion(ldfrom, ldto);
    % Return the landmark definition with name 'name'.
    ld = fromname(name, default);
    % Load a landmark definition from file (.lnddef).
    ld = load(filename);
    % Print the landmark numbering conversion to screen for debugging.
    print_conversion(ldfrom, ldto);
  end
  
  % Methods that return information.
  
  methods
    % Print the current definition to screen.
    print(ld);
  end
end
