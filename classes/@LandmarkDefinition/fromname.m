function ld = fromname(name, default)
% Return the landmark definition with name 'name'.
%
% Input arguments:
%  NAME  String.
%
% Output arguments:
%  LD       LandmarkDefinition instance with `LD.name == NAME`.
%  DEFAULT  Optional LandmarkDefinition instance to be returned when no
%           definition with name NAME exists.
%           Default value is `[]`.
%
% Considerations:
%  - All landmark definitions are (currently) stored as a static method on
%    LandmarkDefinition, and there is a direct mapping from the landmark
%    definition's name to the method name:
%     1. convert the name to lower case
%     2. remove all characters that are not letters a-z or numbers 0-9.
%     3. the resulting string is the method name.
%
  if nargin<2 || isempty(default), default=[]; end
  
  try
    mname = lower(name);
    mname = regexp(mname, '[\w\d]*', 'match');
    mname = strcat(mname{:});
    ld    = LandmarkDefinition.(mname);
  catch
    ld    = default;
  end
end
