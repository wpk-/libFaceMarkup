function print(ld)
% Print the current definition to screen.
%
% Input arguments:
%  LD  LandmarkDefinition instance.
%
  fprintf('Your ID   Universal ID   Description\n');
  fprintf('-------   ------------   ------------------------\n');

  [lx0,~,ulx] = find(ld.lxmap(:));
  desc        = LandmarkEncyclopedia.landmarkid2label(ulx);

  for i = 1:numel(lx0)
    fprintf('%6d%15d     %s\n', lx0(i), ulx(i), desc{i});
  end

  fprintf('-----\n');
  fprintf('LandmarkDefinition: %s\n', ld.name);
end
