function [name, lidx] = lx_yin85()
% BU-3DFE [Yin et al.] extended landmark definition (83 points).
%
% Output arguments:
%  NAME  Unique name for this definition.
%  LIDX  Nx1 (sparse) array mapping the BU-3DFE extended landmark number `i` to
%        Universal landmark number `LIDX(i)` (see @LandmarkDefinition/universe).
%
% See also:
%  http://www.cs.binghamton.edu/~lijun/Research/3DFE/3DFE_Analysis.html
%
% # 83 landmarks defined for the BU-3DFE data set by L. Yin.
%  1. right eye inner corner
%  2. right eye top inner third
%  3. right eye middle top
%  4. right eye top outer third
%  5. right eye outer corner
%  6. right eye bottom outer third
%  7. right eye middle bottom
%  8. right eye bottom inner third
%  9. left eye inner corner
% 10. left eye top inner third
% 11. left eye middle top
% 12. left eye top outer third
% 13. left eye outer corner
% 14. left eye bottom outer third
% 15. left eye middle bottom
% 16. left eye bottom inner third
% 17. right eyebrow inner corner top
% 18. right eyebrow inner quarter top
% 19. right eyebrow middle top
% 20. right eyebrow outer quarter top
% 21. right eyebrow outer corner top
% 22. right eyebrow outer corner bottom
% 23. right eyebrow outer quarter bottom
% 24. right eyebrow middle bottom
% 25. right eyebrow inner quarter bottom
% 26. right eyebrow inner corner bottom
% 27. left eyebrow inner corner top
% 28. left eyebrow inner quarter top
% 29. left eyebrow middle top
% 30. left eyebrow outer quarter top
% 31. left eyebrow outer corner top
% 32. left eyebrow outer corner bottom
% 33. left eyebrow outer quarter bottom
% 34. left eyebrow middle bottom
% 35. left eyebrow inner quarter bottom
% 36. left eyebrow inner corner bottom
% 37. nose bridge right
% 38. nose right saddle
% 39. right nose wing top
% 40. nose right lip junction
% 41. right nostril outside
% 42. right nostril inside
% 43. left nostril inside
% 44. left nostril outside
% 45. nose left lip junction
% 46. left nose wing top
% 47. nose left saddle
% 48. nose bridge left
% 49. mouth right corner
% 50. upper lip right quarter top
% 51. upper lip top right inner third
% 52. upper lip middle top
% 53. upper lip top left inner third
% 54. upper lip left quarter top
% 55. mouth left corner
% 56. lower lip left quarter bottom
% 57. lower lip bottom left inner third
% 58. lower lip middle bottom
% 59. lower lip bottom right inner third
% 60. lower lip right quarter bottom
% 61. mouth right corner inside
% 62. upper lip right quarter bottom
% 63. upper lip middle bottom
% 64. upper lip left quarter bottom
% 65. mouth left corner inside
% 66. lower lip left quarter top
% 67. lower lip middle top
% 68. lower lip right quarter top
% 69. right face contour 0
% 70. right face contour 1
% 71. right face contour 2
% 72. right face contour 3
% 73. right face contour 4.5
% 74. right face contour 6
% 75. right face contour 7
% 76. chin bottom
% 77. left face contour 7
% 78. left face contour 6
% 79. left face contour 4.5
% 80. left face contour 3
% 81. left face contour 2
% 82. left face contour 1
% 83. left face contour 0
% 84. nose tip
% 85. nose lip junction
%
  name = 'Yin-85';
  lidx = [ ...
    5 76 6 75 1 77 7 78 ...                               % right eye
    8 80 9 79 2 81 10 82 ...                              % left eye
    90 91 92 93 94 95 96 97 98 99 ...                     % right eyebrow
    100 101 102 103 104 105 106 107 108 109 ...           % left eyebrow
    110 41 112 47 71 72 74 73 48 113 42 111 ...           % nose contour
    12 31 83 14 85 35 13 38 86 17 84 34 ...               % lips outer contour
    87 32 15 36 88 37 16 33 ...                           % lips inner contour
    114 49 50 51 115 54 55 89 63 62 117 59 58 57 116 ...  % face contour
    3 29]';                                               % nose tip and 29.
end
