function [name, lidx] = lx_ruiz14()
% Conny's [Ruiz] landmark definition.
%
% Output arguments:
%  NAME  Unique name for this definition.
%  LIDX  Nx1 (sparse) array mapping Conny's landmark number `i` to Universal
%        landmark number `LIDX(i)` (see @LandmarkDefinition/universe).
%
% See also:
%  Do we have documentation of Conny's 14 landmarks? Her thesis perhaps. Ask
%  John Illingworth.
%
  name = 'Ruiz-14';
  lidx = [1 2 3 4 5 8 11 13 12 14 17 21 25 26]';
end
