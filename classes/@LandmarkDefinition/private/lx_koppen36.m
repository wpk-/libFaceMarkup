function [name, lidx] = lx_koppen36()
% Morphable Model [Tena et al.] landmark definition (pts 1..26),
% extended with landmarks around the ears by Paul Koppen.
%
% Output arguments:
%  NAME  Unique name for this definition.
%  LIDX  Nx1 (sparse) array mapping the Morphable Model landmark number `i` to
%        Universal landmark number `LIDX(i)` (see @LandmarkDefinition/universe).
%
% See also:
%  morphModel.pdf
%
%   1     Right eye outer corner.
%   2     Left eye outer corner.
%   3     Nose tip.
%   4     Chin tip.
%   5     Right eye inner corner.
%   6     Right eye middle top.
%   7     Right eye middle bottom.
%   8     Left eye inner corner.
%   9     Left eye middle top.
%  10     Left eye middle bottom.
%  11     Nose bridge.
%  12     Right mouth corner.
%  13     Left mouth corner.
%  14     Upper lip middle top.
%  15     Upper lip middle bottom.
%  16     Lower lip middle top.
%  17     Lower lip middle bottom.
%  18     Right eyebrow outer corner.
%  19     Right eyebrow inner corner.
%  20     Right eyebrow middle.
%  21     Right nose-cheek junction.
%  22     Left eyebrow outer corner.
%  23     Left eyebrow inner corner.
%  24     Left eyebrow middle.
%  25     Left nose-cheek junction.
%  26     Chin dip.
% 121     Right ear top junction
% 120     Right ear topmost
% 119     Right ear outermost
% 118     Right ear lobe bottom
%  39     Right ear jaw junction
% 125     Left ear top junction
% 124     Left ear topmost
% 123     Left ear outermost
% 122     Left ear lobe bottom
%  40     Left ear jaw junction
%
  name = 'Koppen-36';
  lidx = [1:26 121:-1:118 39 125:-1:122 40]';
end
