function [name, lidx] = lx_savran26()
% Bosphorus 3D Face Database [Savran et al. 2008] landmark definition.
%
% Output arguments:
%  NAME  Unique name for this definition.
%  LIDX  Nx1 (sparse) array mapping Bosphorus landmark number `i` to Universal
%        landmark number `LIDX(i)` (see @LandmarkDefinition/universe).
%
  name = 'Savran-26';
  lidx = [1 2 3 4 5 8 ...
          12 13 14 15 16 17 18 19 ...
          20 22 23 24 ...
          38 39 ...
          41 42 43 44 45 46]';
end
