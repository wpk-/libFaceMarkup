function [name, lidx] = lx_perakis8()
% Facial Landmarks Annotation [Perakis et al.] landmark definition.
%
% Output arguments:
%  NAME  Unique name for this definition.
%  LIDX  Nx1 (sparse) array mapping Perakis landmark number `i` to Universal
%        landmark number `LIDX(i)` (see @LandmarkDefinition/universe).
%
% See also:
%  Readme.txt shipped with Perakis landmarks:
%  "We use a set of eight anatomical landmarks with the following IDs:
%
%   (1) right eye outer corner (REOC),
%   (2) right eye inner corner (REIC),
%   (3) left eye inner corner (LEIC),
%   (4) left eye outer corner (LEOC),
%   (5) nose tip (NT),
%   (6) mouth right corner (MRC),
%   (7) mouth left corner (MLC) and
%   (8) chin tip (CT)."
%
  name = 'Perakis-8';
  lidx = [1 5 8 2 3 12 13 4]';
end
