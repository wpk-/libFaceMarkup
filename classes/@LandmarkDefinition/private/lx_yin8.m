function [name, lidx] = lx_yin8()
% BU-3DFE [Yin et al.] landmark definition (8 points).
%
% Output arguments:
%  NAME  Unique name for this definition.
%  LIDX  Nx1 (sparse) array mapping the BU-3DFE simple landmark number `i` to
%        Universal landmark number `LIDX(i)` (see @LandmarkDefinition/universe).
%
% See also:
%  http://www.cs.binghamton.edu/~lijun/Research/3DFE/3DFE_Analysis.html
%
% # 8 landmarks defined for the BU-3DFE data set by L. Yin.
% 1. right eye inner corner
% 2. right eye outer corner
% 3. left eye inner corner
% 4. left eye outer corner
% 5. nose right lip junction
% 6. nose left lip junction
% 7. nose tip
% 8. nose lip junction
%
  name = 'Yin-8';
  lidx = [5 1 8 2 47 48 3 29]';
end
