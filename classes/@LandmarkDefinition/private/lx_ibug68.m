function [name, lidx] = lx_ibug68()
% iBug group (Imperial) landmark definition.
%
% Output arguments:
%  NAME  Unique name for this definition.
%  LIDX  Nx1 (sparse) array mapping the iBug group landmark number `i` to
%        Universal landmark number `LIDX(i)` (see @LandmarkDefinition/universe).
%
% See also:
%  https://sites.google.com/view/pasclandmark
%
% # 68 landmarks defined by the iBug group.
%  1. right face contour 1
%  2. right face contour 2
%  3. right face contour 3
%  4. right face contour 4
%  5. right face contour 5
%  6. right face contour 6
%  7. right face contour 7
%  8. right face contour 8
%  9. chin bottom
% 10. left face contour 8
% 11. left face contour 7
% 12. left face contour 6
% 13. left face contour 5
% 14. left face contour 4
% 15. left face contour 3
% 16. left face contour 2
% 17. left face contour 1
% 18. right eyebrow outer corner
% 19. right eyebrow outer quarter
% 20. right eyebrow middle
% 21. right eyebrow inner quarter
% 22. right eyebrow inner corner
% 23. left eyebrow inner corner
% 24. left eyebrow inner quarter
% 25. left eyebrow middle
% 26. left eyebrow outer quarter
% 27. left eyebrow outer corner
% 28. nose bridge
% 29. nose profile one third
% 30. nose profile two thirds
% 31. nose tip
% 32. right nostril outside
% 33. right nostril inside
% 34. nose lip junction
% 35. left nostril inside
% 36. left nostril outside
% 37. right eye outer corner
% 38. right eye top outer third
% 39. right eye top inner third
% 40. right eye inner corner
% 41. right eye bottom inner third
% 42. right eye bottom outer third
% 43. left eye inner corner
% 44. left eye top inner third
% 45. left eye top outer third
% 46. left eye outer corner
% 47. left eye bottom outer third
% 48. left eye bottom inner third
% 49. mouth right corner
% 50. upper lip right quarter top
% 51. upper lip top right inner third
% 52. upper lip middle top
% 53. upper lip top left inner third
% 54. upper lip left quarter top
% 55. mouth left corner
% 56. lower lip left quarter bottom
% 57. lower lip bottom left inner third
% 58. lower lip middle bottom
% 59. lower lip bottom right inner third
% 60. lower lip right quarter bottom
% 61. mouth right corner inside
% 62. upper lip right quarter bottom
% 63. upper lip middle bottom
% 64. upper lip left quarter bottom
% 65. mouth left corner inside
% 66. lower lip left quarter top
% 67. lower lip middle top
% 68. lower lip right quarter top
%
  name = 'iBug-68';
  lidx = [49 50 51 52 53 54 55 56 89 64 63 62 61 60 59 58 57 ... % face contour
          18 65 20 66 19 ...                      % right eyebrow
          23 68 24 67 22 ...                      % left eyebrow
          11 69 70 3 ...                          % nose profile
          71 72 29 74 73 ...                      % nose bottom
          1 75 76 5 78 77 ...                     % right eye
          8 80 79 2 81 82 ...                     % left eye
          12 31 83 14 85 35 13 38 86 17 84 34 ... % mouth outer contour
          87 32 15 36 88 37 16 33 ...             % mouth inner contour
          ]';
end
