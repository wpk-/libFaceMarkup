function [name, lidx] = lx_feng7()
% 7 landmarks used for the IEEE FG 2018 Dense 3D Reconstruction Competition.
%
% Output arguments:
%  NAME  Unique name for this definition.
%  LIDX  Nx1 (sparse) array mapping Feng's landmark number `i` to Universal
%        landmark number `LIDX(i)` (see @LandmarkDefinition/universe).
%
  name = 'Feng-7';
  lidx = [1 5 8 2 3 12 13]';
end
