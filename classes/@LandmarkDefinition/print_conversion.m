function print_conversion(ldfrom, ldto)
% Print the landmark numbering conversion to screen for debugging.
%
% Input arguments:
%  LDFROM  LandmarkDefinition instance.
%  LDTO    LandmarkDefinition instance.
%
  [lidxfrom, lidxto] = LandmarkDefinition.conversion(ldfrom, ldto);
  
  data = cell(3, numel(ldfrom.lxmap));
  data(1,:) = num2cell(1:numel(ldfrom.lxmap));
  data(2,:) = {''};
  data(2,lidxfrom) = arrayfun(@(n)num2str(n), lidxto', 'UniformOutput',0);
  data(3,:) = LandmarkEncyclopedia.landmarkid2label(ldfrom.lxmap)';

  n1 = max(5, numel(ldfrom.name));
  n2 = max(5, numel(ldto.name));
  n3 = 30;

  s1 = repmat('-', 1, n1);
  s2 = repmat('-', 1, n2);
  s3 = repmat('-', 1, n3);
  s4 = repmat('-', 1, n1-2);

  p1 = sprintf('%%%ds   %%%ds   %%s\n', n1, n2);
  p2 = sprintf('%%%dd%%%ds     %%s\n', n1-2, n2+3);
  p3 = '%s\n';
  p4 = 'Landmark conversion from %s to %s.\n';
  
  fprintf(p1, ldfrom.name, ldto.name, 'Meaning');
  fprintf(p1, s1, s2, s3);
  fprintf(p2, data{:});
  fprintf(p3, s4);
  fprintf(p4, ldfrom.name, ldto.name);
  %fprintf('NB. Only landmarks defined in both systems are returned.\n');
end
