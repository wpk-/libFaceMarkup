function [lidxfrom, lidxto] = conversion(ldfrom, ldto)
% Return the matching landmark numbers to convert between definitions.
%
% Input arguments:
%  LDFROM  LandmarkDefinition instance.
%  LDTO    LandmarkDefinition instance.
%
% Output arguments:
%  LIDXFROM  Nx1 array of indices of the landmarks to copy over.
%  LIDXTO    Nx1 array of the new indices of the landmarks selected in LIDXFROM.
%
  [~,lidxfrom,lidxto] = intersect(ldfrom.lxmap, ldto.lxmap);
end
