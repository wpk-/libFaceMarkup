classdef (ConstructOnLoad) EvtPointAtMesh < event.EventData
  %POINTATOBJECTEVENT Summary of this class goes here
  %   Detailed explanation goes here

  properties
    Button;
    FaceIndex;
    Position;
  end

  methods
    function data = EvtPointAtMesh(button, faceindex, position)
      data.Button = button;
      data.FaceIndex = faceindex;
      data.Position = position;
    end
  end
end
