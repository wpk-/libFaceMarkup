classdef KeyboardContext
  %KEYBOARDCONTEXT Summary of this class goes here
  %   Detailed explanation goes here

  properties
    actions;
    hfig;
    keys;
  end

  methods
    function kc = KeyboardContext(hfig, actionset)
      kc.hfig = hfig;
      if nargin>1 && ~isempty(actionset)
        kc.keys = actionset(:,1);
        kc.actions = actionset(:,2);
      end
      kc.attach();
    end
  end

  methods
    function attach(kc)
      set(kc.hfig, 'KeyPressFcn',@kc.onKeyPress);
    end
    function detach(kc)
      set(kc.hfig, 'KeyPressFcn',[]);
    end
  end

  methods
    function onKeyPress(kc, hfig, eventdata)
      % Character, Modifier, Key
      % TODO: rewrite key to add modifier.
      fire = kc.actions(strcmpi(eventdata.Key, kc.keys));
      for i = 1:numel(fire)
        fire{i}(hfig, eventdata);
      end
    end
  end
end
