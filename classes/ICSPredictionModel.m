classdef (Abstract) ICSPredictionModel < handle
% Abstract class for all ICS models.
%
% ICS models learn and predict for a given feature vector, its (angular)
% distance to the face's lateral axis.
%
% This class serves as super class, so we can do type checking:
% `isa(x, 'ICSPredictionModel')`.
%

%   properties
%   end
  
  methods
    [Y, stds] = predict(mdl, X);
    train(mdl, X, Y);
  end
end
