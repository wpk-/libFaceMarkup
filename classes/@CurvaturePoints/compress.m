function compress(cp)
% Extract necessary vertices into sparse `coordinates`, dereference `mesh`.
%
% Input arguments:
%  CP  CurvaturePoints instance.
%
  coords = cp.coordinates;
  ixcup = cp.ixcup;
  ixcap = cp.ixcap;
  ixsad = cp.ixsaddle;
  K = cp.K;
  H = cp.H;

  if cp.verbose
    fprintf('Compress curvature points structure...');
    t0 = tic;
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Find and extract the necessary points.

  [npts,ndim] = size(coords);
  allix = unique([ixcup(:); ixcap(:); ixsad(:)]);  
  numix = numel(allix);

  rows = allix * ones(1, ndim);
  cols = ones(numix, 1) * (1:ndim);
  coords = sparse(rows, cols, coords(allix,:), npts, ndim);

  K = sparse(allix, 1, K(allix));
  H = sparse(allix, 1, H(allix));

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Save on `cp`.

  cp.coordinates = coords;
  cp.K = K;
  cp.H = H;
  cp.mask = [];
  cp.mesh = [];

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Verbosity & visualisation.

  if cp.verbose
    fprintf(' time: %.2f sec.\n', toc(t0));
  end
end
