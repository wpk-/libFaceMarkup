function cp2 = closeto(cp, centre, maxdist)
% Return a subset of the curvature points within the proximity of CENTRE.
%
% Input arguments:
%  CP       CurvaturePoints instance.
%  CENTRE   1x3 array defining the centre of the region.
%  MAXDIST  Scalar defining the radius of the sphere around CENTRE. Points
%           within this region are kept. Points outside this region are
%           discarded.
%
% Output arguments:
%  CP2  CurvaturePoints instance. It is a `compress`ed copy of CP, with the set
%       of points further filtered by the set criteria.
%
  coords = cp.coordinates;
  ixcup = cp.ixcup;
  ixcap = cp.ixcap;
  ixsad = cp.ixsaddle;

  if cp.verbose
    t0 = tic;
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Filter each set of points separately.

  % ~~~~~ Filter each type separately.

  D = pdist2(coords(ixcup,:), centre);
  ixcup = ixcup(D <= maxdist);
  D = pdist2(coords(ixcap,:), centre);
  ixcap = ixcap(D <= maxdist);
  D = pdist2(coords(ixsad,:), centre);
  ixsad = ixsad(D <= maxdist);

  % ~~~~~ Return a copy.

  cp2 = CurvaturePoints.copy(cp);

  cp2.ixcup = ixcup;
  cp2.ixcap = ixcap;
  cp2.ixsaddle = ixsad;

  cp2.compress();

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Verbosity & visualisation.

  if cp.verbose
    fprintf('Discarded far-away curvature points in %.2f sec.\n', toc(t0));
  end
end
