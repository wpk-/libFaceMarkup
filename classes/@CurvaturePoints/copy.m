function cp = copy(cp0)
% Shallow copy a CurvaturePoints instance.
%
% Input arguments:
%  CP0  CurvaturePoints instance, or struct with the same fields.
%
% Output arguments:
%  CP  CurvaturePoints instance.
%
% Considerations:
%  - Because the copy is a shallow copy, and because Mesh is a handle class,
%    `CP.mesh` will reference the same mesh instance as `CP0.mesh`.
%
  props = {
    'coordinates';
    'has_indices_defined';
    'has_curvature_defined';
    'ixcap';
    'ixcup';
    'ixsaddle';
    'K';
    'H';
    'mask';
    'mesh';
    'smoothing_function';
    'smoothing_order';
    'verbose';
  };

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Copy all necessary properties.

  cp = CurvaturePoints();

  for i = 1:numel(props)
    cp.(props{i}) = cp0.(props{i});
  end

  % ~~~~~ `coordinates` will always return `mesh.vertices`,
  %       unless `isempty(mesh)`.

  if ~isempty(cp.mesh)
    cp.coordinates = [];
  end
end
