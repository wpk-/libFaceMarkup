function cp = loadobj(s)
% Restore the CurvaturePoints from a saved state (struct).
%
% Input arguments:
%  S  Struct representation of CurvaturePoints.
%
% Output arguments:
%  CP  CurvaturePoints instance.
%
  if isstruct(s)
    cp = CurvaturePoints.copy(s);
  else
    cp = s;
  end
end
