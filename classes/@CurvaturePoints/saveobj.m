function s = saveobj(cp)
% Return a minimal struct representation of the CurvaturePoints.
%
% Input arguments:
%  CP  CurvaturePoints instance.
%
% Output arguments:
%  S  Struct representation of CP.
%
  props = {
    'coordinates';
    'has_indices_defined';
    'has_curvature_defined';
    'ixcap';
    'ixcup';
    'ixsaddle';
    'K';
    'H';
    'mask';
    'mesh';
    'smoothing_function';
    'smoothing_order';
    'verbose';
  };

  % ~~~~~ Convert `coordinates` to sparse matrix,
  %       setting all unreferenced points to zero.

  cp = CurvaturePoints.copy(cp);
  cp.compress();

  % ~~~~~ Create struct.

  for i = 1:numel(props)
    s.(props{i}) = cp.(props{i});
  end
end
