function cp2 = subset(cp, icup, icap, isad)
% Return a CurvaturePoints instance with a subset of the current points.
%
% Input arguments:
%  CP    CurvaturePoints instance.
%  ICUP  Px1 array indexing elements of `CP.ixcup` to be carried over.
%  ICAP  Qx1 array indexing elements of `CP.ixcap` to be carried over.
%  ISAD  Rx1 array indexing elements of `CP.ixsaddle` to be carried over.
%
% Output arguments:
%  CP2  New CurvaturePoints instance with the selected subset of points.
%
% Considerations:
%  - Because `CP.ixcup`, `CP.ixcap`, and `CP.ixsaddle` are themselves indices
%    (into `CP.points`), the arguments ICUP, ICAP, and ISAD are indices of
%    indices. Beware!
%  - See notes in `CurvaturePoints.copy`.
%
  cp2 = CurvaturePoints.copy(cp);

  cp2.ixcup = cp.ixcup(icup(:));
  cp2.ixcap = cp.ixcap(icap(:));
  cp2.ixsaddle = cp.ixsaddle(isad(:));
end
