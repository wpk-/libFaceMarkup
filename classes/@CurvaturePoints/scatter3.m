function h = scatter3(cp, varargin)
% Scatter plot the curvature points in colours reflecting curvature type.
%
% Input arguments:
%  CP        CurvaturePoints instance.
%  SCUPS     Optional array or scalar for the marker sizes of the cups.
%  SCAPS     Optional array or scalar for the marker sizes of the caps.
%  SSADDLES  Optional array or scalar for the marker sizes of the saddles.
%            Either none of SCUPS, SCAPS, SSADDLES can be specified, or all
%            must be specified.
%  CCUPS     Optional array, matrix, or string specifying cup marker colours.
%  CCAPS     Optional array, matrix, or string specifying cap marker colours.
%  CSADDLES  Optional array, matrix, or string specifying saddle marker colours.
%            Either none of CCUPS, CCAPS, CSADDLES can be specified, or all
%            must be specified. Also, if colour values are specified, sizes
%            be specified too, but can be empty arrays (`[]`).
%  VARARGIN  The X, Y, and Z arguments are taken from CP. Any remaining
%            arguments are directly passed to Matlab's built-in `scatter3`.
%
% Output arguments:
%  H  3x1 Array with handles returned by Matlab's built-in `scatter3`. The
%     handles match the scatter groups for cups, caps, and saddles
%     respectively.
%
  cups = cp.cups;
  caps = cp.caps;
  saddles = cp.saddles;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Parse all arguments.

  % ~~~~~ Get scale and colour.
  %       We use default colour values for convenience.

  S = {[], [], []};
  C = {'g', 'r', 'b'};
  nvargs = numel(varargin);
  %args = {};

  if nvargs>=3 && isnumeric(varargin{1})
    S = varargin(1:3);
    if nvargs>=6 && (isnumeric(varargin{4}) || ...
                      iscolorspec(varargin{4}) || ...
                      islinespec(varargin{4}))
      C = varargin(4:6);
      args = varargin(7:end);
    else
      args = varargin(4:end);
    end
  else
    args = varargin;
  end

  % ~~~~~ Find the axes handle.
  %       Start looking from the end,
  %       because possibly `args{1} == 'filled'`.

  iax = find(strcmpi('Parent', args(end-1:-2:1)), 1);
  if isempty(iax)
    hax = gca;
    % Be explicit about the axis, so that
    % a mouse click in between scatters does
    % not affect the result.
    args = [args {'Parent',hax}];
  else
    iax = numel(args) - 2*iax + 1;
    hax = args{iax+1};
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Scatter plot.

  X = {cups(:,1), caps(:,1), saddles(:,1)};
  Y = {cups(:,2), caps(:,2), saddles(:,2)};
  Z = {cups(:,3), caps(:,3), saddles(:,3)};

  h_ = zeros(3, 1);

  % ~~~~~ Check the hold state
  %       to reset it later.

  ish = ishold(hax);

  % ~~~~~ Scatter cups, caps, and saddles.

  for i = 1:3
    if i == 2
      hold(hax, 'on');
    end
    h_(i) = scatter3(X{i}, Y{i}, Z{i}, S{i}, C{i}, args{:});
  end

  % ~~~~~ Reset the hold state.

  if ~ish
    hold(hax, 'off');
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Return handles.

  % ~~~~~ But only if requested,
  %       keeping the Matlab prompt clean.

  if nargout > 0
    h = h_;
  end
end
