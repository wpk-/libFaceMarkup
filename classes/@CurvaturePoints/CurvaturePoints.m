classdef CurvaturePoints < handle
% Find stationary points of the Gaussian surface curvature.
%
% Tthe Gaussian curvature, K, is the product of the two principal curvatures,
% k1 and k2. Similarly, the mean curvature is the average of those two values.
% As a result, we can identify three different types of stationary points:
%  - cups     K and H are positive,
%  - caps     K is positive and H is negative,
%  - saddles  K is negative.
%
  properties
    mesh;               % Mesh instance with N vertices.
    smoothing_order;    % Integer scalar of the vertex neighbourhood.
    smoothing_function; % Function handle to convert distances to affinities.
    mask;               % Nx1 logical array marking allowed vertices.
  end

  properties
    verbose;            % Scalar. Set to true to print timings etc.
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~ Properties computed on first use, then stored.

  properties
    % Note: ixcup, ixcap, and ixsaddle mark the local curvature extremes.
    ixcap;    % Ix1 array of vertex indices where the surface is cap shaped.
    ixcup;    % Jx1 array of vertex indices where the surface is cup shaped.
    ixsaddle; % Kx1 array of vertex indices where the surface is saddle shaped.
    has_indices_defined = false;    % True after `ix*` have been set.
    K;        % Nx1 array of the Gaussian surface curvature at the vertices.
    H;        % Nx1 array of the Mean curvature at the vertices.
    has_curvature_defined = false;  % True after `K` and `H` have been set.
  end

  properties
    % `coordinates` ALWAYS returns `mesh.vertices`, UNLESS `isempty(mesh)`,
    % then it will return its own value. The vertices are copied to
    % `coordinates` automatically in methods that dereference `mesh` (such
    % as `compress`).
    coordinates;  % Nx3 matrix, possibly sparse (after `compress()`).
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Properties computed every time.

  properties (Dependent)
    caps;       % Ix3 matrix of the cap point coordinates.
    cups;       % Jx3 matrix of the cup point coordinates.
    numcaps;    % Scalar, = I.
    numcups;    % Scalar, = J.
    numsaddles; % Scalar, = K.
    saddles;    % Kx3 matrix of the saddle point coordinates.
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Constructor.

  methods
    function cp = CurvaturePoints(mesh, varargin)
      % Create a CurvaturePoints instance.
      %
      % Input arguments:
      %  MESH     Mesh instance.
      %  OPTIONS  Optional keyword value argument pairs / struct.
      %     'SmoothingOrder'  Scalar specifying the neighbourhood order over
      %                       which to smooth the curvature values. Set to 0
      %                       for no smoothing.
      %                       Default is 0.
      %     'SmoothingFunction'  Function mapping the distance between two
      %                       points to a weight, such that closer points can
      %                       be assigned a higher weight.
      %                       Default is `@(d)normpdf(d,0,sigma)`, where
      %                       `sigma` is twice the smoothing order.
      %     'Mask'            Nx1 logical array marking which vertices in
      %                       `MESH` are acceptable curvature points. Vertices
      %                       for which `MASK` is false will never be in
      %                       `CP.ixcup`, `CP.ixcap`, or `CP.ixsaddle`.
      %                       Default value is `true(N,1)`.
      %     'Verbose'         Scalar. Set to true to print some timings etc.
      %
      % Output arguments:
      %  CP  CurvaturePoints instance.
      %
      if nargin == 0
        % Allow Matlab tricks like `x(100) = CurvaturePoints`.
        return;
      end

      p = inputParser;
      p.FunctionName = 'CurvaturePoints';
      p.addRequired('Mesh', @(x)isa(x,'Mesh'));
      p.addParameter('SmoothingOrder',     0, @isscalar);
      p.addParameter('SmoothingFunction', [], @(x)isa(x,'function_handle'));
      p.addParameter('Mask',              [], @(x)islogical(x)||isempty(x));
      p.addParameter('Verbose',        false, @isscalar);
      p.parse(mesh, varargin{:});

      cp.verbose = p.Results.Verbose;

      if cp.verbose
        fprintf('[CurvaturePoints for %s]\n', mesh.name);
        fprintf('Constructor...');
        t0 = tic;
      end

      cp.mesh = mesh;
      cp.smoothing_order = p.Results.SmoothingOrder;

      if isempty(p.Results.SmoothingFunction)
        sigma = cp.smoothing_order * 2;
        cp.smoothing_function = @(d) normpdf(d, 0, sigma);
      else
        cp.smoothing_function = p.Results.SmoothingFunction;
      end

      if isempty(p.Results.Mask)
        cp.mask = true(mesh.numvertices, 1);
      else
        cp.mask = p.Results.Mask;
      end

      if cp.verbose
        fprintf(' time: %.2f sec.\n', toc(t0));
      end
    end
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ I/O.

  methods (Static)
    % Shallow copy a CurvaturePoints instance.
    cp = copy(cp0);
    % Restore the CurvaturePoints from a saved state (struct).
    cp = loadobj(s);
  end

  methods
    % Return a minimal struct representation of the CurvaturePoints.
    s = saveobj(cp);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Visualisation.

  methods
    % Scatter plot the curvature points in colours reflecting curvature type.
    h = scatter3(cp, varargin);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Modification.

  methods
    % Filter stationary points by distance and number.
    filter(cp, mindist, maxnum);
    % Extract necessary vertices into sparse `coordinates`, dereference `mesh`.
    compress(cp);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Information.

  methods
    % Return a subset of the curvature points within the proximity of CENTRE.
    cp2 = closeto(cp, centre, maxdist);
    % Return a CurvaturePoints instance with a subset of the current points.
    cp2 = subset(cp, icup, icap, isad);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Getters and setters.

  % ~~~~~ Computed and stored.

  methods
    function pts = get.coordinates(cp)
      if isempty(cp.mesh)
        pts = cp.coordinates;
      else
        pts = cp.mesh.vertices;
      end
    end
    function ix = get.ixcap(cp)
      if ~cp.has_indices_defined
        defineIndices(cp);
      end
      ix = cp.ixcap;
    end
    function ix = get.ixcup(cp)
      if ~cp.has_indices_defined
        defineIndices(cp);
      end
      ix = cp.ixcup;
    end
    function ix = get.ixsaddle(cp)
      if ~cp.has_indices_defined
        defineIndices(cp);
      end
      ix = cp.ixsaddle;
    end
    function k = get.K(cp)
      if ~cp.has_curvature_defined
        defineCurvature(cp);
      end
      k = cp.K;
    end
    function k = get.H(cp)
      if ~cp.has_curvature_defined
        defineCurvature(cp);
      end
      k = cp.H;
    end
  end

  % ~~~~~ Computed every time.

  methods
    function pts = get.caps(cp)
      % Get all cap point coordinates.
      pts = cp.coordinates(cp.ixcap,:);
    end
    function pts = get.cups(cp)
      % Get all cup point coordinates.
      pts = cp.coordinates(cp.ixcup,:);
    end
    function n = get.numcaps(cp)
      % Count the number of caps.
      n = numel(cp.ixcap);
    end
    function n = get.numcups(cp)
      % Count the number of cups.
      n = numel(cp.ixcup);
    end
    function pts = get.saddles(cp)
      % Get all saddle point coordinates.
      pts = cp.coordinates(cp.ixsaddle,:);
    end
    function n = get.numsaddles(cp)
      % Count the number of saddles.
      n = numel(cp.ixsaddle);
    end
  end
end
