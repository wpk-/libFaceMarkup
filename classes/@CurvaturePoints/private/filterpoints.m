function ix = filterpoints(pts, ix, mindist, maxnum)
% Filter stationary points by removing too close neighbours.
%
% Input arguments:
%  PTS      Nx3 matrix of points.
%  IX       Px1 array of point indices.
%  MINDIST  Scalar measuring the minimum distance between any two output points.
%  MAXNUM   Scalar specifying the maximum number of indices retained.
%
% Output arguments:
%  IX       Qx1 array of point indices. It is a subset of the input argument and
%           maintains its order. Q <= MAXNUM <= P.
%
% Remarks:
%  - We assume IX is sorted in order of preference, from most preferable `IX(1)`
%    to least preferable `IX(end)`. For example, cups sorted from highest K to
%    smallest K, and saddles sorted from smallest K to highest K.
%  - XXX This function could perhaps be improved. For example, it could find
%    clusters of nearby points, nearby points of nearby points, etc. and treat
%    the cluster at once.
%
  pts = pts(ix,:);

  npts = numel(ix);
  maxnum = min(npts, maxnum);

  if npts < 2
    return;
  end

  ixdone = false(npts, 1);
  ixdone(1) = true;
  windowstart = 2;
  windowend = min(npts, windowstart + maxnum - 1);
  ixwindow = windowstart:windowend;

  while windowstart <= npts
    % First compare the new points (`ixwindow`) against the established group
    % (`ixdone`). If any distance is smaller than allowed, we can reject that
    % point straight away.
    Dmin = pdist2(pts(ixdone,:), pts(ixwindow,:), 'Euclidean', 'Smallest',1);
    qualify = Dmin >= mindist;
    ixwindow = ixwindow(qualify);
    % For the points that passed the first test, we have to establish that
    % their mutual distances are not below `mindist`. If two points are closer
    % together than allowed, only the most favourable (lowest index) is
    % accepted (the `tril` below).
    D = squareform(pdist(pts(ixwindow,:), 'Euclidean'));
    qualify = all(D >= mindist | tril(true(size(D)), 0), 1);
    ixwindow = ixwindow(qualify);
    % All points still remaining can safely be added to the established set.
    % And if we collected enough qualifying points, break quickly.
    ixdone(ixwindow) = true;
    if nnz(ixdone) >= maxnum
      break;
    end
    % We're not done yet. Slide the window to review new candidates.
    windowstart = windowend + 1;
    windowend = min(npts, windowstart + maxnum - 1);
    ixwindow = windowstart:windowend;
  end

  % Although we internally worked with indices, they were actually indices into
  % the indexed points (index indices!). We return the selected elements of
  % `ix`. Also note that the last window may have added more points than needed.
  ix = ix(ixdone);
  ix = ix(1:min(end,maxnum));
end
