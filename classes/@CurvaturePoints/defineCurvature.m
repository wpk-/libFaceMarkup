function defineCurvature(cp, K, H)
% Compute the Gaussian curvature, and optionally smooth it.
%
% Input arguments:
%  CP  CurvaturePoints instance.
%  K   Optional Nx1 array with Gaussian curvature values.
%      By default these are computed.
%  H   Optional Nx1 array with Mean curvature values.
%      By default these are computed.
%
% Considerations:
%  - In principle this function computes the curvature values. However, in some
%    situations it makes sense to compute the curvatures externally, so there
%    is an option to pass them. For example when you need the smoothing matrix
%    for other computations too, it makes sense to compute it just once.
%
  if nargin == 3
    cp.K = K;
    cp.H = H;
    cp.has_curvature_defined = true;
    return;
  else
    assert(nargin == 1, 'You must specify both K and H or neither.');
  end

  if cp.verbose
    fprintf('Define curvature...');
    t0 = tic;
  end

  smoothing_order = cp.smoothing_order;
  smoothing_function = cp.smoothing_function;

  mesh = cp.mesh;
  mask = cp.mask;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Compute the Gaussian surface curvature.

  K = mesh.K;
  H = mesh.H;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Smooth it.

  if smoothing_order
    if cp.verbose
      fprintf(' time: %.2f sec.\n', toc(t0));
      fprintf('Smooth the curvature...');
      t0 = tic;
    end

    W = mesh.smoothmatrix(smoothing_order, smoothing_function, mask);
    K = W * K;
    H = W * H;
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Save on `cp`.

  cp.K = K;
  cp.H = H;

  cp.has_curvature_defined = true;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Verbosity & visualisation.

  if cp.verbose
    fprintf(' time: %.2f sec.\n', toc(t0));
  end
end
