function defineIndices(cp)
% Find the vertex indices of the curvature extrema.
%
% Input arguments:
%  CP  CurvaturePoints instance.
%
  K = cp.K;
  H = cp.H;

  if cp.verbose
    fprintf('Define curvature points indices...');
    t0 = tic;
  end

  mesh = cp.mesh;
  mask = cp.mask;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Extract the extremes.

  [mini,maxi] = mesh.findpeaks(K, mask);
  [~,ixdown] = sort(K, 'descend');

  ispeak = (K > 0) & (H > 0) & maxi & mask;
  ixcup = ixdown(ispeak(ixdown));
  ispeak = (K > 0) & (H < 0) & maxi & mask;
  ixcap = ixdown(ispeak(ixdown));
  ispeak = (K < 0) & mini & mask;
  ixsad = ixdown(ispeak(ixdown));
  ixsad = ixsad(end:-1:1);  % Saddle points sort in K ascending.

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Save on `cp`.

  cp.ixcup = ixcup;
  cp.ixcap = ixcap;
  cp.ixsaddle = ixsad;

  cp.has_indices_defined = true;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Verbosity & visualisation.

  if cp.verbose
    fprintf(' time: %.2f sec.\n', toc(t0));
  end
end
