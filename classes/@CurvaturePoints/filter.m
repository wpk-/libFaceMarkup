function filter(cp, mindist, maxnum)
% Filter stationary points by distance and number.
%
% Input arguments:
%  CP       CurvaturePoints instance.
%  MINDIST  Scalar measuring the minimum distance between any two points.
%  MAXNUM   Maximum number of points to keep of each cups, caps, and saddles.
%
  if nargin<2 || isempty(mindist), mindist=inf; end
  if nargin<3 || isempty(maxnum), maxnum=inf; end

  if isinf(mindist) && isinf(maxnum)
    return;
  end

  coords = cp.coordinates;
  ixcup = cp.ixcup;
  ixcap = cp.ixcap;
  ixsad = cp.ixsaddle;

  if cp.verbose
    fprintf('Filter curvature points...');
    t0 = tic;
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Filter each set of points separately.

  % ~~~~~ Save at once.

  cp.ixcup = filterpoints(coords, ixcup, mindist, maxnum);
  cp.ixcap = filterpoints(coords, ixcap, mindist, maxnum);
  cp.ixsaddle = filterpoints(coords, ixsad, mindist, maxnum);

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Verbosity & visualisation.

  if cp.verbose
    fprintf(' time: %.2f sec.\n', toc(t0));
  end
end
