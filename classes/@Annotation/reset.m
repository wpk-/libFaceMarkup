function reset(ann)
% Undefine all stored landmark coordinates.
%
% Input arguments:
%  ANN  Annotation instance.
%
  warning(['Annotation.reset is deprecated. Use ' ...
          '"ann.coordinates(:) = nan" instead.']);

  ann.coordinates(:) = nan;
  % Not resetting name.
end
