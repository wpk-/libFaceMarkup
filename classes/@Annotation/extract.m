function [lidx,coords] = extract(ann, lidx)
% Read specific landmark coordinates.
%
% Input arguments:
%  ANN   Annotation instance.
%  LIDX  Optional Nx1 array of landmark numbers (in the `ANN.lnddef` numbering
%        system) to be extracted.
%        Default value is `ANN.indices`.
%
% Output arguments:
%  LIDX    Nx1 array marking the indices of the returned points. If specified it
%          is a copy of the input argument, or its default value otherwise.
%  COORDS  NxD matrix of point coordinates selected from `ANN.coordinates`.
%
  warning(['Annotation.extract is deprecated. Use ' ...
          '"ann.coordinates(lidx,:)" instead.']);

  if nargin<2 || isempty(lidx), lidx=ann.indices; end
  
  coords = ann.coordinates(lidx,:);
end
