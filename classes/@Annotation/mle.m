function err = mle(ann, truth)
% Compute the mean landmark error against a ground truth annotation.
%
% Input arguments:
%  ANN    Annotation instance.
%  TRUTH  Annotation instance.
%
% Output arguments:
%  ERR  Scalar providing the mean landmark annotation error.
%
% Considerations:
%  - You are responsible for checking that all necessary landmark points are
%    defined in ANN, because NaN values are ignored.
%
  delta = ann - truth;
  err = nanmean(sqrt(sum(delta.coordinates .^ 2, 2)), 1);
end
