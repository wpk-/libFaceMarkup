function s = saveobj(ann)
% Save data to struct.
%
% Input arguments:
%  ANN  Annotation instance.
%
% Output arguments:
%  S  Struct with all data necessary to reconstruct ANN.
%
  s.lnddef = ann.lnddef;
  s.indices = ann.indices;
  s.coordinates = ann.coordinates(ann.indices,:);
  s.name = ann.name;
end
