function [d,Z,rt,lidx] = procrustes(X, Y, varargin)
% Determine the rigid transformation that best fits the points in Y to X.
%
% Input arguments:
%  X        Annotation instance.
%  Y        Annotation instance.
%  OPTIONS  Optional structure with the following keyword arguments (can
%           alternatively be specified as named function arguments directly):
%   'Reflection'  Flag to allow a reflection component in the transformation.
%                 `true` enforces, `false` forbids, and `'best'` returns the
%                 best fit regardless of reflection.
%                 Default value is `'best'`.
%   'Scaling'     Logical scalar to allow a scaling component.
%                 Default value is `true`.
%
% Output arguments:
%  D     Scalar measuring the (standardised) goodness-of-fit. This is
%        measured only on the points used to compute the transformation.
%  Z     Annotation instance. It is a copy of Y with the transformation
%        TRANSFORM applied to _all_ its coordinates.
%  RT    RigidTransformation instance holding the transformation parameters.
%  LIDX  Nx1 array of those landmark numbers (indices) of Y that were used
%        to compute the transform (see note below). The numbers are in the
%        `Y.lnddef` numbering system.
%
% Considerations:
%  - The transformation is computed only on the points that are defined in both
%    X and Y. That can be substantially fewer than the number of points in X
%    and/or Y individually. The fourth output argument should therefore clarify
%    what points exactly were used.
%
  p = inputParser;
  p.FunctionName = 'procrustes';
  p.addRequired('X',                   @(x)isa(x,'Annotation'));
  p.addRequired('Y',                   @(x)isa(x,'Annotation'));
  p.addParameter('Reflection', 'best', @(x)isscalar(x)||strcmp(x,'best'));
  p.addParameter('Scaling',      true, @isscalar);
  p.parse(X, Y, varargin{:});
  
  Y    = p.Results.Y;
  X    = p.Results.X.as(Y.lnddef);
  opts = {'reflection',p.Results.Reflection, 'scaling',p.Results.Scaling};
  
  lidx = intersect(X.indices, Y.indices);
  Xpts = X.coordinates(lidx,:);
  Ypts = Y.coordinates(lidx,:);
  
  [d,~,T] = procrustes(Xpts, Ypts, opts{:});
  
  if nargout >= 2
    % Convert to annotation, and also, apply the transformation to the whole Y.
    rt = RigidTransformation(T.T, T.c(1,:), T.b);
    Z = rt.transform(Annotation.copy(Y));
  end
end
