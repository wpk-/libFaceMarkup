function print(ann)
% Print the annotation to screen.
%
% Input arguments:
%  ANN  Annotation instance.
%
  %ix   = ann.indices;
  ix   = 1:ann.maxindex;
  pts  = ann.coordinates(ix,:);
  npts = numel(ix);
  ndim = ann.dim;
  
  data = cell(ndim+2, npts);
  data(1,:) = num2cell(ix(:)');
  data(1+(1:ndim),:) = num2cell(pts');
  
  numname        = ann.lnddef.name;
  universal_idx  = ann.lnddef.lxmap(ix);
  data(ndim+2,:) = LandmarkEncyclopedia.landmarkid2label(universal_idx);

  n1 = max(7, numel(numname));
  n2 = max(11, 10 * ndim);
  n3 = 30;
  
  s1 = repmat('-', 1, n1);
  s2 = repmat('-', 1, n2);
  s3 = repmat('-', 1, n3);
  s4 = repmat('-', 1, n1-2);
  
  p1 = sprintf('%%%ds   %%-%ds    %%s\n', n1, n2);
  p2 = sprintf('%%%dd.     %s    %%s\n', n1-3, repmat('%10.2f',1,ndim));
  p3 = '%s\n';
  p4 = 'Landmarks numbered according to %s.\n';
  p5 = 'Annotation name: "%s"\n';
  
  fprintf(p1, numname, 'Coordinates', 'Description');
  fprintf(p1, s1, s2, s3);
  fprintf(p2, data{:});
  fprintf(p3, s4);
  fprintf(p4, numname);
  fprintf(p5, ann.name);
end
