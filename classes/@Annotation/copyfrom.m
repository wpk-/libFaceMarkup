function copyfrom(ann, ann2)
% Copy landmark positions from another annotation.
%
% Input arguments:
%  ANN   Annotation instance.
%  ANN2  Annotation instance providing new coordinates.
%
  assert(isa(ann2, 'Annotation'));
  assert(ann.dim == ann2.dim);
  
  ann2 = ann2.as(ann.lnddef);   % convert to our landmark definition.
  ix   = ann2.indices;          % only copy defined points.
  
  ann.coordinates(ix,:) = ann2.coordinates(ix,:);
  % Does not copy the name.
end
