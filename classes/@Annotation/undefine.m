function undefine(ann, lidx)
% Remove one or more points from the annotation.
%
% Input arguments:
%  ANN   Annotation instance.
%  LIDX  Nx1 array of landmark numbers.
%
  warning(['Annotation:undefine is deprecated. Use ' ...
          '"ann.coordinates(ix,:) = nan" instead.']);
  
  assert(ann.maxindex >= max(lidx(:)));
  
  ann.coordinates(lidx,:) = nan;
end
