function h = scatter3(ann, varargin)
% Scatter plot the annotation points.
%
% Input arguments:
%  ANN       Annotation instance.
%  S         Optional Nx1 vector specifying marker sizes.
%            Default value is [].
%  C         Optional NxD matrix specifying marker colours.
%            Default value are the landmark numbers/indices.
%  FILL      Optional string specifying the markers to be filled (pass 'fill' or
%            'filled').
%  MARKER    Optional string specifying a marker type.
%  VARARGIN  The arguments X, Y, Z are taken from ANN. Any remaining arguments
%            to Matlab's built-in `scatter3` can be passed here.
%
% Output arguments:
%  H  Handle returned by Matlab's built-in `scatter3`.
%
% Considerations:
%  - S and C specify N sizes/colours for the N annotation points defined in
%    `ANN.indices`. Because not all landmarks have to be defined, this is not
%    necessarily 1..N.
%

  % Parse the input arguments.
  
  i = 1;
  
  if nargin <= i || ~isnumeric(varargin{i})
    S = [];
  else
    S = varargin{i};
    i = i + 1;
  end
  if nargin <= i || ~(iscolorspec(varargin{i}) || ...
                      (isvector(varargin{i}) && isnumeric(varargin{i})))
    C = [];
  else
    C = varargin{i};
    i = i + 1;
  end
  if nargin <= i || ~isempty(strfind(varargin{i}, 'fill'))
    fill = [];
  else
    fill = varargin{i};
    i = i + 1;
  end
  if nargin <= i || ~islinespec(varargin{i})
    marker = [];
  else
    marker = varargin{i};
    i = i + 1;
  end
  
  varargs = varargin(i:end);
  
  if ~isempty(marker)
    varargs = [{marker} varargs];
  end
  if ~isempty(fill)
    varargs = [{fill} varargs];
  end
  if ~isempty(C) || ~isempty(S)
    varargs = [{S C} varargs];
  end
%   if ~isempty(S)
%     varargs = [{S} varargs];
%   end
  
  % Get the data points.
  
  ix  = ann.indices;
  pts = ann.coordinates(ix,:);
  
  % Ready to scat.
  
  h_ = scatter3(pts(:,1), pts(:,2), pts(:,3), varargs{:});
  
  if nargout>0, h=h_; end
end
