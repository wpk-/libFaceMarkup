function ann3 = delta(ann, ann2)
% Compute the difference vectors between equal points in two annotations.
%
% Input arguments:
%  ANN   Annotation instance.
%  ANN2  Annotation instance.
%
% Output arguments:
%  ANN3  Annotation instance whose coordinates represent the vectors pointing
%        from ANN to ANN2. Even though `ANN3.coordinates` provides vector data
%        rather than cartesian coordinates, `ANN3.lnddef` still provides the
%        definition of the indices just like it does on regular annotations.
%
% Considerations:
%  - Since ANN and ANN2 can have differend landmark definitions, the differences
%    can (and is) only be computed on points that are defined in both
%    annotations.
%  - `ANN3.lnddef` equals `ANN.lnddef`, which is just an (arbitrary) choice.
%
  warning('Annotation.delta is deprecated. Use "ann3 = ann2 - ann1" instead.');
  
  ann3 = ann2 - ann;
  ann3.convertto(ann.lnddef);
end
