function ann2 = as(ann, lnddef2)
% Return a copy of the annotation in another LandmarkDefinition format.
%
% Input arguments:
%  ANN      Annotation instance.
%  LNDDEF2  LandmarkDefinition instance of the new landmark numbering system.
%
% Output arguments:
%  ANN2  Annotation instance. It defines the same points as ANN, but in the new
%        numbering system (specified in LNDNEW). From a practical viewpoint this
%        basically means that the indices have changed.
%
  if isa(lnddef2,'Annotation'), lnddef2=lnddef2.lnddef; end
  
  lnddef = ann.lnddef;
  ann2   = Annotation.copy(ann);
  
  if ~isequal(lnddef, lnddef2)
    ann2.convertto(lnddef2);
  end
end
