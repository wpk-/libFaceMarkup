function snaptosurface(ann, mesh)
% Move the annotation points to the nearest points on `mesh`.
%
% Input arguments:
%  ANN   Annotation instance.
%  MESH  Mesh instance.
%
  ix  = ann.indices;
  pts = ann.coordinates(ix,:);
  
  for i = 1:numel(ix)
    pts(i,:) = mesh.closestpoint(pts(i,:));
  end
  
  ann.coordinates(ix,:) = pts;
end
