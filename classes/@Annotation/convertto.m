function convertto(ann, lnddef2)
% Convert the annotation (by reference) to a new landmark numbering system.
%
% Input arguments:
%  ANN      Annotation instance.
%  LNDDEF2  LandmarkDefinition instance of the new landmark numbering system.
%
% Notes:
%  - Points defined in `ANN.lnddef` but not in `LNDDEF2` will be lost.
%  - Points defined in `LNDDEF2` but not in `ANN.lnddef` will NaN.
%
  lndsrc = ann.lnddef;
  
  if isequal(lndsrc, lnddef2)
    return;
  end
  
  % Convert indices.
  
  [ixfrom,ixto] = LandmarkDefinition.conversion(lndsrc, lnddef2);
  maxixto       = numel(lnddef2.lxmap);
  
  % Transfer only the (source defined && target defineable) points.
  
  % - Target defineable because in ixfrom.
  % - Source defined because in ann.indices.
  [ixfrom,ii] = intersect(ixfrom, ann.indices);
  % - Apply same filter also to ixto, so ixfrom and ixto are in sync.
  ixto        = ixto(ii);
  
  % Convert annotation.
  
  newcoords         = nan(maxixto, ann.dim);
  newcoords(ixto,:) = ann.coordinates(ixfrom,:);
  
  ann.lnddef      = lnddef2;
  ann.maxindex    = maxixto;
  ann.coordinates = newcoords;
  % ann.indices     = ixto; % not needed. newcoords has nans already.
end
