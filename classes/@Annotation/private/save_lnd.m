function save_lnd(filename, lidx, pts, ldef)
% Write landmark positions to file in "lnd" format.
%
% Input arguments:
%  FILENAME  String with the annotation file name.
%  LIDX      Nx1 array of landmark numbers. Each point is assigned a unique
%            landmark number, and should ultimately refer to a landmark
%            definition. See also: @LandmarkDefinition.
%  PTS       Nx3 matrix of the landmark positions in cartesian coordinates.
%  LDEF      String holding the name of the LandmarkDefinition that provides the
%            interpretation of landmark numbers in LIDX.
%
  if nargin<4 || isempty(ldef), ldef=''; end
  
  ndim = size(pts, 2);
  pat1 = '# lnddef: %s\n';
  pat2 = ['%d' repmat(' %0.6g', 1, ndim) '\n'];
  
  fld = fileparts(filename);
  if ~exist(fld, 'dir')
    mkdir(fld);
  end
  
  fid = fopen(filename, 'w');
  
  if ~isempty(ldef)
    % Note: MFM is not compatible with this. So where compatibility is required,
    %       make sure to create a LandmarkDefinition instance with `name==''`.
    fprintf(fid, pat1, ldef);
  end
  fprintf(fid, pat2, [lidx pts]');
  
  fclose(fid);
end
