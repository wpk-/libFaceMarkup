function save_pts(filename, lidx, pts, ~)
% Write landmark positions to file in XM2VTS "pts" format.
%
% Input arguments:
%  FILENAME  String with the annotation file name.
%  LIDX      Nx1 array of landmark numbers. Each point is assigned a unique
%            landmark number, and should ultimately refer to a landmark
%            definition. See also: @LandmarkDefinition.
%  PTS       Nx3 matrix of the landmark positions in cartesian coordinates.
% 
  version = 1;
  npts = numel(lidx);
  ndim = size(pts, 2);
  
  [lidx,ix] = sort(lidx);
  
  assert(all(lidx(:)' == 1:npts), ...
         'The pts format requires all points 1..n to be defined.');
  
  pts = pts(ix,:);
  
  pat1 = 'version: %d\n';
  pat2 = 'n_points:  %d\n';
  pat3 = ['%.6f' repmat(' %.6f', 1, ndim-1) '\n'];
  
  fid  = fopen(filename, 'w');
  fprintf(fid, pat1, version);
  fprintf(fid, pat2, npts);
  fprintf(fid, '{\n');
  fprintf(fid, pat3, pts');
  fprintf(fid, '}');
  fclose(fid);
end
