function [ix,val,iters] = icp(A, B, mode)
% Minimise A - (B * R + x) under permutation of rows of B.
%
% Input arguments:
%  A       NxD matrix of N points in D dimensions.
%  B       MxD matrix of M points in D dimensions.
%  MODE    Optional string indicating the assignment strategy.
%          'default'  Assign each point in B to its nearest point in A.
%          'single'   Remove any points from B that are doubly assigned to the
%                     same point in A. Keep the one with smallest distance.
%                     (this is per iteration, so can be reassigned in later
%                     rounds).
%          'complete' Use Munkres (Hungarian) algorithm to uniquely assign every
%                     point in B to a specific point in A.
%          Default is 'default'.
%
% Output arguments:
%  IX     Mx1 array of indices, assigning point B(i,:) to A(IX(i),:).
%  VAL    Scalar value reflecting the goodness of fit.
%  ITERS  Number of iterations used to reach solution.
%
  single   = false;
  complete = false;
  if nargin >= 3
    switch mode
      case 'single'
        single = true;
      case 'complete'
        complete = true;
    end
  end
  
  % Prepare loop variables.
  
  nA        = size(A, 1);
  nB        = size(B, 1);
  
  maxiter   = 1000;
  
  asgn      = zeros(maxiter+1, nB);
  vals      = zeros(maxiter+1, 1);
  
  % Make default first guess for the assignment.
  % Alternatively could be initialised random.
  
  [~,asgn(1,:)] = pdist2(A, B, 'euclidean', 'Smallest',1);
  sel           = 0;
  
  % Rigid transform based on assignment.
  % Then reassign based on transformation.
  % Loop until repetition or maximum iterations reached.
  
  for i = 1:maxiter
    % Use the assignment to transform points B.
    
    m           = asgn(i,:) > 0;
    ix          = asgn(i,m);
    [d,~,T]     = procrustes(A(ix,:), B(m,:), 'scaling',0, 'reflection',0);
    Z           = bsxfun(@plus, T.b * B * T.T, T.c(1,:));
    
    vals(i)     = nB * d ./ nnz(m);
    
    % Make a new assignment based on the transformed points.
    
    if complete   % Munkres (Hungarian) algorithm.
      [ix,vals(i)] = assignmentoptimal(pdist2(Z, A));
      ix           = ix';
    else
      [d,ix]    = pdist2(A, Z, 'euclidean', 'Smallest',1);
      vals(i)   = sum(d);
      
      % Satisfy optional extra requirements on the assignment.
      
      if single
        [ixu,~,ic]  = unique(ix);
    
        if numel(ixu) < numel(ix)
          %disp('  [icp] adjust for single assignment.');
          % Remove any points assigned that do not qualify.
          h  = hist(ic, 1:numel(ixu));
          jj = find(h > 1);
          for j = jj
            m         = find(ic == j);
            [~,im]    = min(d(m));
            ix(m)     = 0;
            ix(m(im)) = ixu(j);
          end
        end
      end
    end
    
    % Store the assignment for the next iteration.
    
    asgn(i+1,:) = ix;
    
    % Check whether we are done.
    % Either we arrived at a stationary point, or in a loop.
    
    if all(asgn(i,:) == ix)
      %disp('  [icp] done.');
      sel = i;
      break;
    else
      j = find(all(bsxfun(@eq,asgn(1:i,:),ix), 2), 1);
      if ~isempty(j)
        %fprintf(2, '  [icp] loop. done.\n');
        [~,k] = min(vals(j:i));
        sel   = j + k - 1;
        break;
      end
    end
  end
  
  if sel == 0
    %fprintf(2, '  [icp] max iterations reached. done.\n');
    [~,sel] = min(vals(1:maxiter));
  end
  
  ix    = asgn(sel,:);
  val   = vals(sel);
  iters = i;
end

