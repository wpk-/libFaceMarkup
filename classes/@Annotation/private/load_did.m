function [ldef, lidx, pts] = load_did(filename)
% Read landmark positions from file in "did" format.
%
% Input arguments:
%  FILENAME  String with the annotation file name.
%
% Output arguments:
%  LDEF  An empty string (.did format does not provide a landmark definition).
%  LIDX  Nx1 array of landmark numbers. Each point is assigned a unique landmark
%        number, and should ultimately refer to a landmark definition.
%        See also: @LandmarkDefinition.
%  PTS   NxD matrix of the landmark positions.
%

  % Clever upper limit approach proposed by Doug Schwarz:
  % http://uk.mathworks.com/matlabcentral/newsreader/view_thread/166360
  
  ncol = 10;
  
  pat = repmat(' %f', 1, ncol);

  fid  = fopen(filename, 'r');
  data = textscan(fid, pat, 'CollectOutput',true);
  fclose(fid);
  
  data = data{1};
  nrow = size(data, 1);
  ncol = max(arrayfun(@(r)find([isnan(data(r,:)),true], 1), 1:nrow)) - 1;
  ndim = ncol - 1;
  
  ldef = '';
  lidx = data(:,ncol);
  pts  = data(:,1:ndim);
end
