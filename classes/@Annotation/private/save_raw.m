function save_raw(filename, lidx, pts, ~)
% Write landmark positions to file in "raw" format.
%
% Input arguments:
%  FILENAME  String with the annotation file name.
%  LIDX      Nx1 array of landmark numbers. Each point is assigned a unique
%            landmark number, and should ultimately refer to a landmark
%            definition. See also: @LandmarkDefinition.
%  PTS       Nx3 matrix of the landmark positions in cartesian coordinates.
%
% Considerations:
%  - It is your responsibility that the guessed landmark definition in load_raw
%    is correct, by converting landmarks to the applicable definition prior to
%    saving. (14pts=Ruis-14, 26pts=Tena-26, see `load_raw`)
% 
  npts = numel(lidx);
  ndim = size(pts, 2);
  
  assert(any(npts == [14 26]), ...
        'Invalid number of landmarks for .raw file format.');
  
  pat  = [repmat(' %0.6g', 1, ndim) '\n'];
  pat  = pat(2:end);
  
  fid  = fopen(filename, 'w');
  fprintf(fid, pat, pts');
  fclose(fid);
end
