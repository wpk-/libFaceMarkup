function [ldef, lidx, pts] = load_raw(filename)
% Read landmark positions from file in "raw" format.
%
% Input arguments:
%  FILENAME  String with the annotation file name.
%
% Output arguments:
%  LDEF  String holding the name of the LandmarkDefinition that provides the
%        interpretation of landmark numbers in LIDX.
%        If no name could be found, an empty string is returned.
%        -- .raw has no support for landmark definitions.
%  LIDX  Nx1 array of landmark numbers. Each point is assigned a unique landmark
%        number, and should ultimately refer to a landmark definition.
%        See also: @LandmarkDefinition.
%  PTS   Nx3 matrix of the landmark positions in cartesian coordinates.
%
% Important:
%  The .raw format has been used with `ruiz14` as well as `tena26` landmark
%  definitions. Be sure to use the right definition, as they are not
%  interchangeable.
%
  pts  = dlmread(filename);
  npts = size(pts, 1);
  lidx = (1:npts)';
  
  if npts == 14
    ldef = 'Ruiz-14'; % Safe to assume they are the 14 Ruiz landmarks.
  elseif npts == 26
    ldef = 'Tena-26'; % Safe to assume they are the 26 Tena landmarks.
  else
    ldef = '';  % There is no way to know what landmarks are described.
  end
end
