function save_lm3(filename, lidx, pts, ~)
% Write landmark positions to file in "lm3" (or "abs.bin.lm3") format.
%
% Input arguments:
%  FILENAME  String with the annotation file name.
%  LIDX      Nx1 array of landmark numbers. Each point is assigned a unique
%            landmark number, and should ultimately refer to a landmark
%            definition. See also: @LandmarkDefinition.
%  PTS       Nx3 matrix of the landmark positions in cartesian coordinates.
%
  npts = numel(lidx);
  ndim = size(pts, 2);
  viewpoint = 'CENTER';
  
  datafmt = ['%d' repmat(' %0.6g', 1, ndim) '\n'];

  fid = fopen(filename, 'w');
  fprintf(fid, '%d %d %s\n', npts, ndim, viewpoint);
  fprintf(fid, datafmt, [lidx pts]');
  fclose(fid);
end
