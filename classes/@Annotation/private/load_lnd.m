function [ldef, lidx, pts] = load_lnd(filename)
% Read landmark positions from file in "lnd" format.
%
% Input arguments:
%  FILENAME  String with the annotation file name.
%
% Output arguments:
%  LDEF  String holding the name of the LandmarkDefinition that provides the
%        interpretation of landmark numbers in LIDX.
%        If no name could be found, an empty string is returned.
%  LIDX  Nx1 array of landmark numbers. Each point is assigned a unique landmark
%        number, and should ultimately refer to a landmark definition.
%        See also: @LandmarkDefinition.
%  PTS   Nx3 matrix of the landmark positions in cartesian coordinates.
%
  
  % Clever upper limit approach proposed by Doug Schwarz:
  % http://uk.mathworks.com/matlabcentral/newsreader/view_thread/166360
  
  ncol = 10;
  
  pat1 = '# lnddef: %s';
  pat2 = ['%d' repmat(' %f', 1, ncol)];

  fid  = fopen(filename, 'r');
  ldef = textscan(fid, pat1);
  fseek(fid, 0, 'bof'); % Matlab bug: must rewind, or `data` fails to read.
  data = textscan(fid, pat2, 'CommentStyle','#', 'CollectOutput',true);
  fclose(fid);
  
  if isempty(ldef{1})
    ldef = '';
  else
    ldef = ldef{1}{1};
  end
  lidx = data{1};
  pts  = data{2};
  
  nrow = size(pts, 1);
  ncol = max(arrayfun(@(r)find([isnan(pts(r,:)),true], 1), 1:nrow)) - 1;
  pts  = pts(:,1:ncol);
  
  if isempty(lidx)
    warning('Did not read any data from file: %s', filename);
  end
end
