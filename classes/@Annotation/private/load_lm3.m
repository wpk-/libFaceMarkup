function [ldef, lidx, pts] = load_lm3(filename)
% Read landmark positions from file in "lm3" (or "abs.bin.lm3") format.
%
% Input arguments:
%  FILENAME  String with the annotation file name.
%
% Output arguments:
%  LDEF  String holding the name of the LandmarkDefinition that provides the
%        interpretation of landmark numbers in LIDX.
%        If no name could be found, an empty string is returned.
%        -- .raw has no support for landmark definitions.
%  LIDX  Nx1 array of landmark numbers. Each point is assigned a unique landmark
%        number, and should ultimately refer to a landmark definition.
%        See also: @LandmarkDefinition.
%  PTS   Nx3 matrix of the landmark positions in cartesian coordinates.
%
  fid = fopen(filename);
  
  meta = textscan(fid, '%d %d %s', 1);
  npts = meta{1};
  ndim = meta{2};
  %viewpoint = meta{3}{1};
  
  datafmt = ['%d' repmat(' %f', 1, ndim)];
  data = textscan(fid, datafmt, npts, 'CollectOutput',true);
  lidx = data{1};
  pts  = data{2};
  ldef = 'Perakis-8';  % .lm3 file format is unique to Perakis et al.
  
  fclose(fid);
end
