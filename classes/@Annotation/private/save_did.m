function save_did(filename, lidx, pts, ~)
% Write landmark positions to file in "did" format.
%
% Input arguments:
%  FILENAME  String with the annotation file name.
%  LIDX      Nx1 array of landmark numbers. Each point is assigned a unique
%            landmark number, and should ultimately refer to a landmark
%            definition. See also: @LandmarkDefinition.
%  PTS       NxD matrix of the landmark positions.
%  LDEF      Not used.
%
  
  ndim = size(pts, 2);
  pat = [repmat('%0.6g ', 1, ndim) '%d\n'];
  
  fld = fileparts(filename);
  if ~exist(fld, 'dir')
    mkdir(fld);
  end
  
  fid = fopen(filename, 'w');
  fprintf(fid, pat, [pts lidx]');
  fclose(fid);
end
