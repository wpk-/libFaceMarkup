function [ldef, lidx, pts] = load_bnd(filename)
% Read landmark positions from file in "bnd" format (BU-3DFE).
%
% Input arguments:
%  FILENAME  String with the annotation file name.
%
% Output arguments:
%  LDEF  String holding the name of the LandmarkDefinition that provides the
%        interpretation of landmark numbers in LIDX.
%        If no name could be found, an empty string is returned.
%        -- .bnd has no support for landmark definitions.
%  LIDX  Nx1 array of landmark numbers. Each point is assigned a unique landmark
%        number, and should ultimately refer to a landmark definition.
%        See also: @LandmarkDefinition.
%  PTS   Nx3 matrix of the landmark positions in cartesian coordinates.
%
  pts  = dlmread(filename);
  %vidx = pts(:,1);
  pts  = pts(:,2:end);
  npts = size(pts, 1);
  lidx = (1:npts)';
  ldef = 'Yin-83';  % Until someone else uses the .bnd file extension, too.
end
