function [ldef, lidx, pts] = load_pts(filename)
% Read landmark positions from file in XM2VTS "pts" format.
%
% Input arguments:
%  FILENAME  String with the annotation file name.
%
% Output arguments:
%  LDEF  String holding the name of the LandmarkDefinition that provides the
%        interpretation of landmark numbers in LIDX.
%        If no name could be found, an empty string is returned.
%  LIDX  Nx1 array of landmark numbers. Each point is assigned a unique landmark
%        number, and should ultimately refer to a landmark definition.
%        See also: @LandmarkDefinition.
%  PTS   Nx3 matrix of the landmark positions in cartesian coordinates.
%
  pat1 = 'version: %d';
  pat2 = 'n_points:  %d';
  pat3 = '%f';
  
  fid  = fopen(filename, 'r');
  ver = textscan(fid, pat1);
  npts = textscan(fid, pat2, 'CollectOutput',true);
  data = textscan(fid, pat3, 'CommentStyle','{', 'CollectOutput',true);
  fclose(fid);
  
  assert(isscalar(ver{1}), 'Unknown XM2VTS annotation file version.');
  assert(ver{1} == 1, 'Incompatible file version: %d.', ver{1});
  assert(isscalar(npts{1}), 'Unknown number of points.');
  assert(npts{1} <= 68, 'Incompatible number of points: %d.', npts{1});
  assert(~isempty(data{1}), 'Could not read data.');
  
  npts = npts{1};
  pts = reshape(data{1}, [], npts)';
  lidx = (1:npts)';
  ldef = 'XM2VTS-68';
end
