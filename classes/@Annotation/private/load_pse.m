function [ldef, lidx, pts] = load_pse(filename)
% Read landmark positions from file in "pse" format (BU-3DFE).
%
% Input arguments:
%  FILENAME  String with the annotation file name.
%
% Output arguments:
%  LDEF  String holding the name of the LandmarkDefinition that provides the
%        interpretation of landmark numbers in LIDX.
%        If no name could be found, an empty string is returned.
%        -- .pse has no support for landmark definitions.
%  LIDX  Nx1 array of landmark numbers. Each point is assigned a unique landmark
%        number, and should ultimately refer to a landmark definition.
%        See also: @LandmarkDefinition.
%  PTS   Nx3 matrix of the landmark positions in cartesian coordinates.
%
  fid  = fopen(filename);
  data = textscan(fid, '%d %f %f %f');
  fclose(fid);
  
  %vidx = data{1};
  pts  = [data{2:end}];
  npts = size(pts, 1);
  lidx = (1:npts)';
  ldef = 'Yin-8';  % Until someone else uses the .pse file extension, too.
end
