function ann = load(filename, lnddef, format)
% Read landmark positions from file.
%
% Input arguments:
%  FILENAME  String with the file name of the annotation data.
%  LNDDEF    LandmarkDefinition instance providing interpretation of the
%            read landmark indices. If the annotation is stored under a
%            different landmark definition, it is converted to LNDDEF
%            before being returned.
%  FORMAT    Optional string specifying the annotation file format.
%            Default value is the right side of FILENAME starting after the dot,
%            in lower case.
%
% Output arguments:
%  ANN  Annotation instance. Always uses landmark definition LNDDEF.
%
  [fld,name,ext] = fileparts(filename);
  
  if nargin<2 || isempty(lnddef)
    lnddef = [];
  elseif ischar(lnddef)
    lnddef = LandmarkDefinition.load(lnddef);
  end
  if nargin<3 || isempty(format)
    format = ext;
    if numel(format) > 0
      format = format(2:end);
    end
  end
  
  switch lower(format)
    case 'lnd'
      loadfcn = @load_lnd;
    case 'did'
      loadfcn = @load_did;
    case {'lm3','abs.bin.lm3'}
      loadfcn = @load_lm3;
    case {'raw','bst','raw-init-14','raw-init-26','raw-aam-14','raw-aam-26'}
      loadfcn = @load_raw;
    case 'bnd'
      loadfcn = @load_bnd;
    case 'pse'
      loadfcn = @load_pse;
    case 'pts'
      loadfcn = @load_pts;
    otherwise
      error('Unknown landmark file format: %s', format);
  end

  [ldef,lidx,pts] = loadfcn(filename);
  if isempty(ldef)
    ldef = lnddef;
  else
    ldef = LandmarkDefinition.load(fullfile(fld, ldef));
  end
  
  if ~isa(ldef, 'LandmarkDefinition')
    error('Annotation:noLandmarkDefinition', ['If the annotation file does ' ...
          'not store it, a landmark definition must be supplied.']);
  end
  
  ann      = Annotation.factory(ldef, lidx, pts);
  ann.name = name;

  if ~isempty(lnddef)
    % Always return with the requested landmark definition.
    ann.convertto(lnddef);
  end
end
