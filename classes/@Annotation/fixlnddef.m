function [fixed,ix] = fixlnddef(ref, anno)
% Reassign landmark points to landmark indices so they correspond with `ref`.
%
% Input arguments:
%  REF   Reference Annotation object.
%  ANNO  Raw annotation object with a different --unknown-- landmark definition.
%        Any annotation must have a landmark definition, but this requirement is
%        not meaningful if it is simply unknown (the case here).
%
% Output arguments:
%  FIXED  Copy of ANNO with the landmark definition resolved and converted to
%         `REF.lnddef`.
%  IX     Array providing the new landmark index for each point in `ANNO`.
%
  
  % Extract indices and coordinates from reference.
  
  idref  = ref.indices;
  ptsref = ref.coordinates(idref,:);
  
  % Extract indices and coordinates from annotation.
  
  idanno    = anno.indices;
  ptsanno   = anno.coordinates(idanno,:);
  
  % Initial aligment to improve ICP convergence.
  
  ptsr   = bsxfun(@minus, ptsref, mean(ptsref, 1));
  ptsa   = bsxfun(@minus, ptsanno, mean(ptsanno, 1));
  
  rots   = {[ 1 0 0; 0  1 0; 0 0  1], ... % no rotation
            [-1 0 0; 0 -1 0; 0 0  1], ... % rotate around z-axis
            [-1 0 0; 0  1 0; 0 0 -1], ... % rotate around y-axis
            [ 1 0 0; 0 -1 0; 0 0 -1]};    % rotate around x-axis
  
  % Use ICP to match `ptsanno` to `ptsref`.
  
  [ixes,vals] = cellfun(@(rot)icp(ptsr, ptsa*rot, 'complete'), rots, ...
                        'UniformOutput',0);
  [~,i]       = min([vals{:}]);
  ix          = ixes{i};
  
%   [ix,val,iters] = icp(ptsref, ptsanno, 'single');
%   disp(iters);
%   disp(val);
%   disp(ix);
  
  % Convert the annotation to `REF.lnddef`.
  
  assert(all(ix>0), 'Some points could not be assigned.');
  
  fixed = Annotation.factory(ref.lnddef, idref(ix), ptsanno);
  fixed.name = anno.name;
end
