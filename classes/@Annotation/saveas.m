function saveas(ann, filename, format)
% Write landmark positions to file.
%
% Input arguments:
%  ANN       Annotation instance.
%  FILENAME  String with the file name of the annotation data.
%  FORMAT    Optional string specifying the annotation file format.
%            Default value is the right side of FILENAME starting after the
%            first period (".") in lower case.
%
  if nargin<3 || isempty(format)
    [~,~,format] = fileparts(filename);
    if numel(format) > 0
      format = format(2:end);
    end
  end

  switch format
    case 'lnd'
      savefcn = @save_lnd;
    case 'did'
      savefcn = @save_did;
    case {'lm3','abs.gz.lm3'}
      savefcn = @save_lm3;
    case 'raw'
      savefcn = @save_raw;
    case 'pts'
      savefcn = @save_pts;
    otherwise
      error('Annotation:format', 'Unknown landmark file format: %s', format);
  end
  
  ix     = ann.indices;
  coords = ann.coordinates(ix,:);
  ldef   = ann.lnddef.name;
  
  % FIXME: it might be necessary to save the lnddef too.
  
  savefcn(filename, ix, coords, ldef);
end

