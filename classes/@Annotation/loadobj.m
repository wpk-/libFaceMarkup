function ann = loadobj(s)
% Restore Annotation instance from saved struct.
%
% Input arguments:
%  S  Struct with fields containing all important data to reconstruct ANN
%     (see `saveobj`).
%
% Output arguments:
%  ANN  Annotation instance.
%
  if isstruct(s)
    if isempty(s.lnddef) && isempty(s.coordinates)
      ann = Annotation;
    else
      ann = Annotation.factory(s.lnddef, s.indices, s.coordinates);
    end
    if isfield(s, 'name')
      ann.name = s.name;
    end
  else
    ann = s;
  end
end
