function ann = factory(lnddef, lidx, pts)
% Construct Annotation objects from partial data.
%
% Input arguments:
%  LNDDEF  LandmarkDefinition instance, with `numel(LNDDEF.lxmap) == N`.
%  LIDX    Mx1 array with landmark indices, with `M <= N` and `max(M) <= N`.
%  PTS     MxD matrix defining M points in D dimensions.
%
% Output arguments:
%  ANN  Annotation instance.
%
% Considerations:
%  - The key difference to using the Annotation constructor is that here PTS
%    is MxD instead of NxD. So, use the factory if you only have a matrix with
%    the defined points (partial data), rather than a complete matrix.
%
  [M,D]    = size(pts);
  maxindex = numel(lnddef.lxmap);
  maxlidx  = max([lidx(:); 0]);  % allow lidx to be empty.
  
  assert(M == numel(lidx), 'Number of indices does not match number of points.');
  assert(maxindex >= maxlidx, 'Indices out of range for landmark definition.');
  
  coords         = nan(maxindex, D);
  coords(lidx,:) = pts;

  ann = Annotation(lidx, coords, lnddef);
  % Perhaps even better would be:
  %   ann = Annotation(lnddef, D);
  %   ann(lidx,:) = pts;
  % but that requires us to change the constructor. Which is no fun.
end
