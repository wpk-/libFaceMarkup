function ann2 = transform(ann, trans)
% Rotate and translate (rigidly transform) the annotation by trans.
%
% Input arguments:
%  ANN    Annotation instance.
%  TRANS  RigidTransformation instance.
%
% Output arguments:
%  ANN2  New Annotation instance.
%
  coords = ann.coordinates;
  
  if isa(trans, 'RigidTransformation')
    coords = trans.transform(coords);
  elseif isa(trans, 'CoordinateSystem')
    warning('DEPRECATED. Please pass a RigidTransformation to @Annotation/transform.');
    coords = bsxfun(@plus, coords*trans.basis', trans.origin);
  else
    error('Annotation:badargument', ...
          'TRANS must be a RigidTransformation.');
  end
  
  ann2 = Annotation.copy(ann);
  ann2.coordinates = coords;
end
