function [atran,fidx,dfrom] = transfer(anno, trifrom, trito, extrapolate)
% Transfer annotation from one mesh to another, provided they share topology.
%
% Input arguments:
%  ANNO         Annotation instance. These coordinates will be transformed.
%  TRIFROM      Mesh instance. Annotation is originally on this mesh.
%  TRITO        Mesh instance. Annotation will be transferred onto this mesh.
%  EXTRAPOLATE  Boolean flag to specify whether the transfer should use
%               extrapolation to transfer all annotation points.
%               Each annotation point is transferred based on its UV coordinate
%               for the embedding triangle. Some points do not lie within any
%               triangle and so their transfer cannot be based on interpolation.
%               `extrapolate = true` will transfer those points based on their
%               UV parameterisation in the nearest triangle.
%               `extrapolate = false` (default) will not transfer those points.
%
% Output arguments:
%  ATRAN    Annotation instance with the new (transformed) coordinates.
%           The annotation `name` will be copied from `trito`.
%  FIDX     Optional Mx1 matrix with the index of the embedding triangle (face)
%           for each of the M annotation points. Faces are common between
%           `trifrom` and `trito` so FIDX applies to both.
%  DFROM    Optional Mx1 matrix with the original distances of the input
%           annotation points to the nearest triangle in `trifrom`. If
%           `extrapolate == false` (default), this will be a vector of zeros.
%
  if nargin<4 || isempty(extrapolate), extrapolate=false; end
  
  assert(all(size(trifrom.faces)==size(trito.faces)), ...
        'Meshes have different topology!');


  % 1. Extract original annotation and
  %    allocate output coordinates.
  
  ix    = anno.indices;
  p1    = full(anno.coordinates(ix,:));
  p2    = nan(numel(ix),trito.vertexdim);
  dfrom = zeros(numel(ix),1);

  % 2. Use Barycentric coordinates to
  %    transfer the annotation from `trifrom` to `trito`.
  
  npts = size(p1, 1);
  fidx = zeros(npts, 1);
  
  for i = 1:npts
    % - get the UV coordinates relative to ALL triangles.
    [uv, inside] = trifrom.cart2bary(p1(i,:));
    if ~any(inside)
      if extrapolate
        % - if point lies outside the mesh, find the nearest triangle.
        uvin       = clipuv(uv);
        p1in       = trifrom.bary2cart(uvin);
        [fidx(i),dfrom(i,:)] = dsearchn(p1in, p1(i,:));
      else
        warning('Annotation:transfer:notinside', ...
                'Annotation point %d lies outside the mesh.', i);
        continue;
      end
    else
      % - otherwise simply find the containing triangle.
      fidx(i)    = find(inside, 1);
    end
    
    % - convert from UV back to cartesian coordinates, but in mesh `trito`.
    p2(i,:)      = trito.bary2cart(uv(fidx(i),:), fidx(i));
  end

  m          = fidx > 0;
  fidx       = fidx(m);
  ix         = ix(m);
  p2         = p2(m,:);
  
  atran      = Annotation.factory(anno.lnddef, ix, p2);
  atran.name = trito.name;
end
