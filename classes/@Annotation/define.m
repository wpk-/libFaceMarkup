function define(ann, lidx, pts)
% Set the positions of one or more specified landmarks.
%
% Input arguments:
%  ANN   Annotation instance.
%  LIDX  Nx1 array of landmark numbers.
%  XYZ   Nx3 matrix providing N landmark positions in cartesian coordinates.
%
  warning('Annotation.define is deprecated. Use "ann(lidx,:) = pts" instead.');
  
  assert(ann.dim == size(pts,2));
  assert(ann.maxindex >= max(lidx(:)));
  
  ann.coordinates(lidx(:),:) = pts;
end
