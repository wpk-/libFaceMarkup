function ann3 = minus(ann, ann2)
% Subtract another annotation from this, return as new annotation.
%
% Input arguments:
%  ANN   Annotation instance.
%  ANN2  Annotation instance.
%
% Output arguments:
%  ANN3  Annotation instance whose coordinates represent the vectors pointing
%        from ANN2 to ANN (which is what X - X2 is). Even though
%        `ANN3.coordinates` provides vector data rather than cartesian
%        coordinates, `ANN3.lnddef` still provides the definition of the indices
%        just like it does on regular annotations.
%
% Considerations:
%  - Since ANN and ANN2 can have differend landmark definitions, the differences
%    can (and are) only be computed on points that are defined in both.
%  - `ANN3.lnddef` equals `ANN.lnddef`, which is an (arbitrary) choice.
%  - Similarly, `ANN3.name` equals `ANN.name`.
%
  ann2     = ann2.as(ann.lnddef);
  
  lidx     = intersect(ann.indices, ann2.indices);
  diff     = ann2.coordinates - ann.coordinates;
  
  ann3     = Annotation(lidx, diff, ann.lnddef);
  % There is not really a suitable `name` for ann3.
  % Just take the first. If it matters, then at least there is action.
  ann3.name = ann.name;
end
