function ann = copy(orig)
% Return a copy of the annotation.
%
% Input arguments:
%  ORIG  Annotation instance.
%
% Output arguments:
%  ANN  Another Annotation instance with the same properties as the input.
%
  ann = Annotation(orig.indices, orig.coordinates, orig.lnddef);
  ann.name = orig.name;
end
