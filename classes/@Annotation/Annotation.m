classdef Annotation < handle
% Class that stores annotated landmark positions.
%
% An annotation is no more than a set of points, thus, in principle, an Nx3
% matrix to define N points in 3 dimensions. However, such a storage lacks any
% semantical information. What SHOULD point x mark? The eye corner or the nose
% tip? That is where the LandmarkDefinition class comes in.
%
% LandmarkDefinition.universe is lays out a definition of all possible
% landmarks. Each landmark receives a unique ID, and will receive that same ID
% for the rest of time. For example, the nose tip will always be number 3.
%
% Through time everybody has used their own numbering system, but since
% LandmarkDefinition.universe is universal, it encompasses all those defined
% landmarks, and thus we can provide mappings from those numbering systems into
% the universal one. See e.g. LandmarkDefinition.perakis8 or
% LandmarkDefinition.ruiz14.
%
% Having a mapping from any remote numbering system into the universal one gives
% us the freedom to compare and convert annotations from various sources, as
% long as we save the applicable numbering systems with the annotations.
%
% `Annotation` stores coordinates in local format because that is the most
% compact representation. So e.g. with LandmarkDefinition.ruiz14 the field
% `annotation.coordinates` will be 14x3, while LandmarkDefinition.perakis8
% provides only a 8x3 coordinates field. As explained above, we can only do this
% as long as we save the applicable landmark definition with it, which is stored
% in the field `annotation.lnddef`.
%
  properties
    coordinates;  % NxD array of landmark positions.
    name = '';    % String holding the file base name (no extension).
  end
  
  properties (SetAccess = protected)
    lnddef;       % LandmarkDefinition instance with numel(lnddef.lxmap) == N.
    dim;          % D = Dimensionality of the coordinates.
    maxindex;     % N = Number of points defined in the landmark definition.
  end
  
  properties (Dependent)
    indices;      % Indices of the coordinates that have proper values.
    iscomplete;   % Have all landmark positions been defined?
  end
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Class constructor and Matlab internals.
  
  methods
    function ann = Annotation(indices, coordinates, lnddef)
      % Class constructor for landmark data.
      %
      % Input arguments:
      %  INDICES      Mx1 array providing a unique landmark number for each
      %               defined coordinate.
      %  COORDINATES  Mx3 dense matrix, or Nx3 sparse matrix of landmark point
      %               coordinates. In the first case, INDICES(i) matches
      %               COORDINATES(i,:), in the latter case, INDICES(i) matches
      %               COORDINATES(INDICES(i),:).
      %  LNDDEF       LandmarkDefinition instance.
      %
      % Output arguments:
      %  ANN  Annotation instance.
      %
      % Considerations:
      %  - INDICES does ***NOT*** provide order. It only defines which rows of
      %    COORDINATES to copy over. Any permutation has the exact same effect.
      %  - So why is it an integral part of the constructor, you ask? It's a
      %    remnant from the past. Let's not break all code at once.
      %
      if nargin == 0
        % This allows us to preallocate an array of Annotation objects like so:
        %    A(10,1) = Annotation;
        % This results in an 10x1 array of Annotation objects, which can each in
        % turn be overwritten with a proper annotation. Useful for parfor loops
        % and the like.
        return;
      end
      
      if issparse(coordinates)
        warning('Sparse coordinates are deprecated.');
      end
      
      [N,D]  = size(coordinates);
      maxidx = numel(lnddef.lxmap);
      
      assert(N == maxidx, ...
            'Coordinates matrix does not match landmark definition.');
      
      if D == 0
        % Edge case where no data is available during contruction.
        % Default to 3 dimensions. If it's a bad guess we're screwed I'm afraid.
        D = 3;
        coordinates = nan(N, D);
      end
      
      ann.lnddef      = lnddef;
      ann.dim         = D;
      ann.maxindex    = maxidx;
      ann.coordinates = coordinates;
      ann.indices     = indices;  % "nans out" any coordinate not in `indices`.
    end
  end
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~ Static methods return an Annotation instance.
  
  methods (Static)
    % Return a copy of the annotation.
    ann = copy(orig);
    % Construct Annotation objects from partial data.
    ann = factory(lnddef, lidx, pts);
    % Read landmark positions from file.
    ann = load(filename, lnddef, format);
    % Restore Annotation instance from saved struct.
    ann = loadobj(s);
  end
  
  methods
    % Write landmark positions to file.
    saveas(ann, filename, format);
    % Save data to struct.
    s = saveobj(ann);
  end
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Visualisation methods.
  
  methods
    % Print the annotation to screen.
    print(ann);
    % Scatter plot the annotation points in 2D.
    h = scatter(ann, varargin);
    % Scatter plot the annotation points.
    h = scatter3(ann, varargin);
  end
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Methods that alter data.
  
  % - custom functions.
  methods
    % Convert the annotation (by reference) to a new landmark numbering system.
    convertto(ann, lnddef2);
    % Copy landmark positions from another annotation.
    copyfrom(ann, ann2);
    % Set the positions of one or more specified landmarks.
    define(ann, lidx, pts);
    % Undefine all stored landmark coordinates.
    reset(ann);
    % Move the annotation points to the nearest points on `mesh`.
    snaptosurface(ann, mesh);
    % Remove one or more points from the annotation.
    undefine(ann, lidx);
  end
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Methods that return information.
  
  methods
    % Return a copy of the annotation in another LandmarkDefinition format.
    ann2 = as(ann, lnddef2);
    % Compute the difference vectors between equal points in two annotations.
    [lidx, vecs] = delta(ann, ann2);  % -- deprecated. use minus.
    % Read specific landmark coordinates.
    [lidx,coords] = extract(ann, lidx); % -- deprecated.
    % Reassign landmark points to landmark indices so they correspond with `ref`.
    [fixed,ix] = fixlnddef(ref, anno);
    % Subtract another annotation from this, return as new annotation.
    ann3 = minus(ann, ann2);
    % Compute the mean landmark error against a ground truth annotation.
    err = mle(ann, truth);
    % Determine the rigid transformation that best fits the points in Y to X.
    [d,Z,rt,lidx] = procrustes(X, Y, varargin);
  end
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Getters for the dependent properties.
  
  methods
    function set.coordinates(ann, coords)
      % Overwrite coordinates.
      % As long as the size doesn't change, I'm happy.
      %---
      % By the way, using [maxindex,dim] means we can change those values and
      % then change coordinates. Had we used size(coordinates) this would have
      % been impossible. Use with the utmost care though!
      assert(all([ann.maxindex,ann.dim] == size(coords))); %#ok<MCSUP>
      ann.coordinates = coords;
    end
    function ix = get.indices(ann)
      % Return the indices of the coordinates that have proper values.
      ix = find(~any(isnan(ann.coordinates), 2));
    end
    function set.indices(ann, ix)
      % Basically undefine (NaN) any of the coordinates /not/ in ix.
      m = true(size(ann.indices));
      m(ix) = false;
      ann.coordinates(m,:) = nan;
    end
    function tf = get.iscomplete(ann)
      % Have all landmark positions been defined?
      tf = ~any(isnan(ann.coordinates(:)));
    end
  end
end
