function labels = landmarkid2label(lidx, lnddef)
% Look up a set of landmark numbers and return the corresponding labels.
%
% Input arguments:
%  LIDX    Cx1 array of landmark numbers.
%  LNDDEF  Optional LandmarkDefinition instance to look up the indices for that
%          specific definition.
%          Default is `LandmarkDefinition.universe`.
%
% Output arguments:
%  LABELS  Cx1 cell array of landmark labels (see landmark_encyclopedia.xml).
%          In the landmark encyclopedia, landmark with number `LIDX(i)` is
%          defined with label `LABELS{i}`.
%
  if nargin<2 || isempty(lnddef), lnddef=LandmarkDefinition.universe; end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Get the encyclopedia of all landmarks.

  le = LandmarkEncyclopedia.instance();
  lelabels = le.landmarks(:,1);
  leidx = cell2mat(le.landmarks(:,2));

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Look up the requested numbers.

  lxmap = lnddef.lxmap;
  lidx = lxmap(lidx);

  % If there are aliases, i.e., multiple labels for the same number --a
  % possible necessary evil in the future-- we simply return the first
  % matching one.

  matchedidx = arrayfun(@(ix)find(ix==leidx,1), lidx, 'UniformOutput',0);

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Validate before returning.

  matchcount = cellfun(@numel, matchedidx);

  if any(matchcount ~= 1)
    failed = lidx(matchcount ~= 1);
    failed = sprintf(', %d', failed{:});
    failed = failed(3:end);
    error('Unknown landmark numbers: %s', failed);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ All OK. Return cell of strings.

  labels = lelabels(cell2mat(matchedidx));
end
