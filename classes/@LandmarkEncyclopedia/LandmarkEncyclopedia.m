classdef LandmarkEncyclopedia < Singleton
% LandmarkEncyclopedia represents the universal set of landmarks with their
% definition. Since this data is stored in a file, and we want this file to be
% read only once, the LandmarkEncyclopedia follows the Singleton design pattern
% (http://au.mathworks.com/matlabcentral/fileexchange/24911).
%
  properties (Constant)
    LANDMARKS_COLUMNS = {'Label', 'Landmark ID'};
    SYMMETRY_COLUMNS = {'Left', 'Right'};
    PARTS_COLUMNS = {'Label', 'Landmark IDs'};
  end
  
  properties % Public Access
    landmarks;
    %meta; % TODO: implement meta.
    symmetry;
    parts;
  end

  methods (Access=private)
    % Guard the constructor against external invocation.  We only want
    % to allow a single instance of this class.  See description in
    % Singleton superclass.
    function le = LandmarkEncyclopedia()
      % Initialise your custom properties.
      FLD_LIBFACEMARKUP_AUX = getenv('FLD_LIBFACEMARKUP_AUX');
      xmlfile = fullfile(FLD_LIBFACEMARKUP_AUX, 'landmark_encyclopedia.xml');
      xmldata = xmlread(xmlfile);
      
      points = xmldata.getElementsByTagName('point');
      numpoints = points.getLength;
      le.landmarks = cell(numpoints, 2);
      for i = 0:numpoints-1
        pt = points.item(i);
        id = str2double(pt.getAttribute('id'));
        label = char(pt.getAttribute('label'));
        le.landmarks(id,:) = {label, id};
      end
      
      lines = xmldata.getElementsByTagName('line');
      numlines = lines.getLength;
      le.symmetry = nan(numlines, 2);
      for i = 0:numlines-1
        l = lines.item(i);
        idleft = str2double(l.getAttribute('left'));
        idright = str2double(l.getAttribute('right'));
        le.symmetry(i+1,:) = [idleft idright];
      end
      
      pointsets = xmldata.getElementsByTagName('pointset');
      numparts = pointsets.getLength;
      le.parts = cell(numparts, 2);
      for i = 0:numparts-1
        p = pointsets.item(i);
        label = char(p.getAttribute('label'));
        points = str2num(p.getAttribute('points')); %#ok<ST2NM>
        le.parts(i+1,:) = {label, points};
      end
    end
  end

  methods (Static)
    % Concrete implementation.  See Singleton superclass.
    function le = instance()
      persistent uniqueInstance
      if isempty(uniqueInstance)
        le = LandmarkEncyclopedia();
        uniqueInstance = le;
      else
        le = uniqueInstance;
      end
    end
  end

  methods (Static)  % Public Access
    % Look up a set of labels and return the corresponding landmark numbers.
    lidx = landmarklabel2id(labels, lnddef);
    % Look up a set of landmark numbers and return the corresponding labels.
    labels = landmarkid2label(lidx, lnddef);
  end
end
