function lidx = landmarklabel2id(labels, lnddef)
% Look up a set of labels and return the corresponding landmark numbers.
%
% Input arguments:
%  LABELS  Cx1 cell array of landmark labels (see landmark_encyclopedia.xml).
%  LNDDEF  Optional LandmarkDefinition instance. When specified, the landmark
%          IDs returned will follow the landmark numbering system defined in
%          that system.
%          Default is `LandmarkDefinition.universe` which does, and will
%          always, match the Landmark encyclopedia.
%
% Output arguments:
%  LIDX  Cx1 array of landmark numbers. In the landmark encyclopedia, landmark
%        with number `LIDX(i)` is defined with label `LABELS{i}`.
%
  if nargin<2 || isempty(lnddef), lnddef=LandmarkDefinition.universe; end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Get the encyclopedia of all landmarks.

  % ~~~~~ And extract from that
  %       the selected landmark definition.

  lxmap = lnddef.lxmap;

  le = LandmarkEncyclopedia.instance();
  lelabels = le.landmarks(lxmap,1);
  %leidx = cell2mat(le.landmarks(:,2));

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Look up the requested labels.

  labels = strtrim(labels);
  lidx = cellfun(@(lbl)find(strcmpi(lbl,lelabels)), labels, 'UniformOutput',0);

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Validate before returning.

  matchcount = cellfun(@numel, lidx);

  if any(matchcount ~= 1)
    failed = labels(matchcount ~= 1);
    failed = sprintf(', "%s"', failed{:});
    failed = failed(3:end);
    error('Unknown labels: %s', failed);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ All OK. Return as array.

  lidx = cell2mat(lidx);
end
