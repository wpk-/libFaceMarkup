function defineMidplane(mi, ixplane)
% Select from `symaxes` the next candidate in line.
% Make up the line if this is the first call.
%
% A plane of symmetry qualifies when:
%     1. the nose tip is close to it,
%     2. the projected midpoints of horizontal line segments exhibit little
%        variance.
%
% These criteria are both controlled by thresholds. To get reasonable values
% for them, set to `Inf` and then run `MeshICS.train`.
%
% Input arguments:
%  MI       MeshICS instance.
%  IXPLANE  Optional scalar to define `MI.ixplane`.
%           Default is 1.
%
  if nargin<2 || isempty(ixplane), ixplane=1; end

  symaxes = mi.symaxes;
  symplanes = mi.symplanes;
  symvar = mi.symvar;
  nosetip = mi.nosetip;

  if mi.verbose
    fprintf('Define midplane...');
    t0 = tic;
  end

  tipmaxoffset = mi.tip_maxoffset;
  planemaxvar = mi.plane_maxvar;

  numplanes = numel(symplanes);

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Some quick tests of failure.

  if mi.version == 1 && ixplane > 1
    error('MeshICS:Failed', ...
          'ICS v1 does not track back (%s).', mi.mesh.name);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Find the next `ixplane`.
  % XXX removed sorting the first `plane_sortnum` planes based on `symvar`.
  % XXX removed limiting the number of caps allowed. (if required go to defineNoseTip instead).

  if ixplane <= numplanes
    % Disqualify certain values by threshold.
    tipx = nosetip * symaxes(:,ixplane:end) - symplanes(ixplane:end);
    good = (abs(tipx) < tipmaxoffset) & (symvar(ixplane:end) < planemaxvar);

    ixplane = ixplane + find(good,1) - 1;
    % if not any good, then isempty ixplane.
  end

  % ~~~~~ Track back when ixplane
  %       is out of range.

  if isempty(ixplane) || ixplane > numplanes
    if isempty(ixplane)
      disp('backtracking nosetip because no next plane could be found.');
    else
      disp('backtracking nosetip because ixplane was set to high.');
    end
    mi.backtrackNoseTip();
    mi.defineMidplane();
    return;
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Save on `mi`.

  % `ixplane` indexes into `mi.symaxes`, `mi.symplanes`, and `mi.symvar`.
  mi.ixplane = ixplane;

  mi.has_midplane_defined = true;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Verbosity & visualisation.

  if mi.verbose
    fprintf(' time: %.2f sec.\n', toc(t0));
  end
end
