function backtrackMidplane(mi)
% Provide a back-tracking mechanism over the symmetry plane selection (`ixplane`).
%
% Input arguments:
%  MI       MeshICS instance.
%  IXPLANE  Scalar holding the current value of `MI.ixplane`.
%
% Considerations:
%  - In your application wrap a `try..catch` block to detect the error:
%    'MeshICS:Failed'. Then call `mi.default()` to get a default ICS.
%

  fprintf(2, 'Backtracking midplane...\n');

  mi.defineMidplane(mi.ixplane + 1);

  mi.clearOrigin();
  mi.clearAxes();

  if mi.verbose
    fprintf(' ixplane: %d.\n', mi.ixplane);
  end
end
