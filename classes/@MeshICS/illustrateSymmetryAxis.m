function illustrateSymmetryAxis(mi, fighnd)
% Visualise the process of determining the axis of symmetry.
%
% Input arguments:
%  MI      MeshICS instance.
%  FIGHND  Optional scalar specifying a figure handle for plotting.
%          Default is 103.
%
  if nargin<2 || isempty(fighnd), fighnd=103; end

  % `bins` are the original bin centres.
  % `pbins` are the selected bins moved to optimal position.
  % `ixsrcbins` are the indices of `bins` matching the `pbins`.
  bins = mi.bincentres;
  pbins = mi.symaxes';
  ixsrcbins = mi.ixsrcbins;

  % `p` are the probabilities observed in `pbins`. (lower is better)
  p = mi.binprobs(ixsrcbins);

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Set the figure up.

  f = fighnd;
  figure(f);
  clf(f);
  %cmap = make_colormap(f, 'jet', false, true);
  %colormap(f, cmap);

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Draw.

  a = subplot(1, 2, 1, 'Parent',f);
  plotdensity2d(a, bins, pbins, p, ixsrcbins);
  title(a, 'Local minima of the bins, with updated centres.');

  a = subplot(1, 2, 2, 'Parent',f);
  plotdensity3d(a, bins, pbins, p, ixsrcbins);
  title(a, 'The bins in 3D (original cartesian image space).');
end
