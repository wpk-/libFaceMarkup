function defineMask(mi, mask)
% Find and suppress unwanted vertices by setting their `mask` to false.
%
% Input arguments:
%  MI    MeshICS instance.
%  MASK  Optional Nx1 logical array providing an explicit value for the mask.
%        Alternatively, this can be a scalar between zero (exclusive) and one
%        (inclusive). Then, isolated components are masked, as well as the
%        outermost `1-MASK` quantile of the vertices.
%        Default is 1, which does not exclude any vertices based on quantiles.
%
  if nargin<2 || isempty(mask), mask=1; end

  if mi.verbose
    fprintf('Define mask...');
    t0 = tic;
  end

  nvtx = mi.mesh.numvertices;

  if ~isscalar(mask)
    % Passed explicit mask.
    mi.mask = false(nvtx, 1);
    mi.mask(mask) = true;
    mi.has_mask_defined = true;
    if mi.verbose
      fprintf(' time: %.2f sec.\n', toc(t0));
    end
    return;
  else
    assert(mask>0 && mask<=1, 'MASK must be positive and maximally one.');

% % % % %     quant = mask;
    mask = true(nvtx, 1);
  end

  
% % % % %   % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Suppress noisy vertices.
% % % % % 
% % % % %   %    - small neighbourhood (=islands),
% % % % %   nbsz = full(sum(mi.mesh.neighbourhood(10)));
% % % % %   small = nbsz < quantile(nbsz, .03);
% % % % %   mask(small) = false;
% % % % % 
% % % % %   %    - vertices on the outer edges,
% % % % %   edges = horzcat(mi.mesh.borders{:});
% % % % %   nb1 = mi.mesh.neighbourhood(1);
% % % % %   edges = any(nb1(:,edges), 2);
% % % % %   mask(edges) = false;
% % % % % 
  %    - small isolated components.
  %      (sporadically the biggest surface does not include the face,
  %      so we do a bit difficult for just that one case).
  x = mi.mesh.surfaces;
  d = arrayfun(@(i) nnz(x==i), 1:max(x));
  t = find(cumsum(d)./sum(d) > 0.8, 1);
  bigfaces = x <= t;
  smallvertices = mi.mesh.faces(~bigfaces,:);
  mask(smallvertices) = false;

  % ~~~~~ Stay away from the borders.

  nbhd = mi.mesh.neighbourhood(mi.mesh.default_neighbourhood_order);

  for i = 1:t
    b = mi.mesh.borders(x == i);
    b = mi.mesh.halfedges(b{1},1);
    nb = full(any(nbhd(:,b), 2));
    mask(nb) = false;
  end

% % % % % 
% % % % %   % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Focus on the central quantile of the face.
% % % % % 
% % % % %   if quant < 1
% % % % %     vertices = mi.mesh.vertices(mask,:);
% % % % %     q = true(nnz(mask), 1);
% % % % % 
% % % % %     for j = 1:10
% % % % %       mu = mean(vertices(q,:));
% % % % %       d = pdist2(vertices, mu);
% % % % %       q = d < quantile(d, quant);
% % % % %     end
% % % % % 
% % % % %     ixmask = find(mask);
% % % % %     mask(ixmask(~q)) = false;
% % % % %   end
% % % % % 
% % % % %   % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Suppress noisy vertices more.
% % % % % 
% % % % %   %mask = ~(mi.mesh.neighbourhood(1) * double(~mask));
% % % % % 
% % % % %   % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Done.

  mi.mask = mask;

  mi.has_mask_defined = true;

  if mi.verbose
    fprintf(' time: %.2f sec.\n', toc(t0));
  end
end
