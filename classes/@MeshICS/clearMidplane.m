function clearMidplane(mi)
% Undefine `mi.ixplane` and `mi.ixgoodplane` to be recomputed on next request.
%
% Input arguments:
%  MI  MeshICS instance.
%
  mi.has_midplane_defined = false;
  mi.ixplane = [];
end
