function [anno, anno0, pred] = annotate(mi, refanno, refcosy, evalmodel)
% Copy annotation REFANNO onto `MI.mesh`.
%
% Input arguments:
%  MI   MeshICS instance.
%  REFANNO    Annotation instance. This annotation will first be rigidly
%             transformed onto `MI.mesh` and then the landmarks will be
%             "snapped" to the surface.
%  REFCOSY    CoordinateSystem instance. The rigid transformation of REFANNO is
%             computed from REFCOSY to `MI.coordsys`.
%  EVALMODEL  Object with function `predict()` that takes an Nx3 matrix of
%             coordinate differences (between fitted and reference points) and
%             outputs a scalar value. It outputs lower values for better
%             inputs.
%             If not specified, no prediction will be made. Also, it will not
%             be possible to compare a better fit in cases such as where the
%             x-axis might be flipped.
%
% Output arguments:
%  ANNO   Annotation instance.
%  ANNO0  Annotation instance. The difference between ANNO and ANNO0 is that
%         points in ANNO are snapped to the mesh surface, whereas ANNO0 is a
%         purely rigid transform of refanno.
%  PRED   Scalar of the evaluation of ANNO. Lower values are better.
%
  if nargin<4, evalmodel=[]; end

  [anno,anno0,pred,flip] = fittwo(mi, refanno, refcosy, evalmodel);

  if ~isempty(evalmodel)
    best_ixnosetip = mi.ixnosetip;
    best_ixplane = mi.ixplane;
    best_anno = anno;
    best_anno0 = anno0;
    best_pred = pred;
    best_flip = flip;

    try
disp('DEBUG %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DEBUG 0');
      for i = 1:0
        mi.trackback();
        [anno,anno0,pred,flip] = fittwo(mi, refanno, refcosy, evalmodel);

        if pred < best_pred
fprintf('%d. %.2f < %.2f\n', i, pred, best_pred);
          best_ixnosetip = mi.ixnosetip;
          best_ixplane = mi.ixplane;
          best_anno = anno;
          best_anno0 = anno0;
          best_pred = pred;
          best_flip = flip;
        end
      end
    catch err
      if ~strcmpi(err.identifier, 'MeshICS:Failed')
        rethrow(err);
      end
    end

    mi.clearNoseTip();
    mi.clearCurvaturePoints();
    mi.clearLineSegments();
    mi.clearBinProbabilities();
    mi.clearSymmetry();
    mi.clearMidplane();
    mi.clearOrigin();
    mi.clearAxes();

    mi.ixnosetip = best_ixnosetip;
    mi.ixplane = best_ixplane;
    if best_flip
      mi.flipx();
    end

    anno = best_anno;
    anno0 = best_anno0;
    pred = best_pred;
  end
end

function [anno, anno0, pred, flip] = fittwo(mi, refanno, refcosy, evalmodel)
% Fit the reference annotation to the face shape, including flipping X.
%
  [anno,anno0,pred] = fitone(mi, refanno, refcosy, evalmodel);
  flip = false;

  if ~isempty(evalmodel) && mi.xaxis_flag
    mi.flipx();
    [annox,annox0,predx] = fitone(mi, refanno, refcosy, evalmodel);

    % Prefer the flipped version if its predicted value is better.
    if predx < pred
      anno = annox;
      anno0 = annox0;
      pred = predx;
      flip = true;
    else
      mi.flipx();
    end
  end
end

function [anno, anno0, pred] = fitone(mi, refanno, refcosy, evalmodel)
% Fit the reference annotation to the face shape based on its ICS.
%
% Input arguments:
%  MI         MeshICS instance.
%  REFANNO    Reference Annotation instance.
%  REFCOSY    Reference CoordinateSystem instance.
%  EVALMODEL  Object with function `predict()` that takes an Nx3 matrix of
%             coordinate differences (between fitted and reference points) and
%             outputs a scalar value. It outputs lower values for better
%             inputs.
%             If not specified, no prediction will be made. Also, it will not
%             be possible to compare a better fit in cases such as where the
%             x-axis might be flipped.
%
% Output arguments:
%  ANNO   Annotation instance.
%  ANNO0  Annotation instance. The difference between ANNO and ANNO0 is that
%         points in ANNO are snapped to the mesh surface, whereas ANNO0 is a
%         purely rigid transform of refanno.
%  PRED   Scalar of the evaluation of ANNO. Lower values are better.
%

  % ----- Align the ICS coordinate system
  %       with the reference one.

  cosy = mi.coordsys;
  rt = RigidTransformation.fromcoordinatesystems(refcosy, cosy);

  % ----- Transfer reference annotation
  %       to the ICS coordinate system, and
  %       snap to the face surface.

  anno0 = Annotation.copy(refanno);
  rt.transform(anno0);
  anno = Annotation.copy(anno0);
  anno.snaptosurface(mi.mesh);

  % ----- If an evaluation model was specified,
  %       compute the evaluation of the fit
  %       in the reference coordinate system.

  if ~isempty(evalmodel)
    irt = rt.inverse();

    annoeval = Annotation.copy(anno);
    irt.transform(annoeval);
    delta = annoeval - refanno;
    pred = predict(evalmodel, delta.coordinates(:)');
  else
    pred = nan;
  end
end
