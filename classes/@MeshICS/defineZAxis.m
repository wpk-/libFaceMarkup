function defineZAxis(mi)
% Compute the face intrinsic depth axis (pointing from back to front).
%
% Input arguments:
%  MI     MeshICS instance.
%
  a = mi.a;
  b = mi.b;
  origin = mi.origin;
  symaxis = mi.symaxis;

  if mi.verbose
    fprintf('Define the z-axis...');
    t0 = tic;
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Z points from back to front.

  zaxis = null([symaxis'; b-a]);

  if origin * zaxis < a * zaxis
    zaxis = -zaxis;
  end

  mi.zaxis = zaxis;

  mi.has_zaxis_defined = true;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Verbosity & visualisation.

  if mi.verbose
    fprintf(' time: %.2f sec.\n', toc(t0));

    if mi.verbose > 1
      % TODO: add nice illustration.
    end
  end
end
