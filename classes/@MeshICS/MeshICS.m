classdef MeshICS < handle
%MESHICS Summary of this class goes here
%   Detailed explanation goes here
%
% 7 seconds is taken by computing the 10-neighbourhood. Whether we spend it
% in defineMask or in defineCurvaturePoints, we have to spend it anyway.
%
% 14 additional seconds are spent computing the curvature. This is expensive,
% but is the only properly working implementation so far.
%
% Since we're only after the curvature extremes, could we reduce the number of
% vertices on which we compute this? That might save considerable time.
%
% *****************************************************************************
%   In your application wrap a `try..catch` block to detect the error:
%   'MeshICS:Failed'. Then call `mi.default()` to get a default ICS.
% *****************************************************************************
%
% Order of execution:
%   1. defineMask()
%      .mask
%  ---
%   2. defineCurvature()
%      .K
%      .H
%   3. defineNoseTip()
%      .nosetips
%      .ixnosetip
%      >> .nosetip
%   4. defineCurvaturePoints()
%      .points
%   5. defineLineSegments()
%      .lines
%  ---
%   5. defineBinProbabilities()
%      .bincounts         % posvec counts
%      .binpriors         % negvec counts converted to probabilities
%      .binprobs          % binomial probabilities for posvec counts
%   6. defineSymmetry()   % chooses min prob bins. refines them. sets midpoint.
%      .ixsrcbins
%      .symaxes
%      .symplanes
%      .symvar
%  ---
%   7. defineMidplane()
%      .ixplanes
%      .ixplane
%      >> .symaxis, .symaxisoffset
%  ---
%   8. defineOrigin()    --- backtrackPlaneCapPairs()
%      .a
%      .b
%      .origin
%   9. defineXAxis()
%      .xaxis
%  10. defineZAxis()
%      .zaxis
%  11. defineYAxis()
%      .yaxis
%      >> .coordsys
%
%
%

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Parameters configured in the constructor.

  properties (SetAccess=protected)
    angle_neg;
    angle_pos;
    bincentres;
    curvature_order;
    mesh;                 % Mesh instance of a 3D face surface.
    peaks_mindist;
    peaks_maxnum;
%     planecaps_num;
%     planesort_num;
    plane_maxvar;
    poslines_minnum;
    smoothing_function;
    smoothing_order;
    tip_maxoffset;
  end

  properties
    % You can always adjust the verbosity. Supported values are:
    %  0   = nothing is printed or drawn.
    %  1   = print timings and some extra info.
    %  2   = print timings and draw plots/illustrations.
    verbose;
    % To support older versions to repeat old experiments, you can configure
    % the ICS version. The default value is the latest. In your code you can
    % overwrite it with a specific value if desired.
    %  1   = original version.
    %  2   = MeshICS object, order of solving variables changed (first A&B,
    %        then origin, then X&Z, and Y last), convex hull for speed, nose
    %        tip has max Z, more configuration through options.
    %        Algorithmically, the major change is backtracking over
    %        planes-cap pairs (version 1 only considers first plane & cap).
    %  3   = defineBinProbabilities replaces defineDensities. Because negvec
    %        defines the prior probabilities under random vector sampling. Then
    %        the bin counts of posvec follow the binomial distribution. Lowest
    %        probability is least likely randomly sampled.
    %        For compatibility defineBinProbabilities can return rhoneg-rhopos,
    %        which is -(rhopos-rhoneg) as the original code, so the minimum
    %        is correct.
    %  4   = defineNoseTip early and constrain curvature points to 15cm around
    %        it before filtering. Also adds a trained prediction function for
    %        the annotation quality to decide if it needs to track back.
    version = 4;
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Computed on first use, then stored.

  % We could have defined all variables in one block. However, we chose to
  % group them by the function in which they are defined, so reading this code
  % gives you a clearer understanding of the underlying mechanics.
  % Also, the functions are listed in the order in which they are computed. The
  % first function (`defineMask`) has no other dependence than the properties
  % set in the constructor. The next function, `defineCurvature`, depends on
  % the mask being defined, and so on.
  % When using the MeshICS class you don't need to worry about this though,
  % because the properties are automatically computed when requested. The
  % presentation here is purely to reflect the process, so that reading
  % comments from top to bottom actually makes a sensible story.

  % - defineMask()
  % 
  % To reduce the impact of noisy vertices, a mask can be defined. If
  % `mask(i) == false` then vertex `mesh.vertices(i,:)` is ignored
  % (throughout the whole ICS process). This value can be passed explicitly
  % to the constructor, or a default value will be computed automatically.
  %
  properties (SetAccess=protected)
    mask;     % Vx1 logical array. If mask(i) is false, vertex i is ignored.
    has_mask_defined = false; % true after `defineMask()`.
  end

  % - defineCurvature()
  %
  % Compute and store the Gaussian surface curvature, `K`, as it will be used
  % at different stages, and takes quite some time to compute. The Gaussian
  % curvature is defined as a product of the two principal curvatures
  % `K = k1 .* k2`, the mean curvature as their mean `H = (k1 + k2) ./ 2`.
  % Knowing the sign of `H` permits the distinction between two types of maxima
  % of `K`: a product of two negatives or a product of two positives.
  %
  properties (SetAccess=protected)
    K;        % Vector of the Gaussian curvature at each vertex on the `mesh`.
    H;        % Vector of the Mean curvature at each vertex on the `mesh`.
    has_curvature_defined = false;  % true after `defineCurvature()`.
  end

  % - defineNoseTip()
  %
  % The nose tip is selected as the strongest curvature point of type cap. It
  % is possible, however, that some other points have even stronger curvature,
  % so we add a mechanism to "backtrack" over `ixnosetip` to select weaker
  % curvature points.
  % Defining the nose tip early gives some computational advantages. For
  % example, given the current hypothesis of the nose tip, we can limit the
  % curvature points to a region of 15cm around it. Consider the case where a
  % large portion of the chest is visible in the photo. Limiting to 30 points
  % of each type causes many symmetric points in the face to be missing. But
  % constraining to 15cm around the nose tip, 30 points are always good.
  %
  properties (SetAccess=protected)
    nosetips; % CurvaturePoints instance with caps for nose tip candidates.
    ixnosetip;% Index in `nosetips.caps` to mark the current hypothesis.
    has_nosetip_defined = false;  % true after `defineNosetip()`.
  end
  properties (Dependent)
    nosetip;  % 1x3 3D coordinate of the nose tip (selected by `ixnosetip`).
  end

  % - defineCurvaturePoints()
  % 
  % These variables define vertex indices where the Gaussian curvature is
  % locally extreme. Based on the type of extreme (minimum or maximum) and the
  % sign of `H`, three different types of shape/points can be defined:
  %     1. caps:      `K` max and `H < 0`
  %     2. cups:      `K` max and `H > 0`
  %     3. saddles:   `K` min
  % The properties are stored in a CurvaturePoints object.
  %
  properties (SetAccess=protected)
    points;   % CurvaturePoints instance with all cup, cap, and saddle points.
    has_curvaturepoints_defined = false; % true after `defineCurvaturePoints()`.
  end

  % - defineLineSegments()
  %
  % Line segments connect two points. Here those points are the curvature
  % points defined above. Positive line segments connect two points of the same
  % curvature type, while negative line segments connect points of different
  % type. The idea being that symmetric pairs of points (e.g. the left and
  % right eye outer corners) only occur in the positive set, while the
  % direction defined by any other pair of points occurs in both sets.
  %
  properties (SetAccess=protected)
    lines;    % PosNegLines instance storing all pos and neg line segments.
    has_linesegments_defined = false; % true after `defineLineSegments()`.
  end

  % - defineBinProbabilities()
  %
  % Vectors between pairs of curvature points follow some, unknown,
  % distribution typical to face shape and the face orientation. We can
  % estimate this distribution from the negative vectors (in `lines`), because
  % their endpoints are unrelated. The result is a probability for each bin,
  % measuring how likely it is to draw a line segment falling in that bin. So,
  % with p_i the probability of drawing a line segment that falls in bin i, and
  % 1-p_i the probability that it does not fall in bin i, drawing new line
  % segments follows the Binomial distribution.
  % Since we expect the axis of symmetry to cause non-random variation in the
  % distribution of posvec, it should observe a bin with very low probability.
  % Specifically, we expect the deviation from the random variation to be
  % positive. In other words, the posvec count should be much higher than
  % expected.
  %
  properties (SetAccess=protected)
    binpriors;% Bx1 array of prior bin probabilities based on neg vectors.
    bincounts;% Bx1 array of observed counts of pos vectors.
    binprobs; % Bx1 array of probabilities for the observed counts.
    has_binprobabilities_defined = false; % true after `defineBinProbabilities()`.
  end

  % - defineSymmetry()
  %
  % Symmetry is defined by a plane, which is characterised by a normal vector
  % and an offset along that vector. These values are stored in `symaxes` and
  % `symplanes` respectively. The variable names are plural because we compute
  % and store all candidates. This permits us to select and update the best
  % candidate at a later point when more information is available.
  % The candidates are selected from the bin probabilities above. On the
  % "actual" axis of symmetry, `binprobs` should be particularly low while
  % bincounts is particularly high (so it is much higher than we would expect,
  % resulting in a low probability for the observation).
  %
  properties (SetAccess=protected)
    ixsrcbins;% 1xC array of `bincentres` bin indices stored for each `symaxes`.
    symaxes;  % 3xC matrix of x-axis candidates, from most to least likely.
    symplanes;% 1xC array of offsets positioning the symmetry planes on `symaxes`.
    symvar;   % 1xC array of variances of the midpoints on symaxes.
    has_symmetry_defined = false; % true after `defineSymmetry()`.
  end

  % - defineMidplane()
  %
  % Because `symplanes` is sorted in order of probability, midplane candidates
  % are selected by iterating over them sequentially. Some thresholds are
  % employed to iterate over the list more efficiently. These thresholds are
  % configured through MeshICS constructor parameters.
  %
  % A plane of symmetry qualifies when:
  %     1. the nose tip is close to it,
  %     2. the projected midpoints of horizontal line segments exhibit little
  %        variance.
  %
  properties (SetAccess=protected)
    ixplane;  % Scalar selecting a plane of symmetry from `ixplanes`.
    has_midplane_defined = false; % true after `defineMidplane()`.
  end
  properties (Dependent)
    symaxis;  % 3x1 vector is selected from `symaxes`.
    symaxisoffset;  % Scalar defining the zero (origin) on `symaxis`.
  end

  % - defineOrigin()
  %
  % We define the origin as the point close to the nose tip, furthest out from
  % the face. At the same time we find two points in the plane of symmetry, A
  % and B, that subtend a maximal angle on the origin (and span the y-axis).
  %
  properties (SetAccess=protected)
    a;        % A and B, each a 1x3 array, are the point coordinates that
    b;        % subtend a maximal angle on the origin/nose tip. ==> y axis.
    origin;   % 1x3 array marking the point furthest from the xy plane.
    has_origin_defined = false; % true after `defineOrigin()`.
  end

  % - defineXAxis()
  %
  % In principle `symaxis` already defines the x-axis, but its sign may need
  % to be flipped to point from the face's right to left. Different methods
  % are tried depending on the information available in the mesh (in particular
  % texture information).
  %
  properties (SetAccess=protected)
    xaxis;      % 3x1 column vector of width, pointing from subject's right to left.
    xaxis_flag; % Logical scalar to mark suspicion about the x-axis sign.
    has_xaxis_defined = false;  % true after `defineXAxis()`.
  end

  % - defineZAxis()
  % 
  % The z-axis is orthogonal to the x- and y-axis. Because line AB is parallel
  % to y, z is also orthogonal to that. Equally z is orthogonal to `symaxis`,
  % which means z and x can be computed independently. Lastly z should point
  % out from the face, which means that the earlier defined `origin` should
  % have a larger projected value than the points A or B.
  %
  properties (SetAccess=protected)
    zaxis;    % 3x1 column vector of depth, pointing from the back to the front.
    has_zaxis_defined = false;  % true after `defineZAxis()`.
  end

  % - defineYAxis()
  %
  % Lastly we define the y-axis. With all other variables already defined there
  % are many ways to compute this. The cross product of z and x seems the most
  % straightforward.
  % The `coordsys` property is symply a CoordinateSystem instance with x-, y-,
  % and z-axes, as well as the origin taken from this MeshICS instance. It is
  % defined as a property because subclassing in Matlab is a chore.
  %
  properties (SetAccess=protected)
    yaxis;    % 3x1 column vector of height, pointing upwards.
    has_yaxis_defined = false;  % true after `defineYAxis()`.
  end
  properties (Dependent)
    coordsys; % CoordinateSystem instance because inheritance is a chore.
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Constructor.

  methods
    function mi = MeshICS(mesh, varargin)
      % Construct an estimator for the face intrinsic coordinate system (ICS).
      %
      % Input arguments:
      %  MESH  Mesh instance.
      %  VARARGIN  Keyword arguments (can be passed as struct) with the
      %            following fields. All are optional.
      %  .Mask               Nx1 logical array or scalar between 0 and 1
      %                      indicating the quantile of vertices to be masked
      %                      (ignored) based on their distance to the mean.
      %  .CurvatureOrder     Scalar of the neighbourhood order over which to
      %                      compute the surface curvature. Default is 10.
      %  .SmoothingOrder     Scalar of the neighbourhood order over which to
      %                      smooth the curvature value. Default is 10.
      %  .SmoothingFunction  Function handle. The function should take a matrix
      %                      of distances and return a measure of closeness.
      %                      Default is `@(d)normpdf(d,0,5)`.
      %  .PeaksMinDist       Peaks of surface curvature closer together than
      %                      this scalar (in mm) are filtered. The weaker one
      %                      is filtered out.
      %                      Default is 10.
      %  .PeaksMaxNum        Scalar specifying the maximum number of curvature
      %                      peaks to keep for each type (cup, cap, saddle).
      %                      Default is 30.
      %  .PosAngle           Maximum angle between any pair of POS vectors
      %                      (line segments possibly estimating the lateral
      %                      axis) to be considered pointing in the same
      %                      direction. In radians.
      %                      Default is 3 degrees = 3 * pi/180 radians.
      %  .NegAngle           Maximum angle between any pair of NEG vectors.
      %                      Default is 9 degrees = 9 * pi/180 radians.
      %  .BinCentres         Scalar specifying the minimum angle (in radians)
      %                      between any pair of vectors on a hemisphere.
      %                      Alternatively a Mesh instance can be passed, e.g.
      %                      constructed using `Mesh.hemisphere`, in which case
      %                      the vertices will be taken as directional vectors.
      %                      In either case, the vectors are interpreted as
      %                      pointing in the direction of the bin centres, and
      %                      line segments measured on the face are assigned to
      %                      bins based on their angle towards the bin centre.
      %                      Default is 2 degrees 2 * pi/180 radians.
      %  .TipMaxOffset       Maximum offset of the nose tip w.r.t. the plane of
      %                      symmetry (in mm). Hypothesis with larger offsets
      %                      are automatically ignored.
      %                      Default is 7.5 (based on TwinsUK statistics).
      %  .PlaneMaxVar        Scalar of the maximum allowed variance (median
      %                      absolute deviation) of the line segment midpoints.
      %                      Since these midpoints estimate the offset of the
      %                      plane of symmetry, large variance raises doubt
      %                      about the validity.
      %                      Default is 3 (based on TwinsUK statistics).
      %  .PosLinesMinNum     Scalar of the minimum number of POS line segments.
      %                      Hypotheses with fewer line segments (and thus
      %                      fewer mid points) are automatically ignored.
      %                      Default is 3.
      %  .Verbose            Scalar. Set to true to print out some text during
      %                      the estimation. Set to 2 to also plot some
      %                      figures.
      %                      Default is false.
      %  .Version            Scalar to choose the ICS implementation.
      %                      Default is the latest version (which is currently
      %                      4).
      %
      % Output arguments:
      %  MI  A MeshICS instance.
      %
      if nargin == 0
        % `MeshICS.loadobj()` and for declaring arrays like `mi(100) = MeshICS;`
        % Please, see the Matlab documentation for more help.
        return;
      end

      sf = @(d) normpdf(d, 0, 5);

      p = inputParser;
      p.FunctionName = 'MeshICS';
      p.addRequired('mesh', @(x)isa(x,'Mesh'));
      % --- mesh preprocessing
      p.addParameter('Mask',              [], @(x)(islogical(x)||isnumeric(x))&&(isvector(x)||isempty(x)));
      p.addParameter('CurvatureOrder',    10, @isscalar);   % neighbourhood.
      p.addParameter('SmoothingOrder',    10, @isscalar);   % neighbourhood.
      p.addParameter('SmoothingFunction', sf, @(x)isa(x,'function_handle'));
      % --- mesh encoding parameters
      %     (PosAngle and NegAngle are specific to the --hardcoded-- approach)
      p.addParameter('PeaksMinDist',      10, @isscalar);   % millimetres.
      p.addParameter('PeaksMaxNum',       30, @isscalar);   % count.
      p.addParameter('PosAngle',    3*pi/180, @isscalar);   % radians.
      p.addParameter('NegAngle',    9*pi/180, @isscalar);   % radians.
      % --- predictions
      p.addParameter('BinCentres',  2*pi/180, @(x)isa(x,'Mesh')||isscalar(x));
      p.addParameter('TipMaxOffset',     7.5, @isscalar);   % millimetres.
      p.addParameter('PlaneMaxVar',        3, @isscalar);   % med. abs. dev. (mm)
      p.addParameter('PosLinesMinNum',     3, @isscalar);   % count.
      %p.addParameter('PlaneCapsNum',       3, @isscalar);   % count.
      %p.addParameter('PlaneSortNum',       5, @isscalar);   % count.
      %p.addParameter('PosLinesMinNum',     5, @isscalar);   % count.
      % --- other
      p.addParameter('Verbose', false, @isscalar);
      p.addParameter('Version', mi.version, @isscalar);     % -1 for "latest".
      p.parse(mesh, varargin{:});

      mi.verbose = p.Results.Verbose;

      if p.Results.Version > 0
        mi.version = p.Results.Version;
      end

      if mi.verbose
        fprintf('[MeshICS.v%d for %s]\n', mi.version, mesh.name);
        fprintf('Constructor...');
        t0 = tic;
      end

      mi.mesh = mesh;

      mi.curvature_order = p.Results.CurvatureOrder;
      mi.smoothing_order = p.Results.SmoothingOrder;
      mi.smoothing_function = p.Results.SmoothingFunction;

      mi.peaks_mindist = p.Results.PeaksMinDist;
      mi.peaks_maxnum  = p.Results.PeaksMaxNum;

      mi.angle_pos = p.Results.PosAngle;
      mi.angle_neg = p.Results.NegAngle;

      % Slightly unconventional to specify bins using a Mesh, but the big
      % advantage is that the mesh specifies bin connectivity, so we can find
      % the local maxima and minima, and do assignment to a neighbourhood. The
      % vertices represent the actual bin centres.
      if isscalar(p.Results.BinCentres)
        mi.bincentres = Mesh.hemisphere([], [], p.Results.BinCentres);
      else  % Mesh.
        mi.bincentres = p.Results.BinCentres;
      end

      % Maximum offset from the nose tip to the plane of symmetry.
      mi.tip_maxoffset = p.Results.TipMaxOffset;
      % Maximum amount of variance (median absolute deviation) among midpoints.
      mi.plane_maxvar = p.Results.PlaneMaxVar;
      % Minimum number of pos line segments in a candidate bin (defineSymmetry).
      mi.poslines_minnum = p.Results.PosLinesMinNum;
%       % The first N bins are sorted by variance (definePlaneCapPairs).
%       % The first M strongest caps are candidates for plane-cap pairs.
%       mi.planecaps_num = p.Results.PlaneCapsNum;
%       mi.planesort_num = p.Results.PlaneSortNum;

      if mi.verbose
        fprintf(' time: %.2f sec.\n', toc(t0));
      end

      if ~isempty(p.Results.Mask)
        mi.defineMask(p.Results.Mask)
      end
    end
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ IO.

  methods (Static)
    % Copy a MeshICS or struct.
    mi = copy(mi0);
    % Restore the MeshICS from struct.
    mi = loadobj(obj);
  end

  methods
    % Provide a minimal object to be stored.
    obj = saveobj(tri);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Static methods.   

  methods (Static)
    % Measure some population statistics for MeshICS parameter estimation.
    stats = statistics(fldwrl, fldlnd, params, refanno, refcosy);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Instance methods.

  methods
    % Copy annotation REFANNO onto `MI.mesh`.
    [anno,anno0,pred] = annotate(mi, anno, cosy, evalmodel);
    % When all else fails, default.
    default(mi);
    % Flip the x-axis (and the y-axis).
    flipx(mi);
    % Generate the "next best" ICS hypothesis.
    trackback(mi);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Getters for cached properties.

  % ~~~~~ Mask.

  methods
    function m = get.mask(mi)
      if ~mi.has_mask_defined
        mi.defineMask();
      end
      m = mi.mask;
    end
  end

  % ~~~~~ Curvature.

  methods
    function k = get.K(mi)
      if ~mi.has_curvature_defined
        mi.defineCurvature();
      end
      k = mi.K;
    end
    function k = get.H(mi)
      if ~mi.has_curvature_defined
        mi.defineCurvature();
      end
      k = mi.H;
    end
  end

  % ~~~~~ Nose Tip.

  methods
    function cp = get.nosetips(mi)
      if ~mi.has_nosetip_defined
        %defineNoseTip(mi, mi.ixnosetip);
        mi.ixnosetip;
      end
      cp = mi.nosetips;
    end
    function ix = get.ixnosetip(mi)
      if ~mi.has_nosetip_defined
        defineNoseTip(mi, mi.ixnosetip);
      end
      ix = mi.ixnosetip;
    end
    function pt = get.nosetip(mi)
      ix = mi.ixnosetip;
      pt = mi.nosetips.caps(ix,:);
    end
  end

  % ~~~~~ Curvature Points.
  
  methods
    function cp = get.points(mi)
      if ~mi.has_curvaturepoints_defined
        defineCurvaturePoints(mi);
      end
      cp = mi.points;
    end
  end

  % ~~~~~ Pos/Neg Lines.

  methods
    function pnl = get.lines(mi)
      if ~mi.has_linesegments_defined
        defineLineSegments(mi);
      end
      pnl = mi.lines;
    end
  end

  % ~~~~~ Bin Probabilities.

  methods
    function cnt = get.bincounts(mi)
      if ~mi.has_binprobabilities_defined
        defineBinProbabilities(mi);
      end
      cnt = mi.bincounts;
    end
    function p = get.binpriors(mi)
      if ~mi.has_binprobabilities_defined
        defineBinProbabilities(mi);
      end
      p = mi.binpriors;
    end
    function p = get.binprobs(mi)
      if ~mi.has_binprobabilities_defined
        defineBinProbabilities(mi);
      end
      p = mi.binprobs;
    end
  end

  % ~~~~~ Symmetry.

  methods
    function ax = get.symaxes(mi)
      if ~mi.has_symmetry_defined
        mi.defineSymmetry();
      end
      ax = mi.symaxes;
    end
    function x0 = get.symplanes(mi)
      if ~mi.has_symmetry_defined
        mi.defineSymmetry();
      end
      x0 = mi.symplanes;
    end
    function v = get.symvar(mi)
      if ~mi.has_symmetry_defined
        mi.defineSymmetry();
      end
      v = mi.symvar;
    end
  end

  % ~~~~~ Midplane.

  methods
    function ix = get.ixplane(mi)
      if ~mi.has_midplane_defined
        mi.defineMidplane(mi.ixplane);
      end
      ix = mi.ixplane;
    end
    function ax = get.symaxis(mi)
      ix = mi.ixplane;
      ax = mi.symaxes(:,ix);
    end
    function o = get.symaxisoffset(mi)
      ix = mi.ixplane;
      o = mi.symplanes(ix);
    end
  end

  % ~~~~~ Origin.

  methods
    function pt = get.a(mi)
      if ~mi.has_origin_defined
        mi.defineOrigin();
      end
      pt = mi.a;
    end
    function pt = get.b(mi)
      if ~mi.has_origin_defined
        mi.defineOrigin();
      end
      pt = mi.b;
    end
    function pt = get.origin(mi)
      if ~mi.has_origin_defined
        mi.defineOrigin();
      end
      pt = mi.origin;
    end
  end

  % ~~~~~ Axes.

  methods
    function cs = get.coordsys(mi)
      O = mi.origin;
      R = [mi.xaxis mi.yaxis mi.zaxis];
      cs = CoordinateSystem(O, R);
    end
    function ax = get.xaxis(mi)
      if ~mi.has_xaxis_defined
        mi.defineXAxis();
      end
      ax = mi.xaxis;
    end
    function flg = get.xaxis_flag(mi)
      if ~mi.has_xaxis_defined
        mi.defineXAxis();
      end
      flg = mi.xaxis_flag;
    end
    function ax = get.yaxis(mi)
      if ~mi.has_yaxis_defined
        mi.defineYAxis();
      end
      ax = mi.yaxis;
    end
    function ax = get.zaxis(mi)
      if ~mi.has_zaxis_defined
        mi.defineZAxis();
      end
      ax = mi.zaxis;
    end
  end
end
