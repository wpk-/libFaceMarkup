function plotdensity3d(ax, bins, pbins, p, ixp)
% Scatter plot density values on a 3D mesh (should be a ball or hemisphere).
%
% Input arguments:
%  AX     Axis handle.
%  BINS   Mesh instance with the vertices specifying bin centres.
%  PBINS  Mx3 matrix of bin centres for which P is defined (will be plotted in
%         colour).
%  P      Mx1 array of probability values measured in the directions of PBINS.
%  IXP    Optional Mx1 array in indices into BINS. It marks for each bin in
%         PBINS its original index in BINS. If this value is not set, or left
%         empty, no arrows will be drawn from BINS to PBINS.
%         Default value is `[]`.
%
% Considerations:
%  - If `M == BINS.numvertices` then we use `trisurf(BINS, P)` instead of
%    scatter plotting all circles.
%
  if nargin<5 || isempty(ixp), ixp=[]; end

  [v,m] = min(p);

  if numel(p) == bins.numvertices
    trisurf(bins, p, 'Parent',ax);
    shading(ax, 'interp');
  else
    trimesh(bins, 'EdgeColor',.9*[1 1 1], 'Parent',ax);
    bins = bins.vertices;
    hold(ax, 'on');
    scatter3(ax, pbins(:,1), pbins(:,2), pbins(:,3), [], p, 'LineWidth',2);
    scatter3(ax, pbins(m,1), pbins(m,2), pbins(m,3), [], v, 'filled');
    if ~isempty(ixp)
      quiver3(ax, bins(ixp,1), bins(ixp,2), bins(ixp,3), ...
              pbins(:,1) - bins(ixp,1), ...
              pbins(:,2) - bins(ixp,2), ...
              pbins(:,3) - bins(ixp,3), 0);
    end
    hold(ax, 'off');
  end

  xlabel(ax, 'X');
  ylabel(ax, 'Y');
  zlabel(ax, 'Z');
  if min(p) >= 0
    % If we ever decide to colour `log(p)`, then `min(p) < 0`.
    set(gca, 'CLim',[0 max(p)]);
  end
  colorbar('peer',ax);

  view(ax, 2);
  axis(ax, 'equal', 'tight', 'vis3d');
end
