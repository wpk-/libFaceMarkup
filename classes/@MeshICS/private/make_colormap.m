function cmap = make_colormap(f, base, invert, stretch)
% Create a greyscale colour map for figure F.
%
% Input arguments:
%  F   Figure handle.
%
% Output arguments:
%  CMAP     Nx3 colour map.
%  BASE     Optional string specifying the base colour map from which to
%           create CMAP.
%           Default is `'gray'`.
%  INVERT   Optional logical scalar. Set to true to invert the BASE colour map.
%           Default is `false`.
%  STRETCH  Optional logical scalar. Set to true to stretch the colour map.
%           Default is `false`.
%
  if nargin<2 || isempty(base), base='gray'; end
  if nargin<3 || isempty(invert), invert=false; end
  if nargin<4 || isempty(stretch), stretch=false; end
  
  % Inverse colour map and stretch, to
  % add contrast to the middle grey region.
  
  cmap = colormap(f, base);
  
  if invert
    cmap = cmap(end:-1:1,:);
  end
  
  if stretch
    xi = linspace(0, 2, size(cmap,1));
    xj = 1 - cos(linspace(0, pi, size(cmap,1)));
    cmap = interp1(xi, cmap, xj);
  end
end
