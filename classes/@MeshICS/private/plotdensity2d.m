function plotdensity2d(ax, bins, pbins, p, ixp)
% Scatter plot density values on a azimuth-elevation plane.
%
% Input arguments:
%  AX     Axis handle.
%  BINS   Mesh instance of all bin centres.
%  PBINS  Mx3 matrix of bin centres for which P is defined (will be plotted in
%         colour).
%  P      Mx1 array of observed probabilities in each bin of PBINS.
%  IXP    Optional Mx1 array in indices into BINS. It marks for each bin in
%         PBINS its original index in BINS. If this value is not set, or left
%         empty, no arraws will be drawn from BINS to PBINS.
%         Default value is `[]`.
%
  if nargin<5 || isempty(ixp), ixp=[]; end

  [a,e] = cart2sph(bins.vertices(:,1), bins.vertices(:,3), bins.vertices(:,2));
  [ar,er] = cart2sph(pbins(:,1), pbins(:,3), pbins(:,2));
  [v,m] = min(p); % lower is better.

  if numel(p) == bins.numvertices
    scatter(ax, ar, er, [], p);
    hold(ax, 'on');
    h = scatter(ax, ar(m), er(m), [], v, 'filled');
    hold(ax, 'off');
  else
    scatter(ax, a, e, 'MarkerEdgeColor',.9*[1 1 1]);
    hold(ax, 'on');
    scatter(ax, ar, er, [], p);
    h = scatter(ax, ar(m), er(m), [], v, 'filled');
    if ~isempty(ixp)
      quiver(ax, a(ixp), e(ixp), ar-a(ixp), er-e(ixp), 0);
    end
    hold(ax, 'off');
  end

  xlabel(ax, 'Azimuth');
  ylabel(ax, 'Elevation');
  if min(p) >= 0
    % If we ever decide to colour `log(p)`, then `min(p) < 0`.
    set(gca, 'CLim',[0 max(p)]);
  end
  colorbar('peer',ax);
  legend(ax, h, 'Min');

  axis(ax, 'equal', 'tight'); % tight or square?
end
