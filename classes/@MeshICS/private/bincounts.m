function count = bincounts(data, bincentres, maxangle)
% Count the number of data vectors falling in each bin.
%
% Input arguments:
%  DATA        Nx3 matrix of data vectors.
%  BINCENTRES  Mx3 matrix of unit vectors. Should be near uniform sampling
%              over a hemisphere.
%  MAXANGLE    Scalar of the maximum angle (in radians) between a data vector
%              and the bin centre in order to be counted towards that bin.
%
% Considerations:
%  - Data vectors are undirected, meaning that obtuse angles are measured as
%    180-angle (thus measuring the acute angle).
%
  D = pdist2(data, bincentres, 'cosine');
  D = abs(1 - D);
  count = sum(D > cos(maxangle), 1);
end
