%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Compatibility with previous versions.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rho = density(data, bincentres, angles)
  % Compute angles between data and bin centers.
  % Then take histogram to count number of vectors aligned within desired angle.

  d = pdist2(bincentres, data, 'cosine');
  d = acos(abs(1 - d));
  f = histc(d, [0 angles], 2);  % Prepend 0 so `angles == 3` collects range 0-3.
  f = f(:,1:end-1);             % Bin one now matches 0-3, bin two is exactly 3.

  % Make cumulative and divide by surface area of the spherical cap.
  % --> it is a density.

  area = 2*pi * (1-cos(angles));
  rho  = bsxfun(@rdivide, cumsum(f,2), area);

  % Divide by the number of data vectors for consistency.
  % And for interpretability multiply by 2 pi (area of a hemisphere).

  rho  = rho * 2*pi ./ size(data, 1);
end
