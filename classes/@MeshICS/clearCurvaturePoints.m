function clearCurvaturePoints(mi)
% Undefine `mi.points` so it will be recomputed on the next request.
%
% Input arguments:
%  MI  MeshICS instance.
%
  mi.has_curvaturepoints_defined = false;
  mi.points = [];
end
