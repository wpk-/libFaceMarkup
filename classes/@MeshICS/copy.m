function mi = copy(mi0)
% Copy a MeshICS or struct.
%
% Input arguments:
%  MI0  MeshICS instance or a struct with the appropriate fields.
%
% Output arguments:
%  MI  MeshICS instance with identical values to MI0.
%

  % 1. Copy all necessary properties.

  mi = MeshICS.loadobj(saveobj(mi0));

  % 2. Copy all computed properties.

  % - `mask` is entirely stored in `saveobj`.

  if mi0.has_curvature_defined
    mi.K = mi0.K;
    mi.H = mi0.H;
    mi.has_curvature_defined = mi0.has_curvature_defined;
  end

  if mi0.has_nosetip_defined
    mi.nosetips = mi0.nosetips; % NB. we're copying here BY REFERENCE!
    mi.ixnosetip = mi0.ixnosetip;
    mi.has_nosetip_defined = mi0.has_nosetip_defined;
  end

  if mi0.has_curvaturepoints_defined
    mi.points = mi0.points; % NB. we're copying here BY REFERENCE!
    mi.has_curvaturepoints_defined = mi0.has_curvaturepoints_defined;
  end

  if mi0.has_linesegments_defined
    mi.lines = mi0.lines; % NB. we're copying here BY REFERENCE!
    mi.has_linesegments_defined = mi0.has_linesegments_defined;
  end

  if mi0.has_binprobabilities_defined
    mi.binpriors = mi0.binpriors;
    mi.bincounts = mi0.bincounts;
    mi.binprobs = mi0.binprobs;
    mi.has_binprobabilities_defined = mi0.has_binprobabilities_defined;
  end

  if mi0.has_symmetry_defined
    mi.ixsrcbins = mi0.ixsrcbins;
    mi.symaxes = mi0.symaxes;
    mi.symplanes = mi0.symplanes;
    mi.symvar = mi0.symvar;
    mi.has_symmetry_defined = mi0.has_symmetry_defined;
  end

  if mi0.has_midplane_defined
    mi.ixplane = mi0.ixplane;
    mi.has_midplane_defined = mi0.has_midplane_defined;
  end

  if mi0.has_origin_defined
    mi.a = mi0.a;
    mi.b = mi0.b;
    mi.origin = mi0.origin;
    mi.has_origin_defined = mi0.has_origin_defined;
  end

  if mi0.has_xaxis_defined
    mi.xaxis = mi0.xaxis;
    mi.xaxis_flag = mi0.xaxis_flag;
    mi.has_xaxis_defined = mi0.has_xaxis_defined;
  end

  if mi0.has_zaxis_defined
    mi.zaxis = mi0.zaxis;
    mi.has_zaxis_defined = mi0.has_zaxis_defined;
  end

  if mi0.has_yaxis_defined
    mi.yaxis = mi0.yaxis;
    mi.has_yaxis_defined = mi0.has_yaxis_defined;
  end
end
