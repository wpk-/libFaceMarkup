function defineOrigin(mi)
% Compute the face intrinsic vertical axis (pointing upwards).
%
% Input arguments:
%  MI    MeshICS instance.
%
  symaxisoffset = mi.symaxisoffset;
  symaxis = mi.symaxis;
  nosetip = mi.nosetip;
  mask = mi.mask;

  if mi.verbose
    fprintf('Define the origin...');
    t0 = tic;
  end

  vertices = mi.mesh.vertices;

  % XXX: Devil's advocate: "Why twenty?"
  % Can its existence be a criterion in defineMidplane?
  % If so, we wouldn't have to track back.
  nearnosetip = 20;   % 20mm = 2cm. (but in 2D projection)

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Jump into 2D.

  % ~~~~~ Project onto the plane of symmetry.
  %       From now on, we're in 2D.

  yz = null(symaxis');
  vtxyz = vertices(mask,:) * yz;
  oyz = nosetip * yz;

  % ~~~~~ Reduce to the convex hull.

  vi = convhull(vtxyz);
  hull = vtxyz(vi,:);

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Find O, A and B.

  % ~~~~~ Divide points in two sets:
  %        - origin candidates (near), and
  %        - A/B candidates (far).

  near = pdist2(hull, oyz) < nearnosetip;
  far = ~near;  % semantics ;-)

  if nnz(near) < 1 || nnz(far) < 2
    % A likely case for this failure is that the scan includes so much body
    % around the face that the nose tip did not end up on the convex hull, even
    % though the lateral axis estimate is good.
    % Since we're only interested in the 2D aspect here, we will reduce the
    % vertices based on their distance to the nosetip measured in the plane. If
    % that still fails, the only option left is to  track back over the
    % symmetry planes and nose tips.
    D = pdist2(vtxyz, oyz);
    vtxyz = vtxyz(D<100,:);
    vi = convhull(vtxyz);
    hull = vtxyz(vi,:);
    near = pdist2(hull, oyz) < nearnosetip;
    far = ~near;
    if nnz(near) < 1 || nnz(far) < 2
      if nnz(near) < 1
        disp('tracking back because no point on the hull is near the nose tip.');
      else
        disp('tracking back because no two points on the hull are far.');
      end
      mi.trackback();
      mi.defineOrigin();
      return;
    end
  end

  % ~~~~~ First find A and B.
  %       They subtend a maximal angle on `oyz` (the nose tip).

  vtxfar = hull(far,:);
  [~,ix] = max(pdist(bsxfun(@minus,vtxfar,oyz), 'cosine'));
  [a,b] = pdist_subscripts(nnz(far), ix);
  a = vtxfar(a,:);
  b = vtxfar(b,:);

  % ~~~~~ Then find O.
  %       - it is close to the nose tip, and
  %       - (it lies on the convex hull, and)
  %       - it is the point furthest from line AB.

  o = hull(near,:);

  if nnz(near) > 1
    % `o` is Nx2.
    z = null(b - a);
    d = o * z - b * z;
    [~,ix] = max(abs(d));
    o = o(ix,:);
    % `o` is 1x2.
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Jump back to 3D and save on `mi`.

  dx = symaxisoffset * symaxis';
  mi.a = a * yz' + dx;
  mi.b = b * yz' + dx;
  mi.origin = o * yz' + dx;

  mi.has_origin_defined = true;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Verbosity & visualisation.

  if mi.verbose
    fprintf(' time: %.2f sec.\n', toc(t0));

    if mi.verbose > 1
      % TODO: where do we put the support? they are purely for visualisation.
      % Show the convex hull as red line around scatter(.,.,1) blue pixel dots.
      % Plot origin as black thick circle.
      % Plot A and B as red thick circles.
      %illustrateYAxis(...);
    end
  end
end
