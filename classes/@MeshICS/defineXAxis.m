function defineXAxis(mi)
% Determine the sign of the lateral axis (axis of symmetry).
%
% Input arguments:
%  MI    MeshICS instance.
%
% We can find out if texture image and shape space agree on the x direction,
% simply by correlating texture x values with shape x values. What does this
% tell us?
% - if they agree (positive correlation) either both are flipped or none.
% - if they do not (negative correlation) either one of them is flipped.
%
% How does that help us? It doesn't! Because we can't tell which one is
% flipped. However...
% - I have yet to find a flipped image + shape. So in case 1, we can safely
%   assume the lateral axis relates to positive x.
% - In case 2, we have multiple options: i) consistently trust either shape
%   or texture (the choice may depend on the data set), or ii) come up with
%   an extra test such as "the nose tip must be above the average vertex", or
%   iii) flag the image and continue, then when the delta does not fit the
%   model well try flipping the x-axis.
%
% We do both i, consistently trust lateral axis to relate to positive shape
% space x, and iii, flag the image and continue. Then when the fit is bad and
% the image was flagged, we try if setting to negative x helps.
%

% Some stats:
%               shape upside down        texture upside down
%  PoBI:
%  TwinsUK:             18                          0
%  FRGC:
%  Bosphorus:
%  BU-3DFE:

  mesh = mi.mesh;
  symaxis = mi.symaxis;

  if mi.verbose
    fprintf('Define the x-axis...');
    t0 = tic;
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Sign based on texture image.

  % 1. trust shape space positive x.
  crit = [1 0 0] * symaxis;
  xaxis = sign(crit) * symaxis;

  % 2. flag if shape and texture disagree.
  if ~isempty(mesh.textureindices)
    % a) find the largest texture component.
    comp = mesh.texturecomponents == 1;
    % b) find texture and shape x coordinates.
    tci = mesh.textureindices(comp,1);
    tx = mesh.texturecoords(tci,1);
    sfi = mesh.faces(comp,1);
    sx = mesh.vertices(sfi,1);
    % c) correlate them.
    xaxis_flag = corr(tx,sx) < 0;
  else
    xaxis_flag = 1;
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Save on `mi`.

  mi.xaxis = xaxis;
  mi.xaxis_flag = xaxis_flag;

  mi.has_xaxis_defined = true;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Verbosity & visualisation.

  if mi.verbose
    fprintf(' time: %.2f sec.\n', toc(t0));

    if mi.verbose > 1
      % 
      %illustrateXAxis(...);
    end
  end
end
