function stats = statistics(fldwrl, fldlnd, params, refanno, refcosy)
% Measure some population statistics for MeshICS parameter estimation.
%
% Input arguments:
%  FLDWRL   String specifying a folder name such as `'path/to/'` that contains
%           all .wrl files, or a file name pattern such as `'path/to/*.bnt'`.
%           Alternatively a cell array of strings of file names can be
%           specified.
%  FLDLND   String specifying a folder name containing all .lnd files, or a
%           file name pattern, or a .mat file name storing the fields for an
%           AnnotationSet.
%  PARAMS   Struct with parameters passed to MeshICS. The whole purpose of this
%           function is to find suitable values for those parameters. As a
%           consequence, some parameters are overridden. For example:
%           TipMaxOffset and PlaneMaxVar are both set to Inf.
%           Although the values for SmoothingOrder and SmoothingFunction will
%           not be overridden, we will plot some images with preset values to
%           give the user an idea of their effect.
%           Also, Verbose is set to false.
%  REFANNO  Annotation instance that will be used for annotating new meshes
%           (see `MeshICS.annotate`).
%  REFCOSY  CoordinateSystem instance. The rigid transformation of REFANNO is
%           computed from REFCOSY to `MI.coordsys`, where MI is the MeshICS
%           instance for each mesh.
%
% Output arguments:
%  STATS   Struct with the following fields, each of which contains N rows (one
%          row per file):
%          .name   Name of the mesh.
%          .error  Mean landmark error based on the selection that best matches
%                  the manual annotation.
%          .manual_offset  Offset of the manual nose tip annotation to the ICS
%                  plane of symmetry.
%          .manual_angle  Angle between the ICS axis of symmetry and the
%                  lateral axis fit to the manual annotation.
%          .ixnosetip  Index of the cap point closest to the nose tip.
%          .ixplane  Index of the bin for the plane of symmetry.
%          .midpoints_variance  The variance (median absolute deviation) of
%                  horizontal line segment mid points (projected on the x-axis)
%                  estimating x=0.
%          .nosetip_offset  Distance of the (ICS) nose tip to the plane of
%                  symmetry.
%          .nosetip_K  The Gaussian curvature at the (FIXME manually annotated)
%                  nose tip.
%          .delta  An NxMx3 array of the difference between the rigid
%                  annotation (rigid transformation of REFANNO) and the
%                  annotation points snapped onto the mesh surface. This is
%                  returned relative to REFANNO, and can be used to learn a
%                  classifier.
%
% Considerations:
%  - This only samples the "positive class". We report the population
%    statistics. For best results you may still have to tweak these values.
%  - We use Procrustes to map `refcosy` to the mesh to choose a "good" lateral
%    axis. That is only for setting up. From there, we let MeshICS, `mi`,
%    determine the actual ICS coordinate system, and use that to map back to
%    `refcosy`.
%


% Use training to find good parameter values for:
%
% - SmoothingOrder = the neighbourhood order around each vertex to include in
%                    curvature smoothing. Larger values give smoother curvature
%                    estimates, but too large will completely blur the face.
%                    Smaller values will keep more detail, but may be
%                    susceptible to noise. Usually 5 or 10 is good, depending
%                    on the density of vertices (edge lengths) and the amount
%                    of noise in the data set (FRGC is particularly noisy).
% - SmoothingFunction = the kernel function used for smoothing over the
%                    neighbourhood. Its input is a matrix of distances and its
%                    output is an equally sized matrix of weights.
%                    `@(d)normpdf(d,0,5)` seems to be good and I don't see a
%                    good reason to change this other than maybe the noise in
%                    FRGC. Always prefer to tune SmoothingOrder I would say.
% - TipMaxOffset =   the maximum distance between the nose tip and the plane of
%                    symmetry. By running `train()` you can get a feeling for a
%                    proper value. I think it should include all reported
%                    training values. Setting it too large makes the criterion
%                    completely disfunctional, which may still be OK actually.
%                    But the benefit of a proper value can be that it filters
%                    out some faulty symmetry plane estimates as well as false
%                    nose tip estimates, because they don't combine well. Check
%                    the training reported index to see how often this might be
%                    useful (at least index must be >1).
% - PlaneMaxVar =    the maximum amount of variance (median absolute deviation)
%                    between midpoints of horizontal line segments, projected
%                    on the x-axis. The idea is that you expect small variance.
%                    If it is large, probably a wrong estimate has some
%                    scattered line segments. Check the training reported index
%                    to see how often this can be useful (whenever it is >1).
%
% If you can successfully tune the SmoothingOrder, then probably TipMaxOffset
% and PlaneMaxVar loose a bit of relevance, because the first candidate is
% likely to be the final one, but more experiments are needed to confirm.
%
  reforigin = refcosy.origin;
  refbasis = refcosy.basis;

  %params.SmoothingOrder    = 10;
  %params.SmoothingFunction = @(d)normpdf(d,0,5);
%   params.TipMaxOffset      = inf;
%   params.PlaneMaxVar       = inf;

  if isfield(params, 'Verbose')
    verbose = params.Verbose;
    params.Verbose = false;
  else
    verbose = false;
  end

  % ~~~~~ Find all meshes.

  files = listfiles(fldwrl, '*.wrl');
  [~,names] = cellfun(@fileparts, files, 'UniformOutput',0);

  % ~~~~~ Load annotations.

  if isa(fldlnd, 'AnnotationSet')
    annos = fldlnd;
  else
    annos = AnnotationSet.load(fldlnd, refanno.lnddef);
  end

  % ~~~~~ Sync list of meshes
  %       and list of annotations.

  [~,ixm,ixa] = intersect(names, annos.names);
  files = files(ixm);
  names = names(ixm);
  annos.keep(ixa);

%   % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Visualise smoothing settings.
% 
%   % TODO: Plot with different smoothing order and function, so user can choose.
%   randimg = randi(numel(files));
%   m = Mesh.load(files{randimg});
% 
%   orders = [5 10];
%   widths = [5 10 20];
%   numorders = numel(orders);
%   numwidths = numel(widths);
% 
%   figure;
%   for i = 1:numorders
%     for j = 1:numwidths
%       resetParameters(m);
%       mi = MeshICS(m, struct( ...
%           'SmoothingOrder',orders(i), ...
%           'SmoothingFunction', @(d)normpdf(d,0,widths(j))));
%       subplot(numorders, numwidths, (i-1)*numwidths+j);
%       trisurf(m, mi.K);
%       view(2);
%       title(m.name);
%       xlabel(sprintf('Fn: @(d)normpdf(d,0,%d)', widths(j)));
%       ylabel(sprintf('Order: %d', orders(i)));
%     end
%   end
%   drawnow;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Process all files.

  numfiles = numel(files);
  stats = nan(numfiles, 9+numel(refanno.coordinates));

  parfor i = 1:numfiles
    if verbose
      fprintf('%5d. %s\n', i, names{i});
    end

    % ~~~~~ Load 3D file and annotation, and
    %       measure its nose tip and lateral axis.

    m = Mesh.load(files{i});
    a = annos.annotation(i); %#ok<PFBNS>

    lmnosetip = LandmarkEncyclopedia.landmarklabel2id({'nose tip'}, a.lnddef);
    ptnosetip = a.coordinates(lmnosetip,:);

    [~,~,rt] = a.procrustes(refanno, 'Reflection',0, 'Scaling',0);
    refcopy = CoordinateSystem(reforigin, refbasis);
    acosy = rt.transform(refcopy);
    lateral = acosy.xaxis';

    % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Set up the MeshICS instance.

    mi = MeshICS(m, params);

    % ~~~~~ Given the manual annotation,
    %       find optimal values for `ixnosetip` and `ixplane`.

    tips = mi.nosetips.caps;
    [dnt,ixnt] = pdist2(tips, ptnosetip, 'Euclidean', 'Smallest',1);
    defineNoseTip(mi, ixnt);

    if dnt > 10
      fprintf(2, 'WARNING: Nose tip cannot be located (%s).\n', names{i});
    end

    symaxes = mi.symaxes';
    dps = pdist2(symaxes, lateral, 'Cosine');
    [dps,ixps] = min(acos(abs(1 - dps)));
    defineMidplane(mi, ixps);

    if dps > mi.angle_neg
      fprintf(2, 'WARNING: Plane of symmetry cannot be located (%s).\n', names{i});
    elseif dps > mi.angle_pos
      fprintf(2, 'WARNING: Plane of symmetry is not very accurate (%s).\n', names{i});
    end

    % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Take the ICS measurements.

    [a1,a0] = mi.annotate(refanno, refcosy);
    irt = RigidTransformation.fromcoordinatesystems(mi.coordsys, refcosy);

    if isempty(mi.ixnosetip) || mi.ixnosetip ~= ixnt || ...
        isempty(mi.ixplane) || mi.ixplane ~= ixps
      fprintf(2, 'ERROR: ICS tracked back - skip (%s).\n', names{i});
      continue;
    end

    % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Report:

    res = stats(i,:);

    % - the mean landmark error
    res(1) = a1.mle(a);

    % - the quality of fit of ICS to manual.
    res(2) = ptnosetip * mi.symaxes(:,ixps) - mi.symplanes(ixps);
    res(3) = dps;

    % - the selected indices.
    res(4) = ixnt;
    res(5) = ixps;

    % - the variance of line segment midpoints.
    res(6) = mi.symvar(ixps);

    % - the offset of the ICS nose tip to the plane of symmetry.
    %       this is interesting: the plane offset estimate may be wrong.
    res(7) = mi.nosetip * mi.symaxes(:,ixps) - mi.symplanes(ixps);

    % - the Gaussian curvature value at the nose tip.
    res(8) = mi.points.K(ixnt);

    % - whether the x-axis sign is under doubt (shape or texture is flipped).
    res(9) = mi.xaxis_flag;

    % - the offset from snapping to the surface, aligned with refanno.
    diff = a1 - a0;
    res(10:end) = irt.transform(diff.coordinates);

    stats(i,:) = res;
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Return statistics.

  % ~~~~~ Transform stats to single struct
  %       with array and matrix properties.

  stats = struct( ...
    'names', {names}, ...
    'error', stats(:,1), ...
    'manual_offset', stats(:,2), ...
    'manual_angle', stats(:,3), ...
    'ixnosetip', stats(:,4), ...
    'ixplane', stats(:,5), ...
    'midpoints_variance', stats(:,6), ...
    'nosetip_offset', stats(:,7), ...
    'nosetip_K', stats(:,8), ...
    'xaxis_flag', stats(:,9), ...
    'delta', reshape(stats(:,10:end), [numfiles size(refanno.coordinates)]) ...
  );
end
