function clearSymmetry(mi)
% Undefine symmetry variables so they are recomputed on the next request.
%
% Input arguments:
%  MI  MeshICS instance.
%
  mi.has_symmetry_defined = false;
  mi.symaxes = [];
  mi.symplanes = [];
  mi.symvar = [];
end
