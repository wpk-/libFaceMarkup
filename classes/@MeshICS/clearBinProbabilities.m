function clearBinProbabilities(mi)
% Undefine bin probabilities so they are recomputed on the next request.
%
% Input arguments:
%  MI  MeshICS instance.
%
  mi.has_binprobabilities_defined = false;
  mi.binpriors = [];
  mi.bincounts = [];
  mi.binprobs = [];
end
