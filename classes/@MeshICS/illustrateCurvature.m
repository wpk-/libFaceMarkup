function illustrateCurvature(mi, fighnd)
% Render the mesh textured with curvature values.
%
% Input arguments:
%  MI      MeshICS instance.
%  FIGHND  Optional scalar specifying a figure handle for plotting.
%          Default is 101.
%
  if nargin<2 || isempty(fighnd), fighnd=101; end
  
  K = mi.mask .* mi.K;
  cl = max(abs(K)) * [-1 1]; % center CLim at 0.
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Set the figure up.
  
  f = fighnd;
  figure(f);
  clf(f);
  cmap = make_colormap(f, 'jet');
  colormap(f, cmap);
  a = gca(f);
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Draw mesh with curvature texture.
  
  trisurf(mi.mesh, K, 'Parent',a);
  %shading(a, 'interp');
  set(a, 'CLim',cl);
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Legend.
  
  title(a, mi.mesh.name);
  colorbar('peer',a);
  axis(a, 'off');
  view(a, 2);
end
