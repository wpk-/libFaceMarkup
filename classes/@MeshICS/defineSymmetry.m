function defineSymmetry(mi)
% From `binprobs`, find estimates for the face's plane of symmetry.
%
% Input arguments:
%  MI     MeshICS instance.
%
% Considerations:
%  - Note that we generate multiple hypotheses, so
%    `mi.symaxes` is not 1x3, but Cx3 instead.
%    and likewise `mi.symplanes` is Cx1.
%

  % Get the variables that might require computation.
  % Get them first to play nice with verbosity.
  probs = mi.binprobs;
  counts = mi.bincounts;
  priors = mi.binpriors;
  lines = mi.lines;
  posvec = lines.posvec;

  if mi.verbose
    fprintf('Define symmetry...');
    t0 = tic;
  end

  % Get the relevant data and parameters.
  bincentres = mi.bincentres;
  cosmaxangle = cos(mi.angle_pos);
  poslinesminnum = mi.poslines_minnum;  % PoBI: 2&5 equal, FRGC: 5 probably better

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Find candidates for the symmetry axis.

  % The local minima in `binprobs` are candidates for the symmetry axis.
  % Specifically, we want bins whose counts are *higher* than expected, i.e.,
  % the bin count should be higher than the mean of the Binomial distribution.
  if mi.version > 2
    ixhighcount = counts > sum(counts).*priors;
    probs(~ixhighcount) = Inf;
  end
  % `ixminprob` is usually just a handful of directions/bins.
  % Sort them from lowest (best candidate) to highest value (worst).
  ixminprob = bincentres.findpeaks(probs);
  [~,ixsort] = sort(probs(ixminprob));
  ixminprob = find(ixminprob);
  ixminprob = ixminprob(ixsort);

  % Each selected bin is a candidate for the lateral axis.
  % Transpose so candidates are column vectors,
  % listed along the second dimension.
  bins = bincentres.vertices(ixminprob,:)';

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Refine candidate selection.

  % The mean of the collected vectors
  % better reflects the direction of symmetry.

  % Make the vectors unit length, so
  % dot product equals cosine of angle.
  posvec = rdiv2norm(posvec);     % row vectors
  bins = cdiv2norm(bins);         % column vectors
  D = posvec * bins;

  support = abs(D) > cosmaxangle; % angle < mi.angle_pos

  % Bins with hardly any pos vectors are not interesting.
  % Pos vectors not pointing to an interesting bin can be discarded.
  keepbins = sum(support, 1) >= poslinesminnum;
  keeplines = any(support(:,keepbins), 2);

  support = support(keeplines,keepbins);
  D = D(keeplines,keepbins);
  posvec = posvec(keeplines,:);

  % Update bins to be average of pos vectors.
  % `signed` ensures signs of pos vectors match bins.
  signed = double(support);
  signed(support) = signed(support) .* sign(D(support));
  bins = posvec' * signed;
  bins = cdiv2norm(bins);         % column vectors

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Continue to find the plane offsets.

  midpoints = mi.lines.midpoints(keeplines);

  % Compute a little too much, but at least
  % it's a nice matrix multiplication
  % instead of a nasty for-loop.
  projected = midpoints * bins;
  projected(~support) = nan;

  offsets = nanmedian(projected, 1);
  variances = mad(projected, 1);

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Save to `mi`.

  mi.symaxes = bins;
  mi.symplanes = offsets;
  mi.symvar = variances;

  % store pointer for illustration purposes only.
  mi.ixsrcbins = ixminprob(keepbins);

  mi.has_symmetry_defined = true;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Verbosity & visualisation.

  if mi.verbose
    fprintf(' time: %.2f sec.\n', toc(t0));

    if mi.verbose > 1
      illustrateSymmetryAxis(mi);
      % TODO: illustrateSymmetryPlane(mi, ...); (for i = 1:3)
    end
  end
end
