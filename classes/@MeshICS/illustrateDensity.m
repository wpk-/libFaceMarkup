function illustrateDensity(mi, fighnd)
% Render all directional bins and their density scores.
%
% Input arguments:
%  MI      MeshICS instance.
%  FIGHND  Optional scalar specifying a figure handle for plotting.
%          Default is 103.
%
  if nargin<2 || isempty(fighnd), fighnd=103; end
  
  bins = mi.bincentres;
  rbins = bins.vertices;
  
  rp = mi.bincounts';%mi.rhopos;
  rn = mi.binpriors';%mi.rhoneg;
  r = mi.binprobs';%mi.rho;
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Set the figure up.
  
  f = fighnd;
  figure(f);
  clf(f);
  %cmap = make_colormap(f, 'jet', false, false);
  cmap = 'jet';
  colormap(f, cmap);
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Figure 3. Orientation density.
  
  % ~~~~~ a. rho+
  
  a = subplot(3, 1, 1, 'Parent',f);
  plotdensity2d(a, bins, rbins, rp);
  title(a, 'Bin counts of vectors in V^+.');
  
  fi = figure('Renderer','zbuffer');
  colormap(fi, cmap);
  a = subplot(1, 1, 1, 'Parent',fi);
  plotdensity3d(a, bins, rbins, rp);
  camorbit(90, 0, 'camera');
  title(a, 'Counts of vectors in V^+.');
  figure(f);
  
  % ~~~~~ b. rho-
  
  a = subplot(3, 1, 2, 'Parent',f);
  plotdensity2d(a, bins, rbins, rn);
  title(a, 'Bin priors estimated from V^-.');
  
  fi = figure('Renderer','zbuffer');
  colormap(fi, cmap);
  a = subplot(1, 1, 1, 'Parent',fi);
  plotdensity3d(a, bins, rbins, rn);
  camorbit(90, 0, 'camera');
  title(a, 'Priors estimated on V^-.');
  figure(f);
  
  % ~~~~~ c. rho
  
  a = subplot(3, 1, 3, 'Parent',f);
  plotdensity2d(a, bins, rbins, r);
  title(a, 'Probability of s(u in V^+) given V^-.');
  
  fi = figure('Renderer','zbuffer');
  colormap(fi, cmap);
  a = subplot(1, 1, 1, 'Parent',fi);
  plotdensity3d(a, bins, rbins, r);
  camorbit(90, 0, 'camera');
  title(a, 'Posterior probabilities.');
  figure(f);
  
%   
%   
%   stretch = 1.3;
%   
%   [x,y,z] = sphere;
%   xax     = xaxis' * stretch;
%   pos     = rdiv2norm(lines.posvec) * 1.001;
%   neg     = rdiv2norm(lines.negvec) * 1.001;
%   pos     = [pos; -pos];
%   neg     = [neg; -neg];
% 
%   verts   = [cs.directions; -cs.directions];
%   facs    = convhull(verts);
%   ball    = Mesh(verts, facs);
%   colour  = [Y; Y];
% 
%   d       = meshencoder.encodemesh(tri, xaxis', mask);
%   bins    = meshencoder.binranges(2:end) * 180/pi;
%   nbins   = numel(bins);
%   ipos    = 1:nbins;
%   ineg    = ipos + nbins;
%   irnd    = randperm(size(X, 1));
%   rgb     = (Y-min(Y)) ./ (max(Y)-min(Y));
%   rgb     = .9 * [rgb rgb ones(size(rgb))];
%   xl      = [0 90];
%   %yl      = [0 max([X(:); d(:)])];
% 
%   f = fighnd(3);
%   figure(f);
%   clf(f);
%   % ----- a.
%   a = subplot(2, 3, 1, 'Parent',f);
%   surf(a, x, y, z, 'EdgeColor',.9*[1 1 1], 'FaceAlpha',.9, 'FaceColor','w');
%   hold(a, 'on');
%   hx = quiver3(a, 0, 0, 0, xax(1), xax(2), xax(3), 0, 'filled', 'm', ...
%             'LineWidth',1.5);
%   hp = scatter3(a, pos(:,1), pos(:,2), pos(:,3), 10, 'b', 'filled');
%   hold(a, 'off');
%   xlabel(a, 'x');
%   ylabel(a, 'y');
%   zlabel(a, 'z');
%   title(a, 'Distribution of POS vectors and the lateral axis.');
%   legend(a, [hx hp], 'face''s lateral axis', 'pos vectors', ...
%             'Location','NorthEast');
%   axis(a, 'equal', 'vis3d');
%   % ----- b.
%   a = subplot(2, 3, 2, 'Parent',f);
%   hb = trisurf(ball, colour, 'Parent',a);
%   hold(a, 'on');
%   hx = quiver3(a, 0, 0, 0, xax(1), xax(2), xax(3), 0, 'filled', 'm', ...
%             'LineWidth',1.5);
%   hp = scatter3(a, pos(:,1), pos(:,2), pos(:,3), 10, 'b', 'filled');
%   hold(a, 'off');
%   xlabel(a, 'x');
%   ylabel(a, 'y');
%   zlabel(a, 'z');
%   title(a, 'Model predictions in all directions (lower should be better).');
%   legend(a, [hb hx hp], 'model prediction', 'prediction minimum', ...
%             'pos vectors', 'Location','NorthEast');
%   axis(a, 'equal', 'vis3d');
%   colormap(a, 'gray');
%   % ----- c.
%   a = subplot(2, 3, 3, 'Parent',f, 'ColorOrder',rgb(irnd,:), ...
%             'NextPlot','add');
%   plot(a, bins, X(irnd,ipos)');
%   hold(a, 'on');
%   plot(a, bins, d(ipos), 'Color',[1 0 0], 'LineWidth',1.5);
%   hold(a, 'off');
%   xlabel(a, 'Radius (degrees)');
%   ylabel(a, 'Density (vectors% / area%)');
%   title(a, 'Density of POS vectors at various radii, as a graph.');
%   xlim(a, xl);
%   %ylim(a, yl);
%   % ----- d.
%   a = subplot(2, 3, 4, 'Parent',f);
%   surf(a, x, y, z, 'EdgeColor',.9*[1 1 1], 'FaceAlpha',.9, 'FaceColor','w');
%   hold(a, 'on');
%   hx = quiver3(a, 0, 0, 0, xax(1), xax(2), xax(3), 0, 'filled', 'm', ...
%             'LineWidth',1.5);
%   hn = scatter3(a, neg(:,1), neg(:,2), neg(:,3), 10, 'b', 'filled');
%   hold(a, 'off');
%   xlabel(a, 'x');
%   ylabel(a, 'y');
%   zlabel(a, 'z');
%   title(a, 'Distribution of NEG vectors and the lateral axis.');
%   legend(a, [hx hn], 'face''s lateral axis', 'neg vectors', ...
%             'Location','NorthEast');
%   axis(a, 'equal', 'vis3d');
%   % ----- e.
%   a = subplot(2, 3, 5, 'Parent',f);
%   hb = trisurf(ball, colour, 'Parent',a);
%   hold(a, 'on');
%   hx = quiver3(a, 0, 0, 0, xax(1), xax(2), xax(3), 0, 'filled', 'm', ...
%             'LineWidth',1.5);
%   hp = scatter3(a, neg(:,1), neg(:,2), neg(:,3), 10, 'b', 'filled');
%   hold(a, 'off');
%   xlabel(a, 'x');
%   ylabel(a, 'y');
%   zlabel(a, 'z');
%   title(a, 'Model predictions in all directions (lower should be better).');
%   legend(a, [hb hx hp], 'model prediction', 'prediction minimum', ...
%             'neg vectors', 'Location','NorthEast');
%   axis(a, 'equal', 'vis3d');
%   colormap(a, 'gray');
%   % ----- f.
%   a = subplot(2, 3, 6, 'Parent',f, 'ColorOrder',rgb(irnd,:), ...
%             'NextPlot','add');
%   plot(a, bins, X(irnd,ineg)');
%   hold(a, 'on');
%   plot(a, bins, d(ineg), 'Color',[1 0 0], 'LineWidth',1.5);
%   hold(a, 'off');
%   xlabel(a, 'Radius (degrees)');
%   ylabel(a, 'Density (vectors% / area%)');
%   title(a, 'Density of NEG vectors at various radii, as a graph.');
%   xlim(a, xl);
%   %ylim(a, yl);

end
