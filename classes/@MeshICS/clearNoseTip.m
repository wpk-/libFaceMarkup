function clearNoseTip(mi)
% Undefine `mi.ixnosetip` and `mi.nosetips` to be recomputed on next request.
%
% Input arguments:
%  MI  MeshICS instance.
%
  mi.has_nosetip_defined = false;
  mi.ixnosetip = [];
  mi.nosetips = [];
end
