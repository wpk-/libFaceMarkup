function defineNoseTip(mi, ixnosetip)
% Generate nose tip candidates and select one.
%
% Input arguments:
%  MI         MeshICS instance.
%  IXNOSETIP  Optional scalar index to mark a specific cap point as nose tip.
%             Default is 1.
%
  if nargin<2 || isempty(ixnosetip), ixnosetip=1; end

  K = mi.K;       % Getting these values upfront gives nicer verbose prints.
  H = mi.H;
  mask = mi.mask;

  if mi.verbose
    %fprintf('Define nose tip...');
    t0 = tic;
  end

  verbose = mi.verbose;
  mindist = mi.peaks_mindist;
  maxnum = mi.peaks_maxnum;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Construct all candidates.

  if ~mi.has_nosetip_defined
    % ~~~~~ No need to define smoothing parameters,
    %       because curvature will be copied from `mi`.
    params = struct( ...
      'Mask', mask, ...
      'Verbose', verbose ...
    );
    points = CurvaturePoints(mi.mesh, params);

    % ~~~~~ Copy curvature values.
    defineCurvature(points, K, H);

    % ~~~~~ Reduce the candidate set.
    points = points.subset([], 1:points.numcaps, []);
    points.filter(mindist, maxnum);

    % ~~~~~ Save on `mi`
    mi.nosetips = points;

    %
    numtips = points.numcaps;
  else
    numtips = mi.nosetips.numcaps;
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Last checks.

  if mi.version == 1 && ixnosetip > 1
    error('MeshICS:Failed', ...
          'ICS v1 does not track back over nose tips (%s).', mi.mesh.name);
  elseif ixnosetip > numtips
    error('MeshICS:Failed', ...
          'No more nose tip candidates (%s).', mi.mesh.name);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Define nose tip point.

  mi.ixnosetip = ixnosetip;

  mi.has_nosetip_defined = true;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Verbosity & visualisation.

  if mi.verbose
    fprintf('Defined nose tip in %.2f sec.\n', toc(t0));
    %fprintf(' time: %.2f sec.\n', toc(t0));
  end
end
