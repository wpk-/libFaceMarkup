function defineBinProbabilities(mi)
% Measure the probabilities of posvec counts in each bin.
% ... given the prior probabilities measured from the negvec counts.
%
% Input arguments:
%  MI     MeshICS instance.
%
  posvec = mi.lines.posvec;
  negvec = mi.lines.negvec;

  if mi.verbose
    fprintf('Define bin probabilities...');
    t0 = tic;
  end

  bins = mi.bincentres.vertices;

  posangle = mi.angle_pos;
  negangle = mi.angle_neg;

  % ~~~~~~~~~~~~~~~~~~~~~~~ Compute probabilities based on negvec and posvec.

  % Because uniform sampling is impossible on the ball,
  % the bins overlap -->   angle > furthest neighbouring bins.
  % And stronger, to get a smoother estimate,
  % the neg angle is wider.

  if mi.version > 2
    % ~~~~~ Bin priors from negvec.

    priors = bincounts(negvec, bins, negangle);
    priors = priors + 1;
    priors = priors ./ sum(priors);

    % ~~~~~ Posteriors from posvec counts.

    counts = bincounts(posvec, bins, posangle);
    mass = binopdf(counts, sum(counts), priors);
  else
    % ~~~~~ Compatibility with older versions.
    rhopos = density(posvec, bins, posangle);
    rhoneg = density(negvec, bins, negangle);
    priors = rhoneg;
    counts = rhopos;
    mass = rhoneg - rhopos; % neg - pos so min() is correct.
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Save on `mi`.

  mi.binpriors = priors;
  mi.bincounts = counts;
  mi.binprobs = mass;

  mi.has_binprobabilities_defined = true;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Verbosity & visualisation.

  if mi.verbose
    fprintf(' time: %.2f sec.\n', toc(t0));
  end
end
