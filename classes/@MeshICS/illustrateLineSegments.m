function illustrateLineSegments(mi, fighnd)
% Render the mesh textured with curvature values.
%
% Input arguments:
%  MI      MeshICS instance.
%  FIGHND  Optional scalar specifying a figure handle for plotting.
%
  if nargin<2 || isempty(fighnd), fighnd=102; end

  mask = mi.mask;
  K = mi.K;
  points = mi.points;
  lines = mi.lines;

  t0 = Mesh.copy(mi.mesh);
  t0.vertices(:,3) = min(t0.vertices(:,3));
  K = mask .* K;
  cl = max(abs(K)) * [-1 1];  % center CLim at 0.

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Prepare exemplar data.

  % ~~~~~ Positive line segments.

  rndpos = randperm(lines.numpos, 14);
  ipos = lines.ixpos(rndpos,:);
  icup = ismember(points.ixcup, ipos);
  icap = ismember(points.ixcap, ipos);
  isad = ismember(points.ixsaddle, ipos);
  ptspos = points.subset(icup, icap, isad);
  linpos = lines.subset(rndpos, []);

  % ~~~~~ Negative line segments.

  rndneg = randperm(lines.numneg, 14);
  ineg   = lines.ixneg(rndneg,:);
  icup   = ismember(points.ixcup, ineg);
  icap   = ismember(points.ixcap, ineg);
  isad   = ismember(points.ixsaddle, ineg);
  ptsneg = points.subset(icup, icap, isad);
  linneg = lines.subset([], rndneg);

  % ~~~~~ Lateral line segments.

  linx   = lines.supporting(mi.xaxis, mi.angle_pos);
  ipos   = linx.ixpos;
  icup   = ismember(points.ixcup, ipos);
  icap   = ismember(points.ixcap, ipos);
  isad   = ismember(points.ixsaddle, ipos);
  ptsx   = points.subset(icup, icap, isad);
  %linx   = lines.subset(posx, []);

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Set the figure up.

  f = fighnd;
  figure(f);
  clf(f);
  cmap = make_colormap(f);
  colormap(f, cmap);

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Figure 2. Pos/Neg lines.

  % ~~~~~ a. POS line segments

  a = subplot(1, 3, 1, 'Parent',f);
  hk = trisurf(t0, K, 'Parent',a);
  shading(a, 'interp');
  set(a, 'CLim',cl);
  hold(a, 'on');
  hcur = scatter3(ptspos, 'Parent',a, 'LineWidth',1.5);
  hlin = plot3(linpos, 'c-', 'Parent',a, 'LineWidth',1.5);
  hold(a, 'off');
  title(a, 'A random selection of POS line segments.');
  legend(a, [hk hcur(1) hlin(1)], 'curvature', 'stationary points', ...
            'pos line segments', 'Location','NorthEast');
  axis(a, 'off');
  view(a, 2);
  zoom(f, 0);

  % ~~~~~ b. NEG line segments

  a = subplot(1, 3, 2, 'Parent',f);
  hk = trisurf(t0, K, 'Parent',a);
  shading(a, 'interp');
  set(a, 'CLim',cl);
  hold(a, 'on');
  hcur = scatter3(ptsneg, 'Parent',a, 'LineWidth',1.5);
  hlin = plot3(linneg, 'm-', 'm-', 'Parent',a, 'LineWidth',1.5);
  hold(a, 'off');
  title(a, 'A random selection of NEG line segments.');
  legend(a, [hk hcur(1) hlin(1)], 'curvature', 'stationary points', ...
            'neg line segments', 'Location','NorthEast');
  axis(a, 'off');
  view(a, 2);
  zoom(f, 0);

  % ~~~~~ c. Lateral line segments

  a = subplot(1, 3, 3, 'Parent',f);
  hk = trisurf(t0, K, 'Parent',a);
  shading(a, 'interp');
  set(a, 'CLim',cl);
  hold(a, 'on');
  hcur = scatter3(ptsx, 'Parent',a, 'LineWidth',1.5);
  hlin = plot3(linx, 'c-', 'Parent',a, 'LineWidth',1.5);
  hold(a, 'off');
  title(a, 'POS line segments aligned with the face''s lateral axis.');
  legend(a, [hk hcur(1) hlin(1)], 'curvature', 'stationary points', ...
            'pos line segments', 'Location','NorthEast');
  axis(a, 'off');
  view(a, 2);
  zoom(f, 0);
end
