function defineCurvature(mi)
% Compute and store the Gaussian curvature (and principal curvature).
%
% Input arguments:
%  MI     MeshICS instance.
%
  mask = mi.mask;

  if mi.verbose
    fprintf('Compute curvature...');
    t0 = tic;
  end

  c_order = mi.curvature_order;   % Neighbourhood size for curvature.
  s_order = mi.smoothing_order;   % Neighbourhood size for smoothing.
  s_fcn = mi.smoothing_function;  % Maps distances to "function values".

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Compute the Gaussian surface curvature.

  mi.mesh.default_neighbourhood_order = c_order;
  K = mi.mesh.K;
  H = mi.mesh.H;

  if s_order
    W = mi.mesh.smoothmatrix(s_order, s_fcn, mask);
    K = W * K;
    H = W * H;
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Save to `mi`.

  mi.K = K;
  mi.H = H;

  mi.has_curvature_defined = true;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Verbosity & visualisation.

  if mi.verbose
    fprintf(' time: %.2f sec.\n', toc(t0));

    if mi.verbose > 1
      illustrateCurvature(mi);
    end
  end
end
