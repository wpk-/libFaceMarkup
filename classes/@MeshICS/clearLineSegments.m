function clearLineSegments(mi)
% Undefine `mi.lines` so it will be recomputed on the next request.
%
% Input arguments:
%  MI  MeshICS instance.
%
  mi.has_linesegments_defined = false;
  mi.lines = [];
end
