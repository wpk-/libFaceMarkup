function flipx(mi)
% Flip the x-axis (and the y-axis).
%
% Input arguments:
%  MI  MeshICS instance.
%
  mi.xaxis = -mi.xaxis;
  mi.yaxis = -mi.yaxis;
end
