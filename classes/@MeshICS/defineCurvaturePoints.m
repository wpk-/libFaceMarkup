function defineCurvaturePoints(mi)
% Find the cup-, cap-, and saddle-shaped Gaussian curvature extreme points.
%
% Input arguments:
%  MI     MeshICS instance.
%
  nosetip = mi.nosetip;
  K = mi.K;       % And the same holds for curvature.
  H = mi.H;
  mask = mi.mask;

  if mi.verbose
    %fprintf('Define curvature points...');
    t0 = tic;
  end

  verbose = mi.verbose;
  mindist = mi.peaks_mindist;
  maxnum = mi.peaks_maxnum;
  radius = 150;   % 150mm = 15cm.

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Construct the CurvaturePoints.

  % ~~~~~ No need to define smoothing parameters,
  %       because curvature will be copied from `mi`.

  params = struct( ...
    'Mask', mask, ...
    'Verbose', verbose ...
  );

  points = CurvaturePoints(mi.mesh, params);

  % ~~~~~ Copy curvature values
  %       onto the curvature points.

  defineCurvature(points, K, H);

  % ~~~~~ Reduce the candidate set.

  if version > 3
    points = points.closeto(nosetip, radius);
  end

  points.filter(mindist, maxnum);

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Save on `mi`.

  mi.points = points;

  mi.has_curvaturepoints_defined = true;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Verbosity & visualisation.

  if mi.verbose
    fprintf('Defined curvature points in %.2f sec.\n', toc(t0));
    %fprintf(' time: %.2f sec.\n', toc(t0));
  end
end
