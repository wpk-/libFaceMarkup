function mi = loadobj(s)
% Restore the MeshICS from struct.
%
% Input arguments:
%  S  Struct with the necessary fields to reconstruct MI, excluding most
%     dependent properties.
%
% Output arguments:
%  MI  A MeshICS instance.
%
  if isstruct(s)
    mi = MeshICS;

    flds = fieldnames(s);
    for i = 1:numel(flds)
      f = flds{i};
      mi.(f) = s.(f);
    end
  else
    mi = s;
  end
end
