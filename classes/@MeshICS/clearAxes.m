function clearAxes(mi)
% Undefine axes variables so they are recomputed on the next request.
%
% Input arguments:
%  MI  MeshICS instance.
%
  mi.has_xaxis_defined = false;
  mi.has_yaxis_defined = false;
  mi.has_zaxis_defined = false;
  mi.xaxis = [];
  mi.yaxis = [];
  mi.zaxis = [];
end
