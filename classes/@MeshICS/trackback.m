function trackback(mi)
% Generate the "next best" ICS hypothesis.
%
% Input arguments:
%  MI     MeshICS instance.
%
% Considerations:
%  - In your application wrap a `try..catch` block to detect the error
%    'MeshICS:Failed'. This is thrown when no more solutions are available.
%    Then call `mi.default()` to get a default ICS.
%

  % When `backtrackMidplane` runs out of candidates, it will automatically call
  % `backtrackNoseTip`. When `defineNoseTip` runs out of candidates, it will
  % throw 'MeshICS:Failed'.

  mi.backtrackMidplane();
end
