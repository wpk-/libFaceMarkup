function backtrackNoseTip(mi)
% Provide a back-tracking mechanism over the nose tip selection (`ixnosetip`).
%
% Input arguments:
%  MI     MeshICS instance.
%
  fprintf(2, 'Backtracking nose tip...\n');

  mi.defineNoseTip(mi.ixnosetip + 1);

  mi.clearCurvaturePoints();
  mi.clearLineSegments();
  mi.clearBinProbabilities();
  mi.clearSymmetry();
  mi.clearMidplane();
  mi.clearOrigin();
  mi.clearAxes();

  if mi.verbose
    fprintf('  ixnosetip: %d.\n', mi.ixnosetip);
  end
end
