function illustrateCurvaturePoints(mi, fighnd)
% Render the mesh textured with curvature values.
%
% Input arguments:
%  MI      MeshICS instance.
%  FIGHND  Optional scalar specifying a figure handle for plotting.
%
  if nargin<2 || isempty(fighnd), fighnd=101; end
  
  cups = mi.points.cups;
  caps = mi.points.caps;
  saddles = mi.points.saddles;
  
  K = mi.mask .* mi.K;
  cl = max(abs(K)) * [-1 1]; % center CLim at 0.
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Set the figure up.
  
  f = fighnd;
  figure(f);
  clf(f);
  cmap = make_colormap(f);
  colormap(f, cmap);
  a = gca(f);
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Surface curvature & stationary points.
  
  % 1. Surface.
  
  hk = trisurf(mi.mesh, K, 'Parent',a);
  %shading(a, 'interp');
  set(a, 'CLim',cl);
  
  % 2. Stationary points.
  
  hold(a, 'on');
  scatter3(a, cups(:,1), cups(:,2), cups(:,3), 'c', 'LineWidth',1.5);
  scatter3(a, caps(:,1), caps(:,2), caps(:,3), 'm', 'LineWidth',1.5);
  scatter3(a, saddles(:,1), saddles(:,2), saddles(:,3), 'y', 'LineWidth',1.5);
  hpts = plot(a, nan, nan, 'co', nan, nan, 'mo', nan, nan, 'yo', ...
            'LineWidth',1.5);
  hold(a, 'off');
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Legend.
  
  title(a, 'Gaussian curvature and its stationary points.');
  legend(a, [hk; hpts], 'Gaussian curvature', 'cup', 'cap', 'saddle');
  colorbar('peer',a);
  axis(a, 'off');
  view(a, 2);
end
