function s = saveobj(mi)
% Provide a minimal object to be stored, ignoring all derived properties.
%
% Input arguments:
%  MI  A MeshICS instance.
%
% Output arguments:
%  S  Struct with the necessary fields to reconstruct MI.
%
  copy_properties = {...
    'angle_neg';
    'angle_pos';
    'bincentres';
    'curvature_order';
    'mesh';
    'peaks_mindist';
    'peaks_maxnum';
    'plane_maxvar';
    'poslines_minnum';
    'smoothing_function';
    'smoothing_order';
    'tip_maxoffset';
    'verbose';
    'version';
  };

  for i = 1:numel(copy_properties)
    prop = copy_properties{i};
    s.(prop) = mi.(prop);
  end

  if mi.has_mask_defined
    s.mask = mi.mask;
    s.has_mask_defined = mi.has_mask_defined;
  end

  if mi.has_nosetip_defined
    s.ixnosetip = mi.ixnosetip;
    % Don't set `has_nosetip_defined`,
    % because it will have to recompute things.
  end

  if mi.has_midplane_defined
    s.ixplane = mi.ixplane;
    % Don't set `has_midplane_defined`,
    % because it will have to recompute things.
  end
end
