function default(mi)
% When all else fails, default.
%
% Input arguments:
%  MI      MeshICS instance.
%
  fprintf(2, 'Default.\n');

  vertices = mi.mesh.vertices;

  % Reset memory.
  mi.defineNoseTip(1);
  mi.backtrackNoseTip();
  mi.defineNoseTip(1);

  % Support points for y-axis are undefined.
  mi.a = nan(1, 3);
  mi.b = nan(1, 3);

  % Intrinsic origin is at image origin, though
  % snapping it to the surface is the least we can do.
  [~,ix] = pdist2(vertices, [0 0 0], 'Euclidean', 'Smallest',1);
  mi.origin = vertices(ix,:);

  % x-, y-, and z-axis are also the image axes.
  mi.xaxis = [1; 0; 0];
  mi.yaxis = [0; 1; 0];
  mi.zaxis = [0; 0; 1];

  % Now pretend all went smooth.
  mi.has_origin_defined = true;
  mi.has_xaxis_defined = true;
  mi.has_yaxis_defined = true;
  mi.has_zaxis_defined = true;
end
