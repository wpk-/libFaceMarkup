function defineLineSegments(mi)
% Find all pairs of curvature points, either of the same or of different type.
%
% Input arguments:
%  MI     MeshICS instance.
%
  cp = mi.points;

  if mi.verbose
    %fprintf('Define line segments...');
    t0 = tic;
  end

  verbose = mi.verbose;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Construct the PosNegLines.

  params = struct( ...
    'Verbose', verbose ...
  );

  mi.lines = PosNegLines(cp, params);

  mi.has_linesegments_defined = true;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Verbosity & visualisation.

  if mi.verbose
    fprintf('Defined line segments in %.2f sec.\n', toc(t0));
    %fprintf(' time: %.2f sec.\n', toc(t0));
  end
end
