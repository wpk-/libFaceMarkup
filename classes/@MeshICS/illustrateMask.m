function illustrateMask(mi, fighnd)
% Render the mesh textured with curvature values.
%
% Input arguments:
%  MI      MeshICS instance.
%  FIGHND  Optional scalar specifying a figure handle for plotting.
%          Default is 100.
%
  if nargin<2 || isempty(fighnd), fighnd=100; end
  
  mask = mi.mask;
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Set the figure up.
  
  f = fighnd;
  figure(f);
  clf(f);
  cmap = make_colormap(f);
  colormap(f, cmap);
  a = gca(f);
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Draw mesh with mask texture.
  
  trisurf(mi.mesh, double(mask), 'Parent',a);
  %shading(a, 'interp');
  
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Legend.
  
  title(a, mi.mesh.name);
  axis(a, 'off');
  view(a, 2);
end
