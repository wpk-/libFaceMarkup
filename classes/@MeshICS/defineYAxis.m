function defineYAxis(mi)
% Compute the face intrinsic depth axis (pointing from back to front).
%
% Input arguments:
%  MI     MeshICS instance.
%
  zaxis = mi.zaxis;
  xaxis = mi.xaxis;

  if mi.verbose
    fprintf('Define the y-axis...');
    t0 = tic;
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Y points from bottom to top.

  mi.yaxis = cross(zaxis, xaxis);

  mi.has_yaxis_defined = true;

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Verbosity & visualisation.

  if mi.verbose
    fprintf(' time: %.2f sec.\n', toc(t0));

    if mi.verbose > 1
      % TODO: add nice illustration.
    end
  end
end
