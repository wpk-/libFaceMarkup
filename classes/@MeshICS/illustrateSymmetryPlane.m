function illustrateSymmetry(mi, ax, midpoints, projected)
  warning('TODO: Implement illustrateSymmetry.');
  return;
  
  figure;
  trisurf(mi.mesh);




  linx.points = bsxfun(@minus, linx.points, coordsys.origin) * coordsys.basis;
  linx.points(:,3) = 1;

  m  = linx.midpoints();
  xx = 150 * [-1 1];
  yy = min(linx.points(:,2)) - [20 20];

  t1               = Mesh.copy(tri);
  t1.vertices      = bsxfun(@minus, t1.vertices, coordsys.origin)  * ...
                      coordsys.basis;
  t1.vertices(:,3) = 0;
  t1.facecolor     = repmat(rgb2gray(reshape(t1.facecolor,[],1,3)), 1, 3);
  t1.facecolor     = .2 * t1.facecolor + .8;
  t1.vertexcolor   = repmat(rgb2gray(reshape(t1.vertexcolor,[],1,3)), 1, 3);
  t1.vertexcolor   = .2 * t1.vertexcolor + .8;

  R  = [coordsys.basis(:,[3 2]) zeros(3,1)];

  t2               = Mesh.copy(tri);
  t2.vertices      = t2.vertices * R;
  t2.vertexcolor   = t1.vertexcolor;
  t2.facecolor     = t1.facecolor;

  % Countour edges connect faces with opposing normal vectors.
  he = t2.halfedges;
  ix = he(:,4) > (1:size(he,1))';
  f1 = he(ix,2);
  f2 = he(he(ix,4),2);
  an = ~(isnan(f1) | isnan(f2));
  f1 = f1(an);
  f2 = f2(an);
  c  = dot(t2.facenormals(f1,:), t2.facenormals(f2,:), 2);
  ix = find(ix);
  ix = ix(an);
  e  = ix(c<0);
  v  = [he(e,1) he(he(e,4),1)];
  x  = t2.vertices(:,1);
  y  = t2.vertices(:,2);

  yp = ypts * R;
  l  = (yp - yp([1 1 1],:)) * 2.5 + yp([1 1 1],:);
  p0 = mean(yp([2 3],:));
  ly = (yp([2 3],:) - [p0;p0]) * 2.3 + [p0;p0];

  f = fighnd(4);
  figure(f);
  clf(f);
  % ----- a.
  a = subplot(1, 2, 1, 'Parent',f);
  trisurf(t1, 'Parent',a);
  hold(a, 'on');
  quiver(a, xx(1), yy(1), xx(2)-xx(1), yy(2)-yy(1), 0, 'k', ...
          'LineWidth',1.5, 'MaxHeadSize',.07);
  text(xx(2)-5, yy(2)+5, 'x-axis', 'Parent',a, ...
          'HorizontalAlignment','right', 'VerticalAlignment','bottom');
  hlin = plot3(linx, a, 'k.-');
  hmid = plot3(a, m(:,1), m(:,2), m(:,3), 'mv', 'MarkerSize',7, 'LineWidth',2);
  plot3(a, m(:,1), yy(1), eps, 'm.', 'MarkerSize',16);
  hsym = plot(a, [0 0], ylim, 'b--');
  hold(a, 'off');
  title(a, ['Estimation of the plane of symmetry ' ...
          'from supporting POS line segments.']);
  hleg = legend(a, [hlin(1) hmid(1) hsym], 'pos line segment', 'mid point', ...
          'plane of symmetry', 'Location','NorthEast');
  % Change the symbol in the legend entry to better reflect the line segments.
  set(findobj(hleg, 'Marker','.'), 'Visible','off');
  hl   = findobj(hleg, 'LineStyle','-', 'Type','line');
  hlx  = get(hl, 'XData');
  set(hl, 'Marker','.', 'XData',(hlx-mean(hlx))*.7+mean(hlx));
  axis(a, 'off');
  view(a, 2);
  zoom(f, 0);
  % ----- b.
  a = subplot(1, 2, 2, 'Parent',f);
  trisurf(t2, 'Parent',a);
  hold(a, 'on');
  plot(a, x(v'), y(v'), 'k-');
  htan = plot(a, l([1 1;2 3],1), l([1 1;2 3],2), 'LineWidth',1.5);
  hori = scatter3(a, yp(1,1), yp(1,2), 1, 'k', 'filled');
  hyax = plot3(a, ly(:,1), ly(:,2), [2;2], 'm');
  scatter3(a, yp(2:3,1), yp(2:3,2), [1;1], 'c', 'filled');
  hold(a, 'off');
  title(a, 'Definition of the vertical axis (in the plane of symmetry).');
  hleg = legend(a, [hori htan hyax], 'origin', 'tangent line', ...
                'vertical axis', 'Location','NorthEast');
  hlin = findobj(hleg, 'Type','line', 'Color','b', 'LineStyle','-');
  xd   = get(hlin, 'XData');
  yd   = get(hlin, 'YData');
  par  = get(hlin, 'Parent');
  hold(a, 'on');
  scatter3(a, mean(xd), mean(yd), 1, 'c', 'filled', 'Parent',par);
  hold(a, 'off');
  axis(a, 'off');
  view(a, 2);
  
  
  
  
%     % Figure 4. Estimation of the plane of symmetry. ~~~~~~~~~~~~~~~~~~~~~~~~~~
%     %
%     linx.points = bsxfun(@minus, linx.points, coordsys.origin) * coordsys.basis;
%     linx.points(:,3) = 1;
%     
%     m  = linx.midpoints();
%     xx = 150 * [-1 1];
%     yy = min(linx.points(:,2)) - [20 20];
%     
%     t1               = Mesh.copy(tri);
%     t1.vertices      = bsxfun(@minus, t1.vertices, coordsys.origin)  * ...
%                         coordsys.basis;
%     t1.vertices(:,3) = 0;
%     t1.facecolor     = repmat(rgb2gray(reshape(t1.facecolor,[],1,3)), 1, 3);
%     t1.facecolor     = .2 * t1.facecolor + .8;
%     t1.vertexcolor   = repmat(rgb2gray(reshape(t1.vertexcolor,[],1,3)), 1, 3);
%     t1.vertexcolor   = .2 * t1.vertexcolor + .8;
%     
%     R  = [coordsys.basis(:,[3 2]) zeros(3,1)];
%     
%     t2               = Mesh.copy(tri);
%     t2.vertices      = t2.vertices * R;
%     t2.vertexcolor   = t1.vertexcolor;
%     t2.facecolor     = t1.facecolor;
%     
%     % Countour edges connect faces with opposing normal vectors.
%     he = t2.halfedges;
%     ix = he(:,4) > (1:size(he,1))';
%     f1 = he(ix,2);
%     f2 = he(he(ix,4),2);
%     an = ~(isnan(f1) | isnan(f2));
%     f1 = f1(an);
%     f2 = f2(an);
%     c  = dot(t2.facenormals(f1,:), t2.facenormals(f2,:), 2);
%     ix = find(ix);
%     ix = ix(an);
%     e  = ix(c<0);
%     v  = [he(e,1) he(he(e,4),1)];
%     x  = t2.vertices(:,1);
%     y  = t2.vertices(:,2);
%     
%     yp = ypts * R;
%     l  = (yp - yp([1 1 1],:)) * 2.5 + yp([1 1 1],:);
%     p0 = mean(yp([2 3],:));
%     ly = (yp([2 3],:) - [p0;p0]) * 2.3 + [p0;p0];
%     
%     f = fighnd(4);
%     figure(f);
%     clf(f);
%     % ----- a.
%     a = subplot(1, 2, 1, 'Parent',f);
%     trisurf(t1, 'Parent',a);
%     hold(a, 'on');
%     quiver(a, xx(1), yy(1), xx(2)-xx(1), yy(2)-yy(1), 0, 'k', ...
%             'LineWidth',1.5, 'MaxHeadSize',.07);
%     text(xx(2)-5, yy(2)+5, 'x-axis', 'Parent',a, ...
%             'HorizontalAlignment','right', 'VerticalAlignment','bottom');
%     hlin = plot3(linx, a, 'k.-');
%     hmid = plot3(a, m(:,1), m(:,2), m(:,3), 'mv', 'MarkerSize',7, 'LineWidth',2);
%     plot3(a, m(:,1), yy(1), eps, 'm.', 'MarkerSize',16);
%     hsym = plot(a, [0 0], ylim, 'b--');
%     hold(a, 'off');
%     title(a, ['Estimation of the plane of symmetry ' ...
%             'from supporting POS line segments.']);
%     hleg = legend(a, [hlin(1) hmid(1) hsym], 'pos line segment', 'mid point', ...
%             'plane of symmetry', 'Location','NorthEast');
%     % Change the symbol in the legend entry to better reflect the line segments.
%     set(findobj(hleg, 'Marker','.'), 'Visible','off');
%     hl   = findobj(hleg, 'LineStyle','-', 'Type','line');
%     hlx  = get(hl, 'XData');
%     set(hl, 'Marker','.', 'XData',(hlx-mean(hlx))*.7+mean(hlx));
%     axis(a, 'off');
%     view(a, 2);
%     zoom(f, 0);
%     % ----- b.
%     a = subplot(1, 2, 2, 'Parent',f);
%     trisurf(t2, 'Parent',a);
%     hold(a, 'on');
%     plot(a, x(v'), y(v'), 'k-');
%     htan = plot(a, l([1 1;2 3],1), l([1 1;2 3],2), 'LineWidth',1.5);
%     hori = scatter3(a, yp(1,1), yp(1,2), 1, 'k', 'filled');
%     hyax = plot3(a, ly(:,1), ly(:,2), [2;2], 'm');
%     scatter3(a, yp(2:3,1), yp(2:3,2), [1;1], 'c', 'filled');
%     hold(a, 'off');
%     title(a, 'Definition of the vertical axis (in the plane of symmetry).');
%     hleg = legend(a, [hori htan hyax], 'origin', 'tangent line', ...
%                   'vertical axis', 'Location','NorthEast');
%     hlin = findobj(hleg, 'Type','line', 'Color','b', 'LineStyle','-');
%     xd   = get(hlin, 'XData');
%     yd   = get(hlin, 'YData');
%     par  = get(hlin, 'Parent');
%     hold(a, 'on');
%     scatter3(a, mean(xd), mean(yd), 1, 'c', 'filled', 'Parent',par);
%     hold(a, 'off');
%     axis(a, 'off');
%     view(a, 2);
%     
end
