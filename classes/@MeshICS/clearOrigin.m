function clearOrigin(mi)
% Undefine origin variables so they are recomputed on the next request.
%
% Input arguments:
%  MI  MeshICS instance.
%
  mi.has_origin_defined = false;
  mi.a = [];
  mi.b = [];
  mi.origin = [];
end
