function deletemarker(aui, varargin)
% Delete the selected marker.
%
% Input arguments:
%  AUI  AnnotationUI instance.
%
  ix = aui.selected;

  if isempty(ix) || isnan(ix)
    return;
  end

  if aui.selectpreviousmarker() == ix
    if aui.selectnextmarker() == ix
      aui.selected = [];
    end
  end

  aui.markers(ix,:) = nan;
end
