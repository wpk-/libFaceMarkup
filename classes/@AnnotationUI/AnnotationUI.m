classdef AnnotationUI < handle
% Class to facilitate the annotation of landmarks on 3D objects.
%
% There are four components:
% i.   N markers - each marker marking one landmark.
% ii.  N marker labels - to print the landmark number next to the marker.
% iii. one pointer - the mouse pointer indicator.
% iv.  one selector - the marker highlighting the selected landmark.
%
% Except for the selector, instead of storing coordinates in matrices, we
% interact with the the plotted objects directly to reduce administrative
% overhead.
%

  properties
    label_offset = [3 0 5]; % Draw marker labels at a distance from the markers.
    warn_lnddef = true;     % Warn if a loaded annotation mismatches lnddef.
    warn_incomplete = true; % Warn before saving if an annotation is incomplete.
  end

  properties
    dirty;          % Indicate if data changed since last load/save.
  end

  properties (SetAccess=protected)
    lnddef;         % Get the LandmarkDefinition instance.
    numlandmarks;   % Get the number of landmarks.
  end

  % Handles to the plotted objects:
  properties (Access=protected)
    markerlabels_;  % Nx1 handles to marker labels.
    markers_;       % Nx1 handles to markers.
    pointer_;       % Handle of the mouse pointer indicator.
    selector_;      % Handle of the marker highlighting the selected landmark.
  end

  % Interfaces for component positions:
  properties (Dependent)
    annotation;     % Get an Annotation instance with the marker coordinates.
    markers;        % Get/set the position of all N markers.
    pointer;        % Get/set the position of the mouse pointer indicator.
  end
  properties
    selected;       % Get/set the selected landmark *number*.
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Class constructor.
  
  methods
    function aui = AnnotationUI(lnddef, ax)
      % Create an AnnotationUI instance that keeps track of markers, pointer,
      % and selector.
      %
      % Input arguments:
      %  LNDDEF  LandmarkDefinition instance. This dictates which landmarks
      %          will be annotated. Also used when saving, and to check file
      %          consistency when loading.
      %  AX      Optional axis handle to be used for annotation.
      %          Default value is `gca`.
      %
      % Output arguments:
      %  AUI  AnnotationUI instance.
      %
      if nargin<2 || isempty(ax), ax=gca; end

      aui.lnddef = lnddef;
      aui.numlandmarks = numel(lnddef.lxmap);

      aui.paint(ax);

      aui.selected = 1;
      aui.dirty = false;  % have to initialise before `reset()`.
      aui.reset();
      aui.dirty = false;  % have to revert after `reset()`.
    end

    function delete(aui)
      % Class destructor. Remove all plot data.
      try %#ok<TRYNC>
        markers = get(aui.markers_, 'Parent');
        delete(markers);
      end
      try %#ok<TRYNC>
        markerlabels = aui.markerlabels_;
        delete(markerlabels);
      end
      try %#ok<TRYNC>
        selector = get(aui.selector_, 'Parent');
        delete(selector);
      end
      try %#ok<TRYNC>
        pointer = get(aui.pointer_, 'Parent');
        delete(pointer);
      end
    end
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Class internals.

  methods
    % Plot all markers for all components onto the axes.
    paint(aui, ax);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ I/O.

  methods
    % Load landmarks from .lnd file into the annotation UI.
    loaded = loadannotation(aui, filename);
    % Save annotated landmarks to .lnd file.
    saved = saveannotation(aui, filename);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ User interaction.

  methods
    % Delete the selected marker.
    deletemarker(aui, varargin);
    % Place the pointer at the specified position.
    pointat(aui, xyz);
    % Define the position of next landmark and draw a marker.
    putmarker(aui, varargin);
    % Place the selector at the marker nearest to the specified position.
    selectat(aui, xyz);
    % Select the next marker following after the currently selected marker.
    ix = selectnextmarker(aui, varargin);
    % Select the first marker preceding the currently selected one.
    ix = selectpreviousmarker(aui, varargin);
    % Clear all landmark definitions and their markers.
    reset(aui, varargin);
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %   GETTERS AND SETTERS                                                 %%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  methods
%     nextindex;      % Get the first next not-yet annotated landmark number.

    % Get an Annotation instance with the marker coordinates.
    function anno = get.annotation(aui)
      indices = 1:aui.numlandmarks;
      coordinates = aui.markers;
      anno = Annotation(indices, coordinates, aui.lnddef);
    end

    % Get/set the position of all N markers.
    function xyz = get.markers(aui)
      xyz = get(aui.markers_, {'XData','YData','ZData'});
      xyz = cell2mat(cellfun(@transpose, xyz, 'UniformOutput',0));
    end
    function set.markers(aui, xyz)
      oldxyz = aui.markers;
      npts = aui.numlandmarks;
      offset = aui.label_offset;
      assert(size(xyz, 1) == npts, 'Assignment must match number of markers.');
      changed = ~all(oldxyz(:) == xyz(:));
      aui.dirty = aui.dirty || changed;
      if changed
        set(aui.markers_, 'XData',xyz(:,1), 'YData',xyz(:,2), 'ZData',xyz(:,3));
        for i = 1:npts
          set(aui.markerlabels_(i), 'Position',xyz(i,:)+offset);
        end
        aui.selected = aui.selected;  % force update.
      end
    end

    % Get/set the position of the mouse pointer indicator.
    function xyz = get.pointer(aui)
      xyz = get(aui.pointer_, {'XData','YData','ZData'});
      xyz = cell2mat(xyz);
    end
    function set.pointer(aui, xyz)
      assert(size(xyz, 1) == 1, 'Pointer is just one point.');
      set(aui.pointer_, 'XData',xyz(1), 'YData',xyz(2), 'ZData',xyz(3));
    end

    % Get/set the selected landmark number.
    function ix = get.selected(aui)
      ix = aui.selected;
    end
    function set.selected(aui, ix)
      if isempty(ix)
        ix = [];
        xyz = [nan nan nan];
      else
        assert(numel(ix) == 1, 'Can only select one marker.');
        assert(any(ix == 1:aui.numlandmarks), 'Invalid marker index.');
        xyz = aui.markers(ix,:);
      end
      set(aui.selector_, 'XData',xyz(1), 'YData',xyz(2), 'ZData',xyz(3));
      aui.selected = ix;
    end
  end
end
