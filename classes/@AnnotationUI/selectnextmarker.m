function ix = selectnextmarker(aui, varargin)
% Select the next marker following after the currently selected marker.
%
% Input arguments:
%  AUI  AnnotationUI instance.
%
% Output arguments:
%  IX  Scalar index of the newly selected marker, returned for convenience.
%
% Considerations:
%  The selection is only updated if a next marker is actually available.
%
  pts = aui.markers;
  ix = aui.selected;

  available = find(~any(isnan(pts), 2));
  next = min(available(available>ix));

  if ~isempty(next)
    ix = next;
  end

  aui.selected = ix;
end
