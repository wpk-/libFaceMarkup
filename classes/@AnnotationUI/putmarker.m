function putmarker(aui, varargin)
% Define the position of next landmark and draw a marker.
%
% Input arguments:
%  AUI   AnnotationUI instance.
%
  xyz = aui.pointer;

  if any(isnan(xyz))
    return;
  end

  pts = aui.markers;
  ix = find(any(isnan(pts),2), 1);

  if isempty(ix)
    return;
  end

  aui.markers(ix,:) = xyz;
  aui.selected = ix;
  aui.pointat(nan);
end
