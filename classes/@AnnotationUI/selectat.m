function selectat(aui, xyz)
% Place the selector at the marker nearest to the specified position.
%
% Input arguments:
%  AUI  AnnotationUI instance.
%  XYZ  1x3 array defining the target coodinate.
%
  assert(size(xyz, 1) == 1, 'Can only select one marker.');

  [d,ix] = pdist2(aui.markers, xyz, 'Euclidean', 'Smallest',1);

  if ~isempty(d) && ~isnan(d)
    aui.selected = ix;
  else
    aui.selected = [];
  end
end
