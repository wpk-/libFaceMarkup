function ix = selectpreviousmarker(aui, varargin)
% Select the first marker preceding the currently selected one.
%
% Input arguments:
%  AUI  AnnotationUI instance.
%
% Output arguments:
%  IX  Scalar index of the newly selected marker, returned for convenience.
%
% Considerations:
%  The selection is only updated if a previous marker is actually available.
%
  pts = aui.markers;
  ix = aui.selected;

  available = find(~any(isnan(pts), 2));
  previous = max(available(available < ix));

  if ~isempty(previous)
    ix = previous;
  end

  aui.selected = ix;
end
