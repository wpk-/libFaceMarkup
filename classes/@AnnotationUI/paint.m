function paint(aui, ax)
% Plot all markers for all components onto the axes.
%
% Input arguments:
%  AUI  AnnotationUI instance.
%  AX   Optional axis handle. Default value is `gca`.
%
  if nargin<2, ax=gca; end

  npts = aui.numlandmarks;
  nil = zeros(npts, 1);

  washold = ishold(ax);
  if ~washold
    hold(ax, 'on');
  end

  aui.markers_= scatter3(nil, nil, nil, 20, 'c', 'filled', ...
            'HitTest','off', 'Parent',ax);

  labels = arrayfun(@num2str, 1:npts, 'UniformOutput',0);
  aui.markerlabels_ = text(nil, nil, nil, labels, ...
            'Color','c', 'FontWeight','bold', 'HitTest','off', ...
            'Interpreter','none', 'Parent',ax);

  aui.selector_ = scatter3(0, 0, 0, 100, 'g', ...
            'LineWidth',2, 'HitTest','off', 'Parent',ax);

  aui.pointer_ = scatter3(0, 0, 0, 50, 'g', 'x', ...
            'HitTest','off', 'Parent',ax);

  if ~washold
    hold(ax, 'off');
  end
end
