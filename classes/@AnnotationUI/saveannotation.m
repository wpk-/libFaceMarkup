function saved = saveannotation(aui, filename)
% Save annotated landmarks to .lnd file.
%
% Input arguments:
%  AUI       AnnotationUI instance.
%  FILENAME  Name of the file to write landmark coordinates to.
%
% Output arguments:
%  SAVED  Logical scalar to indicate whether the annotation was saved.
%         It may not have been saved if the annotation was incomplete.
%
  saved = false;
  anno = aui.annotation;

  if aui.warn_incomplete && ~anno.iscomplete
    q = sprintf('Annotation contains %d of the %d markers. Continue?', ...
                numel(anno.indices), aui.numlandmarks);
    btn = questdlg(q, 'AnnotationUI', 'Yes', 'No', 'Yes');
    if ~strcmpi(btn, 'Yes')
      return;
    end
  end

  saveas(anno, filename);
  aui.dirty = false;
  saved = true;
end
