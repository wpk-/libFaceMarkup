function loaded = loadannotation(aui, filename)
% Load landmarks from .lnd file into the annotation UI.
%
% Input arguments:
%  AUI       AnnotationUI instance.
%  FILENAME  Name of the .lnd landmark file.
%
% Output arguments:
%  LOADED  Logical scalar to indicate whether the file was loaded.
%          It may not have been loaded if the landmark definition mismatched.
%
  loaded = false;
  anno = Annotation.load(filename, aui.lnddef);

  if ~isequal(anno.lnddef, aui.lnddef)
    if aui.warn_lnddef
      btn = questdlg(['Unexpected landmark definition. Converting the' ...
                      ' annotation may loose some landmarks. Continue?'], ...
                      'AnnotationUI', 'Yes', 'No', 'Yes');
      if ~strcmpi(btn, 'Yes')
        return;
      end
    end
    anno.convertto(aui.lnddef);
  end

  aui.markers = anno.coordinates;
  aui.selected = 1;
  aui.dirty = false;
  loaded = true;
end
