function pointat(aui, xyz)
% Place the pointer at the specified position.
%
% Input arguments:
%  AUI  AnnotationUI instance.
%  XYZ  1x3 array defining the target coodinate. Pass `nan` to unset the target
%       position.
%
  if nargin<2 || isempty(xyz) || any(isnan(xyz))
    xyz = [nan nan nan];
  else
    assert(size(xyz, 1) == 1, 'Can only point at one coordinate.');
  end

  aui.pointer = xyz;
end
