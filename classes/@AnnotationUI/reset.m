function reset(aui, varargin)
% Clear all landmark definitions and their markers.
%
% Input arguments:
%  AUI  AnnotationUI instance.
%
  aui.markers = nan(aui.numlandmarks, 3);
  aui.selected = 1;
  aui.pointat(nan);
  aui.dirty = false;
end
