function rt2 = inverse(rt)
% Return the inverse transform.
%
% Input arguments:
%  RT  RigidTransform instance.
%
% Output arguments:
%  RT2  RigidTransform instance of the inverse of RT.
%
  R = rt.rotation;
  t = rt.translation;
  s = rt.scaling;

  Rinv = R';
  sinv = 1 ./ s;
  tinv = -t * Rinv * sinv;

  rt2 = RigidTransformation(Rinv, tinv, sinv);
end
