function rt = decompose(matrix)
% Decompose a rigid transformation matrix into rotation, translation, and scale.
%
% Input arguments:
%  MATRIX  NxN or (N+1)xN matrix. If NxN then the translation component is set
%          to zero.
%
% Output arguments:
%  RT  RigidTransformation instance with NxN rotation, Nx1 translation, and
%      scalar (uniform) scaling.
%
% Considerations:
%  - It is not checked that the scaling in MATRIX is uniform. We just take the
%    mean of scaling in x, y, and z.
%
  [m,n] = size(matrix);

  assert((m==n) || (m==n+1), 'RigidTransformation:badmatrix', ...
          'The matrix must be NxN or (N+1)xN.');

  if m > n
    translation = matrix(end,:);
    rotation = matrix(1:n,1:n);
  else
    translation = zeros(1, n);
    rotation = matrix;
  end
  
  [U,S,V] = svd(rotation);
  scaling = mean(diag(S));  % assume S is constant along diagonal.
  rotation = U * V';
  
  rt = RigidTransformation(rotation, translation, scaling);
end
