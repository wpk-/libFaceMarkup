function transformposneglines(pnl, rt)
% Apply the rigid transformation to pos/neg lines, by reference.
%
% Input arguments:
%  PNL  PosNegLines instance.
%  RT   RigidTransformation instance.
%
  pnl.points = pmul(pnl.points, rt.matrix);
end
