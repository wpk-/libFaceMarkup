function transformcurvaturepoints(cp, rt)
% Apply the rigid transformation to curvature points, by reference.
%
% Input arguments:
%  CP  CurvaturePoints instance.
%  RT  RigidTransformation instance.
%
  cp.points = pmul(cp.points, rt.matrix);
end
