function transformcoordinatesystem(cs, rt)
% Apply the rigid transformation to a coordinate system, by reference.
%
% Input arguments:
%  CS  CoordinateSystem instance.
%  RT  RigidTransformation instance.
%
  cs.origin = pmul(cs.origin, rt.matrix);
  cs.basis  = vmul(cs.basis', rt.matrix)';
end
