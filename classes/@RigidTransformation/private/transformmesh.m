function transformmesh(tri, rt)
% Apply the rigid transformation to the vertices of a mesh, by reference.
%
% Input arguments:
%  TRI  Mesh instance.
%  RT   RigidTransformation instance.
%
  tri.vertices = pmul(tri.vertices, rt.matrix);
end
