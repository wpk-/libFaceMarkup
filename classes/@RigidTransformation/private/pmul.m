function C = pmul(A, B)
% Multiply point coordinates with transformation matrix.
%
% Input arguments:
%  A  NxM matrix defining N point coordinates in M-dimensional space.
%  B  (M+1)xM transformation matrix.
%
% Output arguments:
%  C  NxM matrix = [A 1] * B.
%
  C = [A ones(size(A,1),1)] * B;
end
