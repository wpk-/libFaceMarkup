function transformannotation(ann, rt, isvec)
% Apply the rigid transformation to an annotation, by reference.
%
% Input arguments:
%  ANN    Annotation instance.
%  RT     RigidTransformation instance.
%  ISVEC  Logical scalar to specify that the data stored in ANN is vector data.
%         Default value is `false`.
%
% Computes
%   "[ANN 1] * AT"    if ISVEC is false,
%   "[ANN 0] * AT"    if ISVEC is true.
%
  if nargin<3 || isempty(isvec), isvec=false; end
  
  ix = ann.indices;
  
  if isvec
    ann.coordinates(ix,:) = vmul(ann.coordinates(ix,:), rt.matrix);
  else
    ann.coordinates(ix,:) = pmul(ann.coordinates(ix,:), rt.matrix);
  end
end
