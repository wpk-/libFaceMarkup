function transformrigidtransformation(rt0, rt)
% Apply the rigid transformation to another rigid transformation, by reference.
%
% Input arguments:
%  RT0    RigidTransformation instance to be transformed.
%  RT     RigidTransformation instance effectuating the transformation.
%
% Computes "RT0 * RT".
%
  rt0.matrix = [rt0.matrix [zeros(size(rt0.matrix,1),1);1]] * rt.matrix;
end
