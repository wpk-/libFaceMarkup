function C = vmul(A, B)
% Multiply vector data with transformation matrix.
%
% Input arguments:
%  A  NxM matrix defining N vectors in M-dimensional space.
%  B  (M+1)xM transformation matrix.
%
% Output arguments:
%  C  NxM matrix = [A 0] * B.
%
  C = [A zeros(size(A,1),1)] * B;
end
