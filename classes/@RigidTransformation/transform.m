function data = transform(rt, data, isvec)
% Apply the transformation to the input data.
%
% Input arguments:
%  RT     RigidTransformation instance.
%  DATA   MxN matrix, or Annotation instance with MxN coordinates, or
%         CurvaturePoints instance with MxN points, or Mesh instance with MxN
%         vertices, or PosNegLines instance with MxN points, or
%         RigidTransformation instance.
%  ISVEC  Optional logical scalar to specify that DATA contains vector (rather
%         than point) data. This is only applicable when DATA is an Annotation.
%
% Output arguments:
%  DATA  A copy of DATA, transformed according to transformation RT. Except
%        where DATA is copy-value (numeric data), the transformation is carried
%        out by reference, thus changing the original object.
%
% Considerations:
%  - Where DATA is a handle class object, in addition to being returned, its
%    value is also updated by reference.
%  - The function internally converts the input data in DATA to homogeneous
%    coordinates. If your data is already in homogeneous coordinates, the
%    last column will have to be removed before applying.
%
  if nargin<3 || isempty(isvec), isvec=false; end

  if isnumeric(data)
    if isvec
      data = vmuul(data, rt.matrix);
    else
      data = pmul(data, rt.matrix);
    end
  elseif isa(data, 'Annotation')
    transformannotation(data, rt, nargin>2 && isscalar(isvec) && isvec);
  elseif isa(data, 'CoordinateSystem')
    transformcoordinatesystem(data, rt);
  elseif isa(data, 'CurvaturePoints')
    transformcurvaturepoints(data, rt);
  elseif isa(data, 'Mesh')
    transformmesh(data, rt);
  elseif isa(data, 'PosNegLines')
    transformposneglines(data, rt);
  elseif isa(data, 'RigidTransformation')
    transformrigidtransformation(data, rt);
  else
    error('RigidTransformation:badargument', ...
          'Input argument `data` is of unknown type.');
  end
end
