function rt = fromcoordinatesystems(cosyfrom, cosyto)
% Compute the rigid transformation between two coordinate systems.
%
% Input arguments:
%  COSYFROM  CoordinateSystem instance.
%  COSYTO    CoordinateSystem instance.
%
% Output arguments:
%  RT  RigidTransformation instance which, when applied to COSYFROM yields a
%      coodinate system equal to COSYTO. To apply a rigid transformation, use
%      `RT.transform(cosy)`.
%
  rotation = cosyfrom.basis * cosyto.basis';
  translation = cosyto.origin - cosyfrom.origin * rotation;
  scaling = 1;
  
  rt = RigidTransformation(rotation, translation, scaling);
end
