classdef RigidTransformation < handle
  properties
    dim;
    rotation;
    scaling;
    translation;
  end

  properties (Dependent)
    matrix;
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Class constructor.

  methods
    function rt = RigidTransformation(rotation, translation, scaling)
      if nargin == 0
        return; % Allow `rt(100) = RigidTransformation;`
      end

      [d1,d2] = size(rotation);
      [d3,d4] = size(translation);

      assert(ismatrix(rotation) && d1==d2, 'Rotation matrix must be square.');
      assert(isvector(translation) && d1==max([d3,d4]), ['Translation must ' ...
            'be a vector of the same dimension as the rotation matrix.']);
      assert(isscalar(scaling), 'Scaling component must be a scalar.');

      rt.dim = numel(translation);
      rt.rotation = rotation;
      rt.translation = translation(:)';
      rt.scaling = scaling;
    end
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Static methods return a RigidTransformation.

  methods (Static)
    % RigidTransformation from rigid transformation matrix.
    rt = decompose(matrix);
    % Compute the rigid transformation between two coordinate systems.
    rt = fromcoordinatesystems(cosyfrom, cosyto);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Methods that return information.

  methods
    % Return the inverse transform.
    rt2 = inverse(rt);
    % Apply the transformation to the input data.
    data = transform(rt, data, isvec);
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Methods that modify the data.

  % Getters and setters.

  methods
    function M = get.matrix(rt)
      % Note: `rt.scaling` is scalar, so `rt.rotation * rt.scaling` is the same.
      M = [
        rt.rotation * (rt.scaling * diag(ones(rt.dim,1)));
        rt.translation
      ];
    end
    function set.matrix(rt, M)
      f = RigidTransformation.decompose(M);
      assert(f.dim==rt.dim, 'Matrix dimensions mismatch.');
      rt.rotation = f.rotation;
      rt.translation = f.translation;
      rt.scaling = f.scaling;
    end
  end
end
