function h = plot3(cs, varargin)
% Plot the 3D coordinate system.
%
% Input arguments:
%  CS     CoordinateSystem instance.
%  SCALE  Optional scalar specifying the length of the axes.
%         Default value is 1.
%
% Output arguments:
%  H  Handles for the plotted lines.
%
% Considerations:
%  - In order to plot in specific axes, you will have to pass the 'Parent'
%    keyword argument. Passing the axes handle as a first argument is not
%    available.
%
  p = inputParser;
  p.FunctionName = 'CoordinateSystem/plot3';
  p.KeepUnmatched = true;
  p.addRequired('cs',       @(x)isa(x, 'CoordinateSystem'));
  p.addOptional('scale', 1, @(x)isscalar(x) && isnumeric(x));
  p.parse(cs, varargin{:});
  
  varargs = struct2kwargs(p.Unmatched);
  
  scale = p.Results.scale;
  o     = cs.origin;
  b     = cs.basis;
  
  b1 = bsxfun(@plus, o, scale * [0 0 0; b(:,1)']);
  b2 = bsxfun(@plus, o, scale * [0 0 0; b(:,2)']);
  b3 = bsxfun(@plus, o, scale * [0 0 0; b(:,3)']);
  h = plot3(b1(:,1), b1(:,2), b1(:,3), 'r', ...
          b2(:,1), b2(:,2), b2(:,3), 'g', ...
          b3(:,1), b3(:,2), b3(:,3), 'b', 'LineWidth',2, varargs{:});
end

