function h = plot(cs, scale, varargin)
% Plot the 2D coordinate system.
%
% Input arguments:
%  CS     CoordinateSystem instance.
%  SCALE  Optional scalar specifying the length of the axes.
%         Default value is 1.
%
% Output arguments:
%  H  Handles for the plotted lines.
%
  if nargin<2 || isempty(scale), scale=1; end
  
  o = cs.origin;
  b = cs.basis;
  
  b1 = bsxfun(@plus, o, scale * [0 0; b(:,1)']);
  b2 = bsxfun(@plus, o, scale * [0 0; b(:,2)']);
  h = plot(b1(:,1), b1(:,2), 'b', ...
          b2(:,1), b2(:,2), 'g', 'LineWidth',3);
end
