classdef CoordinateSystem < handle
% Class that stores a coodinate system (= origin + basis).
% 
  properties
    origin;
    xaxis;
    yaxis;
    zaxis;
  end
  
  properties (Dependent)
    dim;
    numvectors;
    basis;
  end
  
  methods
    function cs = CoordinateSystem(origin, basis)
      % CoordinateSystem class constructor.
      %
      % Input arguments:
      %  ORIGIN  1xN row vector specifying the location of the coordinate
      %          system's origin in an N-dimensional embedding space.
      %  BASIS   NxP matrix of P basis vectors.
      %
      % Output arguments:
      %  CS  CoordinateSystem instance.
      %
      % Considerations:
      %  - The basis vectors are COLUMN vectors, whereas the origin --a point--
      %    is specified as ROW vector.
      %
      if nargin == 0
        % This allows us to preallocate an array of CoordinateSystem objects:
        %    A(10,1) = CoordinateSystem;
        % This results in an 10x1 array of CoordinateSystem objects, which can
        % each in turn be overwritten with a proper coordinate system. This is
        % useful for parfor loops and the like.
        return;
      end
      
      p = inputParser;
      p.FunctionName = 'CoordinateSystem';
      p.addRequired('origin', @isvector);
      p.addRequired('basis', @ismatrix);
      p.parse(origin, basis);
      
      cs.origin = origin(:)';
      cs.basis = basis;
    end
  end
  
  methods
    % Plot the 2D coordinate system.
    h = plot(cs, scale, varargin);
    % Plot the 3D coordinate system.
    h = plot3(cs, scale, varargin);
  end
  
  methods
    function R = get.basis(cs)
      R = [cs.xaxis cs.yaxis cs.zaxis];
    end
    function set.basis(cs, R)
      assert(all(size(R)==3,2), 'CoordinateSystem is 3D.');
      cs.xaxis = R(:,1);
      cs.yaxis = R(:,2);
      cs.zaxis = R(:,3);
    end
    function n = get.dim(cs)
      n = numel(cs.origin);
    end
    function n = get.numvectors(cs)
      warning('Deprecated. It has 3 vectors, period.');
      n = size(cs.basis, 2);
    end
  end
end
