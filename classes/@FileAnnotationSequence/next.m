function success = next(fas)
% Make the next file selected.
%
% Input arguments:
%  FAS  FileAnnotationSequence instance.
%
% Output arguments:
%  SUCCESS  Logical scalar to indicate that the selected index was changed.
%
  success = false;

  % No success when either:
  % * `fas.numfiles == 0` (then `fas.selected` is empty, and so `ix` is empty),
  % * `fas.numfiles == fas.selected` (then we cannot up the counter).

  if fas.selected < fas.numfiles
    fas.selected = fas.selected + 1;
    success = true;
  end
end
