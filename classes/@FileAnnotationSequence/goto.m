function success = goto(fas, number)
% Make the specified file selected.
%
% Input arguments:
%  FAS  FileAnnotationSequence instance.
%
% Output arguments:
%  SUCCESS  Logical scalar to indicate that the selected index was changed.
%

  % No success when either:
  % * `number` is out of range.
  try
    fas.selected = number;
    success = true;
  catch
    success = false;
  end
end
