function success = previous(fas)
% Make the previous file selected.
%
% Input arguments:
%  FAS  FileAnnotationSequence instance.
%
% Output arguments:
%  SUCCESS  Logical scalar to indicate that the selected index was changed.
%
  success = false;

  % No success when either:
  % * `fas.numfiles == 0` (then `fas.selected` is empty, and so `ix` is empty),
  % * `fas.selected == 1` (then we cannot decrement the counter).

  if fas.selected > 1
    fas.selected = fas.selected - 1;
    success = true;
  end
end
