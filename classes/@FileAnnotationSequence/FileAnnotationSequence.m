classdef FileAnnotationSequence < handle
  %FILEANNOTATIONSEQUENCE Simple class to facilitate source (image) file
  % listing and navigation linked with respective annotation files.
  %

  properties (Dependent)
    numfiles;
    selectedannotation;
    selectedname;
    selectedsource;
  end

  properties
    selected;
  end

  properties (Transient)
    names;
  end

  properties (SetAccess=private)
    sourcefiles;
    annotationfiles;
  end

  methods
    function fas = FileAnnotationSequence(sourcefiles, annotationfiles)
      p = inputParser();
      p.FunctionName = 'FileAnnotationSequence';
      p.addRequired('SourceFiles', @(x) ischar(x) || iscellstr(x));
      p.addRequired('AnnotationFiles', @(x) ischar(x) || iscellstr(x));
      p.parse(sourcefiles, annotationfiles);

      sourcefiles = listfiles(sourcefiles);
      
      if ischar(annotationfiles)
        if exist(annotationfiles, 'dir')
          annotationfolder = annotationfiles;
          annotationpat = {'', '.lnd'};
        else
          assert(numel(strfind(annotationfiles, '*')) == 1, ...
                 'ValueError for input argument ANNOTATIONFILES.');
          [annotationfolder, name, ext] = fileparts(annotationfiles);
          annotationpat = strsplit([name ext], '*');
        end
        [~,sourcenames] = cellfun(@fileparts, sourcefiles, 'UniformOutput',0);
        annotationfiles = cellfun(@(n) fullfile(annotationfolder, ...
                                                strjoin(annotationpat, n)), ...
                                  sourcenames, 'UniformOutput',0);
      end
      assert(iscellstr(annotationfiles) && ...
             numel(annotationfiles) == numel(sourcefiles), ...
             'ValueError for input argument ANNOTATIONFILES.')
      
      fas.sourcefiles = sourcefiles;
      fas.annotationfiles = annotationfiles;
      if numel(fas.sourcefiles)
        fas.selected = 1;
      else
        fas.selected = [];
      end
    end
  end

  methods
    % Make the specified file selected.
    success = goto(fas, number);
    % Make the next file selected.
    success = next(fas);
    % Make the previous file selected.
    success = previous(fas);
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %   GETTERS AND SETTERS                                                 %%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  methods
    % Get the list of file base names.
    function names = get.names(fas)
      if iscellstr(fas.names) && numel(fas.names) == fas.numfiles
        names = fas.names;
      else
        % Only compute when out-of-sync.
        [~,names] = cellfun(@fileparts, fas.sourcefiles, 'UniformOutput',0);
        fas.names = names;
      end
    end

    % Get the number of files listed.
    function n = get.numfiles(fas)
      n = numel(fas.sourcefiles);
    end

    % Set the selected index.
    function set.selected(fas, ix)
      if fas.numfiles == 0 %#ok<MCSUP>
        assert(isempty(ix), 'There are no files to select.');
      else
        assert(isscalar(ix) && ~isnan(ix) && ~mod(ix,1), ...
              'Index must be a positive integer.');
        assert(ix>0 && ix<=fas.numfiles, ... %#ok<MCSUP>
              'Index must lie between 1 and %d inclusive.', ...
              fas.numfiles); %#ok<MCSUP>
      end
      fas.selected = ix;
    end

    % Get the annotation file name for the selected item.
    function annofile = get.selectedannotation(fas)
      if isempty(fas.selected)
        annofile = [];
      else
        annofile = fas.annotationfiles{fas.selected};
      end
    end

    % Get the file base name for the selected item.
    function name = get.selectedname(fas)
      name = fas.names{fas.selected};
    end

    % Get the source file name for the selected item.
    function sourcefile = get.selectedsource(fas)
      if fas.numfiles
        sourcefile = fas.sourcefiles{fas.selected};
      else
        sourcefile = [];
      end
    end
  end
end
