classdef MeshViewerContext < AbstractViewerContext
  %MESHVIEWERCONTEXT Summary of this class goes here
  %   Detailed explanation goes here
  %

  properties (SetAccess=protected)
    haxes;              % Handle to the axes containing the mesh.
    hb;                 % Handle to a boundary container for the mesh.
    hfig;               % Handle to the figure containing the axes.
    hobj;               % Handle to the mesh.
    renderTexture;      % Flag that enables or disables showing the texture.
    light;              % Light object of the scene (off for texturing, on
                        % when viewing without texture).
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %   CONSTRUCT / DESTRUCT                                                %%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  methods
    function mvc = MeshViewerContext(haxes)
      % Construct a mesh viewer context.
      mvc.haxes = haxes;%get(mvc.hobj, 'Parent');
      mvc.hfig = get(mvc.haxes, 'Parent');
      %mvc.init();
    end
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %   INITIALISATION                                                      %%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  methods
    function load(mvc, meshobj)
      if ischar(meshobj)
        meshobj = Mesh.load(meshobj);
      end
      if isa(meshobj, 'Mesh')
        held = ishold(mvc.haxes);
        hold(mvc.haxes, 'on');
        cdata = meshobj.vertexcolor;
        meshobj = trisurf(meshobj, 'FaceVertexCData',meshobj.vertexcolor, ...
                          'FaceColor','interp', 'FaceLighting','gouraud', ...
                          'AmbientStrength',.2, 'DiffuseStrength',.8, ...
                          'SpecularStrength',.05, 'Parent',mvc.haxes);
        s = guidata(meshobj);
        s.vertexcolor = cdata;
        guidata(meshobj, s);
        if ~held
          hold(mvc.haxes, 'off');
        end
      elseif isempty(meshobj)
        mvc.clear();
        return;
      elseif ~ishandle(meshobj)
        error('Can only load meshes or surface handles.');
      end
      mvc.loadHandle(meshobj);
    end

    function clear(mvc)
      mvc.removeListeners();
      if ~isempty(mvc.hobj) && ishandle(mvc.hobj)
        delete(mvc.hobj);
      end
      if ~isempty(mvc.light) && ishandle(mvc.light)
        delete(mvc.light);
      end
      mvc.hobj = [];
      mvc.light = [];
      mvc.hb = [];
    end
    
    function loadHandle(mvc, hobj)
      mvc.clear();
      % ---
      mvc.hobj = hobj;
      mvc.hb = boundary(mvc.hobj);
      mvc.renderTexture = true;
      mvc.light = light('Position',[20 30 200], 'Style','local', 'Visible','off');
      % ---
      mvc.resetViewport();
      mvc.installListeners();
    end
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %   USER INTERACTION                                                    %%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  methods
    function pointAt(mvc, linesegment, button)
      % Emit the PointAtObject event with the following properties:
      %
      % `evtobj.Button`     String specifying the mouse button pressed.
      % `evtobj.Position`   1x3 coordinate on the mesh surface where the mouse
      %                     clicked.
      % `evtobj.FaceIndex`  Scalar specifying the index of the triangle that
      %                     was clicked.
      %
      [xyz,~,~,fi] = intersect(mvc.hb, linesegment);
      if ~isempty(xyz)
        evt = EvtPointAtMesh(button, fi, xyz);
        notify(mvc, 'PointAtObject',evt);
      end
    end

    function resetViewport(mvc)
      set(mvc.haxes, ...
          'CameraViewAngleMode','auto', ...
          'CameraTargetMode','auto');
      view(mvc.haxes, 2);
      zoom(mvc.haxes, 'reset');
      set(mvc.haxes, ...
          'CameraViewAngleMode','manual');
    end

    function rotate(mvc, dxy)
      camorbit(mvc.haxes, -dxy(1), -dxy(2), 'data', 'y');
    end

    function translate(mvc, dxy)
      camdolly(mvc.haxes, -dxy(1), -dxy(2), 0, 'movetarget', 'pixels');
    end

    function zoomBy(mvc, amount)
      factor = 1.2 .^ -amount;
      camzoom(mvc.haxes, factor);
    end
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %   EXTRA METHODS                                                       %%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  methods
    function toggleTexture(mvc, varargin)
      mvc.renderTexture = ~mvc.renderTexture; % need to toggle before
                                              % checking on the next line.
      if(~mvc.renderTexture)
        set(mvc.hobj, 'FaceColor',[.9 .9 .9]);
        set(mvc.light, 'Visible','on');
      else
        set(mvc.hobj, 'FaceColor','interp');
        set(mvc.light, 'Visible','off');
      end
      return
    end
  end
end

