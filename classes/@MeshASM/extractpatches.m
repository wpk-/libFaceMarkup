function patches = extractpatches(asm, uv, up)
% Extract surface patches at specified locations.
%
% Input arguments:
%  UV      Kx2 matrix. Each row specifies the centre point of a patch to be
%          extracted, specified in UV coords.
%  UP      Upvector in 3D space. A 3x1 column vector.
%
% Output arguments:
%  PATCH  Kx1 cell array. The i-th cell is a Mx3 matrix describing the patch of
%         points extracted around the centre specified by `UV(i,:)`. The point
%         coordinates are measured in a local coordinate system:
%         The third axis equals the surface normal vector. The second axis lies
%         in the plane spanned by the normal vector and the upvector.
%
% Considerations:
%  - The radius of the extracted patches is defined in `ASM.patchradius`.
%  - The up and normal vectors should not be parallel or close to that.
%
  template = asm.patchtemplate;
  radius = asm.patchradius + 1; % +1 to get the patch edges nicely too.

  shape = asm.shape;
  texture = asm.texture;
  normals = asm.normals;

  assert(~isempty(shape) && ~isempty(texture) && ~isempty(normals), ...
        'A mesh must be loaded before extracting patches.');

  npts = size(uv, 1);
  patches = cell(npts, 1);

  [fi,B] = texture.pointLocation(uv);
  normal = normals.barycentricToCartesian(fi, B);
  xyz = shape.barycentricToCartesian(fi, B);

  % NOTE: The up and normal vectors should not be parallel or close to that.
  % We should somehow test for that, but would add a lot of code complexity.

  vertical = bsxfun(@minus, up', bsxfun(@times, (normal * up), normal));
  vertical = rdiv2norm(vertical);
  horizontal = cross(vertical, normal, 2);
  hvbasis = [permute(horizontal,[3 2 1]); permute(vertical,[3 2 1])];

  facing = normals.Points * normal' > 0.0872; % max angle = 85 deg. (arbitrary)
  %facing = normals.Points * normal' > 0.1736; % max angle = 80 deg.

  for i = 1:npts
    % - eliminate because not facing us.
    hvo = xyz(i,:) * hvbasis(:,:,i)';
    hvp = shape.Points(facing(:,i),:) * hvbasis(:,:,i)';
    
    % - eliminate because not within radius.
    r = pdist2(hvp, hvo, 'euclidean');
    keep = r < radius;
    facing(facing(:,i),i) = keep;
    hvp = bsxfun(@minus, hvp(keep,:), hvo);
    zp = shape.Points(facing(:,i),:) * normal(i,:)' - xyz(i,:) * normal(i,:)';

    % - eliminate because occluded.
    % While we can do this, it isn't really necessary. We will always be
    % interested in a template of points to extract from each patch. With the
    % current "raw" patch we measure in the hv-plane the nearest neighbours to
    % each template point and take the one with smallest absolute z-value.
    keep = depthTest(hvp, zp);
    %facing(facing(:,i),i) = keep;
    hvp = hvp(keep,:);
    zp = zp(keep,:);

    % - keep the rest.
    patches{i} = subsample([hvp zp], template);
  end
end

function keep = depthTest(xy, z)
% Discard too near and too distant points.
%
% We try to estimate a reasonable interval, starting from the convex hull
% around `X(:,1:2)`, and then updating the interval from variance in the data.
% By no means does this guarantee anything, but it gives reasonable results on
% a test image, and it is thousands times faster that point-in-triangle tests.
%
% Input arguments:
%  X   Nx3 matrix of point coordinates.
%
% Output arguments:
%  KEEP  Nx1 logical array marking those points in X whose depth coordinates,
%        `X(:,3)` fall within the determined range.
%
  npts = size(xy, 1);

  h = convhull(xy);

  keep = true(npts, 1);
  keep(h) = true;

  for i = 1:2
    m = median(z(keep));
    sd = mad(z(keep), 1);
    keep = abs(z - m) < 4 * sd;
  end
end
