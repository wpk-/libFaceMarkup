function loadmesh(asm, mesh)
% Create a continuous, hole-free, UV mapping.
% Define a function mapping from (u,v) to (x,y,z) and to (nx,ny,nz).
% We will ignore texture mapping for now. TODO: implement texture mapping.
%
% Input arguments:
%  ASM   MeshICS instance.
%  MESH  Mesh instance.
%
% Output arguments:
%  SHAPE    Triangulation instance for the 3D shape.
%  TEXTURE  DelaunayTriangulation instance for the 2D UV coordinates.
%           `T.Points` correspond to `X.points` and are in the range (0,0)-(1,1).
%           `T.ConnectivityList` and `X.ConnectivityList` are identical.
%  NORMALS  Triangulation instance for the 3D vertex normals.
%           `N.Points` provide the unit normal vectors at each vertex, `X.Points`.
%           `N.ConnectivityList` and `X.ConnectivityList` are identical.
%
  m = Mesh.copy(mesh);
  s = m.surfaces();
  m.filterfaces(s == 1);  % necessary for uv().

  if nnz(s==1) / numel(s) < .7
    warning('Mesh has more than one major surface (%s)', mesh.name);
    % Which means that we may have selected the surface /not/ being the face.
    % We may ask for the ICS annotation to make a more educated guess which
    % surface will be the face. TODO: base surface selection on annotation.
  end

  % In some cases, e.g. when two vertices are in the exact same position,
  % delaunayTriangulation will remove some points. Because that will put the
  % triangulation points out of sync with the mesh, we should detect that and
  % fix it. However, I can't find an example at the moment :)
  % When that happens, you will read this and issue a fixme.
  uv = m.uv();
  texture = delaunayTriangulation(uv);
  
  asm.texture = texture;
  asm.shape = triangulation(texture.ConnectivityList, m.vertices);
  asm.normals = triangulation(texture.ConnectivityList, m.vertexnormals);
end
