function definePatchTemplate(asm)

  if ~asm.has_patchtemplate_defined
    asm.patchtemplate = uniformpatch(1, asm.patchradius);
    asm.has_patchtemplate_defined = true;
  end

end

