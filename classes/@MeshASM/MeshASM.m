classdef MeshASM < handle
  %MESHASM Summary of this class goes here
  %   Detailed explanation goes here
  
  properties
    normals;
    shape;
    texture;
  end

  properties
    patchradius = 15;
  end
  
  % definePatchTemplate
  % resetPatchTemplate
  % has listener for `patchradius`
  properties (SetAccess = private)
    patchtemplate;
    has_patchtemplate_defined = false;
  end
  
  methods
    function asm = MeshASM(varargin)
      p = inputParser();
      p.FunctionName = 'MeshASM';
      p.addOptional('Mesh', [], @(x)isa(x,'Mesh'));
      p.addParamValue('PatchRadius', 15, @isscalar);
      p.parse(varargin{:});

      asm.patchradius = p.Results.PatchRadius;
      if ~isempty(p.Results.Mesh)
        asm.loadmesh(p.Results.Mesh);
      end
    end
  end
  
  methods (Static)
    p = createpatch(radius);
    model = train(wrlfiles, lndfiles, options);
  end
  
  methods
    loadmesh(asm, mesh, anno);
    patches = extractpatches(asm, uv, up);
  end
  
  methods
    function pts = get.patchtemplate(asm)
      if ~asm.has_patchtemplate_defined
        asm.patchtemplate = uniformpatch(1, asm.patchradius);
      end
      pts = asm.patchtemplate;
    end
    function set.patchradius(asm, r)
      if r ~= asm.patchradius
        resetPatchTemplate(asm);
        asm.patchradius = r;
      end
    end
  end
end
