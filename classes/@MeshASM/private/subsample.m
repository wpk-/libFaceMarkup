function pts = subsample(patch, template)
% Interpolate PATCH on the xy coordinates in TEMPLATE.
%
% Input arguments:
%  PATCH     Nx3 matrix of xyz coordinates.
%  TEMPLATE  Mx2 matrix of xy coordinates.
%
% Output arguments:
%  PTS  Mx3 matrix of xyz coordinates. The x and y coordinates equal TEMPLATE.
%       The z coordinate is sampled from PATCH.
%
  nt = size(template, 1);

  xy = patch(:,1:2);
  z = patch(:,3);

  offset = 1 + max(abs(z));
  absz = offset - abs(z);

  k = 3;
  ix = knnsearch(xy, template, 'K',k);
  ix = ix';
  [~,ix2] = max(absz(ix));
  z = z(ix(ix2 + k * (0:(nt-1))));

  pts = [template z];
end
