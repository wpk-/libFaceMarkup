function X = uniformpatch(r1, r2)
% Generate a uniform distribution of points over a circle, centred on (0,0).
%
% Input arguments:
%  R1  Radius of the smallest ring around the centre.
%  R2  Radius of the largest ring around the centre.
%
% Output arguments:
%  X  Nx2 list of N point coordinates. `X(1,:) == [0, 0]`, and then the points
%     are placed in rings around the centre. Each ring larger than the one
%     before. The first ring is at radius R1 and the last ring at radius R2.
%     Intermediate rings are spaced at R1 intervals. Within each ring the
%     samples are also roughly spaced R1 apart.
%
% Example:
%  `X = uniformpatch(1, 20)` generates 1320 points distributed over 20 rings,
%  plus one sample at the origin. The smallest ring has radius 1 and has 6
%  sample points. The largest ring has radius 20 and has 126 points. The points
%  are roughly spaced a distance of 1 apart.
%
  X = zeros(1, 2);
  o = 0;
  for r = r1:r1:r2
    l = 2*pi * r;
    n = round(l / r1);
    theta = linspace(0, 2*pi, n+1)';
    % `o` appears to be totall unnecessary, but I still like it.
    o = o + theta(2) / 2;
    [x,y] = pol2cart(theta(1:n)+o, r);
    X = cat(1, X, [x y]);
  end
end
