function ptsuv = cart2uv(asm, pts)
% Convert points in 3D cartesian space to 2D UV space.
%
% Input arguments:
%  ASM  MeshASM instance.
%  PTS  Nx3 matrix of points in 3D space.
%
% Output arguments:
%  PTSUV  Nx2 matrix of UV coordinates.
%
  shape = asm.shape;
  texture = asm.texture;

  assert(~isempty(shape) && ~isempty(texture), 'Must load mesh first.');

  npts = size(pts, 1);
  nfaces = size(asm.shape.ConnectivityList, 1);

  ptsuv = nan(npts, 2);

  % Had the Matlab triangulation class had the `pointLocation` function like
  % the delaunayTraingulation, this would have been like two lines of code...

  for i = 1:npts
    xyz = pts(i,:);

    b = shape.cartesianToBarycentric((1:nfaces)', ones(nfaces,1)*xyz);
    fi = find(all(b > 0, 2)); % inside triangle.
    b = b(fi,:);

    xyz2 = shape.barycentricToCartesian(fi, b);
    % XXX knnsearch might be faster.
    [~,j] = pdist2(xyz2, xyz, 'euclidean', 'Smallest',1);

    ptsuv(i,:) = texture.barycentricToCartesian(fi(j), b(j,:));
  end
end
