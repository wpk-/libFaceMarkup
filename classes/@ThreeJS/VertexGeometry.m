function s = VertexGeometry(vertices, varargin)
% Define a `THREE.Geometry` for an Nx3 matrix of vertices.
%
% Input arguments:
%  VERTICES  Nx3 matrix of point coordinates.
%  VARARGIN  Struct or arguments to create a struct. Fields defined in VARARGIN
%            overwrite fields in the default struct. The default struct has
%            type='Geometry', data.faces=[] and data.vertices=VERTICES.
%            A common field to specify in VARARGIN is uuid.
%
% Output arguments:
%  S  Struct with the necessary fields for a vertex/face `THREE.Geometry`.
%
  vertexdata = vertices';

  default = struct('type','Geometry', ...
      'data',struct('faces',[], 'vertices',vertexdata(:)));

  s = updatestruct(default, varargin{:});
end
