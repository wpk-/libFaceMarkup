function importPosNegLines(three, pnl, id, color)
% Import a PosNegLines instance into the ThreeJS scene.
%
% Input arguments:
%  THREE     ThreeJS instance.
%  PNL       PosNegLines instance.
%  ID        String specifying a unique ID for the Line object. Unique IDs for
%            the geometry, image, texture and material are derived from this.
%  COLOR     Optional struct to specify line colours. Use `hex2dec` for easy
%            conversion to colour numbers. Two fields must be set to a colour:
%            `COLOR.pos`, `COLOR.neg`.
%
  if nargin<4 || isempty(color)
    color = struct( ...
        'pos', hex2dec('00ffff'), ... % = cyan
        'neg', hex2dec('ff00ff') ...  % = magenta
    );
  end

  vertexdata_pos = [
      pnl.coordinates(pnl.ixpos(:,1),:) ...
      pnl.coordinates(pnl.ixpos(:,2),:);
  ];
  vertexdata_neg = [
      pnl.coordinates(pnl.ixneg(:,1),:) ...
      pnl.coordinates(pnl.ixneg(:,2),:)
  ];

  if ~isempty(vertexdata_pos)
    importLines(three, sprintf('%s-pos',id), vertexdata_pos, color.pos);
  end
  if ~isempty(vertexdata_neg)
    importLines(three, sprintf('%s-neg',id), vertexdata_neg, color.neg);
  end
end


function [lines, geometry, material] = importLines(three, id, vertexdata, color)
  geometryid = sprintf('%s-geometry', id);
  geometry = ThreeJS.VertexGeometry(vertexdata, 'uuid',geometryid);
  three.tree.geometries{end+1} = geometry;

  materialid = sprintf('%s-material', id);
  material = ThreeJS.LineBasicMaterial('uuid',materialid, 'color',color);
  three.tree.materials{end+1} = material;

  lines = ThreeJS.Line(geometryid, materialid, 'uuid',id, 'mode',1, ...
      'userData',struct('overlay',true));
  three.tree.object.children{end+1} = lines;
end
