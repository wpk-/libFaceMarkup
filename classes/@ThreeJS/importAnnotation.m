function importAnnotation(three, anno, id, color)
% Import an Annotation instance into the ThreeJS scene.
%
% Input arguments:
%  THREE     ThreeJS instance.
%  ANNO      Annotation instance.
%  ID        String specifying a unique ID for the PointCloud object. Unique
%            IDs for the geometry, image, texture and material are derived from
%            this.
%  COLOR     Optional number to specify marker colour. Use `hex2dec` for easy
%            conversion to colour numbers.
%
  yellow = hex2dec('e6e600'); % = 15132160

  % Sort the points back to front for the best rendering performance.
  % (pointcloud.sortParticles has been removed from THREE.js r70).
  vertexdata = anno.coordinates(anno.indices,:);
  [~,ix] = sort(vertexdata(:,3));
  vertexdata = vertexdata(ix,:);

  geometryid = sprintf('%s-geometry', id);
  geometry = ThreeJS.VertexGeometry(vertexdata, 'uuid',geometryid);
  three.tree.geometries{end+1} = geometry;

  % XXX: Not an ideal solution, but `setDefaultMarker()` works for now.
  textureid = three.setDefaultMarker();

  materialid = sprintf('%s-material', id);
  material = ThreeJS.PointCloudMaterial('uuid',materialid, ...
      'alphaTest',0.5, 'map',textureid, 'color',yellow);
  if nargin>3 && ~isempty(color)
    material.color = color;
  end
  three.tree.materials{end+1} = material;

  pointcloud = ThreeJS.PointCloud(geometryid, materialid, 'uuid',id, ...
      'name','Annotation', 'userData',struct('overlay',true));
  three.tree.object.children{end+1} = pointcloud;
end
