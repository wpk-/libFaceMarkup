function s = MeshGeometry(tri, varargin)
% Define a `THREE.Geometry` specifically for a Mesh.
%
% Input arguments:
%  TRI       Mesh instance.
%  VARARGIN  Struct or arguments to create a struct. Fields defined in VARARGIN
%            overwrite fields in the default struct.
%            The default struct has type='Geometry' and data a struct with
%            vertices, faces and optionally color.
%            A common field to specify in VARARGIN is uuid.
%
% Output arguments:
%  S  Struct with the necessary fields for a vertex/face `THREE.Geometry`.
%

  % See private/colortype.m
  ctype = colortype(tri);

  % Face data must reference the use of colour data.
  if ctype == 2
    % 2: Colour the vertices.
    facedata = [128*ones(tri.numfaces,1) tri.faces-1 tri.faces-1]';
  elseif ctype == 1
    % 1: Colour the faces.
    facedata = [64*ones(tri.numfaces,1) tri.faces-1 (1:tri.numfaces)'-1]';
  else
    % 0: Uniform colour.
    facedata = [zeros(tri.numfaces,1) tri.faces-1]';
  end

  vertexdata = tri.vertices';

  default = struct( ...
    'type', 'Geometry', ...
    'data', struct( ...
      'vertices', vertexdata(:), ...
      'faces',    facedata(:) ...
    ) ...
  );

  % Add colour data if needed.
  if ctype
    if ctype == 2
      colordata = tri.vertexcolor;
    elseif ctype == 1
      colordata = tri.facecolor;
    end
    colordata = round(colordata * (2^8-1)) * [2^16  2^8  1]';
    default.data.colors = colordata;
  end

  s = updatestruct(default, varargin{:});
end
