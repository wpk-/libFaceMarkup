function s = Mesh(tri, geometryid, materialid, varargin)
% Define a `THREE.Mesh`.
%
% Input arguments:
%  TRI         Mesh instance.
%  GEOMETRYID  String specifying the uuid of the mesh geometry.
%  MATERIALID  String specifying the uuid of the mesh material.
%  VARARGIN    Struct or arguments to create a struct. Fields defined in
%              VARARGIN overwrite fields in the default struct. The default
%              struct has type='Mesh', name=TRI.name, geometry=GEOMETRYID,
%              material=MATERIALID and the default matrix. A common field to
%              specify in VARARGIN is uuid.
%
% Output arguments:
%  S  Struct with the necessary fields for a `THREE.Mesh`.
%
  default = struct('type','Mesh', 'name',tri.name, ...
      'geometry',geometryid, 'material',materialid, ...
      'matrix',[1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]);

  s = updatestruct(default, varargin{:});
end
