function s = Scene(varargin)
% Define a `THREE.Scene`.
%
% Input arguments:
%  VARARGIN  Struct or arguments to create a struct. Fields defined in VARARGIN
%            overwrite fields in the default struct.
%            The default struct has type='Scene', name='Scene', children={}
%            and the default matrix.
%
% Output arguments:
%  S  Struct with the necessary fields for a `THREE.Scene`.
%
  default = struct('type','Scene', 'name','Scene', 'children',{{}}, ...
            'matrix',[1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]);

  s = updatestruct(default, varargin{:});
end
