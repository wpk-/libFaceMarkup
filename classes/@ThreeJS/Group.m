function s = Group(varargin)
% Define a `THREE.Group`.
%
% Input arguments:
%  VARARGIN  Struct or arguments to create a struct. Fields defined in VARARGIN
%            overwrite fields in the default struct.
%            The default struct has type='Group' and the default matrix.
%            A common field to specify in VARARGIN is children.
%
% Output arguments:
%  S  Struct with the necessary fields for a `THREE.Group`.
%
  default = struct('type','Group', ...
            'matrix',[1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]);

  s = updatestruct(default, varargin{:});
end
