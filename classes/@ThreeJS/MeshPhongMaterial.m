function s = MeshPhongMaterial(~, varargin)
% Define a `THREE.MeshPhongMaterial`.
%
% Input arguments:
%  TRI       Mesh instance. Although this is not used for the output, it
%            provides a consistent set of arguments for all `ThreeJS.Mesh*`
%            functions.
%  VARARGIN  Struct or arguments to create a struct. Fields defined in VARARGIN
%            overwrite fields in the default struct. The default struct has
%            type='MeshPhongMaterial', color=16777215, emissive=0, shininess=5
%            and specular=0.
%            A common field to specify in VARARGIN is uuid.
%
% Output arguments:
%  S  Struct with the necessary fields for a `THREE.MeshPhongMaterial`.
%
  white = hex2dec('ffffff');  % = 16777215

  default = struct('type','MeshPhongMaterial', 'color',white, ...
      'emissive',0, 'shininess',5, 'specular',0);

  % Don't forget to add light to the scene.
  % A spotlight is a good choice.

  s = updatestruct(default, varargin{:});
end
