function clear(three)
% Completely clear the ThreeJS object tree and reset to empty values.
%
% Input arguments:
%  THREE  ThreeJS instance.
%
  three.tree = struct();
  three.tree.geometries = {};
  three.tree.images = {};
  three.tree.materials = {};
  three.tree.textures = {};
  three.tree.object = ThreeJS.Scene();

  three.has_defaultmarker = false;
end
