function [textureid, imageid] = setDefaultMarker(three)
% Adds the Image and Texture definitions for the default marker exactly once.
%
% Input arguments:
%  THREE  ThreeJS instance.
%
% Output arguments:
%  TEXTUREID  uuid field for the `THREE.Texture` object.
%  IMAGEID    uuid field for the `THREE.Image` object.
%
  imageid = 'marker-o-filled-image';
  textureid = 'marker-o-filled-texture';

  if ~three.has_defaultmarker
    three.has_defaultmarker = true;
    image = ThreeJS.MarkerImage(imageid);
    three.tree.images{end+1} = image;
    texture = ThreeJS.Texture(textureid, imageid);
    three.tree.textures{end+1} = texture;
  end
end
