classdef ThreeJS < handle
% A class to produce THREE.js JSON files.
%
  properties
    tree = struct();
    has_defaultmarker = false;
  end

  methods
    function three = ThreeJS()
      % three.tree.geometries = {};
      % three.tree.images = {};
      % three.tree.materials = {};
      % three.tree.textures = {};
      % three.tree.object = ThreeJS.Scene();
      % three.has_defaultmarker = false;
      three.clear();  % (does all the above).
    end

    % Completely clear the ThreeJS object tree and reset to empty values.
    clear(three);
    % Save the object tree to a ThreeJS JSON file.
    saveas(three, filename);

    % Import an Annotation instance into the ThreeJS scene.
    importAnnotation(three, anno, id, color);
    % Import a CoordinateSystem instance into the ThreeJS scene.
    importCoordinateSystem(three, cs, id, size);
    % Import a CurvaturePoints instance into the ThreeJS scene.
    importCurvaturePoints(three, cp, id, size, color);
    % Import a Mesh instance into the ThreeJS scene.
    importMesh(three, tri, id, varargin);
    % TODO: Implement importPosNegLines.
    importPosNegLines(three, pnl, id, varargin);

    % Adds the Image and Texture definitions for the default marker exactly once.
    [textureid,imageid] = setDefaultMarker(three);
  end
  
  methods (Static)
    % Define a `THREE.Group`.
    s = Group(varargin);
    % Define a `THREE.PointCloud`.
    s = Line(geometryid, materialid, varargin);
    % Define a `THREE.LineBasicMaterial`.
    s = LineBasicMaterial(varargin);
    % Define a `THREE.Image` depicting a round marker.
    s = MarkerImage(id, varargin);
    % Define a `THREE.Mesh`.
    s = Mesh(tri, geometryid, materialid, varargin);
    % Define a `THREE.MeshBasicMaterial`.
    s = MeshBasicMaterial(tri, varargin);
    % Define a `THREE.Geometry` specifically for a Mesh.
    s = MeshGeometry(tri, varargin);
    % Define a `THREE.MeshPhongMaterial`.
    s = MeshPhongMaterial(~, varargin);
    % Define a `THREE.PointCloud`.
    s = PointCloud(geometryid, materialid, varargin);
    % Define a `THREE.PointCloudMaterial`.
    s = PointCloudMaterial(varargin);
    % Define a `THREE.Scene`.
    s = Scene(varargin);
    % Define a `THREE.SpotLight`.
    s = SpotLight(varargin);
    % Define a `THREE.Texture`.
    s = Texture(id, imageid, varargin);
    % Define a `THREE.Geometry` for an Nx3 matrix of vertices.
    s = VertexGeometry(vertices, varargin);
  end
end
