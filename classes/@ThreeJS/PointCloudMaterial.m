function s = PointCloudMaterial(varargin)
% Define a `THREE.PointCloudMaterial`.
%
% Input arguments:
%  VARARGIN  Struct or arguments to create a struct. Fields defined in VARARGIN
%            overwrite fields in the default struct. The default struct has
%            type='PointCloudMaterial', size=16 and transparent=true. Common
%            fields to specify in VARARGIN are uuid, color, map (specify an
%            image uuid) and perhaps alphaTest for crisp edges.
%
% Output arguments:
%  S  Struct with the necessary fields for a `THREE.PointCloudMaterial`.
%
  default = struct('type','PointCloudMaterial', 'size',16, 'transparent',true);

  s = updatestruct(default, varargin{:});
end
