function s = Line(geometryid, materialid, varargin)
% Define a `THREE.PointCloud`.
%
% Input arguments:
%  GEOMETRYID  String specifying the uuid of the line geometry.
%  MATERIALID  String specifying the uuid of the line material.
%  VARARGIN    Struct or arguments to create a struct. Fields defined in
%              VARARGIN overwrite fields in the default struct. The default
%              struct has type='Line', geometry=GEOMETRYID, material=MATERIALID,
%              mode=0 (THEE.LineStrip) and the default matrix. Typical fields
%              to set in VARARGIN include uuid, name and userData.overlay=true.
%              Also, set mode to 1 (THREE.LinePieces) to draw pairs of segments
%              instead of a series of connected segments (see THREE.js docs).
%
% Output arguments:
%  S  Struct with the necessary fields for a `THREE.Line`.
%
  default = struct('type','Line', 'geometry',geometryid, ...
      'material',materialid, 'mode',0, ...
      'matrix',[1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]);

  s = updatestruct(default, varargin{:});
end
