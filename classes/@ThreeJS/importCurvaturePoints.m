function importCurvaturePoints(three, cp, id, size, color)
% Import a CurvaturePoints instance into the ThreeJS scene.
%
% Input arguments:
%  THREE     ThreeJS instance.
%  CP        CurvaturePoints instance.
%  ID        String specifying a unique ID for the PointCloud object. Unique
%            IDs for the geometry, image, texture and material are derived from
%            this.
%  SIZE      Optional scalar to specify the size of all markers.
%  COLOR     Optional struct to specify marker colours. Use `hex2dec` for easy
%            conversion to colour numbers. Three fields must be set to a
%            colour: `COLOR.cups`, `COLOR.caps` and `COLOR.saddles`.
%
  if nargin<4 || isempty(size)
    size = 16;
  end
  if nargin<5 || isempty(color)
    color = struct( ...
        'caps',    hex2dec('0000ff'), ... % = strong blue
        'cups',    hex2dec('ffff00'), ... % = strong yellow
        'saddles', hex2dec('ff0000') ...  % = strong red
    );
  end

  vertexdata = [
    cp.caps;
    cp.cups;
    cp.saddles
  ];
  colordata  = [
    ones(cp.numcaps,1) * color.caps;
    ones(cp.numcups,1) * color.cups;
    ones(cp.numsaddles,1) * color.saddles
  ];

  % Sort the points back to front for the best rendering performance.
  % (pointcloud.sortParticles has been removed from THREE.js r70).
  [~,ix] = sort(vertexdata(:,3));
  vertexdata = vertexdata(ix,:);
  colordata = colordata(ix);

  geometryid = sprintf('%s-geometry', id);
  geometry = ThreeJS.VertexGeometry(vertexdata, 'uuid',geometryid);
  geometry.data.colors = colordata(:);
  three.tree.geometries{end+1} = geometry;

  % XXX: Not an ideal solution, but `setDefaultMarker()` works for now.
  textureid = three.setDefaultMarker();

  materialid = sprintf('%s-material', id);
  material = ThreeJS.PointCloudMaterial('uuid',materialid, ...
      'alphaTest',0.5, 'map',textureid, 'vertexColors',2, 'size',size);
  three.tree.materials{end+1} = material;

  pointcloud = ThreeJS.PointCloud(geometryid, materialid, 'uuid',id, ...
      'name','CurvaturePoints', 'userData',struct('overlay',true));
  three.tree.object.children{end+1} = pointcloud;
end
