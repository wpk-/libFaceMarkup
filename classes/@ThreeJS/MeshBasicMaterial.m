function s = MeshBasicMaterial(tri, varargin)
% Define a `THREE.MeshBasicMaterial`.
%
% Input arguments:
%  TRI       Mesh instance.
%  VARARGIN  Struct or arguments to create a struct. Fields defined in VARARGIN
%            overwrite fields in the default struct. The default struct has
%            type='MeshBasicMaterial' and vertexColors=ctype, where ctype is
%            2, 1, or 0 depending on the availability of vertexcolor or
%            facecolor.
%            A common field to specify in VARARGIN is uuid.
%
% Output arguments:
%  S  Struct with the necessary fields for a `THREE.MeshBasicMaterial`.
%

  % See private/colortype.m
  ctype = colortype(tri);

  default = struct('type','MeshBasicMaterial', 'vertexColors',ctype);

  s = updatestruct(default, varargin{:});
end
