function importCoordinateSystem(three, cs, id, size)
% Import a CoordinateSystem instance into the ThreeJS scene.
%
% Create a series of line segments.
% Red marks X, green marks Y, and blue marks Z.
%
% Input arguments:
%  THREE     ThreeJS instance.
%  CS        CoordinateSystem instance.
%  ID        String specifying a unique ID for the Line object. Unique IDs for
%            the geometry, image, texture and material are derived from this.
%
  rgb = [hex2dec('ff0000') hex2dec('00ff00') hex2dec('0000ff')];

  vertexdata = [
    0 0 0;  1 0 0;
    0 0 0;  0 1 0;
    0 0 0;  0 0 1;
  ] .* size;
  colordata = rgb([1 1 2 2 3 3])';

  orientation = [cs.basis cs.origin';
                zeros(1,cs.dim) 1];

  geometryid = sprintf('%s-geometry', id);
  geometry = ThreeJS.VertexGeometry(vertexdata, 'uuid',geometryid);
  geometry.data.colors = colordata;
  three.tree.geometries{end+1} = geometry;

  materialid = sprintf('%s-material', id);
  material = ThreeJS.LineBasicMaterial('uuid',materialid, 'vertexColors',2);
  three.tree.materials{end+1} = material;

  lines = ThreeJS.Line(geometryid, materialid, 'uuid',id, 'mode',1, ...
      'matrix',orientation(:), 'userData',struct('overlay',true));
  three.tree.object.children{end+1} = lines;
end
