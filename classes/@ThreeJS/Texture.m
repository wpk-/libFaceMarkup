function s = Texture(id, imageid, varargin)
% Define a `THREE.Texture`.
%
% Input arguments:
%  ID        Since uuid is a required field, the ID argument is positional and
%            required rather than the usual optional uuid keyword in VARARGIN.
%  IMAGEID   String specifying the uuid of the texture image.
%  VARARGIN  Struct or arguments to create a struct. Fields defined in VARARGIN
%            overwrite fields in the default struct. The default struct has
%            uuid=ID, image=IMAGEID. Common parameters to set in VARARGIN
%            include mapping, wrapS, wrapT and offset.
%
% Output arguments:
%  S  Struct with the necessary fields for a `THREE.Texture`.
%
  default = struct('uuid',id, 'image',imageid);

  s = updatestruct(default, varargin{:});
end
