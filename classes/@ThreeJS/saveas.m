function saveas(three, filename)
% Save the object tree to a ThreeJS JSON file.
%
% Input arguments:
%  THREE     ThreeJS instance.
%  FILENAME  String specifying the file name.
%
  [str, args] = compile2json(three.tree);
  f = fopen(filename, 'w');
  fprintf(f, str, args{:});
  fclose(f);
end
