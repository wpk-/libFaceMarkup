function s = LineBasicMaterial(varargin)
% Define a `THREE.LineBasicMaterial`.
%
% Input arguments:
%  TRI       Mesh instance.
%  VARARGIN  Struct or arguments to create a struct. Fields defined in VARARGIN
%            overwrite fields in the default struct. The default struct has
%            type='LineBasicMaterial' and color=white (0xffffff). Common fields
%            to specify in VARARGIN are uuid, color, and vertexColors.
%
% Output arguments:
%  S  Struct with the necessary fields for a `THREE.LineBasicMaterial`.
%
  white = hex2dec('ffffff');

  default = struct('type','LineBasicMaterial', 'color',white);

  s = updatestruct(default, varargin{:});
end
