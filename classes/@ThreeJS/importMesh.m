function importMesh(three, tri, id, varargin)
% Import a Mesh instance into the ThreeJS scene.
%
% Input arguments:
%  THREE     ThreeJS instance.
%  TRI       Mesh instance.
%  ID        String specifying a unique ID for the Mesh object. Unique IDs for
%            the texture, material and light (if added) are derived from this.
%  VARARGIN  Optional extra arguments to pass to `ThreeJS.Mesh`.
%
  ctype = colortype(tri);

  % Geometry stores vertices, faces, and possibly colour.
  geometryid = sprintf('%s-geometry', id);
  geometry = ThreeJS.MeshGeometry(tri, 'uuid',geometryid);
  three.tree.geometries{end+1} = geometry;

  % Material informs the renderer.
  if ctype
    materialid = sprintf('%s-material', id);
    material = ThreeJS.MeshBasicMaterial(tri, 'uuid',materialid);
  else
    % Add a spotlight to uniform colour phong material.
    spotlightid = sprintf('%s-spotlight', id);
    spotlight = ThreeJS.SpotLight('uuid',spotlightid, 'angle',0.154, ...
        'exponent',100, 'intensity',0.88, ...
        'matrix',[1,0,0,0,0,1,0,0,0,0,1,0,-900,150,480,1]);
    three.tree.object.children{end+1} = spotlight;

    materialid = sprintf('%s-material', id);
    material = ThreeJS.MeshPhongMaterial(tri, 'uuid',materialid);
  end
  three.tree.materials{end+1} = material;

  % The object binds the material to the geometry.
  object = ThreeJS.Mesh(tri, geometryid, materialid, 'uuid',id, varargin{:});
  three.tree.object.children{end+1} = object;
end
