function s = PointCloud(geometryid, materialid, varargin)
% Define a `THREE.PointCloud`.
%
% Input arguments:
%  GEOMETRYID  String specifying the uuid of the point cloud geometry.
%  MATERIALID  String specifying the uuid of the point cloud material.
%  VARARGIN    Struct or arguments to create a struct. Fields defined in
%              VARARGIN overwrite fields in the default struct. The default
%              struct has type='PointCloud', geometry=GEOMETRYID,
%              material=MATERIALID and the default matrix. Typical fields to
%              set in VARARGIN include uuid, name and userData.overlay=true.
%
% Output arguments:
%  S  Struct with the necessary fields for a `THREE.PointCloud`.
%
  default = struct('type','PointCloud', 'geometry',geometryid, ...
      'material',materialid, 'matrix',[1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]);

  s = updatestruct(default, varargin{:});
end
