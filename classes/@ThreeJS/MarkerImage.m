function s = MarkerImage(id, varargin)
% Define a `THREE.Image` depicting a round marker.
%
% Input arguments:
%  ID        Since uuid is a required field, the ID argument is positional and
%            required rather than the usual optional uuid keyword in VARARGIN.
%  VARARGIN  Struct or arguments to create a struct. Fields defined in VARARGIN
%            overwrite fields in the default struct. The default struct has
%            uuid=ID, url=dataurl where dataurl is a string which the web
%            browser renders as a circle.
%
% Output arguments:
%  S  Struct with the necessary fields to render a marker.
%

  % Data URL will be rendered as image (circle marker) by the browser.
  dataurl = ['data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAAB' ...
              'zenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAtt' ...
              'JREFUeNrcV02IUlEUPlq00EKFQLfq9LPXkMqihT9UFEGQm6JZim50V4IbwZq' ...
              'dbnQ9UBtrE/RH6qqRBin3/Yy2EwcCFUIhUDvf6wovfaNPfTLRge9eH+r3fe+' ...
              '8e+85TzcajegwQ0+HHEfV/tDj8eh4usS4zrjI2GBYxNdtxh7jA+MVY6darap' ...
              'KrW7eI2BhZGmT8VCIqgmYeczYZiPDpQ2wuJ2nAuMcrm02GwUCAXK73WS328l' ...
              'sNtNgMKD9/X1qtVpUq9WoXC5Ts9kcU3xkhNjE94UNsPgVnp4zTkI4HA5TMBg' ...
              'kvX72shkOh5KJXC4nmeL4wbjNJt6rNsDiAZ5eMo55vV5KpVJkNBoXWly9Xo+' ...
              'SySRVKhVc/mLcZBPv5hpg8TMidSdCoRDFYrG5dz0rG9lslgoFPEX6yXCziS8' ...
              'HbkMWP8LTE4j7/X6Kx+NLi0vk/F9wgIvjOLiFxoHnwH0sOKvVSolEgnQ63cr' ...
              '7HBzgwjoSi3lT0YDY59hqFI1GyWAwaHbYgCsSiYwvHwitqQxcxj6HU5EyTQO' ...
              'cIgsbQmvKwDUM2OerPPdZ6wHccq1JAxcwuFyutZ37Mu7zSgZOYXA6nWszIOM' ...
              '+rWRAKiwmk2ltBnB0y7X+iXIsN4CSSt1ud21inU7nL61JA98w1Ov1tRmQcX9' ...
              'VMrCLASV1XSHj3lUy8BpDsViUiojWAU5wi3ijZAD1eg81vFQqaW4AnKI/QLe' ...
              '0M2VA9HBb+JzP56nf72smDi5witiSt2mT23Cb8QlO0+k0adGygwNc4u5rQkO' ...
              '5HLOzAU/30DwgZZlMZqX1gP+CQzxSNCR3hcbcluwqTy80bslusfjbRZpSmHi' ...
              'GTgZlFD2Cz+dbpinFnd9RElfTlp/l6SkK2bgtR2eMquZwOMhi+XOkt9ttajQ' ...
              'a0j5HumVteU2k/bMWLyYJFDS1hx7j0covJgpGvIwbondwTrya1cWrGdr5yjz' ...
              'hhQ38t2/HvwUYABjaQiUV1B9IAAAAAElFTkSuQmCC'];
  default = struct('uuid',id, 'url',dataurl);
  
  s = updatestruct(default, varargin{:});
end
