function [str, args] = compile2json(s)
% Compile a variable to a string template (in JSON format) and value arguments.
%
% This compiles a very basic structure and certainly does not cover all
% possible Matlab structures (e.g. not struct arrays -> use a cell to group
% them into an array of objects in the output, also no custom classes). That
% said, this method should allow for simple and efficient conversion of your
% Matlab variables to JSON.
% 
% JSON stands for Javascript Object Notation.
%
% Input arguments:
%  S    Any type of supported variable. Can be a structure, nested structure,
%       cell array, nested cell array, mix of structs and cells. Leaf nodes can
%       be chars, scalars, empty, numeric vectors and matrices. Structs are
%       translated to Javascript objects, cell arrays to Javascript arrays.
%       Numeric vectors convert to one-line arrays.
%
% Output arguments:
%  STR   Template string. This is the JSON object with placeholders for the
%        arguments.
%  ARGS  Cell array of strings and numbers.
%

% isstruct -> {"%s":%s,"%s":%s,...}
% iscell -> [%s,%s,...]
% ischar -> "%s"
% isscalar -> (islogical(x) -> 'tf', (mod(x,1)>0 -> '%d', '%.8g'))
% isvector -> [%s]   <- sprintf('%.8g,',x)(1:end-1)
% isempty -> []
% ismatrix -> [vector,vector,...]

  if isstruct(s)
    [str, args] = compile_struct(s);
  elseif iscell(s)
    [str, args] = compile_cell(s);
  elseif ischar(s)
    [str, args] = compile_char(s);
  elseif isscalar(s)
    [str, args] = compile_scalar(s);
  elseif isvector(s)
    [str, args] = compile_vector(s);
  elseif isempty(s)
    [str, args] = compile_empty(s);
  elseif ismatrix(s)
    [str, args] = compile_matrix(s);
  end
end

% =============================================================================
% - indent(s, n)

function s2 = indent(s, n)
% Indent all --but the first-- lines in string S by N spaces.
%
% Input arguments:
%  S  String.
%  N  Number of spaces.
%
% Output arguments:
%  S2  Indented string.
%
  indentation = repmat(' ', 1, n);
  s2 = strrep(s, char(10), [char(10) indentation]);
end

% =============================================================================
% - compile_cell(s)
% - compile_char(s)
% - compile_empty(s)
% - compile_matrix(s)
% - compile_scalar(s)
% - compile_struct(s)
% - compile_vector(s)

function [str, args] = compile_cell(s)
% Compile cell array S to JSON template string with separate value arguments.
% iscell -> [%s,%s,...]
  [elems, args] = cellfun(@compile2json, s, 'UniformOutput',0);
  str = indent(sprintf(',\n%s', elems{:}), 2);
  str = ['[' char(10) str(3:end) char(10) ']'];
  args = [args{:}];
end

function [str, args] = compile_char(s)
% Compile string S to JSON string without value arguments.
% ischar -> "%s"
  str = ['"' s '"'];
  args = {};
end

function [str, args] = compile_empty(~)
% Compile empty [] to JSON string with no arguments.
% isempty -> []
  str = '[]';
  args = {};
end

function [str, args] = compile_matrix(s)
% Compile numeric matrix S to JSON template string with separate value argument.
% ismatrix -> [vector,vector,...]

% XXX: What is the fastest approach if S is large?
% Also look into `mat2str`. Could replace ";" with "][".
  rows = num2cell(s, 2);
  [str, args] = compile_cell(rows);
end

function [str, args] = compile_scalar(s)
% Compile scalar S to JSON template string with separate value argument.
% isscalar -> (islogical(x) -> 'tf', (mod(x,1)>0 -> '%d', '%.8g'))
  if islogical(s)
    tf = {'false', 'true'};
    str = tf{s+1};
    args = {};
  else
    % XXX: not sure if testing for %d will speed anything up.
    str = '%.8g';
    args = {s};
  end
end

function [str, args] = compile_struct(s)
% Compile struct S to JSON template string with separate value arguments.
% isstruct -> {"%s":%s,"%s":%s,...}

% Design choice:
% a) compile the keys into the template, or     <-- this
% b) store the keys with the arguments.

  keys = fieldnames(s);
  [values, args] = cellfun(@compile2json, struct2cell(s), 'UniformOutput',0);
  params = [keys values]';
  str = indent(sprintf(',\n"%s": %s', params{:}), 2);
  str = ['{' char(10) str(3:end) char(10) '}'];
  args = [args{:}];
end

function [str, args] = compile_vector(s)
% Compile numeric vector S to JSON template string with separate value argument.
% isvector -> [%s]   <- sprintf('%.8g,',x)(1:end-1)

% Not extensively tested, but this seems to be the fastest approach.
% Alternative str = ['[' repmat('%.8g,', n-1) '%.8g]'] is definitely slower.
  str = '[%s]';
  data = sprintf('%.8g,', s);
  args = {data(1:end-1)};
end
