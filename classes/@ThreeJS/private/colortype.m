function ctype = colortype(tri)
% Determine the best type of colour data available on the mesh.
%
% Input arguments:
%  TRI  Mesh instance.
%
% Output arguments:
%  CTYPE  A number with the following interpretation:
%         0 - no colour data is defined on the mesh,
%         1 - only face colour data is defined,
%         2 - vertex colour data is defined.
%
  if ~isempty(tri.vertexcolor)
    ctype = 2;  % 2 = material uses vertex colours at render time.
  elseif ~isempty(tri.facecolor)
    ctype = 1;  % 1 = material uses face colours at render time.
  else
    ctype = 0;  % 0 = flat material color (use "color":...).
  end
end
