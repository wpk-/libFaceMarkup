function s_merged = updatestruct(s_orig, varargin)
% Update the given struct with extra fields.
%
% Input arguments:
%  S_ORIG    Struct to be amended.
%  VARARGIN  Struct or sequence of key/value pairs.
%
% Output arguments:
%  S_MERGED  Copy of S_ORIG with fields in VARARGIN updated with the respective
%            values.
%
  if numel(varargin) == 1
    % Update with a struct.
    s_update = varargin{1};
    updatenames = fieldnames(s_update);
    updatedata = struct2cell(s_update);
  else
    % Update with a cell array of key/value pairs.
    updatenames = varargin(1:2:end)';
    updatedata = varargin(2:2:end)';
  end

  if numel(updatenames) == 0
    % Don't waste our efforts on doing nothing.
    s_merged = s_orig;
    return;
  end

  orignames = fieldnames(s_orig);
  origdata = struct2cell(s_orig);
  [keepnames,keepix] = setdiff(orignames, updatenames);
  keepdata = origdata(keepix);
  s_merged = cell2struct([keepdata;updatedata], [keepnames;updatenames], 1);
end
