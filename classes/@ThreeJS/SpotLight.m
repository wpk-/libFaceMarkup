function s = SpotLight(varargin)
% Define a `THREE.SpotLight`.
%
% Input arguments:
%  VARARGIN  Struct or arguments to create a struct. Fields defined in
%            VARARGIN overwrite fields in the default struct. The default
%            struct has type='SpotLight', angle=pi/10, color=16777215, decay=1,
%            distance=0, exponent=10, intensity=1 and the default matrix
%            moved to (100,200,150). These are the default values from
%            http://threejs.org/editor/ when adding a spotlight. You would
%            generally want to update most of those values in VARARGIN as well
%            as specify name and uuid.
%
% Output arguments:
%  S  Struct with the necessary fields for a `THREE.SpotLight`.
%
  white = hex2dec('ffffff');  % = 16777215

  default = struct('type','SpotLight', 'angle',pi/10, 'color',white, ...
      'decay',1, 'distance',0, 'exponent',10, 'intensity',1, ...
      'matrix',[1,0,0,0,0,1,0,0,0,0,1,0,100,200,150,1]);

  s = updatestruct(default, varargin{:});
end
