% Convert the FRGC 3D annotations by Perakis et al. to the "lnd" format.

FLD_LM3_IN = '/path/to/FRGC/faces/annotation/Perakis/_orig';
% Subfolders:
%  DB00F
%  DB00F_extreme
%  DB00F_mild
%  DB00F_neutral
%  DB00F45RL
%  DB45L
%  DB45R
%  DB60L
%  DB60R

FLD_LND_OUT = '/path/to/FRGC/faces/annotation/Perakis';

lnddef_in = LandmarkDefinition.load('Perakis-8');
lnddef_out = LandmarkDefinition.load('Perakis-8');


% 1/2. Prepare files and folders.

if ~exist(FLD_LND_OUT, 'dir')
  mkdir(FLD_LND_OUT);
end

lm3files = rdir(FLD_LM3_IN, '*.abs.bin.lm3');
nfiles = numel(lm3files);


% 2/2. Process files one by one.

LandmarkDefinition.print_conversion(lnddef_in, lnddef_out);
fprintf('\n');
fprintf('Converting %d files from LM3 to LND format.\n', nfiles);

parfor i = 1:nfiles
%for i = 1:1
  fpath    = lm3files(i).path;
  fname    = lm3files(i).name;
  base     = fname(1:end-12); % remove triple extension: .abs.bin.lm3
  
  fprintf('%4d. %s/%s\n', i, fpath, fname);
  
  filein  = fullfile(FLD_LM3_IN, fpath, fname);
  fileout = fullfile(FLD_LND_OUT, fpath, [base '.lnd']);
  
  fldout = fileparts(fileout);
  if ~exist(fldout, 'dir')
    mkdir(fldout);
  end
  
  anno = Annotation.load(filein, lnddef_in, 'lm3');
  %anno.print();
  anno.convertto(lnddef_out);
  %anno.print();
  anno.saveas(fileout);
end

fprintf('done.\n');
