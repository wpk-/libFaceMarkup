% Demo of how to export a mesh and annotation to JSON (for web visualisation).
%
% We first load a mesh. Then use ICS to estimate the face intrinsic axes.
% The whole bunch of data, including face, curvature points, pos/neg lines and
% the coordinate system are exported to JSON.
%
% You can use the view3d web component to view the resulting file in a browser.
%
% note: Mesh surface texture is saved using vertex or face colour. It does not
%       export the texture image (at time of writing, 2015-09-22).
%
MESH_FILE = fullfile(FLD_LIBFACEMARKUP_AUX, 'Example.wrl');
JSON_FILE = './Example.json';


% The face.
%
disp('1. Load file...');
mesh = Mesh.load(MESH_FILE);
mesh.clean();

% Set up ICS with all default parameters.
%
disp('2. Set up ICS...');
params = struct('SmoothingOrder',5, 'CurvatureOrder',5);
ics = MeshICS(mesh, params);

% Extract all our attributes. (we won't go into annotation)
%
disp('3. Extract coordinate system and things...');
cosy = ics.coordsys;  % CoordinateSystem instance.
cp = ics.points;      % CurvaturePoints instance.
pnl = ics.lines;      % PosNegLines instance.
pnl = pnl.supporting(cosy.xaxis, 3*pi/180, 9*pi/180);

% Build the scene hierarchy.
%
% note: if you want the mesh without texture, first set mesh.vertexcolor and
%       mesh.facecolor to []. importMesh will then set the surface colour to
%       white and add a default spotlight.
%
disp('4. Build the scene...');
json = ThreeJS();

% mesh.vertexcolor = [];
% mesh.facecolor = [];

json.importMesh(mesh, 'meshid');
json.importCoordinateSystem(cosy, 'coordsysid', 100);
json.importCurvaturePoints(cp, 'pointsid');
json.importPosNegLines(pnl, 'linesid');

% Add a group that combines the mesh with the annotations but excludes the
% light (if that was added). We can use the fact that the light is added first,
% before the mesh, or we can just filter the scene children based on type.
%
disp('b) Group objects...');
groupname = [mesh.name ' annotated'];
disp(['   name = "' groupname '"']);

objects = json.tree.object.children;
collect = cellfun(@(o)isempty(regexpi(o.type,'light|camera')), objects);
group = ThreeJS.Group('children',objects(collect), 'name',groupname);

json.tree.object.children = [objects(~collect) {group}];

% Save to file.
%
disp('5. Save...');
json.saveas(JSON_FILE);
disp('done.');
