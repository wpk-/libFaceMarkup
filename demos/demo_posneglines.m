% We demonstrate how to use the PosNegLines class.
%
% The PosNegLines constructor takes a CurvaturePoints instance, and optional
% parameters (which is actually just verbosity). The class exposes .posvec and
% .negvec Nx3 matrices which describe the positive and negative vectors. The
% line segments are defined by their end points, stored in .ixpos and .ixneg
% Nx2 matrices of pairs of point indices (the actual point coordinates stored
% in the aptly named .coordinates, which should equal the mesh vertices).
% 
% The trick with PosNegLines is that one is generally only interested in a
% subset, for example online the line segments that roughly point in a known
% direction. We can query for that using .supporting(), which returns a copy
% with only the supporting line segments. Selecting the mid points of the line
% segments is done with .midpoints().
%
% Similar to CurvaturePoints the properties are lazily evaluated, meaning that
% all intensive computing is only done on the first request.
%
%m = Mesh.load(fullfile(FLD_LIBFACEMARKUP_AUX, 'Example.wrl'));

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Example 1. Create a PosNegLines instance.

% ~~~~~ For CurvaturePoints parameters
%       see ex_curvaturepoints.m.

cp = CurvaturePoints(m);

% ~~~~~ Really, this is all.

params = struct( ...
  'Verbose', true ...
);

pnl = PosNegLines(cp, params);

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Example 1. bis. The Trick.

% ~~~~~ You should NEVER be interested in ALL lines.

% Find line segments aligned with the x-axis.
x = [1 0 0];
posangle = 3 * pi/180;  % Their angle to the x-axis should not be > 3 degrees.
negangle = 3 * pi/180;  % The same for the negative line segments.

pnlx = pnl.supporting(x, posangle, negangle);
% We can even get the line midpoints.
[posmid,negmid] = pnlx.midpoints();

% Find line segments aligned with the y-axis.
% no posangle -> do not select any pos line segments.
y = [0 1 0];
negangle = 9 * pi/180;  % Nine degrees.

pnly = pnl.supporting(y, [], negangle);

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Show the results.

% ~~~~~ a. The x-axis aligned line segments.

figure;
subplot(1, 2, 1);

trisurf(m, 'FaceAlpha',0.2);
title(m.name);
view([0 58]); % slightly tilt for better view.

hold on;
h = plot3(pnlx, '-x', '-x');
h2 = scatter3(posmid(:,1), posmid(:,2), posmid(:,3), [], 'db', 'filled');
h3 = scatter3(negmid(:,1), negmid(:,2), negmid(:,3), [], 'dg', 'filled');
hold off;
legend(h, 'pos', 'neg', 'Location','NorthEast');

% ~~~~~ b. The y-axis aligned line segments.

subplot(1, 2, 2);

trisurf(m, 'FaceAlpha',0.2);
title('Only vertical NEG line segments.');
view([0 58]);

hold on;
h = plot3(pnly, 'c', 'm');
hold off;
legend(h, 'neg', 'Location','NorthEast');
