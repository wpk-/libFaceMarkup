% Get the FRGC mean annotation from Perakis train data.
% This can later be used for annotation in ICS.
%
% Perakis annotate eight points (see annotation/Perakis/_orig/Readme.txt):
%   (1) right eye outer corner
%   (2) right eye inner corner
%   (3) left eye inner corner
%   (4) left eye outer corner
%   (5) nose tip
%   (6) mouth right corner
%   (7) mouth left corner
%   (8) chin tip
%
% These points all overlap with our 26 landmarks as well as with Conny's 14
% landmarks. However, Conny's ASM requires exactly 14 points. Hence, we add the
% missing six from our own.
%
% NB 1: perakis2lnd.m must be run first.
% NB 2: the .lnd files produced by that script use universal landmark definition
%       (`LandmarkDefinition.universe`). This is good because the .lnd file
%       format does not save the landmark definition explicitly. But it is also
%       important to note that it is NOT the perakis8 definition.
%

% Input folders with FRGC annotations.
FLD_FRGC    = '/path/to/FRGC/faces';
FLD_IMAGES  = fullfile(FLD_FRGC, 'images');
FLD_DBTRAIN = fullfile(FLD_FRGC, 'annotation/Perakis/DB_TRAIN');

% Output files for mean FRGC annotation.
FILE_OUT       = fullfile(FLD_LIBFACEMARKUP_AUX, 'frgc-perakis.lnd');
FILE_OUT_SYM   = fullfile(FLD_LIBFACEMARKUP_AUX, 'frgc-perakis-sym.lnd');
FILE_OUT_NOTES = fullfile(FLD_LIBFACEMARKUP_AUX, 'frgc-perakis-README.txt');

% Be very explicit about the various landmark definitions used.
% - the DB_TRAIN/*.lnd files have been saved with 'universe' indices.
ldfm      = LandmarkDefinition.universe;
ldperakis = LandmarkDefinition.perakis8;
ldruiz    = LandmarkDefinition.ruiz14;

% Conny's (Ruiz) 14 landmarks for ASM.
annruiz = Annotation.load(fullfile(FLD_LIBFACEMARKUP_AUX, 'SymRef.lnd'), ldfm);
annruiz.convertto(ldruiz);

% Our mean symmetric 3D face.
zmesh = Mesh.load(fullfile(FLD_LIBFACEMARKUP_AUX, 'SymRef.wrl'));


%------------------------------------------------------------------------------
% Get the list of files in DB_TRAIN.
disp('Listing files DB_TRAIN/*.lnd...');

dbtrainfiles = dir(fullfile(FLD_DBTRAIN, '*.lnd'));
[~,names]    = arrayfun(@(f)fileparts(f.name), dbtrainfiles, 'UniformOutput',0);
nfiles       = numel(names);

fprintf('%5d files.\n', nfiles);


%------------------------------------------------------------------------------
% Render one example FRGC face with annotation.
% - convert to 'perakis8' landmark definition before printing.
disp('Try one image to see if everything is set up OK.');
exidx  = randi(nfiles);
exmesh = Mesh.load(fullfile(FLD_IMAGES, [names{exidx} '.wrl']));
exanno = Annotation.load(fullfile(FLD_DBTRAIN, [names{exidx} '.lnd']), ldfm);
exanno.convertto(ldperakis)

exanno.lnddef.print()
figure;
trisurf(exmesh);
hold on;
scatter3(exanno, 'filled');
hold off;
colorbar;
title('Verify lanmark colours with printed "Your ID". Then close the window.');
disp('Verify lanmark colours with printed "Your ID". Then close the window.');
uiwait();
disp('');


%------------------------------------------------------------------------------
% Load all annotations.
% Then use generalised Procrustes alignment to find their mean.
disp('Establish the mean annotation by generalised Procrustes alignment.');
disp(' - load files...');
lndfiles = arrayfun(@(f)fullfile(FLD_DBTRAIN,f.name), dbtrainfiles, 'UniformOutput',0);
anntrain = cellfun(@(f)Annotation.load(f,ldfm), lndfiles, 'UniformOutput',0);
anntrain = cat(1, anntrain{:});

disp(' - generalised Procrustes alignment...');
annmean  = anntrain(1).copy();
ix       = annmean.indices;
current  = annmean.coordinates(ix,:);
ptstrain = arrayfun(@(a)a.coordinates(ix,:), anntrain, 'UniformOutput',0);
ptstrain = cat(3, ptstrain{:});

disp(' iter    change (Frobenius norm)');
for j = 1:5
  previous = current;
  current  = zeros(size(current));
  for i = 1:nfiles
    [~,z]   = procrustes(previous, ptstrain(:,:,i), 'scaling',1, 'reflection',0);
    current = current + z;
  end
  current     = current ./ nfiles;
  [~,current] = procrustes(previous, current, 'scaling',1, 'reflection',0);
  delta       = norm(current - previous, 'fro');
  fprintf('%5d. %11.8f\n', j, delta);
end
annmean.coordinates(ix,:) =  current;
% `annmean` is in `LandmarkDefinition.universe` (because all DB_TRAIN/*.lnd
% files are stored using that definition), but describes only the eight points
% defined by Perakis (obviously).


%------------------------------------------------------------------------------
% Align `annmean` to `annruiz` so we can augment the six "missing" landmarks.
disp('Update the standard 14 landmarks with the eight mean Perakis ones.');
[~,annmean] = procrustes(annruiz, annmean, 'scaling',0, 'reflection',0);
annper14    = annruiz.copy(); % copy 14 landmarks
annper14.copyfrom(annmean);   % overwrite 8 landmarks

% Visualise for inspection.
% The eight black dots should coincide with eight coloured dots.
% The colours of the coloured dots should match the LandmarkDefinition printed.
annper14.lnddef.print();
trisurf(zmesh);
hold on;
scatter3(annper14, 'filled');         % Plot all 14.
scatter3(annmean, 10, 'k', 'filled'); % Mark the eight updated.
hold off;
colorbar;
title('Check that 8 black dots coincide, and colours match print.');
disp('Check that 8 black dots coincide, and colours match print.');
uiwait;


%------------------------------------------------------------------------------
% Everything seems to be all right ==> Save.
% - annper14 is in ruiz14 landmark definition, which is fine to save.
annper14.saveas(FILE_OUT);

disp('A recap:');
fprintf(' - loaded DB_TRAIN/*.lnd -> %d x 8 points\n', nfiles);
fprintf(' - aligned -> %d x 8 points\n', nfiles);
disp(' - averaged -> 1 x 8 points');
disp(' - aligned with SymRef -> 1 x 8 points');
disp(' - extended with six from Ruiz14 -> 1 x 14 points');
disp(' - converted to Universe landmark definition -> 1 x 14 points');
fprintf(' - saved to: %s\n', FILE_OUT);


%------------------------------------------------------------------------------
% Also save a symmetric copy.
annper14sym      = annper14.copy();  % Has 'Ruiz-14' landmark definition.
% - eye outer and inner corners, mouth corners
pts              = annper14sym.coordinates([1 2 5 6 9 8],:);
pts(1:2:end,1)   = (pts(1:2:end,1) - pts(2:2:end,1)) / 2;
pts(2:2:end,1)   = -pts(1:2:end,1);
pts(1:2:end,2:3) = (pts(1:2:end,2:3) + pts(2:2:end,2:3)) / 2;
pts(2:2:end,2:3) = pts(1:2:end,2:3);
annper14sym.coordinates([1 2 5 6 9 8],:) = pts;
% - nose tip & chin tip
pts              = annper14sym.coordinates([3 4],:);
pts(:,1)         = 0;
annper14sym.coordinates([3 4],:) = pts;

annper14sym.saveas(FILE_OUT_SYM);

disp(' - "symmetrised" -> 1 x 14 points');
fprintf(' - saved to: %s\n', FILE_OUT_SYM);
disp('');


%------------------------------------------------------------------------------
% Save a small text note with the annotations so we know what they are also in
% future.
notes = [
  'FRGCv2 3D face annotations are available from the website of Panagiotis\n'...
  'B. Perakis (http://graphics.di.uoa.gr/people/cv-perakis.html). These\n'...
  'files define the positions of eight landmarks. The DB_TRAIN folder\n'...
  'contains files to be used for training.\n'...
  '\n'...
  'The frgc-perakis*.lnd files were computed by averaging all DB_TRAIN\n'...
  'files. Then we extended these eight points with the six missing from\n'...
  'Maria C. Ruiz ASM model (LandmarkDefinition.ruiz14). They were loaded\n'...
  'from SymRef.lnd.\n'...
  '\n'...
  'Lastly, in frgc-perakis-sym.lnd we have averaged the y and z\n'...
  'coordinates of outer and inner eye corners and mouth corners, mirrored\n'...
  'their x coordinates, and set x=0 for the nose and chin tip.\n'...
  '\n'...
  'The Matlab script for this is frgc_perakis_mean.m\n'
  ];
f = fopen(FILE_OUT_NOTES, 'w');
fprintf(f, notes);
fclose(f);

disp('done.');

