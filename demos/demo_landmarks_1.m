% Transfer annotation from one mesh to another using Barycentric coordinates.
% Both meshes MUST share the same topology.
%

REF_MESH = '~/Data/ZMEAN_Oxf822.wrl';
REF_ANNO = '~/Data/ZMEAN_Oxf822.lnd';

TRG_MESH = '~/Data/ZMEAN.wrl';
TRG_ANNO = '~/Data/ZMEAN.lnd';
% TRG_MESH = fullfile(FLD_LIBFACEMARKUP_AUX, 'ZMEAN_PoBI2593.wrl');
% TRG_ANNO = fullfile(FLD_LIBFACEMARKUP_AUX, 'ZMEAN_PoBI2593.lnd');
% TRG_MESH = fullfile(FLD_LIBFACEMARKUP_AUX, 'SymRef.wrl');
% TRG_ANNO = fullfile(FLD_LIBFACEMARKUP_AUX, 'SymRef.lnd');


% 1. Load the source mesh and its annotation.

m1 = Mesh.load(REF_MESH);

%   - first get the landmark definition, so it is official what the landmark
%     numbers reflect.
ld = LandmarkDefinition.tena26;
%   - then load the coordinates from file.
an = Annotation.load(REF_ANNO, [], ld);
%   - extract the points of interest into a matrix.
ix = an.indices;
p1 = full(an.coordinates(ix,:));


% 2. Load the target mesh. It must have the same topology as `m1`.

m2 = Mesh.load(TRG_MESH);

f1 = m1.faces;
f2 = m2.faces;
assert(all(size(f1)==size(f2)) && all(f1(:)==f2(:)), ...
        'Meshes have different topology!');


% 3. Use Barycentric coordinates to transfer the annotation from `m1` to `m2`.

npts = size(p1,1);

for i = 1:npts
  % - get the UV coordinates relative to ALL triangles.
  [uv, inside] = m1.cart2bary(p1(i,:));
  % - find which triangle actually contains the point.
  j            = find(inside, 1);
  % - convert from UV back to cartesian coordinates, but in mesh `m2`.
  xyz2         = m2.bary2cart(uv(j,:), j);
  % - store the new position in `an`.
  an.define(ix(i), xyz2);
end


% 4. We could save the new annotation to file.

%an.saveas(TRG_ANNO);


% 5. Render the result.

%   - extract the matrix for easy plotting.
p2 = full(an.coordinates(ix,:));

figure;

subplot(1, 2, 1);
trisurf(m1);
view(2);
hold on;
scatter3(p1(:,1), p1(:,2), p1(:,3), 50, 'LineWidth',2);
hold off;

subplot(1, 2, 2);
trisurf(m2);
view(2);
hold on;
% scatter3(p1(:,1), p1(:,2), p1(:,3), 50, 'LineWidth',2);  % original = blue
scatter3(p2(:,1), p2(:,2), p2(:,3), 'filled');  % new position = green
hold off;
