% Convert all 3D images in the FRGC data set to *.wrl (VRML) format.
% The output will also be structured in the same way as the PoBI and TwinsUK
% data sets.

% ~~ If running on Unix, you can get the texture images in *.jpg by the
%    following approach:
%     1. Copy all *.ppm files and convert them to *.jpg files (e.g. using
%        Gimp or Photoshop). Then remove the *.ppm files.
%        If you can convert them without copying, all the better. Either way,
%        you should end up with *.jpg files in FLD_FRGC_OUT.
%     3. Symlink all *.abs.gz files.
%     4. Change this script, setting FLD_FRGC_IN = FLD_FRGC_OUT (see below).
%     5. Run this script.
%        All *.wrl files should now be using the *.jpg texture images.
%     6. Remove the *.abs.gz symlinks.
%    This has been tested and works.
% ~~

FLD_FRGC_IN = '/path/to/frgc/FRGC-2.0-dist/nd1';
% Subfolders with 3D content:
%  Fall2003range
%  Spring2003range
%  Spring2004range

FLD_FRGC_OUT = '/path/to/FRGC/faces/images';


% 1. Make sure the output folder exists.

if ~exist(FLD_FRGC_OUT, 'dir')
  mkdir(FLD_FRGC_OUT);
end


% 2. Find all .abs.gz files.

gzfiles = rdir(FLD_FRGC_IN, '*.abs.gz');

nfiles = numel(gzfiles);


% 3. Process files one by one.

fprintf('Converting %d files from FRGCv2 format to VRML (wrl).\n', nfiles);

parfor i = 1:nfiles
%for i = 1:1
  fpath    = gzfiles(i).path;
  fname    = gzfiles(i).name;
  [~,base] = fileparts(fname);
  [~,base] = fileparts(base); % remove double extension: .abs.gz
  
  fprintf('%4d. %s/%s\n', i, fpath, fname);
  
  filein  = fullfile(FLD_FRGC_IN, fpath, fname);
  fileout = fullfile(FLD_FRGC_OUT, [base '.wrl']);
  
  mesh = Mesh.load(filein);
  mesh.saveas(fileout);
end

fprintf('done.\n');
