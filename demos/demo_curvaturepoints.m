% We demonstrate how to use the CurvaturePoints class.
%
% It is very simple: you construct it with a mesh, then query the data you need.
% It has .cups, .caps, and .saddles that return Nx3 matrices of points. The
% points are a selection of the total set of .coordinates (the mesh vertices).
% To know exactly which vertices are selected you can query .ixcup, .ixcap, and
% .ixsaddle respectively, each returning a column vector of indices.
%
% The funny bit is that the class does lazy computation. So the cups, caps and
% saddles are not computed until you actually request them. Once computed, they
% are stored, so the next query returns instantly.
%
% Being based on the mesh curvature, the object also stores the computed mesh
% curvature. The Gaussian curvature is stored in .K and the mean curvature in
% .H. The latter is only needed to differentiate between cups and caps.
%
m = Mesh.load(fullfile(FLD_LIBFACEMARKUP_AUX, 'Example.wrl'));

% ~~~~~~~~~~~~~~~~~~~~~~~~~~ Example 1. Construct a CurvaturePoints instance.

params = struct( ...
  'CurvatureOrder',     10, ...
  'SmoothingOrder',     5, ...
  'SmoothingFunction',  @(d)normpdf(d,0,10), ...
  'Mask',               true(m.numvertices,1), ...
  'Verbose',            true ...
);

cp = CurvaturePoints(m, params);

% ~~~~~ Show the result.

figure;
subplot(1, 2, 1);

trisurf(m);
title(m.name);
view(2);

hold on;
scatter3(cp);
hold off;

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Example 2. Filter the curvature points.

mindist = 10; % 10mm = 1cm.
maxnum = 20;  % 20 cups, 20 caps, and 20 saddles max.

cp.filter(mindist, maxnum);

% ~~~~~ Show the result.

subplot(1, 2, 2);

trisurf(m, cp.K);
set(gca, 'CLim',0.005*[-1 1]);  % usually 0.02 is good. :-/
colormap summer;
title(sprintf('Min. dist. %.2fmm. Max. num. %.2f.', mindist, maxnum));
view(2);

hold on;
% For cups, caps, and saddles in order:
%   - marker sizes: 150, default, 10
%   - colours: cyan, magenta, yellow
%   - markers: triangles, diamonds, circles.
scatter3(cp, 150, [], 10, '^c', 'dm', 'oy', 'filled');
hold off;
