% Use ICS to annotate a mesh.
%
% We will first construct an ICS instance.
% Then calibrate it with a reference mesh (average face) and annotation.
% Then we transfer the reference annotation to a new mesh using both meshes
% intrinsic coordinate systems.
%
% Not shown in this demo is how to snap the transfered annotation onto the mesh
% surface.
%

% Unknown mesh.
%
if ~exist('mesh', 'var')
  meshwrl = fullfile(FLD_LIBFACEMARKUP_AUX, 'Example.wrl');
  
  mesh = Mesh.load(meshwrl);
  mesh.clean(); % takes a second, but can help a lot.
end

% Reference material.
%
if ~exist('refmesh', 'var') || ~exist('refanno', 'var')
  refwrl  = fullfile(FLD_LIBFACEMARKUP_AUX, 'SymRef.wrl');
  reflnd  = fullfile(FLD_LIBFACEMARKUP_AUX, 'SymRef.lnd');
  lnddef  = LandmarkDefinition.universe;

  refmesh = Mesh.load(refwrl);
  refanno = Annotation.load(reflnd, lnddef);
end


% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%  Construct an ICS instance.
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% ICS parameters.
% - each line has a small explanation.
%
s_fcn = @(d) normpdf(d, 0, 5);
params = struct(...%'Mask', 0.9, ...                % Focus on central 90% of data.
  'CurvatureOrder', 10, ...       % 10-neighbourhood for Gaussian curvature.
  'SmoothingOrder', 10, ...       % 10-neighbourhood smoothing matrix.
  'SmoothingFunction', s_fcn, ... % Normal pdf with sigma=5.
  'PeaksMinDist', 10, ...         % 10 mm between peaks.
  'PeaksMaxNum', 30, ...          % 30 peaks max. (x3 for cups, caps, saddles).
  'PosAngle', 3 * pi/180, ...     % Less than 3 degrees (rad) is aligned.
  'NegAngle', 9 * pi/180, ...     % Consider 9 degrees for neg. line segments.
  'BinCentres', 2 * pi/180, ...   % Bins spaced roughly 2 degrees apart.
  'TipMaxOffset', 7, ...          % Nose tip 7 mm from plane of symmetry.
  'PlaneMaxVar', 3, ...           % Max 3 mm var. in line segment midpoints.
  'PosLinesMinNum', 5, ...        % Require at least 4 pos lines to accept.
  'Verbose', false, ...           % Don't print timings or draw figures.
  'Version', 4 ...
);

% ICS annotation requires one-time calibration with a reference.
% - the reference mesh is used to find the lateral axis, and so construct a
%   reference coordinate system.
% - this allows the reference annotation to be positioned in other coordinate
%   systems (such as ones estimated in new images).
% - `refcosy` is a CoordinateSystem instance (see that class for more info).
%
refics  = MeshICS(refmesh, params);
refcosy = refics.coordsys;


% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%  Find the coordinate system in an example image and use for annotation.
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% Set up the ICS instance for the new mesh.
% - it makes sense to use the same parameters as for the reference.
%
ics = MeshICS(mesh, params);

% For illustration purposes we will go the long way, and do the transfer based
% on coordinate systems manually.
% - note that `ics.annotate` would've done the whole process for us (including
%   some extra checks):
%   [anno,anno0,pred] = ics.annotate(refanno, refcosy);
%   cosy = ics.coordsys; % if you still want it.
% - like `refcosy` is the CoordinateSystem for `refmesh`,
%   `cosy` is the CoordinateSystem for `mesh`.
%
cosy = ics.coordsys;

% Compute the transformation from `refcosy` to `cosy`.
% - `rt` is a RigidTransformation.
%
rt = RigidTransformation.fromcoordinatesystems(refcosy, cosy);

% Now apply the transformation to `refanno` to position the markers
% with respect to `mesh`.
% - after the rigid transformation we snap the points onto the mesh.
% - `anno0` is the rigidly transformed annotation.
% - `anno` is the transformed annotation, snapped onto the surface.
%
anno0 = Annotation.copy(refanno);
rt.transform(anno0);

anno = Annotation.copy(anno0);
anno.snaptosurface(mesh);


% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%  Visualise what we just computed.
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% The problem: how do we annotate the new image?
%
figure(1);
trisculpt(mesh);
view(2);
cameratoolbar;
cameratoolbar('SetMode','orbit');
cameratoolbar('SetCoordSys','y');
title('Press enter to continue.');
disp('Press enter to continue.');
pause;

% The starting point: A reference mesh (with annotation).
%
figure(2);
trisculpt(refmesh);
view(2);
cameratoolbar;
cameratoolbar('SetMode','orbit');
cameratoolbar('SetCoordSys','y');
title('Press enter to continue.');
disp('Press enter to continue.');
pause;

figure(2);
hold on;
scatter3(refanno, 'filled');
hold off;
disp('Press enter to continue.');
pause;

% Step two: Estimated intrinsic coordinate system of the reference.
% - the coordinate system *should* be positioned on the nose with
%   * red pointing to the side,
%   * green pointing up, and
%   * blue pointing towards the camera.
%
figure(2);
hold on;
plot3(refcosy, 100, 'LineWidth',3);
hold off;
disp('Press enter to continue.');
pause;
title('');

% Step three: Estimated intrinsic coordinate system of the new image.
% - again, the coordinate system *should* be positioned on the nose with
%   * red pointing to the side,
%   * green pointing up, and
%   * blue pointing towards the camera.
%
figure(1);
hold on;
plot3(cosy, 100, 'LineWidth',3);
hold off;
title('Press enter to continue.');
disp('Press enter to continue.');
pause;

% Done: Annotation from the reference mesh transferred onto the new image.
%
figure(1);
hold on;
h = scatter3(anno0, 'filled');
hold off;
disp('Press enter to continue.');
pause;

% Lastly: Transferred annotation snapped onto the surface.
%
figure(1);
delete(h);
hold on;
scatter3(anno, 'filled');
hold off;
title('');
