function files = rdir(folder, pattern, subfolder)
% List all files in a folder and its subfolders.
%
% Recursively walk the directory tree under `folder` and find all files that
% match `pattern`.
%
% Input arguments:
% - FOLDER  A string value specifying the base folder to be searched.
% - PATTERN  A string value specifying a file or folder name pattern.
%
% Output arguments:
% - FILES  A struct array, similar to that returned by `dir()`, but augmented
%          with a `path` field to reflect the file's path relative to `folder`.
%   .name     File or folder name (char)
%   .date     Modification date timestamp (char)
%   .bytes    Size of the file in bytes (double)
%   .isdir    True if name is a folder; False if not (logical)
%   .datenum  Modification date as serial date number (double)
%   .path     Location of the file relative to `folder` (char)
%
% Considerations:
% - Folders with names matching `pattern` will be returned too.
% - You can also pass a single input argument: '`folder`/`pattern`'.
%
% See also: `dir`
%
  if nargin < 2
    [folder,fname,ext] = fileparts(folder);
    pattern = [fname ext];
  elseif isempty(folder)
    folder = '';
  end
  if nargin<3 || isempty(subfolder), subfolder=''; end

  % 1. Find files in the current (sub)folder.

  files = dir(fullfile(folder, subfolder, pattern));
  % - Return a cell array of strings:
  %files = arrayfun(@(f) fullfile(subfolder, f.name), files, 'UniformOutput',0);
  % - Return a struct array similar to `dir()`, augmented with a `path` field:
  paths = repmat({subfolder}, size(files));
  [files(:).path] = paths{:};
  % - Return a struct array with *absolute* paths:
  %paths = repmat({fullfile(pwd,folder,subfolder)}, size(files));
  %[files(:).path] = paths{:};

  % 2. Find folders in the current (sub)folder.

  subsubfolders = dir(fullfile(folder, subfolder, '*'));
  subsubfolders = subsubfolders([subsubfolders.isdir]);
  named         = arrayfun(@(fld) ~any(strcmp(fld.name, {'.','..'})), ...
                  subsubfolders);
  subsubfolders = arrayfun(@(fld) fullfile(subfolder, fld.name), ...
                  subsubfolders(named), 'UniformOutput',0);

  % 3. Find files recursively in all sub(sub)folders.

  subfiles = cellfun(@(fld) rdir(folder, pattern, fld), ...
              subsubfolders, 'UniformOutput',0);

  % 4. Append to our list of files in the current (sub)folder.

  files = vertcat(files, subfiles{:});
end
