function b = islinespec(x)
% Determine whether the argument could be a LineSpec.
%
% Input arguments:
%  X  Argument that may or may not be a LineSpec.
%
% Output arguments:
%  B  Logical scalar, `true` if X could be used as LineSpec.
%
  re_ls = '^([bcdghkmoprsvwxy.:+*^<>-]|square|diamond|pentagram|hexagram)*$';
  b     = ischar(x) && ~isempty(regexp(x, re_ls, 'ONCE'));
  % Undocumented Matlab (taken from scatter.m): colstyle(x).
end
