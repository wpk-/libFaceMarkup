function Z = czscore(X, flag)
% Z = CZSCORE( X, [FLAG] )
%
% Subtracts the mean from each column and divides by the standard deviation
%
% Faster than zscore(X, flag).
%
	c = nargin<2 || flag==0;
	
	D = size(X, 1);
	M = (ones(1,D) / D) * X;
	
	% subtract mean
	Y = bsxfun(@minus, X, M);
	
	% D / sum squares
	Z = bsxfun(@ldivide, ones(1,D) * (Y.^2), D-c);
	
	% dividing by sqrt(1/D * sumsquares)
	% equals multiplying by sqrt(D / sumsquares)
	Z = bsxfun(@times, Y, sqrt(Z));
end
