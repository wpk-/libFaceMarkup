function [B,M] = rsubmean(A)
% B = RSUBMEAN( A )
%
% Subtract the mean from each row in matrix A.
%
% B,M = RSUBMEAN( A ) returns in M the subtracted row means.
%
% Faster than using mean() and faster than using repmat()
%
	D = size(A, 2);
	M = A * (ones(D,1) / D);
	B = bsxfun(@minus, A, M);
end
