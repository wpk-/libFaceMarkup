function B = rdiv2norm(A)
% B = RDIV2NORM( A )
%
% Divide rows in A by their 2-norm, thus scale rows to unit length.
%
% Faster and more accurate.
% - `bsxfun` is faster than `repmat`.
% - `* ones(D, 1)` is faster and more accurate (!!) than `sum(..., 2)`.
%   (tested on Matlab 2018b in Windows 7, 64bit)
% - `rdivide` with `sqrt` is faster than `times` with `.^ -0.5`.
%
  D = size(A, 2);
  B = bsxfun(@rdivide, A, sqrt((A.^2) * ones(D, 1)));
end
