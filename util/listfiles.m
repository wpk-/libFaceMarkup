function filenames = listfiles(folder, pattern)
% Find all files in FOLDER that match PATTERN. If FOLDER has a pattern added to
% it already, then list those files and ignore PATTERN. If FOLDER is a text
% file, read relative paths (wrt FOLDER) from it line by line.
%
% Input arguments:
%  FOLDER   String with a folder name or folder name with pattern. Examples:
%           'path/to/here/' or 'path/to/here/*.lnd'. In case of the latter, the
%           PATTERN input argument is ignored. A third option is to specify a
%           text file that lists all files: 'path/to/list.txt', and list.txt
%           then contains lines like 'here/aaa.lnd'.
%  PATTERN  String with a pattern to match files in FOLDER. Example: '*.lnd'.
%
% Output arguments:
%  FILENAMES  Nx1 cell array with the full file names of all matched files. A
%             full file name is the concatenation of FOLDER with the name of
%             the matched file (and possibly inserted file separator if FOLDER
%             did not end in one).
%
% Considerations:
%  - For convenience FOLDER can also be a cell array of strings, in which case
%    no matching is performed at all: the input argument is returned directly.
%
  if ischar(folder)
    [fld,fnm,ext] = fileparts(folder);

    if exist(folder, 'file') && ~exist(folder, 'dir')
      % Passed a text file.
      fid = fopen(folder);
      text = fread(fid,'*char')';
      fclose(fid);
      filenames = cellfun(@(s)fullfile(fld,s), strsplit(text)', ...
                            'UniformOutput',0);
      return;
    end

    if isempty(ext)
      % Passed a folder.
      folder  = fullfile(fld, fnm);
    else
      % Passed a pattern like 'folder/*.wrl'.
      folder  = fld;
      pattern = [fnm ext];
    end
    
    files     = dir(fullfile(folder, pattern));
    filenames = arrayfun(@(f) fullfile(folder, f.name), files, ...
                        'UniformOutput',0);
  elseif iscellstr(folder)
    % Passed a cell array of strings.
    filenames = folder;
  else
    error('Failed to parse FOLDER argument.');
  end
end
