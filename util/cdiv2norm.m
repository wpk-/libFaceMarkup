function B = cdiv2norm(A)
% B = CDIV2NORM( A )
%
% Divide columns in A by their 2-norm, thus scale columns to unit length.
%
% Faster and more accurate
%
  D = size(A, 1);
  B = bsxfun(@rdivide, A, sqrt(ones(1, D) * (A.^2)));
end
