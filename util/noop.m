function noop(varargin)
% Do nothing.
%
% This function intentionally does nothing and returns nothing, while taking any
% number of arguments. It is useful for adding debugging calls to code, where we
% can choose to swap `fprintf` or `disp` with this function if debugging is
% temporarily not needed.

end
