function [h0, h1, h2] = plotmle(err, varargin)
% Plot the mean landmark error (or any error) in a cumulative fashion.
%
% Input arguments:
%  ERR       Nx1 array of errors.
%  LINESPEC  Optional string specifying a LineSpec (see `plot`).
%  VARARGIN  Optional keyword arguments. All `plot` keyword arguments are
%            supported. Additionally the following options are available:
%            'XLim'   To specify `xlim` directly. Default is `[0 20]`,
%            'Names'  Nx1 cell array of strings to specify labels associated
%                     with each error that will be shown in the data tips.
%            'Partition'  Nx1 array of integers (partition labels) or
%                     `cvpartition` instance. When ERR has been obtained by
%                     k-fold cross validation (or hold-out), we can measure the
%                     standard deviation at intervals of error, which will be
%                     plotted with `errorbar`.
%
% Output arguments:
%  H0  Handle to an invisible line that has the complete line spec (markers and
%      line). Use this handle for things like the legend to show the correct
%      line spec.
%  H1  Handle to the markers.
%  H2  Handle to the line.
%
  p = inputParser();
  p.FunctionName = 'plotmle';
  p.KeepUnmatched = true;
  p.addRequired('err',            @isvector);
  p.addOptional('LineSpec',   '', @islinespec);
  p.addParameter('XLim',  [0 20], @noop);
  p.addParameter('Names',     [], @iscellstr);
  p.addParameter('Partition', [], @(x)isa(x,'cvpartition')||isindex(x));
  p.parse(err, varargin{:});

  linespec  = p.Results.LineSpec;
  xl        = p.Results.XLim;
  names     = p.Results.Names;
  partition = p.Results.Partition;

  addnames  = ~isempty(names);

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Process args.

  nfiles = numel(err);

  prc = 100 * (1:nfiles)' / nfiles;
  [serr,ix] = sort(err);

  % Warn about NaN entries.
  % They will push the 100% limit down.
  if any(isnan(err))
    warning('ERR contains %d NaNs.', nnz(isnan(err)));
  end

  [marksx, marksy, stdy] = cumerr(err, max(xl), partition);

  if marksx(1) < serr(1)
    names = [names; {''}];
    ix = [numel(names); ix];
    serr = [marksx(1); serr];
    prc = [marksy(1); prc];
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Plot.

  lineprops = {'Color', 'LineStyle', 'LineWidth'};
  markerprops = {'Marker', 'MarkerEdgeColor', 'MarkerFaceColor', 'MarkerSize'};

  % Plot two lines:
  %  1. markers at unit intervals,
  %  2. detailed line matching item in `err`.

  if ~isempty(partition)
    h1 = errorbar(marksx, marksy, stdy, linespec);
    if ~isempty(p.Unmatched)
      set(h1, p.Unmatched);
    end
  else
    h1 = plot(marksx, marksy, linespec, p.Unmatched);
  end
  l0 = [lineprops markerprops; get(h1, [lineprops markerprops])];
  l2 = [lineprops; get(h1, lineprops)];
  ih = ishold;
  if ~ih, hold on; end
  if isempty(marksx)  % try not to throw an error
    h0 = plot([], [], l0{:});
  else
    h0 = plot(marksx(end), marksy(end), l0{:});
  end
  if addnames
    h2 = plot(serr, prc, 'UserData',names(ix), l2{:});
  else
    h2 = plot(serr, prc, l2{:});
  end
  if ~ih, hold off; end
  uistack(h1, 'top');

  xlim(xl);
  ylim([0 100]);
  xlabel('Error (mm)');
  ylabel('Samples (%)');

  if strcmpi(l2{2}, 'none') % LineStyle
    set(h2, 'LineStyle','-');
  else
    set(h1, 'LineStyle','none');
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Data labels.

  % Add data labels, so the user can
  % click to know which item that is.

  dcm_obj = datacursormode(gcf);
  % FIXME: should test for line handle. if markers, don't add name.
  if addnames
    set(dcm_obj, 'enable','on', 'UpdateFcn',@(~,evt) ...
        sprintf('Error: %.2f\nPrctl: %.2f\nName: %s\nIndex: %d', ...
        evt.Position, names{ix(evt.DataIndex)}, ix(evt.DataIndex)));
  else
    % no names :(
    set(dcm_obj, 'enable','on', 'UpdateFcn',@(~,evt) ...
        sprintf('Error: %.2f\nPrctl: %.2f\nIndex: %d', ...
        evt.Position, ix(evt.DataIndex)));
  end

  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Keep one legend entry only.

  try
    hannos = get([h1 h2], 'Annotation');
    hinfo1 = get(hannos{1}, 'LegendInformation');
    hinfo2 = get(hannos{2}, 'LegendInformation');
    set(hinfo1, 'IconDisplayStyle','off');
    set(hinfo2, 'IconDisplayStyle','off');
  catch e %#ok<NASGU>
    return;
  end
end

function [x, y, stdy] = cumerr(err, x, partition)
% Compute percentage of samples with error < x for a range of x.
% If a partition is provided, also compute the standard deviations.
%
% Input arguments:
%  ERR        Nx1 array of errors.
%  X          Optional 1xM array providing intervals at which to evaluate ERR.
%             Default is `1:ceil(max(ERR))`.
%  PARTITION  Optional partition index array (numbers represent test class) or
%             `cvpartition` instance. This allows for the standard deviations
%             to be computed.
%             Default is `[]`.
%
% Output arguments:
%  Y     Mx1 array of the mean percentage of samples with `ERR < X`.
%  STDY  Mx1 array with standard deviations around Y.
%
  if nargin<3, partition=[]; end
  if nargin<2 || isempty(x), x=max(err); end

  if isscalar(x)
    x = 1:ceil(x);
  end

  err = err(:)';
  x = x(:)';

  assert(all(x(2:end) >= x((2:end)-1)), 'X must be monotonically nondecreasing.');

  % Convert `cvpartition` to partition index array.
  if isa(partition, 'cvpartition')
    p = nan(size(err));
    for i = 1:partition.NumTestSets
      p(partition.test(i)) = i;
    end
    partition = p;
  elseif isempty(partition)
    partition = ones(size(err));
  else
    partition = reshape(partition, size(err));
  end
  numpartitions = max(partition);

  % Remove NaNs. (later multiply to correct)
  ixnan = isnan(err);
  if any(ixnan)
    err = err(~ixnan);
    partition = partition(~ixnan);
  end

  % Prepare measurements.
  maxx = ceil(max([err x(end)])); % don't forget: `err > max(x)` is possible.
  y = nan(numpartitions, numel(x));

  % Measure error distribution.
  for i = 1:numpartitions
    m = partition == i;
    if ~any(m)
      continue; % use `nanmean` and `nanstd`.
    end
    serri = sort(err(m));
    prci = 100 * (1:nnz(m)) ./ nnz(m);
    yi = interp1([0 serri maxx], [0 prci 100], x);
    lbound = x < serri(1);
    yi(lbound) = 0;
    y(i,:) = yi;
  end

  % Statistics of the results.
  mult = (numel(ixnan) - nnz(ixnan)) ./ numel(ixnan);
  stdy = nanstd(y, 0, 1) * mult;
  y = nanmean(y, 1) * mult;
end
