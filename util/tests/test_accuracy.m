function test_accuracy( fn )
%TEST_ACCURACY( FN ) Measures computational accuracy of different functions.
%   We create random matrices of random sizes and compare over multiple
%   trials the error in our function against a "common" Matlab approach.
%
%   FN can be one of:
%      - 'cdiv2norm': divide columns by their vector 2-norm (unit length)
%      - 'csubmean': subtract the mean from each column
%      - 'czscore': standardise columns; subtract mean and divide by std
%      - 'czscore_1': same as czscore but pass flag 1
%      - 'rdiv2norm': divide rows by their vector 2-norm (make unit length)
%      - 'rsubmean': subtract the mean from each row
%      - 'rzscore': standardise rows; subtract mean and divide by std
%      - 'rzscore_1': same as rzscore but pass flag 1
%
%
% @note 11 oct 2011:
%   rdiv2norm() seems to be less accurate than the matlab common version.
%   However cdiv2norm() is not affected.
%   -- What do colums/rows have to do with it?
%      ???
%   With a new test (measuring deviation in 10 recursive calls) it looks
%   like rdiv2norm() is actually more accurate. So does the common approach
%   in Matlab give rise to round-off errors towards 1?
%   Still, the difference between the row and column versions is striking!
%
	rep   = 1000;	% repetitions
	
	d1min = 20;		% number of rows
	d1max = 1000;
	d2min = 20;		% number of columns
	d2max = 1000;
	vmin  = 0;		% range of values (type is double)
	vmax  = 1;
	
	d1rng = d1max - d1min;
	d2rng = d2max - d2min;
	vrng  = vmax  - vmin;
	
	ErrM = zeros(rep, 1);
	ErrR = zeros(rep, 1);
	
	for i = 1:rep
		d1 = ceil(rand * d1rng + d1min);
		d2 = ceil(rand * d2rng + d2min);
		A  = rand(d1, d2) * vrng + vmin;
		
		switch fn
			case 'cdiv2norm'
				Am      = A ./ repmat(sqrt(sum(A.^2,1)), d1, 1);
				ErrM(i) = norm(ones(1, d1) * (Am.^2) - 1);
				%Am2=Am;for j=1:10,Am2=Am2./repmat(sqrt(sum(Am2.^2,1)),d1,1);end
				%ErrM(i)=norm(Am-Am2,'fro');
				Ar      = cdiv2norm(A);
				ErrR(i) = norm(ones(1, d1) * (Ar.^2) - 1);
				%Ar2=Ar;for j=1:10,Ar2=cdiv2norm(Ar2);end
				%ErrR(i)=norm(Ar-Ar2,'fro');
			case 'csubmean'
				Am      = A - repmat(mean(A), d1, 1);
				ErrM(i) = norm(sum(Am));
				Ar      = csubmean(A);
				ErrR(i) = norm(sum(Ar));
			case 'czscore'
				Am      = zscore(A, 0);	% - requires Statistics Toolbox
				ErrM(i) = norm(std(Am, 0) - 1);
				Ar      = czscore(A, 0);
				ErrR(i) = norm(std(Ar, 0) - 1);
			case 'czscore_1'
				Am      = zscore(A, 1);	% - requires Statistics Toolbox
				ErrM(i) = norm(std(Am, 1) - 1);
				Ar      = czscore(A, 1);
				ErrR(i) = norm(std(Ar, 1) - 1);
			case 'rdiv2norm'
        % Note: sum(A.^2, 2) seems to be about half as accurate as
        %       (A.^2)*ones(d2,1), so we're using the latter for
        %       evaluation.
				Am      = A ./ repmat(sqrt(sum(A.^2, 2)), 1, d2);
        ErrM(i) = norm((Am.^2) * ones(d2, 1) - 1);
				%Am2=Am;for j=1:10,Am2=Am2./repmat(sqrt(sum(Am2.^2,2)),1,d2);end
				%ErrM(i) = norm(Am-Am2,'fro');
				Ar      = rdiv2norm(A);
        ErrR(i) = norm((Ar.^2) * ones(d2, 1) - 1);
				%Ar2=Ar;for j=1:10,Ar2=rdiv2norm(Ar2);end
				%ErrR(i) = norm(Ar-Ar2,'fro');
			case 'rsubmean'
				Am      = A - repmat(mean(A, 2), 1, d2);
				ErrM(i) = norm(sum(Am, 2));
				Ar      = rsubmean(A);
				ErrR(i) = norm(sum(Ar, 2));
			case 'rzscore'
				Am      = zscore(A, 0, 2);	% - requires Statistics Toolbox
				ErrM(i) = norm(std(Am, 0, 2) - 1);
				Ar      = rzscore(A, 0);
				ErrR(i) = norm(std(Ar, 0, 2) - 1);
			case 'rzscore_1'
				Am      = zscore(A, 1, 2);	% - requires Statistics Toolbox
				ErrM(i) = norm(std(Am, 1, 2) - 1);
				Ar      = rzscore(A, 1);
				ErrR(i) = norm(std(Ar, 1, 2) - 1);
		end
	end
	
	fprintf('Error in Matlab: %.4g\n', sum(ErrM));
	fprintf('Error in %s: %.4g\n', fn, sum(ErrR));
end

