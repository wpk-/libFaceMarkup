function test_speed( fn )
%TEST_SPEED( FN ) Measures computational efficiency of different functions.
%   We create a large matrix as input to the specified function. The
%   computation on the matrix is performed repeatedly over which the time
%   is measured as a score of efficiency (little time is high efficiency)
%
%   FN can be one of:
%      - 'cdiv2norm': divide columns by their vector 2-norm (unit length)
%      - 'csubmean': subtract the mean from each column
%      - 'czscore': standardise columns; subtract mean and divide by std
%      - 'czscore_1': same as czscore but pass flag 1
%      - 'rdiv2norm': divide rows by their vector 2-norm (make unit length)
%      - 'rsubmean': subtract the mean from each row
%      - 'rzscore': standardise rows; subtract mean and divide by std
%      - 'rzscore_1': same as rzscore but pass flag 1
%
	N = 10000;
	D = 800;
	A = rand(N, D);
	range100 = 1:100;
	
	switch fn
		case 'cdiv2norm'
      fprintf('Alt 1:  ');
			tic; for i=range100, B=cdiv2norm_alt1(A); end; toc;
      fprintf('Alt 2:  ');
			tic; for i=range100, B=cdiv2norm_alt2(A); end; toc;
      fprintf('Main:   ');
			tic; for i=range100, B=cdiv2norm(A);      end; toc;
		case 'csubmean'
      fprintf('Alt 1:  ');
			tic; for i=range100, B=csubmean_alt1(A); end; toc;
      fprintf('Main:   ');
			tic; for i=range100, B=csubmean(A);      end; toc;
		case 'czscore'
      fprintf('Alt 1:  ');
			tic; for i=range100, B=czscore_alt1(A,0); end; toc;
      fprintf('Alt 2:  ');
			tic; for i=range100, B=czscore_alt2(A,0); end; toc;
      if exist('zscore', 'file')	% requires statistics toolbox
        fprintf('Matlab: ');
				tic; for i=range100, B=zscore(A,0);   end; toc;
      end
      fprintf('Main:   ');
			tic; for i=range100, B=czscore(A,0);      end; toc;
		case 'czscore_1'
      fprintf('Alt 1:  ');
			tic; for i=range100, B=czscore_alt1(A,1); end; toc;
      fprintf('Alt 2:  ');
			tic; for i=range100, B=czscore_alt2(A,1); end; toc;
			if exist('zscore', 'file')	% requires statistics toolbox
        fprintf('Matlab: ');
				tic; for i=range100, B=zscore(A,1);   end; toc;
      end
      fprintf('Main:   ');
			tic; for i=range100, B=czscore(A,1);      end; toc;
		case 'rdiv2norm'
      fprintf('Alt 1:  ');
			tic; for i=range100, B=rdiv2norm_alt1(A); end; toc;
      fprintf('Alt 2:  ');
			tic; for i=range100, B=rdiv2norm_alt2(A); end; toc;
      fprintf('Main:   ');
			tic; for i=range100, B=rdiv2norm(A);      end; toc;
		case 'rsubmean'
      fprintf('Alt 1:  ');
			tic; for i=range100, B=rsubmean_alt1(A); end; toc;
      fprintf('Main:   ');
			tic; for i=range100, B=rsubmean(A);      end; toc;
		case 'rzscore'
      fprintf('Alt 1:  ');
			tic; for i=range100, B=rzscore_alt1(A,0); end; toc;
      fprintf('Alt 2:  ');
			tic; for i=range100, B=rzscore_alt2(A,0); end; toc;
			if exist('zscore', 'file')	% requires statistics toolbox
        fprintf('Matlab: ');
				tic; for i=range100, B=zscore(A,0,2); end; toc;
			end
      fprintf('Main:   ');
			tic; for i=range100, B=rzscore(A,0);      end; toc;
		case 'rzscore_1'
      fprintf('Alt 1:  ');
			tic; for i=range100, B=rzscore_alt1(A,1); end; toc;
      fprintf('Alt 2:  ');
			tic; for i=range100, B=rzscore_alt2(A,1); end; toc;
			if exist('zscore', 'file')	% requires statistics toolbox
        fprintf('Matlab: ');
				tic; for i=range100, B=zscore(A,1,2); end; toc;
			end
      fprintf('Main:   ');
			tic; for i=range100, B=rzscore(A,1);      end; toc;
	end
end

function B = cdiv2norm_alt1(A)
	D = size(A, 1);
	B = A ./ repmat(sqrt(sum(A.^2)), D, 1);
end
function B = cdiv2norm_alt2(A)
	B = bsxfun( @rdivide, A, sqrt(dot(A,A,1)) );
end

function B = csubmean_alt1(A)
	B = A - repmat(mean(A), size(A,1), 1);
end

function B = czscore_alt1(A, flag)
	B = A - repmat(mean(A), size(A,1), 1);
	B = B ./ repmat(std(B,flag), size(A,1), 1);
end
function B = czscore_alt2(A, flag)
	B = csubmean(A);
	B = B ./ repmat(std(B,flag), size(A,1), 1);
end

function B = rdiv2norm_alt1(A)
	D = size(A, 2);
	B = A ./ repmat(sqrt(sum(A.^2, 2)), 1, D);
end
function B = rdiv2norm_alt2(A)
	D = size(A, 2);
	B = bsxfun(@ldivide, (A.^2) * ones(D,1), 1);
	B = bsxfun(@times, A, sqrt(B));
end

function B = rsubmean_alt1(A)
	B = A - repmat(mean(A,2), 1, size(A,2));
end

function B = rzscore_alt1(A, flag)
	B = A - repmat(mean(A,2), 1, size(A,2));
	B = B ./ repmat(std(B,flag,2), 1, size(A,2));
end
function B = rzscore_alt2(A, flag)
	B = rsubmean(A);
	B = B ./ repmat(std(B,flag,2), 1, size(A,2));
end
