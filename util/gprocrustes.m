function [X, mu, T] = gprocrustes(X, mu, iters, dbg)
% Generalised Procrustes alignment.
%
% Input arguments:
%  X      NxMxK matrix of N samples of M points in K dimensions.
%  MU     Optional MxK matrix used as the first mean.
%         By default the mean of X is computed.
%  ITERS  Optional scalar specifying the number of procrustes iterations.
%         Default value is 3.
%  DBG    Optional logical scalar. Set to true to print convergence info.
%         Default is false.
%
% Output arguments:
%  X   NxMxK matrix of procrustes aligned samples.
%  MU  Final mean.
%  T   1xN struct of the N sample transformations, with fields `.T` a KxK
%      rotation matrix, `.b` a scalar (scaling component), and `.c` a MxK
%      matrix of the translation component (has M identical rows). 
%
  if nargin<4 || isempty(dbg), dbg=false; end
  if nargin<3 || isempty(iters), iters=3; end
  if nargin<2 || isempty(mu), mu=squeeze(mean(X,1)); end

  if dbg, print=@fprintf; else print=@noop; end

  numsamples = size(X, 1);

  args = {'Reflection',0, 'Scaling',0};

  X = permute(X, [2 3 1]);
  mu2 = mu;

  for i = 1:iters
    X2 = zeros(size(X));
    parfor j = 1:numsamples
      [~,X2(:,:,j),T(j)] = procrustes(mu2, X(:,:,j), args{:}); %#ok<PFBNS>
    end
    [~,mu3] = procrustes(mu, mean(X2,3), args{:});
    delta = norm(mu3 - mu2, 'fro');
    mu2 = mu3;

    print(' %2d. delta: %.6f\n', i, delta);
  end

  X = permute(X2, [3 1 2]);
  mu = mu2;
end
