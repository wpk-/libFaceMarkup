function [B,M] = csubmean(A)
% B = CSUBMEAN( A )
%
% Subtract the mean from each column in matrix A.
%
% B,M = CSUBMEAN( A ) returns in M the subtracted column means.
%
% Faster than using mean() and faster than using repmat()
%
	D = size(A, 1);
	M = (ones(1,D) / D) * A;
	B = bsxfun(@minus, A, M);
end
