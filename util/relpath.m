function p = relpath(thispath, thatpath)
% Return this path relative to that path.
%
% Input arguments:
%  THISPATH  String with the path of interest.
%  THATPATH  Optional string with the path where we "currently are".
%            Default value is the current path (`cd`).
%
% Output arguments:
%  P  String with the relative path from THATPATH to THISPATH.
%
% Examples (we assume that THATPATH is a folder, if it is a file, the filename
% bit will be stripped off by `relpath` before computing the relative path):
%
%   strcmp(relpath('/a/b',     '/a/b/c')   ,  '..')
%   strcmp(relpath('/a/b/c/d', '/a/b/c')   ,  'd')
%   strcmp(relpath('/a/b/c',   '/a/b/c')   ,  '.')
%   strcmp(relpath('/a/b/e',   '/a/b/c')   ,  '../e')
%   strcmp(relpath('/a/e/e',   '/a/b/c')   ,  '../../e/e')
%   strcmp(relpath('/e/e/e',   '/a/b/c')   ,  '../../../e/e/e')
%
%   % Assume working directory is /a
%   strcmp(relpath('/a/b',     'b/c')   ,  '..')
%   strcmp(relpath('/a/b/c/d', 'b/c')   ,  'd')
%   strcmp(relpath('/a/b/c',   'b/c')   ,  '.')
%   strcmp(relpath('/a/b/e',   'b/c')   ,  '../e')
%   strcmp(relpath('/a/e/e',   'b/c')   ,  '../../e/e')
%   strcmp(relpath('/e/e/e',   'b/c')   ,  '../../../e/e/e')
%
%   strcmp(relpath('e',        '/a/b/c')   ,  'e')
%   strcmp(relpath('..',       '/a/b/c')   ,  '..')
%   strcmp(relpath('../e',     '/a/b/c')   ,  '../e')
%
%   strcmp(relpath('H:/a/b',     'H:/a/b/c')   ,  '..')
%   strcmp(relpath('H:/a/b/c/d', 'H:/a/b/c')   ,  'd')
%   strcmp(relpath('H:/a/b/c',   'H:/a/b/c')   ,  '.')
%   strcmp(relpath('H:/a/b/e',   'H:/a/b/c')   ,  '../e')
%   strcmp(relpath('H:/a/e/e',   'H:/a/b/c')   ,  '../../e/e')
%   strcmp(relpath('H:/e/e/e',   'H:/a/b/c')   ,  '../../../e/e/e')
%
%   % Assume working directory is H:/a
%   strcmp(relpath('H:/a/b',     'b/c')   ,  '..')
%   strcmp(relpath('H:/a/b/c/d', 'b/c')   ,  'd')
%   strcmp(relpath('H:/a/b/c',   'b/c')   ,  '.')
%   strcmp(relpath('H:/a/b/e',   'b/c')   ,  '../e')
%   strcmp(relpath('H:/a/e/e',   'b/c')   ,  '../../e/e')
%   strcmp(relpath('H:/e/e/e',   'b/c')   ,  '../../../e/e/e')
%
%   strcmp(relpath('e',        'H:/a/b/c')   ,  'e')
%   strcmp(relpath('..',       'H:/a/b/c')   ,  '..')
%   strcmp(relpath('../e',     'H:/a/b/c')   ,  '../e')
%
%   strcmp(relpath('E:/a/b/c', 'H:/a/b/c')   ,  'E:/a/b/c')
%   strcmp(relpath('E:/e/e/e', 'H:/a/b/c')   ,  'E:/e/e/e')
%   strcmp(relpath('E:',       'H:/a/b/c')   ,  'E:')
%
% replace '/' in the answers with '\' if Matlab runs on Windows.
%
% Considerations:
%  - Technically, in the Unix flavours, you can have folders named "X:". For the
%    sake of argument, however, we are going to assume that if a path starts
%    with "X:" (where X is a letter), then that refers to a drive. This way, we
%    have consistent behaviour on Windows and the Unixes.
%  - If THISPATH is in itself a relative path, then it is always interpreted as
%    relative to THATPATH. However, ...
%  - If THATPATH is relative, THATPATH is always interpreted relative to the
%    current working directory (`cd`).
%
  if nargin<2 || isempty(thatpath), thatpath=cd; end
  
  if exist(thatpath, 'file') && ~exist(thatpath, 'dir')
    % Bloody Matlab, why does it return file=true when it is a folder!!!??
    thatpath = fileparts(thatpath);
  end
  
  % Break the paths into cell arrays of folder names.
  % Also, test for relative paths and resolve properly.
  [thiscell, thislength] = pathparts(thispath);
  if isempty(thiscell(1))
    % THISPATH is relative. We're done.
    p = fullfile(thispath);
    return;
  end
  
  [thatcell, thatlength] = pathparts(thatpath);
  if isempty(thatcell{1})
    % THATPATH is relative. Make absolute and recompute.
    [thatcell, thatlength] = pathparts(fullfile(pwd, thatpath));
  end
  
  % Find where `thispath` and `thatpath` part.
  maxlength = max(thislength, thatlength);
  ix = find(~strcmpi(thiscell, thatcell), 1);
  if ix > maxlength
    % full match.
    p = '.'; % or '' or './' if you prefer, but I prefer '.'.
  elseif ix == 1
    % different drive.
    p = fullfile(thiscell{1:thislength});
  else
    % the paths divide somewhere mid way.
    back = strjoin(repmat({'..'},1,1+thatlength-ix), filesep);
    p = fullfile(back, thiscell{ix:thislength});
  end
end

function [pathcell, pathlength, somepath] = pathparts(somepath)
  pathcell = cell(1, 256);
  pathlength = 1;
  [parent,folder,ext] = fileparts(somepath);
  while ~isempty([folder ext])
    pathcell{pathlength} = [folder ext];
    [parent,folder,ext] = fileparts(parent);
    pathlength = pathlength + 1;
  end
  pathcell{pathlength} = parent;
  % Windows drive letter also interpreted as such on Unix OSes.
  if ~ispc && isempty(parent) && pathlength > 1
    drive = pathcell{pathlength - 1};
    if numel(drive) == 2 && drive(2) == ':'
      pathlength = pathlength - 1;
    end
%   elseif ispc && numel(parent) == 3 && strcmp(parent(2:3), ':\')
%     % On Windows:
%     %    'H:\' = fileparts('H:\')       but
%     %    'H:' = fileparts('H:/')
%     pathcell{pathlength} = parent(1:2);
  end
  % Re-reverse.
  pathcell(1:pathlength) = pathcell(pathlength:-1:1);
end
