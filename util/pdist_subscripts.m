function [r, c] = pdist_subscripts(K, I)
% Convert the positions in a vector returned by pdist into matrix subscripts.
%
% d = pdist(X, ...);  % -> d = Nx1
% D = squareform(d);  % -> D = KxK
% d(i) == D(r,c)      % -> d(i) = distance between X(r,:) and X(c,:),
% d(i) == D(c,r)      %                and between X(c,:) and X(r,:) obviously.
%
% This function converts the linear index `i` to the matrix subscripts `r` and
% `c`.
%
% Input arguments:
%  K  Scalar of the number of points passed to `pdist`.
%  I  Optional vector of linear indices into the `pdist` vector.
%     Default value is `1:N`.
%
% Output arguments:
%  R  Vector of the row subscripts for the linear indices.
%  C  Vector of the column subscripts for the linear indices.
%
% Considerations:
%  - R and C are the same size as I.
%
  N = K * (K-1) / 2;
  
  if nargin<2 || isempty(I)
    I = (1:N)';
  end
  
  c = K - round(sqrt(2 * (1 + N - I)));
  r = mod(I + c.*(c+1)/2 - 1, K) + 1;
end
