function s1 = greedysearch(s0, ex, f, goal)
% Greedily solve a graph search problem with unknown set of states.
%
% Input arguments:
%  S0     Initial state.
%  EX     Expansion function, producing a cell array of states reachable
%         from the given state.
%  F      Evaluation function, returning a value for the given state. A
%         lower value means a better valuation.
%  GOAL   Function that returns true for any goal state and false
%         otherwise.
%
% Output arguments:
%  S1     The goal state.
%
% Considerations:
%  - If you need the path, make it so that it is saved in the state (a task
%    for the EX function).
%  - This function is NOT memory efficient.
%
  states = {s0};
  
  while ~goal(states{end})
    ss = ex(states{end});
    ssf = cellfun(f, ss);
    [~, ix] = sort(ssf, 'descend');
    states = [states(1:end-1); ss(ix)];
    
    fprintf('%.4f   (%d)\n', ssf(ix(end)), numel(states));
  end
  
  s1 = states{end};
end
