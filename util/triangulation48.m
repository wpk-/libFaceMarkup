function T = triangulation48(nrows, ncols, transposed_index)
% Create a 4/8 triangulation for a grid of 2D points.
%
% Input arguments:
%  NROWS             Scalar specifying the number of rows in the grid.
%  NCOLS             Scalar specifying the number of columns.
%  TRANSPOSED_INDEX  Boolean flag to indicate vertices are ordered
%                    column-first rather than row-first. (In Matlab the
%                    latter is common, whereas in other languages the first
%                    is standard.) By default, this argument is false, and
%                    a row-first vertex order is assumed.
%
% Output arguments:
%  T  Triangulation over NROWS * NCOLS points with 4/8 connectivity.
%
% Considerations:
%  - The triangulation is computed for a rectangular grid of rougly equally
%    spaced vertices, such as used in images from the FRGC and Bosphorus data
%    sets.
%  - The 4/8 triangulation has no directional bias in vertex neighbours and is
%    therefore better for surface properties computed over neighbourhoods, such
%    as surface curvature.
%  - An example of 4/8 connected vertices (* are vertices |, \, /, - are edges):
%         *-*-*-*-*
%         |\|/|\|/|
%         *-*-*-*-*
%         |/|\|/|\|
%         *-*-*-*-*
%  - An example of 6 connected vertices (output from `delaunay`):
%         *-*-*-*-*
%         |\|\|\|\|
%         *-*-*-*-*
%         |\|\|\|\|
%         *-*-*-*-*
%
  if nargin<3 || isempty(transposed_index), transposed_index = false; end

  evenrows = (2:2:nrows)';
  oddrows = (3:2:nrows)';
  evencols = nrows * (1:2:(ncols-1));
  oddcols = nrows * (2:2:(ncols-1));

  % ~~~~~ "A" has \ diagonal,
  %       "B" has / diagonal.
  ai1 = bsxfun(@plus, evenrows, evencols);
  ai2 = bsxfun(@plus, oddrows, oddcols);
  ai = [ai1(:); ai2(:)];
  bi1 = bsxfun(@plus, oddrows, evencols);
  bi2 = bsxfun(@plus, evenrows, oddcols);
  bi = [bi1(:); bi2(:)];

  % ~~~~~ Above we calculated box offsets.
  %       Each box is divided in two triangles.
  %       Here we make all the triangles.
  T = [
    bsxfun(@plus, ai, [-nrows-1         0      -1]);  % "\|" triangle shape.
    bsxfun(@plus, ai, [-nrows-1    -nrows       0]);  % "|\"
    bsxfun(@plus, bi, [-nrows-1    -nrows      -1]);  % "|/"
    bsxfun(@plus, bi, [-nrows           0      -1]);  % "/|"
  ];

  % ~~~~~ If the vertices are gridded row-after-row
  %       (unlike Matlab), transpose.
  if transposed_index
    [r, c] = ind2sub([nrows ncols], T);
    T = sub2ind([ncols nrows], c, r);
  end
end
