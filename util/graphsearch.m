function s1 = graphsearch(s0, id, ex, f, goal)
% Solve a graph search problem with unknown set of states.
%
% Input arguments:
%  S0     Initial state.
%  ID     Identity function, assigning a number to each state. Different
%         states can map to the same number. The state with the best value
%         will be kept.
%  EX     Expansion function, producing a cell array of states reachable
%         from the given state.
%  F      Evaluation function, returning a value for the given state. A
%         lower value means a better valuation.
%  GOAL   Function that returns true for any goal state and false
%         otherwise.
%
% Output arguments:
%  S1     The goal state.
%
% Considerations:
%  If you need the path, make it so that it is saved in the state. (Task
%  for the EX function.)
%
  states = {s0};
  sid = id(s0);   %[id(s0)];
  sf = f(s0);     %[f(s0)];
  sopen = true;   %[true];
  
  ix = 1;
  
  while ~goal(states{ix})
    %fprintf('Expand state %s (f = %.4f).\n', dec2bin(sid(ix)), sf(ix));
    
    sopen(ix) = false;
    
    ss = ex(states{ix});
    ssid = cellfun(id, ss);
    ssf = cellfun(f, ss);
    
    [known,pos] = ismember(ssid, sid);
    
    % Add new states to the list of states.
    
    states = cat(1, states, ss(~known));
    sid = cat(1, sid, ssid(~known));
    sf = cat(1, sf, ssf(~known));
    sopen = cat(1, sopen, true(nnz(~known),1));
    
    % Update any improved open states.
    
    pos = pos(known);
    improved = ssf(known) < sf(pos);
    % TODO: confirm that all improved nodes are open.
    pos = pos(improved);
    known(known) = improved;
    states(pos) = ss(known);
    sf(pos) = ssf(known);
    
    % Determine the next state to expand.
    
    [~,ix] = min(sf(sopen));
    ix = find(sopen, ix);
    ix = ix(end);
    fprintf('%.4f   (%d)\n', sf(ix), ix);
  end
  
  %fprintf('Goal state reached (%d).\n', sid(ix));
  
  s1 = states{ix};
end
