function b = iscolorspec(x)
% Determine whether the argument could be a ColorSpec.
%
% Input arguments:
%  X  Argument that may or may not be a ColorSpec.
%
% Output arguments:
%  B  Logical scalar, `true` if X could be used as ColorSpec.
%
  colours = {'y','m','c','r','g','b','w','k',...
            'yellow','magenta','cyan','red','green','blue','white','black'};
  
  b = (isnumeric(x) && size(x,2) == 3) || ...
      (ischar(x) && any(strcmp(x, colours)));
end
