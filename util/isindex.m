function b = isindex(x)
% Determine whether the argument could be used as array/matrix index.
%
% Input arguments:
%  X  Argument that may or may not be an index.
%
% Output arguments:
%  B  Logical scalar, `true` if X could be used as index.
%
  b = ~ischar(x) && ismatrix(x);
end
