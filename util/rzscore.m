function Z = rzscore(X, flag)
% Z = RZSCORE( X, [FLAG] )
%
% Subtracts the mean from each row and divides by the standard deviation.
%
% Faster than zscore(X, flag, 2).
%
	c = nargin<2 || flag==0;
	
	D = size(X, 2);
	M = X * (ones(D,1) / D);
	
	% subtract mean
	Y = bsxfun(@minus, X, M);
	
	% D / sum squares
	Z = bsxfun(@ldivide, (Y.^2) * ones(D,1), D-c);
	
	% dividing by sqrt(1/D * sumsquares)
	% equals multiplying by sqrt(D / sumsquares)
	Z = bsxfun(@times, Y, sqrt(Z));
end
