function UV = clipuv(UV)%, Vap, V0, V1)
% Clip UV coordinates to lie within their triangle.
%
% Input arguments:
%  UV   Mx2 matrix of UV coordinates.
%  V0   MxD matrix defining the first basis vector (along the side B-A).
%  V1   MxD matrix defining the second basis vector (along the side C-A).
%
% Output arguments:
%  UV   MxD matrix with UV coordinates all within the triangle.
%
  W           = 1 - sum(UV, 2);
  far         = W < 0;
  UV(far,:)   = bsxfun(@plus, UV(far,:), W(far)./2);
  
%   far         = sum(UV, 2) > 1;
% 	Vbp         = Vap(far,:) - V0(far,:);
% 	Vbc         = V1(far,:) - V0(far,:);
% 	W           = dot(Vbp, Vbc, 2) ./ dot(Vbc, Vbc, 2);
% 	UV(far,1)   = 1 - W;
% 	UV(far,2)   = W;

	UV(UV<0)    = 0;
	UV(UV>1)    = 1;
end
