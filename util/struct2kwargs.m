function kwargs = struct2kwargs(s)
% Convert a struct to a cell array of keyword/value pairs.
%
% Input arguments:
%  S  Struct with N fields.
%
% Output arguments:
%  KWARGS  2xN cell array with the first row holding all the field names of S,
%          and the second row holding the associated values. The cell array can
%          be expanded to named function arguments directly as `fun(KWARGS{:})`.
%
  names  = fieldnames(s);
  values = cellfun(@(n)s.(n), names, 'UniformOutput',0);
  kwargs = cat(2, names, values)';
end
